﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp.AuditLogging.EntityFrameworkCore;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;

namespace FRTTMO.CAM.PromotionAPI.EntityFrameworkCore
{
    [ConnectionStringName("AbpAuditLogging")]
    public class AbpAuditLoggingDbContext : AbpDbContext<AbpAuditLoggingDbContext>
    {
        public AbpAuditLoggingDbContext(
            DbContextOptions<AbpAuditLoggingDbContext> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            /* Include modules to your migration db context */
            //builder.ConfigurePermissionManagement();
            //builder.ConfigureSettingManagement();
            modelBuilder.ConfigureAuditLogging();
        }
    }
}