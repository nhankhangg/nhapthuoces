﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace FRTTMO.CAM.PromotionAPI.EntityFrameworkCore
{
    public class AbpAuditLoggingMigratioDbContextFactory : IDesignTimeDbContextFactory<AbpAuditLoggingDbContext>
    {
        public AbpAuditLoggingDbContext CreateDbContext(string[] args)
        {
            var configuration = BuildConfiguration();

            var builder = new DbContextOptionsBuilder<AbpAuditLoggingDbContext>()
                .UseSqlServer(configuration.GetConnectionString("AbpAuditLogging"));

            return new AbpAuditLoggingDbContext(builder.Options);
        }

        private static IConfigurationRoot BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false)
                .AddJsonFile("appsettings.Development.json", optional: false);

            return builder.Build();
        }
    }
}