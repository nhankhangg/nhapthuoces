﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;

public class PromotionAPIHttpApiHostMigrationsDbContext : AbpDbContext<PromotionAPIHttpApiHostMigrationsDbContext>
{
    public PromotionAPIHttpApiHostMigrationsDbContext(DbContextOptions<PromotionAPIHttpApiHostMigrationsDbContext> options)
        : base(options)
    {

    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.ConfigurePromotionAPI();
    }
}
