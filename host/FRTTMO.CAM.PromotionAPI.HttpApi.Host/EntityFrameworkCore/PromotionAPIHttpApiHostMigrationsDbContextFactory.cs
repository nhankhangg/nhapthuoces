﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;

public class PromotionAPIHttpApiHostMigrationsDbContextFactory : IDesignTimeDbContextFactory<PromotionAPIHttpApiHostMigrationsDbContext>
{
    public PromotionAPIHttpApiHostMigrationsDbContext CreateDbContext(string[] args)
    {
        var configuration = BuildConfiguration();

        var builder = new DbContextOptionsBuilder<PromotionAPIHttpApiHostMigrationsDbContext>()
            .UseSqlServer(configuration.GetConnectionString("PromotionAPI"));

        return new PromotionAPIHttpApiHostMigrationsDbContext(builder.Options);
    }

        private static IConfigurationRoot BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false)
                .AddJsonFile("appsettings.Development.json", optional: false);

        return builder.Build();
    }
}
