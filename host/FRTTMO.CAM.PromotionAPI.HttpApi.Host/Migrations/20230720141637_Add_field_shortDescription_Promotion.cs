﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FRTTMO.CAM.PromotionAPI.Migrations
{
    public partial class Add_field_shortDescription_Promotion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ShortDescription",
                table: "Promotion",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ShortDescription",
                table: "Promotion");
        }
    }
}
