﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FRTTMO.CAM.PromotionAPI.Migrations
{
    public partial class Add_field_IsBoom_Promotion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsBoom",
                table: "Promotion",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsBoom",
                table: "Promotion");
        }
    }
}
