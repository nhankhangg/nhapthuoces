﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FRTTMO.CAM.PromotionAPI.Migrations
{
    public partial class add_field_DescriptionBoom_Promotion_field_NoteBoom_Output : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DescriptionBoom",
                table: "Promotion",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NoteBoom",
                table: "OutputReplace",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NoteBoom",
                table: "Output",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DescriptionBoom",
                table: "Promotion");

            migrationBuilder.DropColumn(
                name: "NoteBoom",
                table: "OutputReplace");

            migrationBuilder.DropColumn(
                name: "NoteBoom",
                table: "Output");
        }
    }
}
