﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FRTTMO.CAM.PromotionAPI.Migrations
{
    public partial class add_field_discountPlaceCode_Output : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DiscountPlaceCode",
                table: "OutputReplace",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DiscountPlaceName",
                table: "OutputReplace",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DiscountPlaceCode",
                table: "Output",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DiscountPlaceName",
                table: "Output",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DiscountPlaceCode",
                table: "OutputReplace");

            migrationBuilder.DropColumn(
                name: "DiscountPlaceName",
                table: "OutputReplace");

            migrationBuilder.DropColumn(
                name: "DiscountPlaceCode",
                table: "Output");

            migrationBuilder.DropColumn(
                name: "DiscountPlaceName",
                table: "Output");
        }
    }
}
