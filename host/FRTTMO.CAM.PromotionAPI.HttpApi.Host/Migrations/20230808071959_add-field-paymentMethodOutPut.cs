﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FRTTMO.CAM.PromotionAPI.Migrations
{
    public partial class addfieldpaymentMethodOutPut : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DiscountPlaceCode",
                table: "PaymentMethodOutput",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NoteBoom",
                table: "PaymentMethodOutput",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NoteBoom",
                table: "GetItemOutput",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NoteBoom",
                table: "DiscountItemOutput",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DiscountPlaceCode",
                table: "PaymentMethodOutput");

            migrationBuilder.DropColumn(
                name: "NoteBoom",
                table: "PaymentMethodOutput");

            migrationBuilder.DropColumn(
                name: "NoteBoom",
                table: "GetItemOutput");

            migrationBuilder.DropColumn(
                name: "NoteBoom",
                table: "DiscountItemOutput");
        }
    }
}
