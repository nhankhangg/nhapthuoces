﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FRTTMO.CAM.PromotionAPI.Migrations
{
    public partial class add_PaymentGateWay_InstallmentCondition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DownPayment",
                table: "InstallmentCondition");

            migrationBuilder.AddColumn<string>(
                name: "PaymentGateWay",
                table: "InstallmentCondition",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PaymentGateWay",
                table: "InstallmentCondition");

            migrationBuilder.AddColumn<decimal>(
                name: "DownPayment",
                table: "InstallmentCondition",
                type: "decimal(18,2)",
                nullable: true);
        }
    }
}
