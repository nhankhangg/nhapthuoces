﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FRTTMO.CAM.PromotionAPI.Migrations
{
    public partial class add_PaymentForms : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PaymentForms",
                table: "PaymentCondition",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PaymentForms",
                table: "InstallmentCondition",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PaymentForms",
                table: "PaymentCondition");

            migrationBuilder.DropColumn(
                name: "PaymentForms",
                table: "InstallmentCondition");
        }
    }
}
