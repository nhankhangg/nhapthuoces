﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FRTTMO.CAM.PromotionAPI.Migrations
{
    public partial class updateoutputintegation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PromotionCode",
                table: "PaymentMethodOutput",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UnitCode",
                table: "PaymentMethodOutput",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PrmotionCode",
                table: "GetAscendingDiscountOutPut",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UnitCode",
                table: "GetAscendingDiscountOutPut",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UnitCode",
                table: "DiscountItemOutput",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PromotionCode",
                table: "PaymentMethodOutput");

            migrationBuilder.DropColumn(
                name: "UnitCode",
                table: "PaymentMethodOutput");

            migrationBuilder.DropColumn(
                name: "PrmotionCode",
                table: "GetAscendingDiscountOutPut");

            migrationBuilder.DropColumn(
                name: "UnitCode",
                table: "GetAscendingDiscountOutPut");

            migrationBuilder.DropColumn(
                name: "UnitCode",
                table: "DiscountItemOutput");
        }
    }
}
