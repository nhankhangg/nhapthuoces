﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FRTTMO.CAM.PromotionAPI.Migrations
{
    public partial class UpdateunitNameOutputintegation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ItemCode",
                table: "PaymentMethodOutput",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ItemName",
                table: "PaymentMethodOutput",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UnitName",
                table: "PaymentMethodOutput",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ItemName",
                table: "GetItemOutput",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UnitName",
                table: "GetItemOutput",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UnitName",
                table: "GetAscendingDiscountOutPut",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ItemName",
                table: "DiscountItemOutput",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UnitName",
                table: "DiscountItemOutput",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ItemCode",
                table: "PaymentMethodOutput");

            migrationBuilder.DropColumn(
                name: "ItemName",
                table: "PaymentMethodOutput");

            migrationBuilder.DropColumn(
                name: "UnitName",
                table: "PaymentMethodOutput");

            migrationBuilder.DropColumn(
                name: "ItemName",
                table: "GetItemOutput");

            migrationBuilder.DropColumn(
                name: "UnitName",
                table: "GetItemOutput");

            migrationBuilder.DropColumn(
                name: "UnitName",
                table: "GetAscendingDiscountOutPut");

            migrationBuilder.DropColumn(
                name: "ItemName",
                table: "DiscountItemOutput");

            migrationBuilder.DropColumn(
                name: "UnitName",
                table: "DiscountItemOutput");
        }
    }
}
