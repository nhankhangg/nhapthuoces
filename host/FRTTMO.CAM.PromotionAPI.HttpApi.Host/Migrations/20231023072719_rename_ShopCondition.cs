﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FRTTMO.CAM.PromotionAPI.Migrations
{
    public partial class rename_ShopCondition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ShopCondition");

            migrationBuilder.RenameColumn(
                name: "ShopCode",
                table: "QuotaHistories",
                newName: "ProvinceCode");

            migrationBuilder.CreateTable(
                name: "ProvinceCondition",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PromotionId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PromotionCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    ProvinceCode = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    ProvinceName = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: false),
                    FromDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ToDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedByName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdateByName = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProvinceCondition", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProvinceCondition_PromotionCode",
                table: "ProvinceCondition",
                column: "PromotionCode");

            migrationBuilder.CreateIndex(
                name: "IX_ProvinceCondition_PromotionId",
                table: "ProvinceCondition",
                column: "PromotionId");

            migrationBuilder.CreateIndex(
                name: "IX_ProvinceCondition_ProvinceCode",
                table: "ProvinceCondition",
                column: "ProvinceCode");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProvinceCondition");

            migrationBuilder.RenameColumn(
                name: "ProvinceCode",
                table: "QuotaHistories",
                newName: "ShopCode");

            migrationBuilder.CreateTable(
                name: "ShopCondition",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedByName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    FromDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    PromotionCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    PromotionId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ShopCode = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    ShopName = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: false),
                    ToDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdateByName = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShopCondition", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ShopCondition_PromotionCode",
                table: "ShopCondition",
                column: "PromotionCode");

            migrationBuilder.CreateIndex(
                name: "IX_ShopCondition_PromotionId",
                table: "ShopCondition",
                column: "PromotionId");

            migrationBuilder.CreateIndex(
                name: "IX_ShopCondition_ShopCode",
                table: "ShopCondition",
                column: "ShopCode");
        }
    }
}
