using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using Customize.Logging;
using Elastic.Apm;
using Elastic.Apm.DiagnosticSource;
using Elastic.Apm.EntityFrameworkCore;
using FRT.Common.Authorize;
using FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;
using FRTTMO.CAM.PromotionAPI.Extensions;
using FRTTMO.CAM.PromotionAPI.Options;
using FRTTMO.CAM.PromotionAPI.Services;
using Hangfire;
using Hangfire.RecurringJobExtensions;
using HangfireBasicAuthenticationFilter;
using IdentityModel;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc.UI.MultiTenancy;
using Volo.Abp.AspNetCore.Serilog;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs.Hangfire;
using Volo.Abp.Caching;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore.SqlServer;
using Volo.Abp.Http.Client;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.Swashbuckle;

namespace FRTTMO.CAM.PromotionAPI;

[DependsOn(
    typeof(PromotionAPIApplicationModule),
    typeof(PromotionAPIEntityFrameworkCoreModule),
    typeof(PromotionAPIHttpApiModule),
    typeof(AbpAspNetCoreMvcUiMultiTenancyModule),
    typeof(AbpAutofacModule),
    typeof(AbpEntityFrameworkCoreSqlServerModule),
    //typeof(AbpAuditLoggingEntityFrameworkCoreModule),
    typeof(AbpAspNetCoreSerilogModule),
    typeof(AbpSwashbuckleModule),
    typeof(AbpBackgroundJobsHangfireModule)
)]
public class PromotionAPIHttpApiHostModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        PreConfigure<AbpHttpClientBuilderOptions>(options =>
        {
            options.ProxyClientBuildActions.Add((remoteServiceName, clientBuilder) =>
            {
                clientBuilder.AddHttpMessageHandler<HttpApiDelegatingHandler>();
            });
        });
    }

    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        const string apiVersion = "0.1.8";
        const string apiTitle = "CAM-PromotionAPI API";

        var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
        var configuration = context.Services.GetConfiguration();

        Configure<AbpDbContextOptions>(options => 
        { 
            options.UseSqlServer();
        });

        context.Services.AddElasticSearchService(configuration);

        context.Services.AddAbpSwaggerGenWithOAuth(
            configuration["AuthServer:Authority"],
            new Dictionary<string, string>
            {
                {"PromotionAPI", apiTitle}
            },
            options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo {Title = apiTitle, Version = apiVersion});
                options.DocInclusionPredicate((docName, description) => true);
                options.CustomSchemaIds(type => type.FullName);
                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Please enter a valid token",
                    Name = "Authorization",
                    Type = SecuritySchemeType.Http,
                    BearerFormat = "JWT",
                    Scheme = "Bearer"
                });
                options.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        Array.Empty<string>()
                    }
                });
            });

        Configure<ElasticSearchOption>(options =>
        {
            context.Services.GetConfiguration().GetSection("ElasticSearch").Bind(options,
                bindOptions => { bindOptions.BindNonPublicProperties = true; });
        });

        Configure<AbpLocalizationOptions>(options =>
        {
            options.Languages.Add(new LanguageInfo("vi", "vi", "vietnamese"));
        });

        Configure<AbpDistributedCacheOptions>(options => { options.KeyPrefix = "Promotion:"; });

        JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Add("preferred_username", JwtClaimTypes.Name);

        context.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddIdentityServerAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme, options =>
            {
                options.Authority = configuration["AuthServer:Authority"];
                options.RequireHttpsMetadata = false;
                options.Audience = configuration["AuthServer:ApiName"];
                options.TokenValidationParameters.ValidIssuers = new[]
                {
                    "https://ci-identity.nhapthuoc.com",
                    "https://uat-identity.nhapthuoc.com",
                    "https://identity.nhapthuoc.com"
                };
            }, null);

        context.Services.AddHangfire(config =>
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            {
                config.UseSqlServerStorage(configuration.GetConnectionString("AbpAuditLogging"))
                    .UseRecurringJob(typeof(HangFireAppService));
            }
        });

        context.Services.AddHangfireServer(options =>
        {
            options.Queues = new[]
            {
                options.ServerName = $"{Environment.MachineName}:km",
                configuration["Hangfire:PrefixPromotion"]
            };
        });

        context.Services.AddHangfireServer(options =>
        {
            options.Queues = new[] {
                configuration["Hangfire:PrefixDailyPromotion"], 
                configuration["Hangfire:PrefixDailyPromotionRecache"],
                configuration["Hangfire:PrefixDailyPromotionActiveDate"],
                configuration["Hangfire:PrefixDalyCampaignActiveDate"]
            };
        });
        context.Services.AddHttpClient("PIM").ConfigureHttpClient((service, client) =>
        {
            var options = service.GetRequiredService<IOptions<AbpRemoteServiceOptions>>();
            var posOption = options.Value.RemoteServices.GetConfigurationOrDefault("PIM");
            client.BaseAddress = new Uri(posOption.BaseUrl);
        });

        context.Services.AddCors(options =>
        {
            options.AddDefaultPolicy(builder =>
            {
                builder
                    .WithOrigins(
                        configuration["App:CorsOrigins"]
                            .Split(",", StringSplitOptions.RemoveEmptyEntries)
                            .Select(o => o.RemovePostFix("/"))
                            .ToArray()
                    )
                    .WithAbpExposedHeaders()
                    .SetIsOriginAllowedToAllowWildcardSubdomains()
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials();
            });
        });

        //context.Services.ConfigureKafkaEventBus(configuration);
        context.Services.AddAuthorizeCommon();
    }

    public override void OnApplicationInitialization(ApplicationInitializationContext context)
    {
        var app = context.GetApplicationBuilder();
        var env = context.GetEnvironment();

        Agent.Subscribe(new HttpDiagnosticsSubscriber());
        Agent.Subscribe(new EfCoreDiagnosticsSubscriber());

        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        else
        {
            app.UseHsts();
        }

        app.UseMiddleware<RequestResponseLoggerMiddleware>();
        app.UseHttpsRedirection();
        app.UseCorrelationId();
        app.UseStaticFiles();
        app.UseRouting();
        //app.UseCors(x => x.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
        app.UseAuthentication();
        app.UseAbpRequestLocalization();
        app.UseAuthorization();
        app.UseSwagger();
        app.UseAbpSwaggerUI(options =>
        {
            options.SwaggerEndpoint("/swagger/v1/swagger.json", "Support APP API");

            var configuration = context.GetConfiguration();
            options.OAuthClientId(configuration["AuthServer:SwaggerClientId"]);
            options.OAuthScopes("PromotionAPI");
        });
        app.UseAuditing();
        app.UseAbpSerilogEnrichers();
        app.UseConfiguredEndpoints();

        var configuration = context.GetConfiguration();

        app.UseHangfireDashboard("/hangfire", new DashboardOptions
        {
            DashboardTitle = $"Hangfire Dashboard - Promotion",
            Authorization = new[]
            {
                new HangfireCustomBasicAuthenticationFilter
                {
                    User = configuration["Hangfire:UserName"],
                    Pass = configuration["Hangfire:Password"]
                }
            },
            IgnoreAntiforgeryToken = true
        });

        RecurringJob.AddOrUpdate<IHangFireAppService>
        (p => p.JobActiveDailyAsync(), "59 23 * * *", TimeZoneInfo.Local,
            $"{configuration["Hangfire:PrefixDailyPromotion"]}");

        RecurringJob.AddOrUpdate<IPromotionAppService>
        (p => p.ScheduleReCacheAsync(), "00 02 * * *", TimeZoneInfo.Local,
            $"{configuration["Hangfire:PrefixDailyPromotionRecache"]}");

        RecurringJob.AddOrUpdate<IPromotionAppService>
        (p => p.JobSyncDataDailyFromActiveDateAsync(), "00 01 * * *", TimeZoneInfo.Local,
            $"{configuration["Hangfire:PrefixDailyPromotionActiveDate"]}");

        RecurringJob.AddOrUpdate<IHangFireAppService>
        (p => p.JobActiveCampaignDailyAsync(), "59 23 * * *", TimeZoneInfo.Local,
            $"{configuration["Hangfire:PrefixDalyCampaignActiveDate"]}");

    }
}