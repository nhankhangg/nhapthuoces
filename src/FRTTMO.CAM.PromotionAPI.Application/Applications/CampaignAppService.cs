﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FRTTMO.CAM.PromotionAPI.Documents;
using FRTTMO.CAM.PromotionAPI.Documents.Campaign;
using FRTTMO.CAM.PromotionAPI.Dtos.Campaigns;
using FRTTMO.CAM.PromotionAPI.DTOs.Campaigns;
using FRTTMO.CAM.PromotionAPI.DTOs.Promotion;
using FRTTMO.CAM.PromotionAPI.Entities.Campaign;
using FRTTMO.CAM.PromotionAPI.Entities.Common;
using FRTTMO.CAM.PromotionAPI.Entities.Promotion;
using FRTTMO.CAM.PromotionAPI.Entities.RegionCondition;
using FRTTMO.CAM.PromotionAPI.Enum;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;
using FRTTMO.CAM.PromotionAPI.ExceptionCodes;
using FRTTMO.CAM.PromotionAPI.Helpers;
using FRTTMO.CAM.PromotionAPI.Localization;
using FRTTMO.CAM.PromotionAPI.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using NUglify.Helpers;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Auditing;
using Volo.Abp.Uow;

namespace FRTTMO.CAM.PromotionAPI.Applications;

public class CampaignAppService : PromotionAPIAppService, ICampaignAppService
{
    private readonly ICampaignRepository _campaignRepository;
    private readonly IConfiguration _configuration;
    private readonly ICounterRepository _counterRepository;
    private readonly IPromotionRepository _promotionRepository;
    private readonly IStringLocalizer<PromotionAPIResource> _stringLocalize;
    private readonly IUnitOfWorkManager _unitOfWorkManager;
    private readonly IProvinceConditionRepository _shopConditionRepository;
    private readonly PromotionAppService _promotionAppService;
    private readonly IElasticSearchAppService _elasticSearchAppService;

    public CampaignAppService(
        ICampaignRepository campaignRepository,
        IUnitOfWorkManager unitOfWorkManager,
        ICounterRepository counterRepository,
        IConfiguration configuration,
        IPromotionRepository promotionRepository,
        IStringLocalizer<PromotionAPIResource> stringLocalize,
        IProvinceConditionRepository shopConditionRepository,
        PromotionAppService promotionAppService,
        IElasticSearchAppService elasticSearchAppService
    )
    {
        _configuration = configuration;
        _stringLocalize = stringLocalize;
        _campaignRepository = campaignRepository;
        _counterRepository = counterRepository;
        _unitOfWorkManager = unitOfWorkManager;
        _promotionRepository = promotionRepository;
        _shopConditionRepository = shopConditionRepository;
        _promotionAppService = promotionAppService;
        _elasticSearchAppService = elasticSearchAppService;
    }

    public async Task<CampaignDto> GetAsync(Guid id)
    {
        var campaign = await _campaignRepository.FindAsync(id);
        return ObjectMapper.Map<Campaign, CampaignDto>(campaign);
    }

    public async Task<CampaignDto> CreateAsync(CreateCampaignDto createDto)
    {
        using (var uow = _unitOfWorkManager.Begin(new AbpUnitOfWorkOptions { IsTransactional = false }, requiresNew: true))
        {
            var codeGenerator = await _counterRepository.GenerateCode(_configuration["PrefixCodeGenerator:Campaign"]);
            var campaign = ObjectMapper.Map<CreateCampaignDto, Campaign>(createDto);
            MapAuditedUser(ref campaign, "create");
            campaign.Code = codeGenerator;
            await _campaignRepository.InsertAsync(campaign);
            var campaignDocument = ObjectMapper.Map<Campaign, CampaignDocument>(campaign);
            await _elasticSearchAppService.CreateCampaignDocumentAsync(campaignDocument);

            await uow.CompleteAsync();
            await uow.SaveChangesAsync();
            return ObjectMapper.Map<Campaign, CampaignDto>(campaign);
        }
    }

    public async Task<CampaignDto> UpdateAsync(Guid id, UpdateCampaignDto campaignInput)
    {
        using (var uow = _unitOfWorkManager.Begin(new AbpUnitOfWorkOptions { IsTransactional = false }, requiresNew: true))
        {
            var campaignEntity = await _campaignRepository.FindAsync(id);
            if (campaignEntity == null)
            {
                throw new FrtPromotionValidationException(ExceptionCode.NotFound, _stringLocalize[ExceptionCode.NotFound],
                    $"CampaignId : {id}");
            }
            var promotionExist = await _promotionRepository.GetListAsync(prom => prom.CampaignId == id);
            var promotionWithDate = promotionExist.Where(c => c.FromDate < campaignInput.FromDate || c.ToDate > campaignInput.ToDate).ToList();
            if (promotionWithDate is { Count: > 0 })
            {
                var promotionMinDate = promotionExist.MinBy(p => p.FromDate);
                var promotionMaxDate = promotionExist.MaxBy(p => p.ToDate);
                throw new FrtPromotionValidationException(ExceptionCode.BadRequest, _stringLocalize[ExceptionCode.BadRequest],
                    $"Đã tồn tại chương trình khuyến mãi [{promotionMinDate!.Code},{promotionMaxDate!.Code}] " +
                    $"với ngày bắt đầu [{promotionMinDate!.FromDate.Date}] " +
                    $"và ngày kết thúc [{promotionMaxDate!.ToDate.Date}]");
            }
            //Update promotion khi status của campaign là IsActived
            if (campaignInput.Status == 3 && promotionExist.Any())
            {
                foreach (var promotion in promotionExist)
                {
                    if (promotion != null && campaignInput.Status == (int)PromotionStatusEnum.InActive && promotion.Status != (int)PromotionStatusEnum.InActive)
                    {
                        var shopConditions =
                             await _shopConditionRepository.GetListAsync(x => x.PromotionCode == promotion.Code);

                        await _promotionAppService.TimeExpireCacheAsync(new List<PromotionTimeExpireEto>
                    {
                        new PromotionTimeExpireEto
                        {
                            PromotionId = promotion.Id,
                            PromotionCode = promotion.Code,
                            PromotionType = promotion.PromotionType,
                            ProvinceConditions =shopConditions.Select(x => x.ProvinceCode).ToList()
                        }
                    });

                        var promotionES = await _elasticSearchAppService.GetPromotionByIdAsync(promotion.Id);
                        if (promotionES != null)
                        {
                            promotionES.Status = campaignInput.Status;
                            await _elasticSearchAppService.UpdateAsync(promotion.Id, promotionES);
                        }
                    }
                }
                promotionExist.ForEach(x => x.Status = 3);
                await _promotionRepository.UpdateManyAsync(promotionExist);
            }
            var campaignUpdating = ObjectMapper.Map(campaignInput, campaignEntity);
            MapAuditedUser(ref campaignEntity, "update");
            await _campaignRepository.UpdateAsync(campaignUpdating);
            var campaignDocument = ObjectMapper.Map<Campaign, CampaignDocument>(campaignUpdating);
            await _elasticSearchAppService.CreateCampaignDocumentAsync(campaignDocument);
            await uow.CompleteAsync();
            await uow.SaveChangesAsync();
            return ObjectMapper.Map<Campaign, CampaignDto>(campaignUpdating);
        }
    }

    [DisableAuditing]
    public async Task<PagedResultDto<CampaignDto>> SearchCampaignAsync(SearchCampaignDto searchCampaign)
    {
        List<Campaign> campaigns;

        if (searchCampaign.Keyword.IsNullOrEmpty())
        {
            campaigns = await _campaignRepository.GetListAsync();
        }
        else
        {
            campaigns = await _campaignRepository.GetListAsync(p => p.Code.Contains(searchCampaign.Keyword)
                                                                    || p.Name.Contains(searchCampaign.Keyword));
        }

        if (searchCampaign.CreatedBy.Any())
        {
            campaigns = campaigns.Where(p => searchCampaign.CreatedBy.Contains(p.CreatedBy)).ToList();
        }

        if (searchCampaign.Status != null)
        {
            campaigns = campaigns.Where(p => p.Status == (int)searchCampaign.Status).ToList();
        }

        if (searchCampaign.FromDate != null && searchCampaign.ToDate == null)
        {
            campaigns = campaigns.Where(p => p.FromDate >= searchCampaign.FromDate).ToList();
        }

        if (searchCampaign.FromDate == null && searchCampaign.ToDate != null)
        {
            campaigns = campaigns.Where(p => p.ToDate <= searchCampaign.ToDate).ToList();
        }

        if (searchCampaign.FromDate != null && searchCampaign.ToDate != null)
        {
            campaigns = campaigns.Where(p => p.FromDate >= searchCampaign.FromDate
                                             && p.ToDate <= searchCampaign.ToDate).ToList();
        }

        if (searchCampaign.CampaignId != null)
        {
            campaigns = campaigns.Where(p => p.Id == searchCampaign.CampaignId).ToList();
        }

        var pagedResult = SortHelpers
            .Sort(campaigns, searchCampaign.Sorting.ColumnName, searchCampaign.Sorting.SortType)
            .Skip(searchCampaign.SkipCount)
            .Take(searchCampaign.MaxResultCount)
            .ToList();

        return new PagedResultDto<CampaignDto>
        {
            TotalCount = campaigns.Count,
            Items = ObjectMapper.Map<List<Campaign>, List<CampaignDto>>(pagedResult)
        };
    }

    private void MapAuditedUser(ref Campaign campaign, string action)
    {
        var userId = CurrentUser?.FindClaim("employee_code")?.Value ?? "";
        var userName = CurrentUser?.FindClaim("full_name")?.Value ?? "";
        switch (action)
        {
            case "create":
                {
                    campaign.CreatedBy = userId;
                    campaign.CreatedByName = userName;
                    return;
                }
            case "update":
                {
                    campaign.UpdateBy = userId;
                    campaign.UpdateByName = userName;
                    return;
                }
        }
    }
}