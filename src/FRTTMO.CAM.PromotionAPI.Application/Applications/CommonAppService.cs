﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FRTTMO.CAM.PromotionAPI.Constants;
using FRTTMO.CAM.PromotionAPI.DTOs.Common;
using FRTTMO.CAM.PromotionAPI.DTOs.FieldConfigures;
using FRTTMO.CAM.PromotionAPI.Entities.FieldConfigure;
using FRTTMO.CAM.PromotionAPI.Entities.ImageConfigure;
using FRTTMO.CAM.PromotionAPI.Entities.Promotion;
using FRTTMO.CAM.PromotionAPI.Entities.QualifierConfigure;
using FRTTMO.CAM.PromotionAPI.ExceptionCodes;
using FRTTMO.CAM.PromotionAPI.Helpers;
using FRTTMO.CAM.PromotionAPI.Localization;
using FRTTMO.CAM.PromotionAPI.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Auditing;

namespace FRTTMO.CAM.PromotionAPI.Applications;

public class CommonAppService : PromotionAPIAppService, ICommonAppService
{
    private readonly IFieldConfigureRepository _fieldConfigureRepository;
    private readonly IQualifierConfigureRepository _qualifierConfigureRepository;
    private readonly IStringLocalizer<PromotionAPIResource> _stringLocalize;
    private readonly IImageConfigureRepository _imageConfigureRepository;
    private readonly IPromotionRepository _promotionRepository;
    private readonly IApiClientService _iApiClientService;
    private readonly IConfiguration _config;
    public CommonAppService(IFieldConfigureRepository fieldConfigureRepository,
        IQualifierConfigureRepository qualifierConfigureRepository,
        IStringLocalizer<PromotionAPIResource> stringLocalize,
        IImageConfigureRepository imageConfigureRepository,
        IPromotionRepository promotionRepository,
        IApiClientService iApiClientService, 
        IConfiguration config)
    {
        _fieldConfigureRepository = fieldConfigureRepository;
        _qualifierConfigureRepository = qualifierConfigureRepository;
        _stringLocalize = stringLocalize;
        _imageConfigureRepository = imageConfigureRepository;
        _promotionRepository = promotionRepository;
        _iApiClientService = iApiClientService;
        _config = config;
    }

    public async Task<List<CommonDto>> CreatePromotionImageAsync(List<CreateCommonDto> input)
    {
        var imageConfigures = ObjectMapper.Map<List<CreateCommonDto>, List<ImageConfigure>>(input);
        if (imageConfigures.Any(c => c.Group != FieldConfigureConstants.PromotionImage && c.Group != FieldConfigureConstants.VoucherImage))
        {
            throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"Không thể thêm vì Group không phải là PromotionImage hoặc VoucherImage");
        }
        await _imageConfigureRepository.InsertManyAsync(imageConfigures);

        return ObjectMapper.Map<List<ImageConfigure>, List<CommonDto>>(imageConfigures);
    }
    public static string UrlEncodeExtended(string value)
    {
        char[] chars = value.ToCharArray();
        StringBuilder encodedValue = new StringBuilder();
        foreach (char c in chars)
        {
            encodedValue.Append("%" + ((int)c).ToString("X2"));
        }
        return encodedValue.ToString();
    }
    public async Task<bool> DeletePromotionImageAsync(string group, IList<string> code)
    {
        var listImage = await _imageConfigureRepository.GetListAsync(x => code.Contains(x.Code) && x.Group == group);
        if(listImage.Any()) 
        {
            await _imageConfigureRepository.DeleteManyAsync(listImage);
            return true;
        }
        return false;
    }

    [DisableAuditing]
    public async Task<PagedResultDto<CommonDto>> FilterPromotionImageAsync(FilterPromotionImageDto imageDto)
    {
        var listImage = await _imageConfigureRepository.GetListAsync();

        if(imageDto.Keyword != null)
        {
            listImage = listImage.Where(x => x.Code.ToLower().Contains(imageDto.Keyword.ToLower()) || x.Name.ToLower().Contains(imageDto.Keyword.ToLower())).ToList();
        }

        if(imageDto.Group != null)
        {
            listImage = listImage.Where(x => x.Group == imageDto.Group).ToList();
        }

        var pagedResult = SortHelpers.Sort(listImage,
                  imageDto.Sorting.ColumnName,
                  imageDto.Sorting.SortType)
            .Skip(imageDto.SkipCount)

            .Take(imageDto.MaxResultCount)
            .ToList();

        return new PagedResultDto<CommonDto>
        {
            TotalCount = listImage.Count,
            Items = ObjectMapper.Map<List<ImageConfigure>, List<CommonDto>>(pagedResult)
        };
    }

    public async Task<List<CommonDto>> GetFieldConfiguresAsync(FieldConfigureFilterDto input)
    {
        var fieldConfigures = await _fieldConfigureRepository.GetListAsync();
        //var listCreatedBy = (await _promotionRepository.GetQueryableAsync()).Select(x => new { CreatedBy = x.CreatedBy, CreatedByName = x.CreatedByName}).ToList();

        //var listMasterDataCreatedBy = listCreatedBy.Select(c => new FieldConfigure()
        //{
        //    Code = c.CreatedBy,
        //    Name = c.CreatedByName,
        //    Group = "CreatedBy"
        //}).DistinctBy(c => c.Code).ToList();


        //fieldConfigures.AddRange(listMasterDataCreatedBy);

        var result = ObjectMapper.Map<List<FieldConfigure>, List<CommonDto>>(fieldConfigures.OrderBy(s => s.LineNumber).ThenBy(s => s.Code).ToList());
        return result.ToList();
    }

    public async Task<List<QualifierConfigureDto>> GetQualifierConfigureAsync(QualifierConfigureFilterDto input)
    {
        // Retrieve all QualifierConfigure to store the list in the session storage
        if (input.GetType().GetProperties().All(x => x.GetValue(input) == null))
        {
            var qualifiersConfigure = await _qualifierConfigureRepository.GetListAsync();
            var qualifiersCode = qualifiersConfigure.Select(x => x.QualifierCode).Distinct();
            var fieldConfigures = await _fieldConfigureRepository.GetListAsync(x => (qualifiersCode.Contains(x.Code) && x.Group == "Qualifier") || x.Group == "Operator");

            return qualifiersConfigure.Select(x => new QualifierConfigureDto
            {
                Code = x.QualifierCode,
                Name = fieldConfigures.FirstOrDefault(w => w.Code == x.QualifierCode)?.Name,
                PromotionType = x.PromotionType,
                IsInput = x.IsInput,
                IsOutput = x.IsOutput,
                Operators = fieldConfigures.Where(w => x.GetListOperator().Contains(w.Code)).Select(s1 => new CommonDto
                {
                    Code = s1.Code,
                    Name = s1.Name
                }).ToList()
            }).ToList();
        }
        var result = new List<QualifierConfigureDto>();
        var qualifierConfigures = await _qualifierConfigureRepository.GetListAsync(w =>
            w.PromotionClass.Equals(input.PromotionClass) && w.PromotionType.Equals(input.PromotionType));
        switch (input.Type.ToLower())
        {
            case "input":
                qualifierConfigures = qualifierConfigures.Where(w => w.IsInput).ToList();
                break;
            case "output":
                qualifierConfigures = qualifierConfigures.Where(w => w.IsOutput).ToList();
                break;
            case "condition":
                qualifierConfigures = qualifierConfigures.Where(w => w.IsCondition).ToList();
                break;
        }

        var operatorQualifierConfigures =
            qualifierConfigures.SelectMany(s => s.GetListOperator()).Select(s1 => s1).ToList();
        var operators = await _fieldConfigureRepository.GetListAsync(w =>
            w.Group == "Operator" && operatorQualifierConfigures.Contains(w.Code));
        var qualifierName = await _fieldConfigureRepository.GetListAsync(w =>
            w.Group == "Qualifier" && qualifierConfigures.Select(s => s.QualifierCode).Contains(w.Code));
        qualifierConfigures.ForEach(s =>
        {
            var item = new QualifierConfigureDto
            {
                Code = s.QualifierCode,
                Name = qualifierName.FirstOrDefault(w => w.Code == s.QualifierCode)?.Name,
                Operators = operators.Where(w => s.GetListOperator().Contains(w.Code)).Select(s1 => new CommonDto
                {
                    Code = s1.Code,
                    Name = s1.Name
                }).ToList()
            };
            result.Add(item);
        });
        return result;
    }

    public async Task<CommonDto> GetAppVersionAsync()
    {
        var getAppVersion = await _fieldConfigureRepository.GetAsync(c => c.Group == "AppVersion");
        if (getAppVersion == null)
        {
            return null;
        }
        return ObjectMapper.Map<FieldConfigure, CommonDto>(getAppVersion);
    }

    public async Task<CommonDto> UpdateAppVersionAsync(string version)
    {
        var getAppVersion = await _fieldConfigureRepository.GetAsync(c => c.Group == "AppVersion");
        if (getAppVersion == null)
        {
            return null;
        }
        getAppVersion.Code = version;
        return ObjectMapper.Map<FieldConfigure, CommonDto>(getAppVersion);
    }

    public async Task<List<VoucherDefineDto>> GetVoucherDefineAsync(List<string> voucherCodes)
    {
        var voucherRules = await _iApiClientService.FetchAsync<List<VoucherDefineDto>>(
                        _config["RemoteServices:VoucherRuleAPI:BaseUrl"],
                        "api/voucher-rule/voucher-define","POST",
                        JsonConvert.SerializeObject(voucherCodes));

        return voucherRules;
    }
}