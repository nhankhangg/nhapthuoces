﻿using FRTTMO.CAM.PromotionAPI.DTOs.CustomerFiles;
using FRTTMO.CAM.PromotionAPI.Entities.CustomerTags;
using FRTTMO.CAM.PromotionAPI.ExceptionCodes;
using FRTTMO.CAM.PromotionAPI.Helpers;
using FRTTMO.CAM.PromotionAPI.Localization;
using FRTTMO.CAM.PromotionAPI.Services;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Uow;

namespace FRTTMO.CAM.PromotionAPI.Applications
{
    public class CustomerTagAppService : PromotionAPIAppService, ICustomerTagAppService
    {
        private readonly ICustomerTagsRepository _customerFileRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IStringLocalizer<PromotionAPIResource> _stringLocalize;
        public CustomerTagAppService(ICustomerTagsRepository customerFileRepository, 
            IUnitOfWorkManager unitOfWorkManager,
            IStringLocalizer<PromotionAPIResource> stringLocalize)
        {
            _customerFileRepository = customerFileRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _stringLocalize = stringLocalize;
        }

        public async Task<CustomerTagDto> CreateAsync(CreateCustomerTagDto createCustomerFile)
        {
            using (var uow = _unitOfWorkManager.Begin(new AbpUnitOfWorkOptions { IsTransactional = false }, requiresNew: true))
            {
                var getListCustomerTag = await _customerFileRepository.GetListAsync(c => c.PromotionCode == createCustomerFile.PromotionCode);
                if(getListCustomerTag.Any(c => c.CustomerGroupCode == createCustomerFile.CustomerGroupCode))
                {
                    throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"{createCustomerFile.PromotionCode} và {createCustomerFile.CustomerGroupCode} đã tồn tại trên hệ thống");
                }
                var customerFile = ObjectMapper.Map<CreateCustomerTagDto, CustomerTags>(createCustomerFile);
                MapAuditedUser(ref customerFile, "create");
                await _customerFileRepository.InsertAsync(customerFile);

                await uow.CompleteAsync();
                await uow.SaveChangesAsync();
                return ObjectMapper.Map<CustomerTags, CustomerTagDto>(customerFile);
            }                                                              
        }

        public async Task<CustomerTagDto> GetAsync(Guid id)
        {
            var customerFile = await _customerFileRepository.FindAsync(id);
            return ObjectMapper.Map<CustomerTags, CustomerTagDto>(customerFile);
        }

        public async Task<PagedResultDto<CustomerTagDto>> SearchCustomerFileAsync(SearchCustomerTagDto searchCustomer)
        {
            List<CustomerTags> customerFiles;

            if (searchCustomer.Keyword.IsNullOrEmpty())
            {
                customerFiles = await _customerFileRepository.GetListAsync();
            }
            else
            {
                customerFiles = await _customerFileRepository.GetListAsync(p => p.CustomerGroupName.Contains(searchCustomer.Keyword)
                                                                        || p.CustomerGroupCode.Contains(searchCustomer.Keyword));
            }

            if (searchCustomer.Status != null)
            {
                customerFiles = customerFiles.Where(p => p.Status == (int)searchCustomer.Status).ToList();
            }

            if (searchCustomer.PromotionCode != null)
            {
                customerFiles = customerFiles.Where(p => p.PromotionCode == searchCustomer.PromotionCode).ToList();
            }

            var pagedResult = SortHelpers
                .Sort(customerFiles, searchCustomer.Sorting.ColumnName, searchCustomer.Sorting.SortType)
                .Skip(searchCustomer.SkipCount)
                .Take(searchCustomer.MaxResultCount)
                .ToList();

            return new PagedResultDto<CustomerTagDto>
            {
                TotalCount = customerFiles.Count,
                Items = ObjectMapper.Map<List<CustomerTags>, List<CustomerTagDto>>(pagedResult)
            };
        }

        public async Task<CustomerTagDto> UpdateAsync(Guid id, UpdateCustomerTagDto updateCustomerFile)
        {
            using (var uow = _unitOfWorkManager.Begin(new AbpUnitOfWorkOptions { IsTransactional = false }, requiresNew: true))
            {
                var customerFileEntity = await _customerFileRepository.FindAsync(id);
                if (customerFileEntity == null)
                {
                    throw new FrtPromotionValidationException(ExceptionCode.NotFound, _stringLocalize[ExceptionCode.NotFound],
                        $"CustomerFileId : {id}");
                }

                var getListCustomerTag = await _customerFileRepository.GetListAsync(c => c.PromotionCode == updateCustomerFile.PromotionCode);
                if (getListCustomerTag.Any(c => c.CustomerGroupCode == updateCustomerFile.CustomerGroupCode 
                                                && c.Tick == updateCustomerFile.Tick 
                                                && c.Status == updateCustomerFile.Status))
                {
                    throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"{updateCustomerFile.PromotionCode} và {updateCustomerFile.CustomerGroupCode} đã tồn tại trên hệ thống");
                }

                var customerFileUpdating = ObjectMapper.Map(updateCustomerFile, customerFileEntity);
                MapAuditedUser(ref customerFileEntity, "update");
                await _customerFileRepository.UpdateAsync(customerFileUpdating);
                await uow.CompleteAsync();
                await uow.SaveChangesAsync();
                return ObjectMapper.Map<CustomerTags, CustomerTagDto>(customerFileUpdating);
            }
        }

        private void MapAuditedUser(ref CustomerTags customerFile, string action)
        {
            var userId = CurrentUser?.FindClaim("employee_code")?.Value ?? "";
            var userName = CurrentUser?.FindClaim("full_name")?.Value ?? "";
            switch (action)
            {
                case "create":
                    {
                        customerFile.CreatedBy = userId;
                        customerFile.CreatedByName = userName;
                        return;
                    }
                case "update":
                    {
                        customerFile.UpdateBy = userId;
                        customerFile.UpdateByName = userName;
                        return;
                    }
            }
        }
    }
}
