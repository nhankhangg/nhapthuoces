﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Elasticsearch.Net;
using FRTTMO.CAM.PromotionAPI.Documents;
using FRTTMO.CAM.PromotionAPI.DTOs;
using FRTTMO.CAM.PromotionAPI.DTOs.Promotion;
using FRTTMO.CAM.PromotionAPI.DTOs.Quota;
using FRTTMO.CAM.PromotionAPI.Enum;
using FRTTMO.CAM.PromotionAPI.ExceptionCodes;
using FRTTMO.CAM.PromotionAPI.Localization;
using FRTTMO.CAM.PromotionAPI.Options;
using FRTTMO.CAM.PromotionAPI.Paginations;
using FRTTMO.CAM.PromotionAPI.Services;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using FRTTMO.CAM.PromotionAPI.Documents.Campaign;
using System.Threading;
using FRTTMO.CAM.PromotionAPI.Documents.PromotionInputReplaces;
using FRTTMO.CAM.PromotionAPI.Documents.PromotionOutputReplaces;
using Elastic.Apm.Api;
using System.Linq.Expressions;
using FRTTMO.CAM.PromotionAPI.Entities.Campaign;
using FRTTMO.CAM.PromotionAPI.Helpers;

namespace FRTTMO.CAM.PromotionAPI.Applications
{
    public class ElasticSearchAppService : PromotionAPIAppService, IElasticSearchAppService
    {
        private readonly ITracer _tracer;
        private readonly IElasticClient _elasticClient;
        private readonly IOptions<ElasticSearchOption> _elasticSearchOption;
        private readonly IStringLocalizer<PromotionAPIResource> _stringLocalize;

        public ElasticSearchAppService(
            ITracer tracer,
            IElasticClient elasticClient,
            IOptions<ElasticSearchOption> elasticSearchOption,
            IStringLocalizer<PromotionAPIResource> stringLocalize)
        {
            _tracer = tracer;
            _elasticClient = elasticClient;
            _stringLocalize = stringLocalize;
            _elasticSearchOption = elasticSearchOption;
        }

        #region crud promotion doc

        public async Task<bool> DeleteInputReplacesAsync(string promotionCode)
        {
            var transactionTracer = _tracer?.StartTransaction($"ElasticSearchDeleteInputReplacesAsync", ApiConstants.TypeRequest);
            var span = _tracer?.CurrentTransaction?.StartSpan($"{promotionCode}", ApiConstants.ActionExec);
            try
            {
                if (!promotionCode.IsNullOrEmpty())
                {
                    while (true)
                    {
                        var deleteResponse = await _elasticClient.DeleteByQueryAsync<InputReplaceDocument>(d => d
                             .Index(_elasticSearchOption.Value.IndexPromotionInPutReplace)
                             .Query(p => p.Match(m => m.Field("promotionCode").Query(promotionCode))));
                        if (deleteResponse.ApiCall.Success && deleteResponse.Deleted == 0 && deleteResponse.Total == 0)
                        {
                            break;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                span?.CaptureException(ex);
                transactionTracer?.CaptureException(ex);
            }
            finally
            {
                span?.End();
                transactionTracer?.End();
            }

            return true;
        }

        public async Task<bool> DeleteOutputReplacesAsync(string promotionCode)
        {
            var transactionTracer = _tracer?.StartTransaction($"ElasticSearchDeleteOutputReplacesAsync", ApiConstants.TypeRequest);
            var span = _tracer?.CurrentTransaction?.StartSpan($"{promotionCode}", ApiConstants.ActionExec);
            try
            {
                if (!promotionCode.IsNullOrEmpty())
                {
                    while (true)
                    {
                        var deleteResponse = await _elasticClient.DeleteByQueryAsync<InputReplaceDocument>(d => d
                             .Index(_elasticSearchOption.Value.IndexPromotionOutPutReplace)
                             .Query(p => p.Match(m => m.Field("promotionCode").Query(promotionCode))));
                        if (deleteResponse.ApiCall.Success && deleteResponse.Deleted == 0 && deleteResponse.Total == 0)
                        {
                            break;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                span?.CaptureException(ex);
                transactionTracer?.CaptureException(ex);
            }
            finally
            {
                span?.End();
                transactionTracer?.End();
            }
            return true;
        }

        public void CreateUpdateInputReplaces(List<InputReplaceDocument> inputReplaceDocument)
        {
            inputReplaceDocument.RemoveAll(p => p.PromotionCode.IsNullOrEmpty());
            if (inputReplaceDocument is { Count: > 0 })
            {
                var transactionTracer = _tracer?.StartTransaction($"ElasticSearchCreateUpdateInputReplaces", ApiConstants.TypeRequest);
                var span = _tracer?.CurrentTransaction?.StartSpan($"{inputReplaceDocument?.FirstOrDefault()?.PromotionCode}", ApiConstants.ActionExec);

                try
                {
                    var bulkInsertRequest = _elasticClient.BulkAll(inputReplaceDocument, selector => selector
                    .MaxDegreeOfParallelism(4)
                    .Index(_elasticSearchOption.Value.IndexPromotionInPutReplace)
                    .BackOffTime(TimeSpan.FromSeconds(30))
                    .Size(10000));

                    ManualResetEvent waitHandle = new(false);
                    Exception ex = null;
                    BulkAllObserver bulkAllObserver = new(
                        onNext: bulkAllResponse =>
                        {
                        },
                        onError: exception =>
                        {
                            ex = exception;
                            waitHandle.Set();
                        },
                        onCompleted: () =>
                        {
                            waitHandle.Set();
                        });

                    bulkInsertRequest.Subscribe(bulkAllObserver);

                    waitHandle.WaitOne();

                    if (ex != null)
                    {
                        throw ex;
                    }
                }
                catch (Exception ex)
                {
                    transactionTracer?.CaptureException(ex);
                    throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest], $"Error: {ex.Message}");
                }
                finally
                {
                    span?.End();
                    transactionTracer?.End();
                }
            }
        }

        public void CreateUpdateOutputReplacesAsync(List<OutputReplaceDocument> outputReplaceDocument)
        {
            outputReplaceDocument.RemoveAll(p => p.PromotionCode.IsNullOrEmpty());
            if (outputReplaceDocument is { Count: > 0 })
            {
                var transactionTracer = _tracer?.StartTransaction($"ElasticSearchCreateUpdateOutputReplacesAsync", ApiConstants.TypeRequest);
                var span = _tracer?.CurrentTransaction?.StartSpan($"{outputReplaceDocument?.FirstOrDefault()?.PromotionCode}", ApiConstants.ActionExec);
                try
                {

                    var bulkInsertRequest = _elasticClient.BulkAll(outputReplaceDocument, selector => selector
                    .MaxDegreeOfParallelism(4)
                    .Index(_elasticSearchOption.Value.IndexPromotionOutPutReplace)
                    .BackOffTime(TimeSpan.FromSeconds(30))
                    .Size(10000));

                    ManualResetEvent waitHandle = new(false);
                    Exception ex = null;
                    BulkAllObserver bulkAllObserver = new(
                        onNext: bulkAllResponse =>
                        {
                        },
                        onError: exception =>
                        {
                            ex = exception;
                            waitHandle.Set();
                        },
                        onCompleted: () =>
                        {
                            waitHandle.Set();
                        });

                    bulkInsertRequest.Subscribe(bulkAllObserver);

                    waitHandle.WaitOne();

                    if (ex != null)
                    {
                        throw ex;
                    }
                }
                catch (Exception ex)
                {
                    transactionTracer?.CaptureException(ex);
                    throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest], $"Error: {ex.Message}");
                }
                finally
                {
                    span?.End();
                    transactionTracer?.End();
                }
            }
        }

        public async Task CreateUpdatePromotionExcludesAsync(List<PromotionExcludeDocument> promotionExcludeDocument)
        {
            promotionExcludeDocument.RemoveAll(p => p.Code.IsNullOrEmpty());
            promotionExcludeDocument.RemoveAll(p => p.PromotionCode.IsNullOrEmpty());
            if (promotionExcludeDocument is { Count: > 0 } && promotionExcludeDocument != null)
            {
                var promotionCode = promotionExcludeDocument?.FirstOrDefault(p => p.PromotionCode != null)?.PromotionCode;
                var transactionTracer = _tracer?.StartTransaction($"ElasticSearchCreateUpdatePromotionExcludesAsync", ApiConstants.TypeRequest);
                var span = _tracer?.CurrentTransaction?.StartSpan($"{promotionCode}", ApiConstants.ActionExec);
                try
                {
                    if (!promotionCode.IsNullOrEmpty())
                    {
                        var baseBoolQuery = new BoolQuery
                        {
                            Must = new List<QueryContainer>
                            {
                                new TermQuery
                                {
                                    Field = "promotionCode",
                                    Value = promotionCode
                                }
                            }
                        };
                        while (true)
                        {
                            var deleteResponse = await _elasticClient.DeleteByQueryAsync<PromotionExcludeDocument>(d => d
                            .Index(_elasticSearchOption.Value.IndexPromotionExclude)
                            .Query(p => new QueryContainer(baseBoolQuery)));

                            if (deleteResponse.ApiCall.Success && deleteResponse.Deleted == 0 && deleteResponse.Total == 0)
                            {
                                break;
                            }
                        }
                    }

                    var bulkInsertRequest = _elasticClient.BulkAll(promotionExcludeDocument, selector => selector
                    .MaxDegreeOfParallelism(4)
                    .Index(_elasticSearchOption.Value.IndexPromotionExclude)
                    .BackOffTime(TimeSpan.FromSeconds(10))
                    .Size(10000));

                    ManualResetEvent waitHandle = new(false);
                    Exception ex = null;
                    BulkAllObserver bulkAllObserver = new(
                        onNext: bulkAllResponse =>
                        {
                        },
                        onError: exception =>
                        {
                            ex = exception;
                            waitHandle.Set();
                        },
                        onCompleted: () =>
                        {
                            waitHandle.Set();
                        });

                    bulkInsertRequest.Subscribe(bulkAllObserver);

                    waitHandle.WaitOne();

                    if (ex != null)
                    {
                        throw ex;
                    }
                }
                catch (Exception ex)
                {
                    transactionTracer?.CaptureException(ex);
                }
                finally
                {
                    span?.End();
                    transactionTracer?.End();
                }
            }
        }

        public async Task<bool> CreateUpdatePromotionShopConditionAsync(List<ProvinceConditionDocument> shopConditionDocuments)
        {
            var transactionTracer = _tracer?.StartTransaction($"ElasticSearchCreateUpdatePromotionShopConditionAsync", ApiConstants.TypeRequest);
            var span = _tracer?.CurrentTransaction?.StartSpan($"{shopConditionDocuments?.FirstOrDefault()?.PromotionCode}", ApiConstants.ActionExec);
            try
            {
                var baseBoolQuery = new BoolQuery
                {
                    Must = new List<QueryContainer>
                        {
                            new TermQuery
                            {
                                Field = "promotionCode",
                                Value = shopConditionDocuments.FirstOrDefault().PromotionCode
                            }
                        }
                };

                var deleteResponse = await _elasticClient.DeleteByQueryAsync<ProvinceConditionDocument>(d => d
                    .Index(_elasticSearchOption.Value.IndexPromotionShopCondition)
                    .Query(p => new QueryContainer(baseBoolQuery))
                );

                if (!deleteResponse.IsValid)
                {
                    throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest], $"Error: {deleteResponse.ServerError}");
                }

                var bulkInsertRequest = _elasticClient.BulkAll(shopConditionDocuments, selector => selector
                .MaxDegreeOfParallelism(4)
                .Index(_elasticSearchOption.Value.IndexPromotionShopCondition)
                .BackOffTime(TimeSpan.FromSeconds(10))
                .BackOffRetries(2)
                .Size(10000));

                ManualResetEvent waitHandle = new(false);
                Exception ex = null;
                BulkAllObserver bulkAllObserver = new(
                    onNext: bulkAllResponse =>
                    {
                    },
                    onError: exception =>
                    {
                        ex = exception;
                        waitHandle.Set();
                    },
                    onCompleted: () =>
                    {
                        waitHandle.Set();
                    });

                bulkInsertRequest.Subscribe(bulkAllObserver);

                waitHandle.WaitOne();

                if (ex != null)
                {
                    throw ex;
                }
                return true;
            }
            catch (Exception ex)
            {
                transactionTracer?.CaptureException(ex);
                throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                    _stringLocalize[ExceptionCode.BadRequest], $"Error: {ex.Message}");
            }
            finally
            {
                span?.End();
                transactionTracer?.End();
            }
        }

        public async Task<bool> CreateCampaignDocumentAsync(CampaignDocument campaignDocument)
        {
            try
            {
                var resultCreateCampaign = await _elasticClient
                    .IndexAsync(campaignDocument, i => i.Index(_elasticSearchOption.Value.IndexCampaign)
                        .Refresh(Refresh.True));

                return resultCreateCampaign.Result == Result.Created;

            }
            catch (Exception e)
            {
                throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                    _stringLocalize[ExceptionCode.BadRequest], $"Error: {e.Message}");
            }
        }

        public async Task<bool> UpdateCampaignDocumentAsync(Guid id, CampaignDocument campaignDocument)
        {
            try
            {
                var resultUpdatePromotion = await _elasticClient
                .UpdateAsync<CampaignDocument>(id, i => i.Index(_elasticSearchOption.Value.IndexCampaign)
                        .Doc(campaignDocument)
                        .Refresh(Refresh.True));

                return resultUpdatePromotion.Result == Result.Updated;

            }
            catch (Exception e)
            {
                throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                    _stringLocalize[ExceptionCode.BadRequest], $"Error: {e.Message}");
            }
        }

        public async Task<PromotionDocument> GetPromotionByIdAsync(Guid id, bool throwEx = false)
        {
            try
            {
                var promotion =
                    await _elasticClient.GetAsync<PromotionDocument>(id,
                        i => i.Index(_elasticSearchOption.Value.IndexPromotion));

                if (promotion.Source == null || throwEx)
                {
                    return null;
                }

                return promotion.Source;
            }
            catch (Exception e)
            {
                throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                    _stringLocalize[ExceptionCode.BadRequest], $"Error: {e.Message}");
            }

        }

        public async Task<bool> CreateUpdatePromotionAsync(PromotionDocument promotionDocument)
        {
            var resultCreatePromotionDocument = await _elasticClient
                .IndexAsync(promotionDocument, i => i.Index(_elasticSearchOption.Value.IndexPromotion)
                    .Refresh(Refresh.True));
            return resultCreatePromotionDocument.Result == Result.Created;
        }

        public async Task<bool> UpdateAsync(Guid id, PromotionDocument promotionDocument)
        {
            var resultUpdatePromotion = await _elasticClient
            .UpdateAsync<PromotionDocument>(id, i => i.Index(_elasticSearchOption.Value.IndexPromotion)
                    .Doc(promotionDocument)
                    .Refresh(Refresh.True));

            return resultUpdatePromotion.Result == Result.Updated;
        }

        #endregion

        public async Task<List<PromotionDocument>> GetListPromotionByTypeAsync(string keyword, List<PromotionStatusEnum> statuses,
            List<PromotionTypeEnum> promotionTypes, List<string> promotionExcludes)
        {
            try
            {
                var mustQuery = new List<Func<QueryContainerDescriptor<PromotionDocument>, QueryContainer>>();
                if (!keyword.IsNullOrEmpty())
                {
                    mustQuery.Add(b => b.Bool(b => b.Should(s => s.Wildcard(w => w.Field(f => f.Code).Value($"*{keyword}*"))
                                                                || s.MatchPhrasePrefix(mp => mp.Field(f => f.Name).Query(keyword)))
                                                    .MinimumShouldMatch(1)));

                };
                if (promotionTypes.Any())
                {
                    mustQuery.Add(s => s.Terms(ts => ts.Field(f => f.PromotionType)
                                            .Terms(promotionTypes.Select(x => x.ToString()))));
                }
                if (statuses.Any())
                {
                    mustQuery.Add(s => s.Terms(ts => ts.Field(f => f.Status)
                                            .Terms(statuses)));
                }

                Func<QueryContainerDescriptor<PromotionDocument>, QueryContainer> mustNot = b => null;
                if (promotionExcludes?.Any() ?? false)
                {
                    mustNot = t => t.Terms(ts => ts.Field(f => f.Code)
                                                        .Terms(promotionExcludes));
                }

                var promotions = await _elasticClient.SearchAsync<PromotionDocument>(s =>
                    s.Index(_elasticSearchOption.Value.IndexPromotion)
                    .Size(10000)
                    .Query(q => q.Bool(b => b.Filter(mustQuery)
                                            .MustNot(mustNot)))
                    .Sort(s => s.Descending(f => f.FromDate)));

                return promotions.Hits.Select(x => x.Source).ToList();
            }
            catch (Exception)
            {
                return new List<PromotionDocument>();
            }
        }

        public async Task<Tuple<long, List<PromotionDocument>>> GetListPromotionByItemAsync(string itemCode, PromotionStatusEnum? status, string displayArea, Pagination pagination)
        {
            var inputs = await _elasticClient.SearchAsync<InputReplaceDocument>(s =>
                    s.Index(_elasticSearchOption.Value.IndexPromotionInPutReplace)
                     .Size(10000)
                     .Query(s => s.Term(t => t.Field(f => f.ItemCode).Value(itemCode)))
                );
            if (!inputs.Documents.Any())
            {
                return new(0, new List<PromotionDocument>());
            }

            var mustQuery = new List<Func<QueryContainerDescriptor<PromotionDocument>, QueryContainer>>()
            {
                m => m.Terms(ts => ts.Field(f => f.Code).Terms(inputs.Documents.Select(d => d.PromotionCode).ToHashSet()))
            };
            if (status.HasValue)
            {
                mustQuery.Add(m => m.Term(t => t.Field(f => f.Status).Value(status)));
            }

            var promotionByStatus = await _elasticClient.SearchAsync<PromotionDocument>(s =>
                        s.Index(_elasticSearchOption.Value.IndexPromotion)
                        .From(pagination.SkipCount)
                        .Size(pagination.MaxResultCount)
                        .Query(q => q
                            .Bool(b => b
                                .Must(mustQuery))));

            return new(promotionByStatus.Total, promotionByStatus.Hits.Select(c => c.Source).ToList());
        }

        public async Task UpdatePromotionStatusAsync(List<Guid> promotionIds, PromotionStatusEnum status)
        {
            try
            {
                await _elasticClient.UpdateByQueryAsync<PromotionDocument>(u =>
                    u.Index(_elasticSearchOption.Value.IndexPromotion)
                    .Query(q => q.Terms(ts => ts.Field("id.keyword").Terms(promotionIds)))
                    .Script(s => s.Source("ctx._source.status = params.StatusValue")
                                .Params(p => p.Add("StatusValue", status))));
            }
            catch (Exception)
            {

            }
        }

        public async Task<bool> CreateQuotaHistoryAsync(QuotaHistoryDocument document)
        {
            var result = await _elasticClient.CreateAsync(document,
                    i => i.Index(_elasticSearchOption.Value.IndexQuotaHistory)
                        .Refresh(Refresh.True));

            return result.Result == Result.Created;
        }

        public async Task<UsedQuotaDto> GetUsedQuotaAsync(string promotionCode, QuotaDto quota)
        {
            try
            {
                var (isGetQuotaShop, isOnlyGetToday) = quota.ResetQuotaType switch
                {
                    "ResetPromotion" => (false, true),
                    "ResetShop" => (true, true),
                    _ => (quota.LimitQuantityShop > 0, false)
                };

                Func<QueryContainerDescriptor<QuotaHistoryDocument>, QueryContainer> quotaTodayQuery = q => null;
                if (isOnlyGetToday)
                {
                    quotaTodayQuery = q => q.DateRange(r => r.Field(f => f.UsedDateTime).GreaterThanOrEquals(DateTime.Now.Date));
                }

                Func<AggregationContainerDescriptor<QuotaHistoryDocument>, AggregationContainerDescriptor<QuotaHistoryDocument>> aggs =
                    g => g.Sum("CountQuantity", su => su.Field(f => f.Quantity));


                var result = new UsedQuotaDto();
                if (isGetQuotaShop)
                {
                    var quotas = await _elasticClient.SearchAsync<QuotaHistoryDocument>(
                        u => u.Index(_elasticSearchOption.Value.IndexQuotaHistory)
                            .Source(false)
                            .Query(q => q.Term(t => t.Field(f => f.PromotionCode).Value(promotionCode))
                                        && quotaTodayQuery(q))
                            .Aggregations(a => a.Terms("GroupByShopCode", at => at.Field(f => f.ShopCode).Size(10000)
                                                    .Aggregations(ata => aggs(ata) && ata.TopHits("HitSource", ath => ath.Source(true).Size(1))))));

                    result.QuotaShops = quotas.Aggregations.Terms("GroupByShopCode").Buckets
                        .Select(b => new UsedQuotaShopDto()
                        {
                            ShopCode = b.Key,
                            ShopName = b.TopHits("HitSource").Hits<QuotaHistoryDocument>().FirstOrDefault().Source?.ShopName,
                            Quantity = quota.LimitQuantityShop,
                            CurrentQuantity = Math.Min((int)b.Sum("CountQuantity").Value.GetValueOrDefault(0), quota.LimitQuantityShop)
                        }).ToList();
                }

                if (quota.Quantity > 0)
                {
                    if (!isGetQuotaShop)
                    {
                        var totalQuota = await _elasticClient.SearchAsync<QuotaHistoryDocument>(
                            u => u.Index(_elasticSearchOption.Value.IndexQuotaHistory)
                                .Source(false)
                                .Query(q => q.Term(t => t.Field(f => f.PromotionCode).Value(promotionCode))
                                            && quotaTodayQuery(q))
                                .Aggregations(aggs));

                        result.Quota = new QuotaAmountDto()
                        {
                            Quantity = quota.Quantity,
                            CurrentQuantity = Math.Min((int)totalQuota.Aggregations.Sum("CountQuantity").Value.GetValueOrDefault(0), quota.Quantity)
                        };
                    }
                    else if (quota.ResetQuotaType?.Equals("ResetShop") ?? false)
                    {
                        var totalQuota = await _elasticClient.SearchAsync<QuotaHistoryDocument>(
                            u => u.Index(_elasticSearchOption.Value.IndexQuotaHistory)
                                .Source(false)
                                .Query(q => q.Term(t => t.Field(f => f.PromotionCode).Value(promotionCode)))
                                .Aggregations(aggs));

                        result.Quota = new QuotaAmountDto()
                        {
                            Quantity = quota.Quantity,
                            CurrentQuantity = Math.Min((int)totalQuota.Aggregations.Sum("CountQuantity").Value.GetValueOrDefault(0), quota.Quantity)
                        };
                    }
                    else
                    {
                        result.Quota = new QuotaAmountDto()
                        {
                            Quantity = quota.Quantity,
                            CurrentQuantity = Math.Min(result.QuotaShops.Sum(s => s.CurrentQuantity), quota.Quantity)
                        };
                    }
                }

                return result;
            }
            catch (Exception)
            {
                return new UsedQuotaDto();
            }
        }

        public async Task UpdateQuatityQuotaHistoryAsync(string promotionCode, QuotaDocument quota)
        {
            try
            {
                await _elasticClient.UpdateByQueryAsync<QuotaHistoryDocument>(u =>
                    u.Index(_elasticSearchOption.Value.IndexQuotaHistory)
                    .Query(q => q.Term(t => t.Field(f => f.PromotionCode).Value(promotionCode))
                            && q.DateRange(r => r.Field(f => f.UsedDateTime).GreaterThanOrEquals(DateTime.Now.Date)))
                    .Script(s => s.Source("ctx._source.quantity = params.quantityValue; " +
                                          "ctx._source.maxQuantityShop = params.maxQuantityShopValue; " +
                                          "ctx._source.maxQuantityPhone = params.maxQuantityPhoneValue")
                                .Params(p => p.Add("quantityValue", quota.Quantity)
                                            .Add("maxQuantityShopValue", quota.LimitQuantityShop)
                                            .Add("maxQuantityPhoneValue", quota.LimitQuantityPhone))));
            }
            catch (Exception)
            {

            }
        }

        public async Task<List<string>> SuggestPromotionExcludeAsync(ItemDetailSuggestPromotionExcludeDto input, List<string> promotionIncludes)
        {
            try
            {
                var categoryShouldQuery = new List<Func<QueryContainerDescriptor<InputReplaceDocument>, QueryContainer>>();
                if (input.Code.Any())
                {
                    categoryShouldQuery.Add(s => s.Terms(ts => ts.Field(f => f.ItemCode)
                                            .Terms(input.Code)));
                }
                if (input.CategoryCode.Any())
                {
                    categoryShouldQuery.Add(s => s.Terms(ts => ts.Field(f => f.CategoryCode)
                                            .Terms(input.CategoryCode)));
                }
                if (input.TypeCode.Any())
                {
                    categoryShouldQuery.Add(s => s.Terms(ts => ts.Field(f => f.TypeCode)
                                            .Terms(input.TypeCode)));
                }
                if (input.GroupCode.Any())
                {
                    categoryShouldQuery.Add(s => s.Terms(ts => ts.Field(f => f.GroupCode)
                                            .Terms(input.GroupCode)));
                }
                if (input.CategoryWithBrand.Any())
                {
                    input.CategoryWithBrand.ForEach(category =>
                    {
                        Expression<Func<InputReplaceDocument, string>> GetFieldNameExpress = category.Level switch
                        {
                            PIMCategoryLevelEnum.Category => f => f.CategoryCode,
                            PIMCategoryLevelEnum.Type => f => f.TypeCode,
                            PIMCategoryLevelEnum.Group => f => f.GroupCode,
                            _ => throw new NotImplementedException()
                        };

                        var filterCategoryWithBrandQuery = new List<Func<QueryContainerDescriptor<InputReplaceDocument>, QueryContainer>>()
                        {
                            m => m.Term(mt => mt.Field(GetFieldNameExpress).Value(category.Code))
                        };

                        if (!category.BrandCode.IsNullOrEmpty())
                        {
                            filterCategoryWithBrandQuery.Add(m => m.Term(mt => mt.Field(f => f.BrandCode).Value(category.BrandCode)));
                        }
                        if (!category.ModelCode.IsNullOrEmpty())
                        {
                            filterCategoryWithBrandQuery.Add(m => m.Term(mt => mt.Field(f => f.ModelCode).Value(category.ModelCode)));
                        }

                        categoryShouldQuery.Add(s => s.Bool(sb => sb.Filter(filterCategoryWithBrandQuery)));
                    });
                }

                // if input items don't have any
                if (!categoryShouldQuery.Any())
                {
                    return new List<string>();
                }

                Func<QueryContainerDescriptor<InputReplaceDocument>, QueryContainer> filterPromotionIncludeQuery = b => null;
                if (promotionIncludes?.Any() ?? false)
                {
                    filterPromotionIncludeQuery = t => t.Terms(ts => ts.Field(f => f.PromotionCode)
                                                        .Terms(promotionIncludes));
                }

                var promotions = await _elasticClient.SearchAsync<InputReplaceDocument>(s =>
                    s.Index(_elasticSearchOption.Value.IndexPromotionInPutReplace)
                    .Source(false)
                    .Query(q => q.Bool(b => b.Should(categoryShouldQuery)
                                            .MinimumShouldMatch(1)
                                            .Filter(filterPromotionIncludeQuery)))
                    .Aggregations(a => a.Terms("GroupByPromotion", at => at.Field(f => f.PromotionCode).Size(10000))));


                return promotions.Aggregations.Terms("GroupByPromotion")
                        .Buckets
                        .Select(x => x.Key)
                        .ToList();
            }
            catch (Exception)
            {
                return new List<string>();
            }
        }

        public async Task UpdateCampaignStatusAsync(List<Guid> campaignIds, CampaignStatusEnum status)
        {
            try
            {
                await _elasticClient.UpdateByQueryAsync<CampaignDocument>(u =>
                    u.Index(_elasticSearchOption.Value.IndexCampaign)
                    .Query(q => q.Terms(ts => ts.Field("id.keyword").Terms(campaignIds)))
                    .Script(s => s.Source("ctx._source.status = params.StatusValue")
                                .Params(p => p.Add("StatusValue", status))));
            }
            catch (Exception)
            {

            }
        }
    }
}