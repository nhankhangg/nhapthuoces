﻿using Castle.Core.Internal;
using ExcelDataReader;
using FRTTMO.CAM.PromotionAPI.Constants;
using FRTTMO.CAM.PromotionAPI.DTOs;
using FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels;
using FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels.Discounts;
using FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels.GetProducts;
using FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels.ItemExcludes;
using FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels.Shops;
using FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels.TotalBill;
using FRTTMO.CAM.PromotionAPI.DTOs.Insides;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInput;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInputExclude;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInputReplace;
using FRTTMO.CAM.PromotionAPI.DTOs.Output;
using FRTTMO.CAM.PromotionAPI.DTOs.OutputReplace;
using FRTTMO.CAM.PromotionAPI.DTOs.PIM;
using FRTTMO.CAM.PromotionAPI.Entities.FieldConfigure;
using FRTTMO.CAM.PromotionAPI.ExceptionCodes;
using FRTTMO.CAM.PromotionAPI.Localization;
using FRTTMO.CAM.PromotionAPI.Options;
using FRTTMO.CAM.PromotionAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FRTTMO.CAM.PromotionAPI.Applications
{
    public class ImportExcelAppService : PromotionAPIAppService, IImportExcelAppService
    {
        private readonly IApiClientService _iApiClientService;
        private readonly IOptions<RemoteServicesOption> _remoteServicesOption;
        private readonly IStringLocalizer<PromotionAPIResource> _stringLocalize;
        private readonly IFieldConfigureRepository _fieldConfigureRepository;
        private readonly IConfiguration _config;
        public ImportExcelAppService(IApiClientService iApiClientService,
            IOptions<RemoteServicesOption> remoteServicesOption,
            IStringLocalizer<PromotionAPIResource> stringLocalize,
            IFieldConfigureRepository fieldConfigureRepository,
            IConfiguration config)
        {
            _iApiClientService = iApiClientService;
            _remoteServicesOption = remoteServicesOption;
            _stringLocalize = stringLocalize;
            _fieldConfigureRepository = fieldConfigureRepository;
            _config = config;
        }

        public async Task<ImportExcelDto> ImportExcelSchemeAsync(IFormFile formFile, string promotionType, string templateType, bool isInput)
        {
            var importExcelResults = new ImportExcelDto();
            switch (promotionType)
            {
                case ImportExcelConstants.BuyComboGetDiscount:
                case ImportExcelConstants.BuyProductGetDiscount:
                    if (templateType == ImportExcelConstants.Product)
                    {
                        importExcelResults = await ImportExcelDiscountProductAsync(formFile, promotionType);
                    }
                    if (templateType == ImportExcelConstants.Category)
                    {
                        importExcelResults = await ImportExcelDiscountCategoryAsync(formFile, promotionType);
                    }
                    break;
                case ImportExcelConstants.BuyProductGetProduct:
                case ImportExcelConstants.BuyComboGetProduct:
                case ImportExcelConstants.UsePaymentMethod:
                    if (templateType == ImportExcelConstants.Product && isInput)
                    {
                        importExcelResults = await ImportExcelGetProductInputAsync(formFile, promotionType);
                    }
                    if (templateType == ImportExcelConstants.Category && isInput)
                    {
                        importExcelResults = await ImportExcelCategoryGetProductInputAsync(formFile, promotionType);
                    }
                    if (templateType == ImportExcelConstants.Product && !isInput)
                    {
                        importExcelResults = await ImportExcelGetProductOutputAsync(formFile, promotionType, isInput);
                    }
                    break;
                case ImportExcelConstants.TotalBillAmount:
                case ImportExcelConstants.TotalProductAmount:
                    if (templateType == ImportExcelConstants.Product && isInput)
                    {
                        importExcelResults = await ImportExcelGetProductInputAsync(formFile, promotionType);
                    }
                    if (templateType == ImportExcelConstants.Category && isInput)
                    {
                        importExcelResults = await ImportExcelCategoryGetProductInputAsync(formFile, promotionType);
                    }
                    if (!isInput)
                    {
                        importExcelResults = await ImportExcelTotalBillByOrderOutputAsync(formFile, promotionType);
                    }
                    break;
                case ImportExcelConstants.BuyProductGetAscendingDiscount:
                    if (isInput)
                    {
                        importExcelResults = await ImportExcelGetProductInputAsync(formFile, promotionType);
                    }
                    break;
            }

            if (importExcelResults.IsError)
            {
                var errorMessages = importExcelResults.Errors
                    .GroupBy(x => x.Line)
                    .Select(x => new MessageErrorDto
                    {
                        Line = x.Key,
                        Messages = x.Select(x => x.Message).ToList()
                    })
                    .ToList();
                importExcelResults.Errors = errorMessages;
                return importExcelResults;
            }

            return importExcelResults;
        }

        public async Task<ImportItemExcludesDto> ImportItemExcludesAsync(IFormFile formFile)
        {
            var importExcelResults = new ImportItemExcludesDto();
            importExcelResults = await ImportExcelItemExcludesAsync(formFile);
            if (importExcelResults.IsError)
            {
                var errorMessages = importExcelResults.Errors
                    .GroupBy(x => x.Line)
                    .Select(x => new MessageErrorDto
                    {
                        Line = x.Key,
                        Messages = x.Select(x => x.Message).ToList()
                    })
                    .ToList();
                importExcelResults.Errors = errorMessages;
                return importExcelResults;
            }

            return importExcelResults;
        }
        //Import scheme mua combo giảm giá sản phẩm
        public async Task<ImportExcelDto> ImportExcelDiscountProductAsync(IFormFile FormFile, string promotionType)
        {
            var importExcelDiscount = new ImportExcelDto();
            using (var stream = new MemoryStream())
            {
                FormFile.CopyTo(stream);
                stream.Position = 0;
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                using var reader = ExcelReaderFactory.CreateReader(stream);
                var headers = new List<string>();
                var conf = new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = a => new ExcelDataTableConfiguration
                    {
                        UseHeaderRow = true,
                    }
                };

                DataSet dataSet = reader.AsDataSet(conf);
                var table = dataSet.Tables[0];

                foreach (DataColumn dataColumn in table.Columns)
                {
                    if (promotionType == ImportExcelConstants.BuyProductGetDiscount &&
                        dataColumn.ColumnName != "BuyProductGetDiscountSP")
                    {
                        throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"Template không hợp lệ. Vui lòng tải lại");
                    }
                    if (promotionType == ImportExcelConstants.BuyComboGetDiscount &&
                        dataColumn.ColumnName != "BuyComboGetDiscountSP")
                    {
                        throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"Template không hợp lệ. Vui lòng tải lại");
                    }
                    break;
                }


                var listDataDiscountProduct = ReadDiscountProduct(table, promotionType);
                

                var errorRequired = listDataDiscountProduct.Where(c => c.Messages.Count > 0).ToList();



                //line báo message
                List<MessageErrorDto> messageError = new List<MessageErrorDto>();



                //Check quantity, qualifier
                var mapperLineFirst = ObjectMapper.Map<DiscountProductExcelDto, ValidateQuantityOutputDto>(listDataDiscountProduct.FirstOrDefault());
                var mapperRemoveFirst = ObjectMapper.Map<List<DiscountProductExcelDto>, List<ValidateQuantityOutputDto>>(listDataDiscountProduct);
                var checkQuantityOutput = ValidateQuantityOutput(mapperLineFirst, mapperRemoveFirst);

                messageError.AddRange(checkQuantityOutput);

                //Add error message
                foreach (var error in errorRequired)
                {
                    foreach (var message in error.Messages)
                    {
                        var errorRequire = new MessageErrorDto();
                        errorRequire.Message = message;
                        errorRequire.Line = error.RowNumber;
                        messageError.Add(errorRequire);
                    }
                }



                var duplicateItemCode = listDataDiscountProduct.GroupBy(c => c.ItemCode);
                foreach (var dup in duplicateItemCode.Where(c => c.Count() > 1))
                {
                    foreach (var item in dup)
                    {
                        messageError.Add(new MessageErrorDto()
                        {
                            Line = item.RowNumber,
                            Message = $"Mã {item.ItemCode} bị trùng trong danh sách"
                        });
                    }
                }

                var listGroupBy = listDataDiscountProduct.GroupBy(c => c.LineNumber);
                var listInput = new List<CreateItemInputDto>();

                //Lấy tên sản phẩm từ PIM-
                var listItemCode = listDataDiscountProduct.Select(c => c.ItemCode.ToUpper()).ToList();

                if (!listItemCode.Any())
                {
                    throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                             _stringLocalize[ExceptionCode.NotFound], $"Không tồn tại dữ liệu trong file");
                }

                int batchSize = 300;
                int pageNumber = 0;
                var getDetailProductPIM = new List<ItemDetails>();

                while (true)
                {

                    if (listItemCode.Count() > 300)
                    {
                        var batch = listItemCode.Skip(pageNumber * batchSize).Take(batchSize).ToList();

                        var listDetail = await ListProductDetailAsync(batch);

                        getDetailProductPIM.AddRange(listDetail);
                        pageNumber++;

                        if (batch.Count() < batchSize)
                        {
                            break;
                        }
                    }
                    else
                    {
                        var listDetail = await ListProductDetailAsync(listItemCode);
                        getDetailProductPIM.AddRange(listDetail);
                        break;
                    }
                }

                //Check trường hợp scheme mua sản phẩm giảm giá không có loại đầu ra (loại đầu ra bắt buộc nhập)
                if (promotionType == "BuyProductGetDiscount")
                {
                    var listQualiferNameIsNull = listDataDiscountProduct.Where(c => c.QualifierName.IsNullOrWhiteSpace()).ToList();
                    foreach (var item in listQualiferNameIsNull)
                    {
                        messageError.Add(new MessageErrorDto()
                        {
                            Line = item.RowNumber,
                            Message = $"Mã {item.ItemCode} không có loại đầu ra"
                        });
                    }
                }


                //Check trường hợp scheme mua combo giảm giá không có loại đầu ra (loại đầu ra bắt buộc nhập) 
                if (promotionType == "BuyComboGetDiscount")
                {
                    foreach (var itemGroupBy in listGroupBy)
                    {
                        var listQualiferNameIsNull = itemGroupBy.Where(c => c.QualifierName.IsNullOrWhiteSpace()).ToList();
                        foreach (var item in listQualiferNameIsNull)
                        {
                            messageError.Add(new MessageErrorDto()
                            {
                                Line = item.RowNumber,
                                Message = $"Mã {item.ItemCode} không có loại đầu ra"
                            });
                        }
                    }

                }


                //Lấy tên kho từ database
                var getListWarehouse = await _fieldConfigureRepository.GetListAsync(c => c.Group == "Warehouse");

                //lấy từng itemGroup cho input khi group được từ line number
                foreach (var item in listGroupBy)
                {
                    var firstItem = item.First();
                    var itemReplace = item.Where(c => c.ItemCode != firstItem.ItemCode).ToList();

                    //Gắn vào input và inputReplace
                    var itemInput = new CreateItemInputDto();
                    itemInput.ItemCode = firstItem.ItemCode;
                    itemInput.ItemName = getDetailProductPIM.Where(c => c.Code == firstItem.ItemCode?.ToUpper().Trim()).FirstOrDefault()?.Name;

                    //Trường hợp item Code không có sản phẩm khi check pên PIM
                    if (itemInput.ItemName == null)
                    {
                        messageError.Add(new MessageErrorDto()
                        {
                            Line = firstItem.RowNumber,
                            Message = $"Không tồn tại sản phẩm với mã {itemInput.ItemCode}"
                        });
                    }
                    itemInput.Quantity = firstItem.Quantity ?? 0;
                    itemInput.WarehouseName = firstItem.WarehouseName;
                    itemInput.WarehouseCode = getListWarehouse.Where(c => c.Name == firstItem.WarehouseName).FirstOrDefault()?.Code;
                    var checkUnit = getDetailProductPIM.Where(c => c.Code == firstItem.ItemCode).FirstOrDefault();
                    if (firstItem.UnitName != null &&
                        !checkUnit.Measures.Any(c => c.MeasureUnitName == firstItem.UnitName) &&
                            firstItem.UnitName != ImportExcelConstants.AllUnit)
                    {
                        messageError.Add(new MessageErrorDto()
                        {
                            Line = firstItem.RowNumber,
                            Message = $"Không tồn tại đơn vị {firstItem.UnitName} với mã {firstItem.ItemCode}"
                        });
                    }
                    if(checkUnit.Measures.Any(c => c.MeasureUnitName == firstItem.UnitName))
                    {
                        itemInput.UnitCode = checkUnit.Measures.Where(c => c.MeasureUnitName == firstItem.UnitName).FirstOrDefault()?.MeasureUnitId;
                        itemInput.UnitName = checkUnit.Measures.Where(c => c.MeasureUnitName == firstItem.UnitName).FirstOrDefault()?.MeasureUnitName;
                    }
                    if (firstItem.UnitName == null)
                    {
                        itemInput.UnitCode = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitId;
                        itemInput.UnitName = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitName;
                    }
                    if (firstItem.UnitName == ImportExcelConstants.AllUnit)
                    {
                        itemInput.UnitCode = 0;
                        itemInput.UnitName = ImportExcelConstants.AllUnit;
                    }
                    itemInput.LineNumber = firstItem.LineNumber.Value;
                    itemInput.ItemInputReplaces = itemReplace.Select(x =>
                    {
                        var itemInputReplace = new CreateItemInputReplaceDto();
                        itemInputReplace.ItemCode = x.ItemCode;
                        itemInputReplace.ItemName = getDetailProductPIM.Where(c => c.Code == x.ItemCode?.ToUpper().Trim()).FirstOrDefault()?.Name;
                        if (itemInputReplace.ItemName == null)
                        {
                            messageError.Add(new MessageErrorDto()
                            {
                                Line = x.RowNumber,
                                Message = $"Không tồn tại sản phẩm với mã {x.ItemCode}"
                            }); 
                        }
                        itemInputReplace.Quantity = x.Quantity ?? 0;
                        itemInputReplace.WarehouseName = firstItem.WarehouseName;
                        itemInputReplace.WarehouseCode = getListWarehouse.Where(c => c.Name == x.WarehouseName).FirstOrDefault()?.Code;
                        var checkUnit = getDetailProductPIM.Where(c => c.Code == x.ItemCode).FirstOrDefault();
                        if (x.UnitName != null &&
                        !checkUnit.Measures.Any(c => c.MeasureUnitName == x.UnitName) &&
                            x.UnitName != ImportExcelConstants.AllUnit)
                        {
                            messageError.Add(new MessageErrorDto()
                            {
                                Line = x.RowNumber,
                                Message = $"Không tồn tại đơn vị {x.UnitName} với mã {x.ItemCode}"
                            });
                        }
                        else
                        {
                            itemInputReplace.UnitCode = checkUnit.Measures.Where(c => c.MeasureUnitName == x.UnitName).FirstOrDefault()?.MeasureUnitId;
                            itemInputReplace.UnitName = checkUnit.Measures.Where(c => c.MeasureUnitName == x.UnitName).FirstOrDefault()?.MeasureUnitName;
                        }
                        if (x.UnitName == null)
                        {
                            itemInputReplace.UnitCode = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitId;
                            itemInputReplace.UnitName = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitName;
                        }
                        if (x.UnitName == ImportExcelConstants.AllUnit)
                        {
                            itemInputReplace.UnitCode = 0;
                            itemInputReplace.UnitName = ImportExcelConstants.AllUnit;
                        }
                        itemInputReplace.LineNumber = firstItem.LineNumber.Value;
                        return itemInputReplace;
                    }).ToList();
                    listInput.Add(itemInput);
                }


                var listOutput = new List<CreateOutputDto>();
                //Lấy output có loại đầu ra
                var getOutputQualifier = listDataDiscountProduct.Where(c => c.QualifierName != null).GroupBy(c => c.LineNumber);

                //Lấy data qualifier
                var getListQualifier = await _fieldConfigureRepository.GetListAsync(c => c.Group == "Qualifier");
                foreach (var outputQualifier in getOutputQualifier)
                {
                    var firstItem = outputQualifier.First();
                    var outputReplace = outputQualifier.Where(c => c.ItemCode != firstItem.ItemCode).ToList();
                    var qualifier = getListQualifier.Where(c => c.Name == firstItem.QualifierName).FirstOrDefault()?.Code;
                    
                    var checkUnit = getDetailProductPIM.Where(c => c.Code == firstItem.ItemCode).FirstOrDefault();
                    switch (qualifier)
                    {
                        case "ProductAmountOff":
                            //Gắn vào output và outputReplace
                            var outputAmountOff = new CreateOutputDto();
                            outputAmountOff.ItemCode = firstItem.ItemCode;
                            outputAmountOff.ItemName = getDetailProductPIM.Where(c => c.Code == firstItem.ItemCode?.ToUpper().Trim()).FirstOrDefault()?.Name;
                            outputAmountOff.Quantity = firstItem.Quantity ?? 0;
                            outputAmountOff.WarehouseName = firstItem.WarehouseName;
                            outputAmountOff.WarehouseCode = getListWarehouse.Where(c => c.Name == firstItem.WarehouseName).FirstOrDefault()?.Code;
                            if (checkUnit.Measures.Any(c => c.MeasureUnitName == firstItem.UnitName))
                            {
                                outputAmountOff.UnitCode = checkUnit.Measures.Where(c => c.MeasureUnitName == firstItem.UnitName).FirstOrDefault()?.MeasureUnitId;
                                outputAmountOff.UnitName = checkUnit.Measures.Where(c => c.MeasureUnitName == firstItem.UnitName).FirstOrDefault()?.MeasureUnitName;
                            }
                            if (firstItem.UnitName == null)
                            {
                                outputAmountOff.UnitCode = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitId;
                                outputAmountOff.UnitName = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitName;
                            }
                            if (firstItem.UnitName == ImportExcelConstants.AllUnit)
                            {
                                outputAmountOff.UnitCode = 0;
                                outputAmountOff.UnitName = ImportExcelConstants.AllUnit;
                            }
                            outputAmountOff.LineNumber = firstItem.LineNumber.Value;
                            outputAmountOff.MaxQuantity = firstItem.MaxQuantity ?? 0;
                            outputAmountOff.Discount = firstItem.Discount ?? 0;
                            outputAmountOff.Note = firstItem.Note;
                            outputAmountOff.QualifierCode = qualifier;
                            outputAmountOff.OperatorCode = "Equal";
                            outputAmountOff.OutputReplaces = outputReplace.Select(x =>
                            {
                                var outputAmountOffReplace = new CreateOutputReplaceDto();
                                outputAmountOffReplace.ItemCode = x.ItemCode;
                                outputAmountOffReplace.ItemName = getDetailProductPIM.Where(c => c.Code == x.ItemCode?.ToUpper().Trim()).FirstOrDefault()?.Name;
                                outputAmountOffReplace.Quantity = x.Quantity ?? 0;
                                outputAmountOffReplace.WarehouseName = x.WarehouseName;
                                outputAmountOffReplace.WarehouseCode = getListWarehouse.FirstOrDefault(c => c.Name == x.WarehouseName)?.Code;
                                var checkUnit = getDetailProductPIM.Where(c => c.Code == x.ItemCode).FirstOrDefault();
                                if (checkUnit.Measures.Any(c => c.MeasureUnitName == x.UnitName))
                                {
                                    outputAmountOffReplace.UnitCode = checkUnit.Measures.Where(c => c.MeasureUnitName == x.UnitName).FirstOrDefault()?.MeasureUnitId;
                                    outputAmountOffReplace.UnitName = checkUnit.Measures.Where(c => c.MeasureUnitName == x.UnitName).FirstOrDefault()?.MeasureUnitName;
                                }
                                if (x.UnitName == null)
                                {
                                    outputAmountOffReplace.UnitCode = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitId;
                                    outputAmountOffReplace.UnitName = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitName;
                                }
                                if (x.UnitName == ImportExcelConstants.AllUnit)
                                {
                                    outputAmountOffReplace.UnitCode = 0;
                                    outputAmountOffReplace.UnitName = ImportExcelConstants.AllUnit;
                                }
                                outputAmountOffReplace.LineNumber = x.LineNumber.Value;
                                outputAmountOffReplace.MaxQuantity = x.MaxQuantity ?? 0;
                                outputAmountOffReplace.Discount = x.Discount ?? 0;
                                outputAmountOffReplace.Note = x.Note;
                                outputAmountOffReplace.QualifierCode = qualifier;
                                outputAmountOffReplace.OperatorCode = "Equal";
                                return outputAmountOffReplace;
                            }).ToList();

                            listOutput.Add(outputAmountOff);
                            break;
                        case "ProductFixedPrice":
                            //Gắn vào output và outputReplace
                            var outputFixedPrice = new CreateOutputDto();
                            outputFixedPrice.ItemCode = firstItem.ItemCode;
                            outputFixedPrice.ItemName = getDetailProductPIM.Where(c => c.Code == firstItem.ItemCode?.ToUpper().Trim()).FirstOrDefault()?.Name;
                            outputFixedPrice.Quantity = firstItem.Quantity.Value;
                            outputFixedPrice.WarehouseName = firstItem.WarehouseName;
                            outputFixedPrice.WarehouseCode = getListWarehouse.Where(c => c.Name == firstItem.WarehouseName).FirstOrDefault()?.Code;
                            if (checkUnit.Measures.Any(c => c.MeasureUnitName == firstItem.UnitName))
                            {
                                outputFixedPrice.UnitCode = checkUnit.Measures.Where(c => c.MeasureUnitName == firstItem.UnitName).FirstOrDefault()?.MeasureUnitId;
                                outputFixedPrice.UnitName = checkUnit.Measures.Where(c => c.MeasureUnitName == firstItem.UnitName).FirstOrDefault()?.MeasureUnitName;
                            }
                            if (firstItem.UnitName == null)
                            {
                                outputFixedPrice.UnitCode = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitId;
                                outputFixedPrice.UnitName = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitName;
                            }
                            if (firstItem.UnitName == ImportExcelConstants.AllUnit)
                            {
                                outputFixedPrice.UnitCode = 0;
                                outputFixedPrice.UnitName = ImportExcelConstants.AllUnit;
                            }
                            outputFixedPrice.LineNumber = firstItem.LineNumber.Value;
                            outputFixedPrice.MaxQuantity = firstItem.MaxQuantity ?? 0;
                            outputFixedPrice.Discount = firstItem.Discount ?? decimal.Zero;
                            outputFixedPrice.Note = firstItem.Note;
                            outputFixedPrice.QualifierCode = qualifier;
                            outputFixedPrice.OperatorCode = "Equal";
                            outputFixedPrice.OutputReplaces = outputReplace.Select(x =>
                            {
                                var outputFixedPriceReplace = new CreateOutputReplaceDto();
                                outputFixedPriceReplace.ItemCode = x.ItemCode;
                                outputFixedPriceReplace.ItemName = getDetailProductPIM.Where(c => c.Code == x.ItemCode?.ToUpper().Trim()).FirstOrDefault()?.Name;
                                outputFixedPriceReplace.Quantity = x.Quantity.Value;
                                outputFixedPriceReplace.WarehouseName = x.WarehouseName;
                                outputFixedPriceReplace.WarehouseCode = getListWarehouse.Where(c => c.Name == x.WarehouseName).FirstOrDefault()?.Code;
                                var checkUnit = getDetailProductPIM.Where(c => c.Code == x.ItemCode).FirstOrDefault();
                                if (checkUnit.Measures.Any(c => c.MeasureUnitName == x.UnitName))
                                {
                                    outputFixedPriceReplace.UnitCode = checkUnit.Measures.Where(c => c.MeasureUnitName == x.UnitName).FirstOrDefault()?.MeasureUnitId;
                                    outputFixedPriceReplace.UnitName = checkUnit.Measures.Where(c => c.MeasureUnitName == x.UnitName).FirstOrDefault()?.MeasureUnitName;
                                }
                                if (x.UnitName == null)
                                {
                                    outputFixedPriceReplace.UnitCode = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitId;
                                    outputFixedPriceReplace.UnitName = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitName;
                                }
                                if (x.UnitName == ImportExcelConstants.AllUnit)
                                {
                                    outputFixedPriceReplace.UnitCode = 0;
                                    outputFixedPriceReplace.UnitName = ImportExcelConstants.AllUnit;
                                }
                                outputFixedPriceReplace.LineNumber = x.LineNumber.Value;
                                outputFixedPriceReplace.MaxQuantity = x.MaxQuantity ?? 0;
                                outputFixedPriceReplace.Discount = x.Discount ?? decimal.Zero;
                                outputFixedPriceReplace.Note = x.Note;
                                outputFixedPriceReplace.QualifierCode = qualifier;
                                outputFixedPriceReplace.OperatorCode = "Equal";
                                return outputFixedPriceReplace;
                            }).ToList();

                            listOutput.Add(outputFixedPrice);
                            break;
                        case "ProductPercentOff":
                            //Gắn vào output và outputReplace
                            var outputPercentOff = new CreateOutputDto();
                            outputPercentOff.ItemCode = firstItem.ItemCode;
                            outputPercentOff.ItemName = getDetailProductPIM.Where(c => c.Code == firstItem.ItemCode?.ToUpper().Trim()).FirstOrDefault()?.Name;
                            outputPercentOff.Quantity = firstItem.Quantity.Value;
                            outputPercentOff.WarehouseName = firstItem.WarehouseName;
                            outputPercentOff.WarehouseCode = getListWarehouse.Where(c => c.Name == firstItem.WarehouseName).FirstOrDefault()?.Code;
                            if (checkUnit.Measures.Any(c => c.MeasureUnitName == firstItem.UnitName))
                            {
                                outputPercentOff.UnitCode = checkUnit.Measures.Where(c => c.MeasureUnitName == firstItem.UnitName).FirstOrDefault()?.MeasureUnitId;
                                outputPercentOff.UnitName = checkUnit.Measures.Where(c => c.MeasureUnitName == firstItem.UnitName).FirstOrDefault()?.MeasureUnitName;
                            }
                            if (firstItem.UnitName == null)
                            {
                                outputPercentOff.UnitCode = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitId;
                                outputPercentOff.UnitName = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitName;
                            }
                            if (firstItem.UnitName == ImportExcelConstants.AllUnit)
                            {
                                outputPercentOff.UnitCode = 0;
                                outputPercentOff.UnitName = ImportExcelConstants.AllUnit;
                            }
                            outputPercentOff.LineNumber = firstItem.LineNumber ?? 0;
                            outputPercentOff.MaxQuantity = firstItem.MaxQuantity ?? 0;
                            outputPercentOff.Discount = firstItem.Discount ?? decimal.Zero;
                            outputPercentOff.QualifierCode = qualifier;
                            outputPercentOff.OperatorCode = "Equal";
                            outputPercentOff.Note = firstItem.Note;
                            outputPercentOff.MaxValue = firstItem.MaxValue ?? decimal.Zero;
                            outputPercentOff.OutputReplaces = outputReplace.Select(x =>
                            {
                                var outputPercentOffReplace = new CreateOutputReplaceDto();
                                outputPercentOffReplace.ItemCode = x.ItemCode;
                                outputPercentOffReplace.ItemName = getDetailProductPIM.Where(c => c.Code == x.ItemCode?.ToUpper().Trim()).FirstOrDefault()?.Name;
                                outputPercentOffReplace.Quantity = x.Quantity.Value;
                                outputPercentOffReplace.WarehouseName = x.WarehouseName;
                                outputPercentOffReplace.WarehouseCode = getListWarehouse.Where(c => c.Name == x.WarehouseName).FirstOrDefault()?.Code;
                                var checkUnit = getDetailProductPIM.Where(c => c.Code == x.ItemCode).FirstOrDefault();
                                if (checkUnit.Measures.Any(c => c.MeasureUnitName == x.UnitName))
                                {
                                    outputPercentOffReplace.UnitCode = checkUnit.Measures.Where(c => c.MeasureUnitName == x.UnitName).FirstOrDefault()?.MeasureUnitId;
                                    outputPercentOffReplace.UnitName = checkUnit.Measures.Where(c => c.MeasureUnitName == x.UnitName).FirstOrDefault()?.MeasureUnitName;
                                }
                                if (x.UnitName == null)
                                {
                                    outputPercentOffReplace.UnitCode = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitId;
                                    outputPercentOffReplace.UnitName = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitName;
                                }
                                if (x.UnitName == ImportExcelConstants.AllUnit)
                                {
                                    outputPercentOffReplace.UnitCode = 0;
                                    outputPercentOffReplace.UnitName = ImportExcelConstants.AllUnit;
                                }
                                outputPercentOffReplace.LineNumber = x.LineNumber.Value;
                                outputPercentOffReplace.MaxQuantity = x.MaxQuantity ?? 0;
                                outputPercentOffReplace.Discount = x.Discount ?? decimal.Zero;
                                outputPercentOffReplace.QualifierCode = qualifier;
                                outputPercentOffReplace.OperatorCode = "Equal";
                                outputPercentOffReplace.Note = x.Note;
                                outputPercentOffReplace.MaxValue = x.MaxValue ?? decimal.Zero;
                                return outputPercentOffReplace;
                            }).ToList();

                            listOutput.Add(outputPercentOff);
                            break;
                    };

                }

                if (messageError.Any())
                {
                    importExcelDiscount.Errors = messageError.OrderBy(c => c.Line).ToList();
                }
                else
                {
                    importExcelDiscount.ItemInputs = listInput;
                    importExcelDiscount.Outputs = listOutput;
                    if (promotionType == ImportExcelConstants.BuyProductGetDiscount)
                    {
                        var countItemInput = listInput.Count() + listInput.SelectMany(c => c.ItemInputReplaces).Count();
                        var countItemOutput = listInput.Count() + listOutput.SelectMany(c => c.OutputReplaces).Count();
                        if (countItemInput != countItemOutput)
                        {
                            messageError.Add(new MessageErrorDto()
                            {
                                Message = $"Số lượng sản phẩm đầu vào và đầu ra không bằng nhau"
                            });
                            importExcelDiscount.Errors = messageError.OrderBy(c => c.Line).ToList();
                        }
                    }
                }
            }
            return importExcelDiscount;
        }

        //Import scheme mua combo giảm giá ngành, loại, nhãn
        public async Task<ImportExcelDto> ImportExcelDiscountCategoryAsync(IFormFile FormFile, string promotionType)
        {
            var importExcelDiscount = new ImportExcelDto();
            using (var stream = new MemoryStream())
            {
                FormFile.CopyTo(stream);
                stream.Position = 0;
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                using var reader = ExcelReaderFactory.CreateReader(stream);
                var headers = new List<string>();
                var conf = new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = a => new ExcelDataTableConfiguration
                    {
                        UseHeaderRow = true,
                    }
                };

                DataSet dataSet = reader.AsDataSet(conf);
                var table = dataSet.Tables[0];

                foreach (DataColumn dataColumn in table.Columns)
                {
                    if (promotionType == ImportExcelConstants.BuyProductGetDiscount && 
                        dataColumn.ColumnName != "BuyProductGetDiscountNH")
                    {
                        throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"Template không hợp lệ. Vui lòng tải lại");
                    }
                    if (promotionType == ImportExcelConstants.BuyComboGetDiscount &&
                        dataColumn.ColumnName != "BuyComboGetDiscountNH")
                    {
                        throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"Template không hợp lệ. Vui lòng tải lại");
                    }
                    break;
                }


                var listDataDiscountCategory = ReadDiscountCategory(table, promotionType);

                var errorRequired = listDataDiscountCategory.Where(c => c.Messages.Count > 0).ToList();



                //line báo message
                List<MessageErrorDto> messageError = new List<MessageErrorDto>();

                //Check quantity, qualifier
                var mapperLineFirst = ObjectMapper.Map<DiscountCategoryExcelDto, ValidateQuantityOutputDto>(listDataDiscountCategory.FirstOrDefault());
                var mapperRemoveFirst = ObjectMapper.Map<List<DiscountCategoryExcelDto>, List<ValidateQuantityOutputDto>>(listDataDiscountCategory);
                var checkQuantityOutput = ValidateQuantityOutput(mapperLineFirst, mapperRemoveFirst);

                messageError.AddRange(checkQuantityOutput);

                //Add error message
                foreach (var error in errorRequired)
                {
                    foreach (var message in error.Messages)
                    {
                        var errorRequire = new MessageErrorDto();
                        errorRequire.Message = message;
                        errorRequire.Line = error.RowNumber;
                        messageError.Add(errorRequire);
                    }
                }

                messageError.AddRange(ValiadteImportCategory(listDataDiscountCategory));

                if (messageError.Any())
                {
                    importExcelDiscount.Errors = messageError.OrderBy(c => c.Line).ToList();
                    return importExcelDiscount;
                }

                //Lấy list category code
                var categoryCodes = listDataDiscountCategory.Where(x => x.CategoryCode != null).Select(x => x.CategoryCode.Trim()).ToList();

                //Lấy list type code
                var typeCodes = listDataDiscountCategory.Where(x => x.TypeCode != null).Select(x => x.TypeCode.Trim()).ToList();

                var result = new List<PIMManyCategoryDto>();
                if (categoryCodes.Any())
                {
                    var getdata = await GetCategoryAsync(categoryCodes, "ids");
                    result.AddRange(getdata);
                }

                if (typeCodes.Any())
                {
                    var getdata = await GetCategoryAsync(typeCodes, "ids");
                    result.AddRange(getdata);
                }

                var listGroupBy = listDataDiscountCategory.GroupBy(c => c.LineNumber);

                //Lấy tên kho từ database
                
                var getListWarehouse = await _fieldConfigureRepository.GetListAsync(c => c.Group == "Warehouse");

                //Lấy unit từ database

                var getUnit = await _fieldConfigureRepository.GetListAsync(c => c.Group == "Unit");

                var listInput = new List<CreateItemInputDto>();
                //lấy từng itemGroup cho input khi group được từ line number
                foreach (var item in listGroupBy)
                {
                    var firstItem = item.First();
                    var itemReplace = item.Skip(1).ToList();

                    //Gắn vào input và inputReplace
                    var itemInput = new CreateItemInputDto();
                    if (firstItem.CategoryCode != null)
                    {
                        itemInput.CategoryCode = firstItem.CategoryCode;
                        itemInput.CategoryName = result.Where(c => c.CategoryUniqueId == firstItem.CategoryCode.Trim()).FirstOrDefault()?.CategoryName;

                        //Trường hợp category Code không có sản phẩm khi check pên PIM
                        if (itemInput.CategoryName == null)
                        {
                            messageError.Add(new MessageErrorDto()
                            {
                                Line = firstItem.RowNumber,
                                Message = $"Không tồn tại ngành hàng với mã {itemInput.CategoryCode.Trim()}"
                            });
                        }
                        if (firstItem.TypeCode != null)
                        {
                            itemInput.TypeCode = firstItem.TypeCode;
                            itemInput.TypeName = result.Where(c => c.GroupUniqueId == firstItem.TypeCode.Trim()).FirstOrDefault()?.GroupName;

                            //Trường hợp type Code không có sản phẩm khi check pên PIM
                            if (itemInput.TypeName == null)
                            {
                                messageError.Add(new MessageErrorDto()
                                {
                                    Line = firstItem.RowNumber,
                                    Message = $"Không tồn tại loại với mã {itemInput.TypeCode.Trim()}"
                                });
                            }
                        }
                    }
                    itemInput.Quantity = firstItem.Quantity ?? 0;
                    itemInput.WarehouseName = firstItem.WarehouseName;
                    itemInput.WarehouseCode = getListWarehouse.Where(c => c.Name == firstItem.WarehouseName).FirstOrDefault()?.Code;
                    itemInput.UnitCode = Int32.Parse(getUnit.Where(c => c.Name == firstItem.UnitName).FirstOrDefault()?.Code);
                    itemInput.UnitName = firstItem.UnitName;
                    itemInput.LineNumber = firstItem.LineNumber.Value;
                    itemInput.ItemInputReplaces = itemReplace.Select(x =>
                    {
                        var itemInputReplace = new CreateItemInputReplaceDto();
                        if (x.CategoryCode != null)
                        {
                            itemInputReplace.CategoryCode = x.CategoryCode;
                            itemInputReplace.CategoryName = result.Where(c => c.CategoryUniqueId == x.CategoryCode.Trim()).FirstOrDefault()?.CategoryName;

                            //Trường hợp category Code không có sản phẩm khi check pên PIM
                            if (itemInputReplace.CategoryName == null)
                            {
                                messageError.Add(new MessageErrorDto()
                                {
                                    Line = x.RowNumber,
                                    Message = $"Không tồn tại ngành hàng với mã {itemInputReplace.CategoryCode.Trim()}"
                                });
                            }
                            if (x.TypeCode != null)
                            {
                                itemInputReplace.TypeCode = x.TypeCode;
                                itemInputReplace.TypeName = result.Where(c => c.GroupUniqueId == x.TypeCode.Trim()).FirstOrDefault()?.GroupName;
                                //Trường hợp type Code không có sản phẩm khi check pên PIM
                                if (itemInputReplace.TypeName == null)
                                {
                                    messageError.Add(new MessageErrorDto()
                                    {
                                        Line = x.RowNumber,
                                        Message = $"Không tồn tại loại với mã {itemInputReplace.TypeCode.Trim()}"
                                    });
                                }
                            }
                        }
                        itemInputReplace.Quantity = x.Quantity ?? 0;
                        itemInputReplace.WarehouseName = firstItem.WarehouseName;
                        itemInputReplace.WarehouseCode = getListWarehouse.Where(c => c.Name == x.WarehouseName).FirstOrDefault()?.Code;
                        itemInputReplace.UnitCode = Int32.Parse(getUnit.Where(c => c.Name == x.UnitName).FirstOrDefault()?.Code);
                        itemInputReplace.UnitName = x.UnitName;
                        itemInputReplace.LineNumber = firstItem.LineNumber.Value;
                        return itemInputReplace;
                    }).ToList();

                    listInput.Add(itemInput);
                }

                var listOutput = new List<CreateOutputDto>();
                ////Lấy output có loại đầu ra
                var getOutputQualifier = listDataDiscountCategory.Where(c => c.QualifierName != null).GroupBy(c => c.LineNumber);


                //Lấy data qualifier
                var getListQualifier = await _fieldConfigureRepository.GetListAsync(c => c.Group == "Qualifier");
                foreach (var outputQualifier in getOutputQualifier)
                {
                    var firstItem = outputQualifier.First();
                    var outputReplace = outputQualifier.Skip(1).ToList();
                    var qualifier = getListQualifier.Where(c => c.Name == firstItem.QualifierName).FirstOrDefault()?.Code;
                    switch (qualifier)
                    {
                        case "ProductAmountOff":
                            //Gắn vào output và outputReplace
                            var outputAmountOff = new CreateOutputDto();
                            if (firstItem.CategoryCode != null)
                            {
                                outputAmountOff.CategoryCode = firstItem.CategoryCode;
                                outputAmountOff.CategoryName = result.Where(c => c.CategoryUniqueId == firstItem.CategoryCode.Trim()).FirstOrDefault()?.CategoryName;
                                if (firstItem.TypeCode != null)
                                {
                                    outputAmountOff.TypeCode = firstItem.TypeCode;
                                    outputAmountOff.TypeName = result.Where(c => c.GroupUniqueId == firstItem.TypeCode.Trim()).FirstOrDefault()?.GroupName;
                                }
                            }
                            outputAmountOff.Quantity = firstItem.Quantity ?? 0;
                            outputAmountOff.WarehouseName = firstItem.WarehouseName;
                            outputAmountOff.WarehouseCode = getListWarehouse.Where(c => c.Name == firstItem.WarehouseName).FirstOrDefault()?.Code;
                            outputAmountOff.UnitCode = Int32.Parse(getUnit.Where(c => c.Name == firstItem.UnitName).FirstOrDefault()?.Code);
                            outputAmountOff.UnitName = firstItem.UnitName;
                            outputAmountOff.LineNumber = firstItem.LineNumber.Value;
                            outputAmountOff.MaxQuantity = firstItem.MaxQuantity ?? 0;
                            outputAmountOff.Discount = firstItem.Discount ?? 0;
                            outputAmountOff.Note = firstItem.Note;
                            outputAmountOff.QualifierCode = qualifier;
                            outputAmountOff.OperatorCode = "Equal";
                            outputAmountOff.NoteBoom = firstItem.NoteBoom;
                            outputAmountOff.OutputReplaces = outputReplace.Select(x =>
                            {
                                var outputAmountOffReplace = new CreateOutputReplaceDto();
                                if (x.CategoryCode != null)
                                {
                                    outputAmountOffReplace.CategoryCode = x.CategoryCode;
                                    outputAmountOffReplace.CategoryName = result.Where(c => c.CategoryUniqueId == x.CategoryCode.Trim()).FirstOrDefault()?.CategoryName;
                                    if (x.TypeCode != null)
                                    {
                                        outputAmountOffReplace.TypeCode = x.TypeCode;
                                        outputAmountOffReplace.TypeName = result.Where(c => c.GroupUniqueId == x.TypeCode.Trim()).FirstOrDefault()?.GroupName;
                                    }
                                }
                                outputAmountOffReplace.Quantity = x.Quantity ?? 0;
                                outputAmountOffReplace.WarehouseName = x.WarehouseName;
                                outputAmountOffReplace.WarehouseCode = getListWarehouse.Where(c => c.Name == x.WarehouseName).FirstOrDefault().Code;
                                outputAmountOffReplace.UnitCode = Int32.Parse(getUnit.Where(c => c.Name == x.UnitName).FirstOrDefault()?.Code);
                                outputAmountOffReplace.UnitName = x.UnitName;
                                outputAmountOffReplace.LineNumber = x.LineNumber.Value;
                                outputAmountOffReplace.MaxQuantity = x.MaxQuantity ?? 0;
                                outputAmountOffReplace.Discount = x.Discount ?? 0;
                                outputAmountOffReplace.Note = x.Note;
                                outputAmountOffReplace.QualifierCode = qualifier;
                                outputAmountOffReplace.OperatorCode = "Equal";
                                outputAmountOffReplace.NoteBoom = x.NoteBoom;
                                return outputAmountOffReplace;
                            }).ToList();

                            listOutput.Add(outputAmountOff);
                            break;
                        case "ProductFixedPrice":
                            //Gắn vào output và outputReplace
                            var outputFixedPrice = new CreateOutputDto();
                            if (firstItem.CategoryCode != null)
                            {
                                outputFixedPrice.CategoryCode = firstItem.CategoryCode;
                                outputFixedPrice.CategoryName = result.Where(c => c.CategoryUniqueId == firstItem.CategoryCode.Trim()).FirstOrDefault()?.CategoryName;
                                if (firstItem.TypeCode != null)
                                {
                                    outputFixedPrice.TypeCode = firstItem.TypeCode;
                                    outputFixedPrice.TypeName = result.Where(c => c.GroupUniqueId == firstItem.TypeCode.Trim()).FirstOrDefault()?.GroupName;
                                }
                            }
                            outputFixedPrice.Quantity = firstItem.Quantity ?? 0;
                            outputFixedPrice.WarehouseName = firstItem.WarehouseName;
                            outputFixedPrice.WarehouseCode = getListWarehouse.Where(c => c.Name == firstItem.WarehouseName).FirstOrDefault().Code;
                            outputFixedPrice.UnitCode = Int32.Parse(getUnit.Where(c => c.Name == firstItem.UnitName).FirstOrDefault()?.Code);
                            outputFixedPrice.UnitName = firstItem.UnitName;
                            outputFixedPrice.LineNumber = firstItem.LineNumber.Value;
                            outputFixedPrice.MaxQuantity = firstItem.MaxQuantity ?? 0;
                            outputFixedPrice.Discount = firstItem.Discount ?? 0;
                            outputFixedPrice.Note = firstItem.Note;
                            outputFixedPrice.QualifierCode = qualifier;
                            outputFixedPrice.OperatorCode = "Equal";
                            outputFixedPrice.NoteBoom = firstItem.NoteBoom;
                            outputFixedPrice.OutputReplaces = outputReplace.Select(x =>
                            {
                                var outputFixedPriceReplace = new CreateOutputReplaceDto();
                                if (x.CategoryCode != null)
                                {
                                    outputFixedPriceReplace.CategoryCode = x.CategoryCode;
                                    outputFixedPriceReplace.CategoryName = result.Where(c => c.CategoryUniqueId == x.CategoryCode.Trim()).FirstOrDefault()?.CategoryName;
                                    if (x.TypeCode != null)
                                    {
                                        outputFixedPriceReplace.TypeCode = x.TypeCode;
                                        outputFixedPriceReplace.TypeName = result.Where(c => c.GroupUniqueId == x.TypeCode.Trim()).FirstOrDefault()?.GroupName;
                                    }
                                }
                                outputFixedPriceReplace.Quantity = x.Quantity ?? 0;
                                outputFixedPriceReplace.WarehouseName = x.WarehouseName;
                                outputFixedPriceReplace.WarehouseCode = getListWarehouse.Where(c => c.Name == x.WarehouseName).FirstOrDefault().Code;
                                outputFixedPriceReplace.UnitCode = Int32.Parse(getUnit.Where(c => c.Name == x.UnitName).FirstOrDefault()?.Code);
                                outputFixedPriceReplace.UnitName = x.UnitName;
                                outputFixedPriceReplace.LineNumber = x.LineNumber.Value;
                                outputFixedPriceReplace.MaxQuantity = x.MaxQuantity ?? 0;
                                outputFixedPriceReplace.Discount = x.Discount ?? 0;
                                outputFixedPriceReplace.Note = x.Note;
                                outputFixedPriceReplace.QualifierCode = qualifier;
                                outputFixedPriceReplace.OperatorCode = "Equal";
                                outputFixedPriceReplace.NoteBoom = x.NoteBoom;
                                return outputFixedPriceReplace;
                            }).ToList();

                            listOutput.Add(outputFixedPrice);
                            break;
                        case "ProductPercentOff":
                            //Gắn vào output và outputReplace
                            var outputPercentOff = new CreateOutputDto();
                            if (firstItem.CategoryCode != null)
                            {
                                outputPercentOff.CategoryCode = firstItem.CategoryCode;
                                outputPercentOff.CategoryName = result.Where(c => c.CategoryUniqueId == firstItem.CategoryCode.Trim()).FirstOrDefault()?.CategoryName;
                                if (firstItem.TypeCode != null)
                                {
                                    outputPercentOff.TypeCode = firstItem.TypeCode;
                                    outputPercentOff.TypeName = result.Where(c => c.GroupUniqueId == firstItem.TypeCode.Trim()).FirstOrDefault()?.GroupName;
                                }
                            }
                            outputPercentOff.Quantity = firstItem.Quantity ?? 0;
                            outputPercentOff.WarehouseName = firstItem.WarehouseName;
                            outputPercentOff.WarehouseCode = getListWarehouse.Where(c => c.Name == firstItem.WarehouseName).FirstOrDefault().Code;
                            outputPercentOff.UnitCode = Int32.Parse(getUnit.Where(c => c.Name == firstItem.UnitName).FirstOrDefault()?.Code);
                            outputPercentOff.UnitName = firstItem.UnitName;
                            outputPercentOff.LineNumber = firstItem.LineNumber.Value;
                            outputPercentOff.MaxQuantity = firstItem.MaxQuantity ?? 0;
                            outputPercentOff.Discount = firstItem.Discount ?? 0;
                            outputPercentOff.Note = firstItem.Note;
                            outputPercentOff.MaxValue = firstItem.MaxValue ?? 0;
                            outputPercentOff.QualifierCode = qualifier;
                            outputPercentOff.OperatorCode = "Equal";
                            outputPercentOff.NoteBoom = firstItem.NoteBoom;
                            outputPercentOff.OutputReplaces = outputReplace.Select(x =>
                            {
                                var outputPercentOffReplace = new CreateOutputReplaceDto();
                                if (x.CategoryCode != null)
                                {
                                    outputPercentOffReplace.CategoryCode = x.CategoryCode;
                                    outputPercentOffReplace.CategoryName = result.Where(c => c.CategoryUniqueId == x.CategoryCode.Trim()).FirstOrDefault()?.CategoryName;
                                    if (x.TypeCode != null)
                                    {
                                        outputPercentOffReplace.TypeCode = x.TypeCode;
                                        outputPercentOffReplace.TypeName = result.Where(c => c.GroupUniqueId == x.TypeCode.Trim()).FirstOrDefault()?.GroupName;
                                    }
                                }

                                outputPercentOffReplace.Quantity = x.Quantity ?? 0;
                                outputPercentOffReplace.WarehouseName = x.WarehouseName;
                                outputPercentOffReplace.WarehouseCode = getListWarehouse.Where(c => c.Name == x.WarehouseName).FirstOrDefault().Code;
                                outputPercentOffReplace.UnitCode = Int32.Parse(getUnit.Where(c => c.Name == x.UnitName).FirstOrDefault()?.Code);
                                outputPercentOffReplace.UnitName = x.UnitName;
                                outputPercentOffReplace.LineNumber = x.LineNumber.Value;
                                outputPercentOffReplace.MaxQuantity = x.MaxQuantity ?? 0;
                                outputPercentOffReplace.Discount = x.Discount ?? 0;
                                outputPercentOffReplace.Note = x.Note;
                                outputPercentOffReplace.MaxValue = x.MaxValue ?? 0;
                                outputPercentOffReplace.QualifierCode = qualifier;
                                outputPercentOffReplace.OperatorCode = "Equal";
                                outputPercentOffReplace.NoteBoom = x.NoteBoom;
                                return outputPercentOffReplace;
                            }).ToList();

                            listOutput.Add(outputPercentOff);
                            break;
                    };

                }


                if (messageError.Any())
                {
                    importExcelDiscount.Errors = messageError.OrderBy(c => c.Line).ToList();
                }
                else
                {
                    importExcelDiscount.ItemInputs = listInput;
                    importExcelDiscount.Outputs = listOutput;
                    if (promotionType == ImportExcelConstants.BuyProductGetDiscount)
                    {
                        var countItemInput = listInput.Count + listInput.SelectMany(c => c.ItemInputReplaces).Count();
                        var countItemOutput = listInput.Count + listOutput.SelectMany(c => c.OutputReplaces).Count();
                        if (countItemInput != countItemOutput)
                        {
                            messageError.Add(new MessageErrorDto()
                            {
                                Message = $"Số lượng đầu vào và đầu ra không bằng nhau"
                            });
                            importExcelDiscount.Errors = messageError.OrderBy(c => c.Line).ToList();
                        }
                    }
                }
            }

            return importExcelDiscount;
        }


        //Import scheme mua sản phẩm tặng sản phẩm và mua combo tặng sản phẩm (Input)
        public async Task<ImportExcelDto> ImportExcelGetProductInputAsync(IFormFile FormFile, string promotionType)
        {
            var importExcelGetProduct = new ImportExcelDto();
            using (var stream = new MemoryStream())
            {
                FormFile.CopyTo(stream);
                stream.Position = 0;
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                using var reader = ExcelReaderFactory.CreateReader(stream);
                var headers = new List<string>();
                var conf = new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = a => new ExcelDataTableConfiguration
                    {
                        UseHeaderRow = true,
                    }
                };

                DataSet dataSet = reader.AsDataSet(conf);
                var table = dataSet.Tables[0];

                foreach (DataColumn dataColumn in table.Columns)
                {
                    if (promotionType == ImportExcelConstants.BuyComboGetProduct && dataColumn.ColumnName != "BuyComboGetProductSP")
                    {
                        throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"Template không hợp lệ. Vui lòng tải lại");
                    }
                    if (promotionType == ImportExcelConstants.BuyProductGetProduct && dataColumn.ColumnName != "BuyProductGetProductSP")
                    {
                        throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"Template không hợp lệ. Vui lòng tải lại");
                    }
                    if (promotionType == ImportExcelConstants.TotalProductAmount && dataColumn.ColumnName != "TotalProductAmountSP")
                    {
                        throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"Template không hợp lệ. Vui lòng tải lại");
                    }
                    if (promotionType == ImportExcelConstants.BuyProductGetAscendingDiscount && dataColumn.ColumnName != "BuyProductGetAscendingDiscountSP")
                    {
                        throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"Template không hợp lệ. Vui lòng tải lại");
                    }
                    if (promotionType == ImportExcelConstants.UsePaymentMethod && dataColumn.ColumnName != "UsePaymentMethodSP")
                    {
                        throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"Template không hợp lệ. Vui lòng tải lại");
                    }
                    break;
                }

                var listDataGetProduct = ReadGetProduct(table, promotionType);
                var errorRequired = listDataGetProduct.Where(c => c.Messages.Count > 0).ToList();



                //line báo message
                List<MessageErrorDto> messageError = new List<MessageErrorDto>();

                //Check quantity, qualifier
                var mapperLineFirst = ObjectMapper.Map<ProductGetProductInputExcelDto, ValidateQuantityOutputDto>(listDataGetProduct.FirstOrDefault());
                var mapperRemoveFirst = ObjectMapper.Map<List<ProductGetProductInputExcelDto>, List<ValidateQuantityOutputDto>>(listDataGetProduct);
                var checkQuantityOutput = ValidateQuantityOutput(mapperLineFirst, mapperRemoveFirst);

                messageError.AddRange(checkQuantityOutput);

                //Add error message
                foreach (var error in errorRequired)
                {
                    foreach (var message in error.Messages)
                    {
                        var errorRequire = new MessageErrorDto();
                        errorRequire.Message = message;
                        errorRequire.Line = error.RowNumber;
                        messageError.Add(errorRequire);
                    }
                }



                var duplicateItemCode = listDataGetProduct.GroupBy(c => c.ItemCode);
                foreach (var dup in duplicateItemCode.Where(c => c.Count() > 1))
                {
                    foreach (var item in dup)
                    {
                        messageError.Add(new MessageErrorDto()
                        {
                            Line = item.RowNumber,
                            Message = $"Mã {item.ItemCode} bị trùng trong danh sách"
                        });
                    }
                }

                var listGroupBy = listDataGetProduct.GroupBy(c => c.LineNumber);
                var listInput = new List<CreateItemInputDto>();

                //Lấy tên sản phẩm từ PIM-
                var listItemCode = listDataGetProduct.Select(c => c.ItemCode).ToList();

                if (!listItemCode.Any())
                {
                    throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                             _stringLocalize[ExceptionCode.NotFound], $"Không tồn tại dữ liệu trong file");
                }
                int batchSize = 300;
                int pageNumber = 0;
                var getDetailProductPIM = new List<ItemDetails>();

                while (true)
                {

                    if (listItemCode.Count() > 300)
                    {
                        var batch = listItemCode.Skip(pageNumber * batchSize).Take(batchSize).ToList();

                        var listDetail = await ListProductDetailAsync(batch);

                        getDetailProductPIM.AddRange(listDetail);
                        pageNumber++;

                        if (batch.Count() < batchSize)
                        {
                            break;
                        }
                    }
                    else
                    {
                        var listDetail = await ListProductDetailAsync(listItemCode);
                        getDetailProductPIM.AddRange(listDetail);
                        break;
                    }
                }

                //Lấy tên kho từ database
                var getListWarehouse = await _fieldConfigureRepository.GetListAsync(c => c.Group == "Warehouse");
                //lấy từng itemGroup cho input khi group được từ line number
                foreach (var item in listGroupBy)
                {
                    var firstItem = item.First();
                    var itemReplace = item.Where(c => c.ItemCode != firstItem.ItemCode).ToList();

                    //Gắn vào input và inputReplace
                    var itemInput = new CreateItemInputDto();
                    itemInput.ItemCode = firstItem.ItemCode;
                    itemInput.ItemName = getDetailProductPIM.Where(c => c.Code == firstItem.ItemCode?.ToUpper().Trim()).FirstOrDefault()?.Name;

                    //Trường hợp item Code không có sản phẩm khi check pên PIM
                    if (itemInput.ItemName == null)
                    {
                        messageError.Add(new MessageErrorDto()
                        {
                            Line = firstItem.RowNumber,
                            Message = $"Không tồn tại sản phẩm với mã {itemInput.ItemCode}"
                        });
                    }
                    itemInput.Quantity = firstItem.Quantity ?? 0;
                    itemInput.WarehouseName = firstItem.WarehouseName;
                    itemInput.WarehouseCode = getListWarehouse.Where(c => c.Name == firstItem.WarehouseName).FirstOrDefault()?.Code;
                    var checkUnit = getDetailProductPIM.Where(c => c.Code == firstItem.ItemCode).FirstOrDefault();
                    if(firstItem.UnitName != null && 
                        !checkUnit.Measures.Any(c => c.MeasureUnitName == firstItem.UnitName) && 
                            firstItem.UnitName != ImportExcelConstants.AllUnit)
                    {
                        messageError.Add(new MessageErrorDto()
                        {
                            Line = firstItem.RowNumber,
                            Message = $"Không tồn tại đơn vị {firstItem.UnitName} với mã {firstItem.ItemCode}"
                        });
                    }
                    if(checkUnit.Measures.Any(c => c.MeasureUnitName == firstItem.UnitName))
                    {
                        itemInput.UnitCode = checkUnit.Measures.Where(c => c.MeasureUnitName == firstItem.UnitName).FirstOrDefault()?.MeasureUnitId;
                        itemInput.UnitName = checkUnit.Measures.Where(c => c.MeasureUnitName == firstItem.UnitName).FirstOrDefault()?.MeasureUnitName;
                    }
                    if (firstItem.UnitName == null)
                    {
                        itemInput.UnitCode = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitId;
                        itemInput.UnitName = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitName;
                    }
                    if(firstItem.UnitName == ImportExcelConstants.AllUnit)
                    {
                        itemInput.UnitCode = 0;
                        itemInput.UnitName = ImportExcelConstants.AllUnit;
                    }
                    itemInput.LineNumber = firstItem.LineNumber;
                    itemInput.ItemInputReplaces = itemReplace.Select(x =>
                    {
                        var itemInputReplace = new CreateItemInputReplaceDto();
                        itemInputReplace.ItemCode = x.ItemCode;
                        itemInputReplace.ItemName = getDetailProductPIM.Where(c => c.Code == x.ItemCode?.ToUpper().Trim()).FirstOrDefault()?.Name;
                        if (itemInputReplace.ItemName == null)
                        {
                            messageError.Add(new MessageErrorDto()
                            {
                                Line = x.RowNumber,
                                Message = $"Không tồn tại sản phẩm với mã {x.ItemCode}"
                            });
                        }
                        itemInputReplace.Quantity = x.Quantity ?? 0;
                        itemInputReplace.WarehouseName = firstItem.WarehouseName;
                        itemInputReplace.WarehouseCode = getListWarehouse.Where(c => c.Name == x.WarehouseName).FirstOrDefault()?.Code;
                        var checkUnit = getDetailProductPIM.Where(c => c.Code == x.ItemCode).FirstOrDefault();
                        if (x.UnitName != null &&
                        !checkUnit.Measures.Any(c => c.MeasureUnitName == x.UnitName) &&
                            x.UnitName != ImportExcelConstants.AllUnit)
                        {
                            messageError.Add(new MessageErrorDto()
                            {
                                Line = x.RowNumber,
                                Message = $"Không tồn tại đơn vị {x.UnitName} với mã {x.ItemCode}"
                            });
                        }
                        if(checkUnit.Measures.Any(c => c.MeasureUnitName == x.UnitName))
                        {
                            itemInputReplace.UnitCode = checkUnit.Measures.Where(c => c.MeasureUnitName == x.UnitName).FirstOrDefault()?.MeasureUnitId;
                            itemInputReplace.UnitName = checkUnit.Measures.Where(c => c.MeasureUnitName == x.UnitName).FirstOrDefault()?.MeasureUnitName;
                        }
                        if (x.UnitName == null)
                        {
                            itemInputReplace.UnitCode = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitId;
                            itemInputReplace.UnitName = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitName;
                        }
                        if (x.UnitName == ImportExcelConstants.AllUnit)
                        {
                            itemInputReplace.UnitCode = 0;
                            itemInputReplace.UnitName = ImportExcelConstants.AllUnit;
                        }
                        itemInputReplace.LineNumber = firstItem.LineNumber;
                        return itemInputReplace;
                    }).ToList();
                    listInput.Add(itemInput);
                }

                if (messageError.Any())
                {
                    importExcelGetProduct.Errors = messageError.OrderBy(c => c.Line).ToList();
                }
                else
                {
                    importExcelGetProduct.ItemInputs = listInput;
                }
            }
            return importExcelGetProduct;
        }

        //Import scheme mua ngành hàng tặng sản phẩm (Input)
        public async Task<ImportExcelDto> ImportExcelCategoryGetProductInputAsync(IFormFile FormFile, string promotionType)
        {
            var importExcelGetProduct = new ImportExcelDto();
            using (var stream = new MemoryStream())
            {
                FormFile.CopyTo(stream);
                stream.Position = 0;
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                using var reader = ExcelReaderFactory.CreateReader(stream);
                var headers = new List<string>();
                var conf = new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = a => new ExcelDataTableConfiguration
                    {
                        UseHeaderRow = true,
                    }
                };

                DataSet dataSet = reader.AsDataSet(conf);
                var table = dataSet.Tables[0];

                foreach (DataColumn dataColumn in table.Columns)
                {
                    if (promotionType == ImportExcelConstants.BuyComboGetProduct && dataColumn.ColumnName != "BuyComboGetProductNH")
                    {
                        throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"Template không hợp lệ. Vui lòng tải lại");
                    }
                    if (promotionType == ImportExcelConstants.BuyProductGetProduct && dataColumn.ColumnName != "BuyProductGetProductNH")
                    {
                        throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"Template không hợp lệ. Vui lòng tải lại");
                    }
                    if (promotionType == ImportExcelConstants.TotalProductAmount && dataColumn.ColumnName != "TotalProductAmountNH")
                    {
                        throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"Template không hợp lệ. Vui lòng tải lại");
                    }
                    if (promotionType == ImportExcelConstants.BuyProductGetAscendingDiscount && dataColumn.ColumnName != "BuyProductGetAscendingDiscountNH")
                    {
                        throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"Template không hợp lệ. Vui lòng tải lại");
                    }
                    if (promotionType == ImportExcelConstants.UsePaymentMethod && dataColumn.ColumnName != "UsePaymentMethodNH")
                    {
                        throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"Template không hợp lệ. Vui lòng tải lại");
                    }
                    break;
                }

                var listDataGetProduct = ReadCategoryGetProduct(table, promotionType);

                var errorRequired = listDataGetProduct.Where(c => c.Messages.Count > 0).ToList();



                //line báo message
                List<MessageErrorDto> messageError = new List<MessageErrorDto>();

                //Check quantity, qualifier
                var mapperLineFirst = ObjectMapper.Map<CategoryGetProductInputExcelDto, ValidateQuantityOutputDto>(listDataGetProduct.FirstOrDefault());
                var mapperRemoveFirst = ObjectMapper.Map<List<CategoryGetProductInputExcelDto>, List<ValidateQuantityOutputDto>>(listDataGetProduct);
                var checkQuantityOutput = ValidateQuantityOutput(mapperLineFirst, mapperRemoveFirst);

                messageError.AddRange(checkQuantityOutput);

                //Add error message
                foreach (var error in errorRequired)
                {
                    foreach (var message in error.Messages)
                    {
                        var errorRequire = new MessageErrorDto();
                        errorRequire.Message = message;
                        errorRequire.Line = error.RowNumber;
                        messageError.Add(errorRequire);
                    }
                }
                messageError.AddRange(ValiadteImportCategory(ObjectMapper.Map<List<CategoryGetProductInputExcelDto>, List<DiscountCategoryExcelDto>>(listDataGetProduct)));

                if (messageError.Any())
                {
                    importExcelGetProduct.Errors = messageError.OrderBy(c => c.Line).ToList();
                    return importExcelGetProduct;
                }

                //Lấy list category code
                var categoryCodes = listDataGetProduct.Where(x => x.CategoryCode != null).Select(x => x.CategoryCode.Trim()).ToList();

                //Lấy list type code
                var typeCodes = listDataGetProduct.Where(x => x.TypeCode != null).Select(x => x.TypeCode.Trim()).ToList();

                var result = new List<PIMManyCategoryDto>();
                if (categoryCodes.Any())
                {
                    var getdata = await GetCategoryAsync(categoryCodes, "ids");
                    result.AddRange(getdata);
                }

                if (typeCodes.Any())
                {
                    var getdata = await GetCategoryAsync(typeCodes, "ids");
                    result.AddRange(getdata);
                }

                var listGroupBy = listDataGetProduct.GroupBy(c => c.LineNumber);

                //Lấy tên kho từ database
                var getListWarehouse = await _fieldConfigureRepository.GetListAsync(c => c.Group == "Warehouse");

                //Lấy Unit từ database
                var getListUnit = await _fieldConfigureRepository.GetListAsync(c => c.Group == "Unit");

                var listInput = new List<CreateItemInputDto>();
                //lấy từng itemGroup cho input khi group được từ line number
                foreach (var item in listGroupBy)
                {
                    var firstItem = item.First();
                    var itemReplace = item.Skip(1).ToList();

                    //Gắn vào input và inputReplace
                    var itemInput = new CreateItemInputDto();
                    if (firstItem.CategoryCode != null)
                    {
                        itemInput.CategoryCode = firstItem.CategoryCode;
                        itemInput.CategoryName = result.Where(c => c.CategoryUniqueId == firstItem.CategoryCode.ToUpper().Trim()).FirstOrDefault()?.CategoryName;

                        //Trường hợp category Code không có sản phẩm khi check pên PIM
                        if (itemInput.CategoryName == null)
                        {
                            messageError.Add(new MessageErrorDto()
                            {
                                Line = firstItem.RowNumber,
                                Message = $"Không tồn tại ngành hàng với mã {itemInput.CategoryCode.Trim()}"
                            });
                        }
                        if (firstItem.TypeCode != null)
                        {
                            itemInput.TypeCode = firstItem.TypeCode;
                            itemInput.TypeName = result.Where(c => c.GroupUniqueId == firstItem.TypeCode.ToUpper().Trim()).FirstOrDefault()?.GroupName;

                            //Trường hợp type Code không có sản phẩm khi check pên PIM
                            if (itemInput.TypeName == null)
                            {
                                messageError.Add(new MessageErrorDto()
                                {
                                    Line = firstItem.RowNumber,
                                    Message = $"Không tồn tại loại với mã {itemInput.TypeCode.Trim()}"
                                });
                            }
                        }
                    }
                    itemInput.Quantity = firstItem.Quantity ?? 0;
                    itemInput.WarehouseName = firstItem.WarehouseName;
                    itemInput.WarehouseCode = getListWarehouse.Where(c => c.Name == firstItem.WarehouseName).FirstOrDefault()?.Code;
                    itemInput.UnitCode = Int32.Parse(getListUnit.Where(c => c.Name == firstItem.UnitName).FirstOrDefault()?.Code);
                    itemInput.UnitName = firstItem.UnitName;
                    itemInput.LineNumber = firstItem.LineNumber;
                    itemInput.ItemInputReplaces = itemReplace.Select(x =>
                    {
                        var itemInputReplace = new CreateItemInputReplaceDto();
                        if (x.CategoryCode != null)
                        {
                            itemInputReplace.CategoryCode = x.CategoryCode;
                            itemInputReplace.CategoryName = result.Where(c => c.CategoryUniqueId == x.CategoryCode.ToUpper().Trim()).FirstOrDefault()?.CategoryName;

                            //Trường hợp category Code không có sản phẩm khi check pên PIM
                            if (itemInputReplace.CategoryName == null)
                            {
                                messageError.Add(new MessageErrorDto()
                                {
                                    Line = x.RowNumber,
                                    Message = $"Không tồn tại ngành hàng với mã {itemInputReplace.CategoryCode.Trim()}"
                                });
                            }
                            if (x.TypeCode != null)
                            {
                                itemInputReplace.TypeCode = x.TypeCode;
                                itemInputReplace.TypeName = result.Where(c => c.GroupUniqueId == x.TypeCode.ToUpper().Trim()).FirstOrDefault()?.GroupName;
                                //Trường hợp type Code không có sản phẩm khi check pên PIM
                                if (itemInputReplace.TypeName == null)
                                {
                                    messageError.Add(new MessageErrorDto()
                                    {
                                        Line = x.RowNumber,
                                        Message = $"Không tồn tại loại với mã {itemInputReplace.TypeCode.Trim()}"
                                    });
                                }
                            }
                        }

                        itemInputReplace.Quantity = x.Quantity ?? 0;
                        itemInputReplace.WarehouseName = firstItem.WarehouseName;
                        itemInputReplace.WarehouseCode = getListWarehouse.Where(c => c.Name == x.WarehouseName).FirstOrDefault()?.Code;
                        itemInputReplace.UnitCode = Int32.Parse(getListUnit.Where(c => c.Name == firstItem.UnitName).FirstOrDefault()?.Code);
                        itemInputReplace.UnitName = firstItem.UnitName;
                        itemInputReplace.LineNumber = firstItem.LineNumber;
                        return itemInputReplace;
                    }).ToList();

                    listInput.Add(itemInput);
                }

                if (messageError.Any())
                {
                    importExcelGetProduct.Errors = messageError.OrderBy(c => c.Line).ToList();
                }
                else
                {
                    importExcelGetProduct.ItemInputs = listInput;
                }
            }
            return importExcelGetProduct;
        }

        //Import scheme mua sản phẩm tặng sản phẩm và mua combo tặng sản phẩm(Output)
        public async Task<ImportExcelDto> ImportExcelGetProductOutputAsync(IFormFile FormFile, string promotionType, bool isInput)
        {
            var importExcelGetProduct = new ImportExcelDto();
            using (var stream = new MemoryStream())
            {
                FormFile.CopyTo(stream);
                stream.Position = 0;
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                using var reader = ExcelReaderFactory.CreateReader(stream);
                var headers = new List<string>();
                var conf = new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = a => new ExcelDataTableConfiguration
                    {
                        UseHeaderRow = true,
                    }
                };

                DataSet dataSet = reader.AsDataSet(conf);
                var table = dataSet.Tables[0];
                var listDataGetProduct = new List<ProductGetProductOutputExcelDto>();

                foreach (DataColumn dataColumn in table.Columns)
                {
                    if (dataColumn.ColumnName != "OutputProductCommon")
                    {
                        throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"Không đúng template import đầu ra sản phẩm");
                    }
                    break;
                }

                int rowNumber = 3;
                //Lấy data từ file excel
                foreach (DataRow dataRow in table.AsEnumerable().Skip(1))
                {
                    var dataGetProduct = new ProductGetProductOutputExcelDto()
                    {
                        LineNumber = (promotionType == "BuyProductGetProduct") && isInput ? 1 : Convert.ToInt32(dataRow.ItemArray[0]),
                        ItemCode = dataRow.ItemArray[1].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[1].ToString(),
                        Quantity = dataRow.ItemArray[2].ToString().IsNullOrEmpty() ? 0 : Convert.ToInt32(dataRow.ItemArray[2]),
                        MaxQuantity = dataRow.ItemArray[3].ToString().IsNullOrEmpty() ? 0 : Convert.ToInt32(dataRow.ItemArray[3]),
                        UnitName = dataRow.ItemArray[4].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[4].ToString(),
                        WarehouseName = dataRow.ItemArray[5].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[5].ToString(),
                        RowNumber = rowNumber
                    };
                    dataGetProduct.Validate();
                    listDataGetProduct.Add(dataGetProduct);
                    rowNumber++;
                }
                var errorRequired = listDataGetProduct.Where(c => c.Messages.Count > 0).ToList();

                //line báo message
                List<MessageErrorDto> messageError = new List<MessageErrorDto>();

                //Add error message
                foreach (var error in errorRequired)
                {
                    foreach (var message in error.Messages)
                    {
                        var errorRequire = new MessageErrorDto();
                        errorRequire.Message = message;
                        errorRequire.Line = error.RowNumber;
                        messageError.Add(errorRequire);
                    }
                }

                var duplicateItemCode = listDataGetProduct.GroupBy(c => c.ItemCode);
                foreach (var dup in duplicateItemCode.Where(c => c.Count() > 1))
                {
                    foreach (var item in dup)
                    {
                        messageError.Add(new MessageErrorDto()
                        {
                            Line = item.RowNumber,
                            Message = $"Mã {item.ItemCode} bị trùng trong danh sách"
                        });
                    }
                }

                //Check SL tặng tối đa phải bé hơn SL tặng 
                var checkMaximumGiftProduct = listDataGetProduct.Where(c => c.Quantity < c.MaxQuantity).ToList();
                foreach (var checkMax in checkMaximumGiftProduct)
                {
                    messageError.Add(new MessageErrorDto()
                    {
                        Line = checkMax.RowNumber,
                        Message = $"Số lượng tặng tối đa {checkMax.MaxQuantity} lớn hơn Số lượng tặng {checkMax.Quantity}"
                    });
                }

                var listOutput = new List<CreateOutputDto>();
                var listGroupBy = listDataGetProduct.GroupBy(c => c.LineNumber);

                //Lấy tên sản phẩm từ PIM-
                var listItemCode = listDataGetProduct.Select(c => c.ItemCode.ToUpper()).ToList();

                if (!listItemCode.Any())
                {
                    throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                             _stringLocalize[ExceptionCode.NotFound], $"Không tồn tại dữ liệu trong file");
                }
                int batchSize = 300;
                int pageNumber = 0;
                var getDetailProductPIM = new List<ItemDetails>();

                while (true)
                {

                    if (listItemCode.Count() > 300)
                    {
                        var batch = listItemCode.Skip(pageNumber * batchSize).Take(batchSize).ToList();

                        var listDetail = await ListProductDetailAsync(batch);

                        getDetailProductPIM.AddRange(listDetail);
                        pageNumber++;

                        if (batch.Count() < batchSize)
                        {
                            break;
                        }
                    }
                    else
                    {
                        var listDetail = await ListProductDetailAsync(listItemCode);
                        getDetailProductPIM.AddRange(listDetail);
                        break;
                    }
                }

                //Lấy tên kho từ database
                var getListWarehouse = await _fieldConfigureRepository.GetListAsync(c => c.Group == "Warehouse");

                foreach (var dataOutput in listGroupBy)
                {
                    var firstItem = dataOutput.First();
                    var outputReplace = dataOutput.Skip(1).ToList();
                    //Gắn vào output và outputReplace
                    var output = new CreateOutputDto();
                    output.ItemCode = firstItem.ItemCode;
                    output.ItemName = getDetailProductPIM.Where(c => c.Code == firstItem.ItemCode?.ToUpper().Trim()).FirstOrDefault()?.Name;

                    //Trường hợp item Code không có sản phẩm khi check pên PIM
                    if (output.ItemName == null)
                    {
                        messageError.Add(new MessageErrorDto()
                        {
                            Line = firstItem.RowNumber,
                            Message = $"Không tồn tại sản phẩm với mã {output.ItemCode}"
                        });
                    }
                    output.Quantity = firstItem.Quantity ?? 0;
                    output.WarehouseName = firstItem.WarehouseName;
                    output.WarehouseCode = getListWarehouse.Where(c => c.Name == firstItem.WarehouseName).FirstOrDefault()?.Code;
                    var checkUnit = getDetailProductPIM.Where(c => c.Code == firstItem.ItemCode).FirstOrDefault();
                    if (firstItem.UnitName != null &&
                        !checkUnit.Measures.Any(c => c.MeasureUnitName == firstItem.UnitName) &&
                            firstItem.UnitName != ImportExcelConstants.AllUnit)
                    {
                        messageError.Add(new MessageErrorDto()
                        {
                            Line = firstItem.RowNumber,
                            Message = $"Không tồn tại đơn vị {firstItem.UnitName} với mã {firstItem.ItemCode}"
                        });
                    }
                    else
                    {
                        output.UnitCode = checkUnit.Measures.Where(c => c.MeasureUnitName == firstItem.UnitName).FirstOrDefault()?.MeasureUnitId;
                        output.UnitName = checkUnit.Measures.Where(c => c.MeasureUnitName == firstItem.UnitName).FirstOrDefault()?.MeasureUnitName;
                    }
                    if (firstItem.UnitName == null)
                    {
                        output.UnitCode = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitId;
                        output.UnitName = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitName;
                    }
                    if (firstItem.UnitName == ImportExcelConstants.AllUnit)
                    {
                        output.UnitCode = 0;
                        output.UnitName = ImportExcelConstants.AllUnit;
                    }
                    output.LineNumber = firstItem.LineNumber.Value;
                    output.QualifierCode = "GetItemCode";
                    output.MaxQuantity = firstItem.MaxQuantity ?? 0;
                    output.OutputReplaces = outputReplace.Select(x =>
                    {
                        var outputReplace = new CreateOutputReplaceDto();
                        outputReplace.ItemCode = x.ItemCode;
                        outputReplace.ItemName = getDetailProductPIM.Where(c => c.Code == x.ItemCode?.ToUpper().Trim()).FirstOrDefault()?.Name;
                        if (outputReplace.ItemName == null)
                        {
                            messageError.Add(new MessageErrorDto()
                            {
                                Line = x.RowNumber,
                                Message = $"Không tồn tại sản phẩm với mã {x.ItemCode}"
                            });
                        }
                        outputReplace.Quantity = x.Quantity ?? 0;
                        outputReplace.WarehouseName = firstItem.WarehouseName;
                        outputReplace.WarehouseCode = getListWarehouse.Where(c => c.Name == x.WarehouseName).FirstOrDefault()?.Code;
                        var checkUnit = getDetailProductPIM.Where(c => c.Code == x.ItemCode).FirstOrDefault();
                        if (x.UnitName != null &&
                        !checkUnit.Measures.Any(c => c.MeasureUnitName == x.UnitName) &&
                            x.UnitName != ImportExcelConstants.AllUnit)
                        {
                            messageError.Add(new MessageErrorDto()
                            {
                                Line = x.RowNumber,
                                Message = $"Không tồn tại đơn vị {x.UnitName} với mã {x.ItemCode}"
                            });
                        }
                        else
                        {
                            outputReplace.UnitCode = checkUnit.Measures.Where(c => c.MeasureUnitName == x.UnitName).FirstOrDefault()?.MeasureUnitId;
                            outputReplace.UnitName = checkUnit.Measures.Where(c => c.MeasureUnitName == x.UnitName).FirstOrDefault()?.MeasureUnitName;
                        }
                        if (x.UnitName == null)
                        {
                            outputReplace.UnitCode = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitId;
                            outputReplace.UnitName = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitName;
                        }
                        if (x.UnitName == ImportExcelConstants.AllUnit)
                        {
                            outputReplace.UnitCode = 0;
                            outputReplace.UnitName = ImportExcelConstants.AllUnit;
                        }
                        outputReplace.LineNumber = firstItem.LineNumber.Value;
                        outputReplace.QualifierCode = "GetItemCode";
                        outputReplace.MaxQuantity = x.MaxQuantity ?? 0;
                        return outputReplace;
                    }).ToList();
                    listOutput.Add(output);
                }
                if (messageError.Any())
                {
                    importExcelGetProduct.Errors = messageError.OrderBy(c => c.Line).ToList();
                }
                else
                {
                    importExcelGetProduct.Outputs = listOutput;
                }
            }
            return importExcelGetProduct;
        }

        //Import scheme mua ngành hàng tặng sản phẩm (Output)
        public async Task<ImportExcelDto> ImportExcelCategoryGetProductOutputAsync(IFormFile FormFile, string promotionType)
        {
            var importExcelGetProduct = new ImportExcelDto();
            using (var stream = new MemoryStream())
            {
                FormFile.CopyTo(stream);
                stream.Position = 0;
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                using var reader = ExcelReaderFactory.CreateReader(stream);
                var headers = new List<string>();
                var conf = new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = a => new ExcelDataTableConfiguration
                    {
                        UseHeaderRow = true,
                    }
                };

                DataSet dataSet = reader.AsDataSet(conf);
                var table = dataSet.Tables[0];
                var listDataGetProduct = new List<CategoryGetProductOutputExcelDto>();

                foreach (DataColumn dataColumn in table.Columns)
                {
                    if (dataColumn.ColumnName != "CategoryGetProduct")
                    {
                        throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"Không đúng template combo hoặc sản phẩm tặng sản phẩm");
                    }
                    break;
                }

                int rowNumber = 3;
                //Lấy data từ file excel
                foreach (DataRow dataRow in table.AsEnumerable().Skip(1))
                {
                    var dataGetProduct = new CategoryGetProductOutputExcelDto()
                    {
                        LineNumber = (promotionType == "BuyProductGetProduct") ? 1 : Convert.ToInt32(dataRow.ItemArray[0]),
                        CategoryCode = dataRow.ItemArray[1].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[1].ToString(),
                        TypeCode = dataRow.ItemArray[2].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[2].ToString(),
                        GroupCode = dataRow.ItemArray[3].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[3].ToString(),
                        BrandCode = dataRow.ItemArray[4].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[4].ToString(),
                        ModelCode = dataRow.ItemArray[5].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[5].ToString(),
                        Quantity = dataRow.ItemArray[6].ToString().IsNullOrEmpty() ? null : Convert.ToInt32(dataRow.ItemArray[6]),
                        MaxQuantity = dataRow.ItemArray[7].ToString().IsNullOrEmpty() ? null : Convert.ToInt32(dataRow.ItemArray[7]),
                        WarehouseName = dataRow.ItemArray[8].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[8].ToString(),
                        RowNumber = rowNumber
                    };
                    dataGetProduct.Validate();
                    listDataGetProduct.Add(dataGetProduct);
                    rowNumber++;
                }
                var errorRequired = listDataGetProduct.Where(c => c.Messages.Count > 0).ToList();

                //line báo message
                List<MessageErrorDto> messageError = new List<MessageErrorDto>();

                //Add error message
                foreach (var error in errorRequired)
                {
                    foreach (var message in error.Messages)
                    {
                        var errorRequire = new MessageErrorDto();
                        errorRequire.Message = message;
                        errorRequire.Line = error.RowNumber;
                        messageError.Add(errorRequire);
                    }
                }

                var listOutput = new List<CreateOutputDto>();
                //Lấy list category code
                var categoryCodes = listDataGetProduct.Where(x => x.CategoryCode != null).Select(x => x.CategoryCode).ToList();

                //Lấy list type code
                var typeCodes = listDataGetProduct.Where(x => x.TypeCode != null).Select(x => x.TypeCode).ToList();

                //Lấy list group code
                var groupCodes = listDataGetProduct.Where(x => x.GroupCode != null).Select(x => x.GroupCode).ToList();

                //Lấy list brand code
                var brandCodes = listDataGetProduct.Where(x => x.BrandCode != null).Select(x => x.BrandCode).ToList();

                //Lấy list model code
                var modelCodes = listDataGetProduct.Where(x => x.ModelCode != null).Select(x => x.ModelCode).ToList();

                var queryParams = new Dictionary<string, object>();
                if (categoryCodes.Any())
                {
                    queryParams.Add("categoryIds", categoryCodes.JoinAsString(","));
                }

                if (typeCodes.Any())
                {
                    queryParams.Add("typeIds", typeCodes.JoinAsString(","));
                }

                if (groupCodes.Any())
                {
                    queryParams.Add("groupIds", groupCodes.JoinAsString(","));
                }

                if (modelCodes.Any())
                {
                    queryParams.Add("modelIds", modelCodes.JoinAsString(","));
                }

                if (brandCodes.Any())
                {
                    queryParams.Add("brandIds", brandCodes.JoinAsString(","));
                }

                var result = await _iApiClientService.FetchAsync<List<PIMManyCategoryDto>>(
                    _remoteServicesOption.Value.PIMAPI.BaseUrl,
                    "/api/v1/promotion/categories/many",
                    "GET",
                    queryParams: queryParams
                );

                var listGroupBy = listDataGetProduct.GroupBy(c => c.LineNumber);

                //Lấy tên kho từ database
                var getListWarehouse = await _fieldConfigureRepository.GetListAsync(c => c.Group == "Warehouse");

                //foreach (var dataOutput in listGroupBy)
                //{
                //    var firstItem = dataOutput.First();
                //    var outputReplace = dataOutput.Skip(1).ToList();
                //    //Gắn vào output và outputReplace
                //    //Gắn vào output và outputReplace
                //    var output = new CreateOutputDto();
                //    if (firstItem.CategoryCode != null)
                //    {
                //        output.CategoryCode = firstItem.CategoryCode;
                //        output.CategoryName = result.Where(c => c.UniqueId == firstItem.CategoryCode).FirstOrDefault()?.Name;
                //        if (firstItem.TypeCode != null)
                //        {
                //            output.TypeCode = firstItem.TypeCode;
                //            output.TypeName = result.Where(c => c.ParentUniqueId == firstItem.CategoryCode && c.UniqueId == firstItem.TypeCode).FirstOrDefault()?.Name;
                //            if (firstItem.GroupCode != null)
                //            {
                //                output.GroupCode = firstItem.GroupCode;
                //                output.GroupName = result.Where(c => c.ParentUniqueId == firstItem.TypeCode && c.UniqueId == firstItem.GroupCode).FirstOrDefault()?.Name;
                //                if (firstItem.BrandCode != null)
                //                {
                //                    output.BrandCode = firstItem.BrandCode;
                //                    output.BrandName = result.Where(c => c.ParentUniqueId == firstItem.GroupCode && c.UniqueId == firstItem.BrandCode).FirstOrDefault()?.Name;
                //                    if (firstItem.ModelCode != null)
                //                    {
                //                        output.ModelCode = firstItem.ModelCode;
                //                        output.ModelName = result.Where(c => c.ParentUniqueId == firstItem.BrandCode && c.UniqueId == firstItem.ModelCode).FirstOrDefault()?.Name;
                //                    }
                //                }
                //            }
                //        }
                //    }
                //    output.Quantity = firstItem.Quantity.Value;
                //    output.WarehouseName = firstItem.WarehouseName;
                //    output.WarehouseCode = getListWarehouse.Where(c => c.Name == firstItem.WarehouseName).FirstOrDefault()?.Code;
                //    output.UnitCode = 8;
                //    output.UnitName = "Chiếc";
                //    output.LineNumber = firstItem.LineNumber;
                //    output.MaxQuantity = firstItem.MaxQuantity.Value;
                //    output.QualifierCode = "GetItemCode";
                //    output.OperatorCode = "Equal";
                //    output.OutputReplaces = outputReplace.Select(x =>
                //    {
                //        var outputReplace = new CreateOutputReplaceDto();
                //        if (x.CategoryCode != null)
                //        {
                //            outputReplace.CategoryCode = x.CategoryCode;
                //            outputReplace.CategoryName = result.Where(c => c.UniqueId == x.CategoryCode).FirstOrDefault()?.Name;
                //            if (x.TypeCode != null)
                //            {
                //                outputReplace.TypeCode = x.TypeCode;
                //                outputReplace.TypeName = result.Where(c => c.ParentUniqueId == x.CategoryCode && c.UniqueId == x.TypeCode).FirstOrDefault()?.Name;
                //                if (x.GroupCode != null)
                //                {
                //                    outputReplace.GroupCode = x.GroupCode;
                //                    outputReplace.GroupName = result.Where(c => c.ParentUniqueId == x.TypeCode && c.UniqueId == x.GroupCode).FirstOrDefault()?.Name;
                //                    if (x.BrandCode != null)
                //                    {
                //                        outputReplace.BrandCode = x.BrandCode;
                //                        outputReplace.BrandName = result.Where(c => c.ParentUniqueId == x.GroupCode && c.UniqueId == x.BrandCode).FirstOrDefault()?.Name;
                //                        if (x.ModelCode != null)
                //                        {
                //                            outputReplace.ModelCode = x.ModelCode;
                //                            outputReplace.ModelName = result.Where(c => c.ParentUniqueId == x.BrandCode && c.UniqueId == x.ModelCode).FirstOrDefault()?.Name;
                //                        }
                //                    }
                //                }
                //            }
                //        }
                //        outputReplace.Quantity = x.Quantity.Value;
                //        outputReplace.WarehouseName = x.WarehouseName;
                //        outputReplace.WarehouseCode = getListWarehouse.Where(c => c.Name == x.WarehouseName).FirstOrDefault().Code;
                //        outputReplace.UnitCode = 8;
                //        outputReplace.UnitName = "Chiếc";
                //        outputReplace.LineNumber = x.LineNumber;
                //        outputReplace.MaxQuantity = x.MaxQuantity.Value;
                //        outputReplace.QualifierCode = "GetItemCode";
                //        outputReplace.OperatorCode = "Equal";
                //        return outputReplace;
                //    }).ToList();

                //    listOutput.Add(output);
                //}
                if (messageError.Any())
                {
                    importExcelGetProduct.Errors = messageError.OrderBy(c => c.Line).ToList();
                }
                else
                {
                    importExcelGetProduct.Outputs = listOutput;
                }
            }
            return importExcelGetProduct;
        }

        //Import scheme tổng bill theo đơn hàng (Output)
        public async Task<ImportExcelDto> ImportExcelTotalBillByOrderOutputAsync(IFormFile FormFile, string promotionTye)
        {
            var importExcelTotalBillByOrder = new ImportExcelDto();
            using (var stream = new MemoryStream())
            {
                FormFile.CopyTo(stream);
                stream.Position = 0;
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                using var reader = ExcelReaderFactory.CreateReader(stream);
                var headers = new List<string>();
                var conf = new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = a => new ExcelDataTableConfiguration
                    {
                        UseHeaderRow = true,
                    }
                };

                DataSet dataSet = reader.AsDataSet(conf);
                var table = dataSet.Tables[0];
                var listDataTotalBillByOrder = new List<TotalBillByOrderExcelDto>();

                foreach (DataColumn dataColumn in table.Columns)
                {
                    if (dataColumn.ColumnName != "TotalBillOrderAndProduct")
                    {
                        throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"Không đúng template tổng bill theo đơn hàng hoặc sản phẩm");
                    }
                    break;
                }

                int rowNumber = 3;
                foreach (DataRow dataRow in table.AsEnumerable().Skip(1))
                {
                    var dataGetTotalBill = new TotalBillByOrderExcelDto()
                    {
                        LineNumber = dataRow.ItemArray[0].ToString().IsNullOrEmpty() ? null : Convert.ToInt32(dataRow.ItemArray[0]),
                        ItemCode = dataRow.ItemArray[1].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[1].ToString(),
                        Quantity = dataRow.ItemArray[2].ToString().IsNullOrEmpty() ? 0 : Convert.ToInt32(dataRow.ItemArray[2]),
                        UnitName = dataRow.ItemArray[3].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[3].ToString(),
                        WarehouseName = dataRow.ItemArray[4].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[4].ToString(),
                        RowNumber = rowNumber
                    };
                    dataGetTotalBill.Validate();
                    listDataTotalBillByOrder.Add(dataGetTotalBill);
                    rowNumber++;
                }

                var errorRequired = listDataTotalBillByOrder.Where(c => c.Messages.Count > 0).ToList();

                //line báo message
                List<MessageErrorDto> messageError = new List<MessageErrorDto>();

                //Add error message
                foreach (var error in errorRequired)
                {
                    foreach (var message in error.Messages)
                    {
                        var errorRequire = new MessageErrorDto();
                        errorRequire.Message = message;
                        errorRequire.Line = error.RowNumber;
                        messageError.Add(errorRequire);
                    }
                }

                var duplicateItemCode = listDataTotalBillByOrder.GroupBy(c => c.ItemCode);
                foreach (var dup in duplicateItemCode.Where(c => c.Count() > 1))
                {
                    foreach (var item in dup)
                    {
                        messageError.Add(new MessageErrorDto()
                        {
                            Line = item.RowNumber,
                            Message = $"Mã {item.ItemCode} bị trùng trong danh sách"
                        });
                    }
                }

                //var listGroupBy = listDataTotalBillByOrder.GroupBy(c => c.LineNumber);
                var listOutput = new List<CreateOutputDto>();

                //Lấy tên sản phẩm từ PIM-
                var listItemCode = listDataTotalBillByOrder.Select(c => c.ItemCode.ToUpper()).ToList();
                if (!listItemCode.Any())
                {
                    throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                             _stringLocalize[ExceptionCode.NotFound], $"Không tồn tại dữ liệu trong file");
                }
                int batchSize = 300;
                int pageNumber = 0;
                var getDetailProductPIM = new List<ItemDetails>();

                while (true)
                {

                    if (listItemCode.Count() > 300)
                    {
                        var batch = listItemCode.Skip(pageNumber * batchSize).Take(batchSize).ToList();

                        var listDetail = await ListProductDetailAsync(batch);

                        getDetailProductPIM.AddRange(listDetail);
                        pageNumber++;

                        if (batch.Count() < batchSize)
                        {
                            break;
                        }
                    }
                    else
                    {
                        var listDetail = await ListProductDetailAsync(listItemCode);
                        getDetailProductPIM.AddRange(listDetail);
                        break;
                    }
                }

                //Lấy tên kho từ database
                var getListWarehouse = await _fieldConfigureRepository.GetListAsync(c => c.Group == "Warehouse");

                var listGroupBy = listDataTotalBillByOrder.GroupBy(c => c.LineNumber);

                foreach (var dataOutput in listGroupBy)
                {
                    var firstItem = dataOutput.First();
                    var itemReplace = dataOutput.Skip(1).ToList();
                    //Gắn vào output và outputReplace
                    var output = new CreateOutputDto();
                    output.ItemCode = firstItem.ItemCode;
                    output.ItemName = getDetailProductPIM.Where(c => c.Code == firstItem.ItemCode?.ToUpper().Trim()).FirstOrDefault()?.Name;

                    //Trường hợp item Code không có sản phẩm khi check pên PIM
                    if (output.ItemName == null)
                    {
                        messageError.Add(new MessageErrorDto()
                        {
                            Line = firstItem.RowNumber,
                            Message = $"Không tồn tại sản phẩm với mã {output.ItemCode}"
                        });
                    }
                    output.Quantity = firstItem.Quantity ?? 0;
                    output.WarehouseName = firstItem.WarehouseName;
                    output.WarehouseCode = getListWarehouse.Where(c => c.Name == firstItem.WarehouseName).FirstOrDefault()?.Code;
                    var checkUnit = getDetailProductPIM.Where(c => c.Code == firstItem.ItemCode).FirstOrDefault();
                    if (firstItem.UnitName != null &&
                        !checkUnit.Measures.Any(c => c.MeasureUnitName == firstItem.UnitName) &&
                            firstItem.UnitName != ImportExcelConstants.AllUnit)
                    {
                        messageError.Add(new MessageErrorDto()
                        {
                            Line = firstItem.RowNumber,
                            Message = $"Không tồn tại đơn vị {firstItem.UnitName} với mã {firstItem.ItemCode}"
                        });
                    }
                    else
                    {
                        output.UnitCode = checkUnit.Measures.Where(c => c.MeasureUnitName == firstItem.UnitName).FirstOrDefault()?.MeasureUnitId;
                        output.UnitName = checkUnit.Measures.Where(c => c.MeasureUnitName == firstItem.UnitName).FirstOrDefault()?.MeasureUnitName;
                    }
                    if (firstItem.UnitName == null)
                    {
                        output.UnitCode = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitId;
                        output.UnitName = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitName;
                    }
                    if (firstItem.UnitName == ImportExcelConstants.AllUnit)
                    {
                        output.UnitCode = 0;
                        output.UnitName = ImportExcelConstants.AllUnit;
                    }
                    output.LineNumber = firstItem.LineNumber.Value;
                    output.QualifierCode = "GetItemCode";
                    output.OperatorCode = "Equal";
                    output.OutputReplaces = itemReplace.Select(x =>
                    {
                        var outputReplace = new CreateOutputReplaceDto();
                        outputReplace.ItemCode = x.ItemCode;
                        outputReplace.ItemName = getDetailProductPIM.Where(c => c.Code == x.ItemCode?.ToUpper().Trim()).FirstOrDefault()?.Name;
                        outputReplace.Quantity = x.Quantity ?? 0;
                        outputReplace.WarehouseName = x.WarehouseName;
                        outputReplace.WarehouseCode = getListWarehouse.Where(c => c.Name == x.WarehouseName).FirstOrDefault()?.Code;
                        var checkUnit = getDetailProductPIM.Where(c => c.Code == x.ItemCode).FirstOrDefault();
                        if (x.UnitName != null &&
                        !checkUnit.Measures.Any(c => c.MeasureUnitName == x.UnitName) &&
                            x.UnitName != ImportExcelConstants.AllUnit)
                        {
                            messageError.Add(new MessageErrorDto()
                            {
                                Line = x.RowNumber,
                                Message = $"Không tồn tại đơn vị {x.UnitName} với mã {x.ItemCode}"
                            });
                        }
                        else
                        {
                            outputReplace.UnitCode = checkUnit.Measures.Where(c => c.MeasureUnitName == x.UnitName).FirstOrDefault()?.MeasureUnitId;
                            outputReplace.UnitName = checkUnit.Measures.Where(c => c.MeasureUnitName == x.UnitName).FirstOrDefault()?.MeasureUnitName;
                        }
                        if (x.UnitName == null)
                        {
                            outputReplace.UnitCode = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitId;
                            outputReplace.UnitName = checkUnit.Measures.FirstOrDefault(c => c.Level == checkUnit.Measures.Min(c => c.Level)).MeasureUnitName;
                        }
                        if (x.UnitName == ImportExcelConstants.AllUnit)
                        {
                            outputReplace.UnitCode = 0;
                            outputReplace.UnitName = ImportExcelConstants.AllUnit;
                        }
                        outputReplace.LineNumber = x.LineNumber.Value;
                        outputReplace.QualifierCode = "GetItemCode";
                        outputReplace.OperatorCode = "Equal";
                        return outputReplace;
                    }).ToList();
                    listOutput.Add(output);
                }

                if (messageError.Any())
                {
                    importExcelTotalBillByOrder.Errors = messageError.OrderBy(c => c.Line).ToList();
                }
                else
                {
                    importExcelTotalBillByOrder.Outputs = listOutput;
                }

            }

            return importExcelTotalBillByOrder;
        }

        public async Task<ImportItemExcludesDto> ImportExcelItemExcludesAsync(IFormFile FormFile)
        {
            var itemExcludes = new ImportItemExcludesDto();
            using (var stream = new MemoryStream())
            {
                FormFile.CopyTo(stream);
                stream.Position = 0;
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                using var reader = ExcelReaderFactory.CreateReader(stream);
                var headers = new List<string>();
                var conf = new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = a => new ExcelDataTableConfiguration
                    {
                        UseHeaderRow = true,
                    }
                };

                DataSet dataSet = reader.AsDataSet(conf);
                var table = dataSet.Tables[0];
                var listItemExcludes = new List<ItemInputExcludesExcelDto>();
                foreach (DataColumn dataColumn in table.Columns)
                {
                    if (dataColumn.ColumnName != "ProductExcludes")
                    {
                        throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"Không đúng template sản phẩm loại trừ");
                    }
                    break;
                }

                int rowNumber = 3;
                //Lấy data từ file excel
                foreach (DataRow dataRow in table.AsEnumerable().Skip(1))
                {
                    var dataItemExclude = new ItemInputExcludesExcelDto()
                    {
                        ItemCode = dataRow.ItemArray[0].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[0].ToString(),
                        RowNumber = rowNumber
                    };
                    dataItemExclude.Validate();
                    listItemExcludes.Add(dataItemExclude);
                    rowNumber++;
                }
                var errorRequired = listItemExcludes.Where(c => c.Messages.Count > 0).ToList();

                //line báo message
                List<MessageErrorDto> messageError = new List<MessageErrorDto>();

                //Add error message
                foreach (var error in errorRequired)
                {
                    foreach (var message in error.Messages)
                    {
                        var errorRequire = new MessageErrorDto();
                        errorRequire.Message = message;
                        errorRequire.Line = error.RowNumber;
                        messageError.Add(errorRequire);
                    }
                }

                var duplicateItemCode = listItemExcludes.GroupBy(c => c.ItemCode);
                foreach (var dup in duplicateItemCode.Where(c => c.Count() > 1))
                {
                    foreach (var item in dup)
                    {
                        messageError.Add(new MessageErrorDto()
                        {
                            Line = item.RowNumber,
                            Message = $"Mã {item.ItemCode} bị trùng trong danh sách"
                        });
                    }
                }
                //Lấy tên sản phẩm từ PIM-
                var listItemCode = listItemExcludes.Select(c => c.ItemCode.ToUpper()).ToList();

                if (!listItemCode.Any())
                {
                    throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                             _stringLocalize[ExceptionCode.NotFound], $"Không tồn tại dữ liệu trong file");
                }
                int batchSize = 300;
                int pageNumber = 0;
                var getDetailProductPIM = new List<ItemDetails>();

                while (true)
                {

                    if (listItemCode.Count() > 300)
                    {
                        var batch = listItemCode.Skip(pageNumber * batchSize).Take(batchSize).ToList();

                        var listDetail = await ListProductDetailAsync(batch);

                        getDetailProductPIM.AddRange(listDetail);
                        pageNumber++;

                        if (batch.Count() < batchSize)
                        {
                            break;
                        }
                    }
                    else
                    {
                        var listDetail = await ListProductDetailAsync(listItemCode);
                        getDetailProductPIM.AddRange(listDetail);
                        break;
                    }
                }

                var listExcludes = new List<CreateItemInputExcludeDto>();

                foreach (var itemExclude in listItemExcludes)
                {
                    var createItemExclude = new CreateItemInputExcludeDto();
                    createItemExclude.ItemCode = itemExclude.ItemCode;
                    createItemExclude.ItemName = getDetailProductPIM.Where(c => c.Code == itemExclude.ItemCode?.ToUpper().Trim()).FirstOrDefault()?.Name;

                    //Trường hợp item Code không có sản phẩm khi check pên PIM
                    if (createItemExclude.ItemName == null)
                    {
                        messageError.Add(new MessageErrorDto()
                        {
                            Line = itemExclude.RowNumber,
                            Message = $"Không tồn tại sản phẩm với mã {itemExclude.ItemCode}"
                        });
                    }

                    listExcludes.Add(createItemExclude);
                }

                if (messageError.Any())
                {
                    itemExcludes.Errors = messageError.OrderBy(c => c.Line).ToList();
                }
                else
                {
                    itemExcludes.ItemInputExcludes = listExcludes;
                }
            }
            return itemExcludes;
        }
        public async Task<List<ItemDetails>> ListProductDetailAsync(List<string> sKus)
        {
            var payload = new PimCacheSearchMany()
            {
                sku = sKus
            };

            var result = await _iApiClientService.FetchAsync<PimProductCacheDto>(
                _remoteServicesOption.Value.PIMAPICache.BaseUrl,
                "/promotion/products/list",
                "POST",
                payload: JsonConvert.SerializeObject(payload)
            );

            return ObjectMapper.Map<List<ProductCache>, List<ItemDetails>>(result.Data);
        }

        private async Task<List<PIMManyCategoryDto>> GetCategoryAsync(List<string> list, string param)
        {
            if (list.Any())
            {
                int batchSize = 300;
                int pageNumber = 0;
                var getCategoryPIM = new List<PIMManyCategoryDto>();

                while (true)
                {

                    if (list.Count() > 300)
                    {
                        var batch = list.Skip(pageNumber * batchSize).Take(batchSize).ToList();

                        var queryParam = new Dictionary<string, object>();
                        queryParam.Add(param, batch.Distinct().JoinAsString(","));
                        var result = await _iApiClientService.FetchAsync<GetManyCategoryDto>(
                                    _remoteServicesOption.Value.PIMAPICache.BaseUrl,
                                    "/promotion/categories/many",
                                    "GET",
                                    queryParams: queryParam
                                );

                        getCategoryPIM.AddRange(result.Data);
                        pageNumber++;

                        if (batch.Count() < batchSize)
                        {
                            break;
                        }
                    }
                    else
                    {
                        var queryParam = new Dictionary<string, object>();
                        queryParam.Add(param, list.Distinct().JoinAsString(","));
                        var result = await _iApiClientService.FetchAsync<GetManyCategoryDto>(
                                    _remoteServicesOption.Value.PIMAPICache.BaseUrl,
                                    "/promotion/categories/many",
                                    "GET",
                                    queryParams: queryParam
                                );

                        getCategoryPIM.AddRange(result.Data);
                        break;
                    }
                }
                return getCategoryPIM;
            }

            return new List<PIMManyCategoryDto>();
        }

        public async Task<ImportItemShopDto> ImportShopAsync(List<string> storeType, DateTime fromDate, DateTime toDate, IFormFile formFile)
        {
            var importShop = new ImportItemShopDto();
            using (var stream = new MemoryStream())
            {
                formFile.CopyTo(stream);
                stream.Position = 0;
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                using var reader = ExcelReaderFactory.CreateReader(stream);
                var headers = new List<string>();
                var conf = new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = a => new ExcelDataTableConfiguration
                    {
                        UseHeaderRow = true,
                    }
                };

                DataSet dataSet = reader.AsDataSet(conf);
                var table = dataSet.Tables[0];
                var listItemShop = new List<ShopExcelDto>();
                foreach (DataColumn dataColumn in table.Columns)
                {
                    if (dataColumn.ColumnName != "RegionCondition")
                    {
                        throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"Không đúng template shop");
                    }
                    break;
                }
                int rowNumber = 3;
                //Lấy data từ file excel
                foreach (DataRow dataRow in table.AsEnumerable().Skip(1))
                {
                    var dataItemShop = new ShopExcelDto()
                    {
                        ShopCode = dataRow.ItemArray[0].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[0].ToString(),
                        StartDate = dataRow.ItemArray[2].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[2].ToString(),
                        EndDate = dataRow.ItemArray[3].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[3].ToString(),
                        RowNumber = rowNumber
                    };
                    dataItemShop.Validate();
                    listItemShop.Add(dataItemShop);
                    rowNumber++;
                }

                var errorRequired = listItemShop.Where(c => c.Messages.Count > 0).ToList();

                //line báo message
                List<MessageErrorDto> messageError = new List<MessageErrorDto>();

                //Add error message
                foreach (var error in errorRequired)
                {
                    foreach (var message in error.Messages)
                    {
                        var errorRequire = new MessageErrorDto();
                        errorRequire.Message = message;
                        errorRequire.Line = error.RowNumber;
                        messageError.Add(errorRequire);
                    }
                }

                var duplicateShopCode = listItemShop.GroupBy(c => c.ShopCode);
                foreach (var dup in duplicateShopCode.Where(c => c.Count() > 1))
                {
                    foreach (var item in dup)
                    {
                        messageError.Add(new MessageErrorDto()
                        {
                            Line = item.RowNumber,
                            Message = $"Mã {item.ShopCode} bị trùng trong danh sách"
                        });
                    }
                }


                var shops = await _iApiClientService.FetchAsync<List<ShopAllDto>>(
                _config["RemoteServices:DSMSAPI:BaseUrl"],
                "api/data/nhapthuoc/shop/all",
                payload: string.Empty,
                method: "GET",
                authorization: string.Empty
                );
                var listCreateShop = new List<CreateShopConditionExcelDto>();

                if (storeType.FirstOrDefault() == ImportExcelConstants.FShop)
                {
                    shops = shops.Where(c => c.ShopFeatures != null && c.ShopFeatures.Any(a => a.Code != "2" || a.Code != "3") && c.ShopType.Code == "F").ToList();
                    foreach (var item in listItemShop)
                    {
                        var createShop = new CreateShopConditionExcelDto();
                        createShop.ShopCode = item.ShopCode;
                        createShop.ShopName = shops.Where(c => c.Code == item.ShopCode?.ToUpper().Trim()).FirstOrDefault()?.Name;

                        ValidateImportShop(fromDate, toDate, messageError, item, createShop);

                        listCreateShop.Add(createShop);
                    }
                }
                else if (storeType.FirstOrDefault() == ImportExcelConstants.FStudio)
                {
                    shops = shops.Where(c => c.ShopFeatures != null && c.ShopFeatures.Any(a => a.Code == "2" || a.Code == "3") && c.ShopType.Code == "F").ToList();
                    foreach (var item in listItemShop)
                    {
                        var createShop = new CreateShopConditionExcelDto();
                        createShop.ShopCode = item.ShopCode;
                        createShop.ShopName = shops.Where(c => c.Code == item.ShopCode?.ToUpper().Trim()).FirstOrDefault()?.Name;

                        ValidateImportShop(fromDate, toDate, messageError, item, createShop);

                        listCreateShop.Add(createShop);
                    }
                }
                else if (!storeType.Any())
                {
                    foreach (var item in listItemShop)
                    {
                        var createShop = new CreateShopConditionExcelDto();
                        createShop.ShopCode = item.ShopCode;
                        createShop.ShopName = shops.Where(c => c.Code == item.ShopCode?.ToUpper().Trim()).FirstOrDefault()?.Name;

                        //Trường hợp shop Code không có shop
                        ValidateImportShop(fromDate, toDate, messageError, item, createShop);

                        listCreateShop.Add(createShop);
                    }
                }

                if (messageError.Any())
                {
                    importShop.Errors = messageError.OrderBy(c => c.Line).ToList();

                    var errorMessages = messageError.OrderBy(c => c.Line)
                    .GroupBy(x => x.Line)
                    .Select(x => new MessageErrorDto
                    {
                        Line = x.Key,
                        Messages = x.Select(x => x.Message).ToList()
                    })
                    .ToList();
                    importShop.Errors = errorMessages;
                }
                else
                {
                    importShop.ShopConditions = listCreateShop;
                }

            }
            return importShop;
        }

        private static void ValidateImportShop(DateTime fromDate, DateTime toDate, List<MessageErrorDto> messageError, ShopExcelDto item, CreateShopConditionExcelDto createShop)
        {
            if (createShop.ShopName == null)
            {
                messageError.Add(new MessageErrorDto()
                {
                    Line = item.RowNumber,
                    Message = $"Không tồn tại shop với mã {item.ShopCode} trong hệ thống toàn shop"
                });
            }

            if (item.StartDate == null && item.EndDate == null)
            {
                createShop.FromDate = null;
                createShop.ToDate = null;
            }

            if (item.StartDate != null && fromDate.Date > DateTime.Parse(item.StartDate).Date)
            {
                messageError.Add(new MessageErrorDto()
                {
                    Line = item.RowNumber,
                    Message = $"Ngày bắt đầu {DateTime.Parse(item.StartDate).ToString("dd/MM/yyyy")} không được bé hơn {fromDate.ToString("dd/MM/yyyy")}"
                });
            }
            else if (item.StartDate != null && fromDate.Date <= DateTime.Parse(item.StartDate).Date)
            {
                createShop.FromDate = DateTime.Parse(item.StartDate);
            }
            if (item.EndDate != null && toDate.Date < DateTime.Parse(item.EndDate).Date)
            {
                messageError.Add(new MessageErrorDto()
                {
                    Line = item.RowNumber,
                    Message = $"Ngày kết thúc {DateTime.Parse(item.EndDate).ToString("dd/MM/yyyy")} không được lớn hơn {toDate.ToString("dd/MM/yyyy")}"
                });
            }
            else if (item.EndDate != null && toDate.Date >= DateTime.Parse(item.EndDate).Date)
            {
                createShop.ToDate = DateTime.Parse(item.EndDate);
            }

            if ((item.EndDate != null && item.StartDate != null) && (DateTime.Parse(item.StartDate).Date > DateTime.Parse(item.EndDate).Date))
            {
                messageError.Add(new MessageErrorDto()
                {
                    Line = item.RowNumber,
                    Message = $"Ngày bắt đầu {DateTime.Parse(item.StartDate).ToString("dd/MM/yyyy")} không được lớn hơn ngày kết thúc {DateTime.Parse(item.EndDate).ToString("dd/MM/yyyy")}"
                });
            }
        }

        private List<MessageErrorDto> ValiadteImportCategory(List<DiscountCategoryExcelDto> categoryExcelDtos)
        {
            var messageError = new List<MessageErrorDto>();
            #region Check ngành loại nhóm nhãn
            var groupByModelBrands = categoryExcelDtos.Where(item => !string.IsNullOrEmpty(item.CategoryCode) 
                                                                   && !string.IsNullOrEmpty(item.TypeCode)
                                                                   && !string.IsNullOrEmpty(item.ModelCode)
                                                                   && !string.IsNullOrEmpty(item.BrandCode))
                                                      .GroupBy(c => new { c.CategoryCode, c.TypeCode, c.ModelCode, c.BrandCode })      
                                                      .ToList();
            foreach (var itemModelBrand in groupByModelBrands)
            {
                if(itemModelBrand.Count() > 1)
                {
                    foreach (var item in itemModelBrand)
                    {
                        var listItem = itemModelBrand.ToList();
                        var lineExceptItem = listItem.Remove(item);
                        messageError.Add(new MessageErrorDto()
                        {
                            Line = item.RowNumber,
                            Message = $"Ngành, loại, nhóm, nhãn bị trùng với line " +
                                                $"{listItem.Select(c => c.RowNumber).JoinAsString(",")}"
                        });
                    }
                }
            }
            #endregion

            #region Check ngành duy nhất và các line khác có ngành trùng
            var groupByCategory = categoryExcelDtos.GroupBy(c => new { c.CategoryCode })
                                                      .ToList();

            foreach(var itemCategory in groupByCategory)
            {
                var category = itemCategory.Where(c => c.CategoryCode != null
                                        && c.GroupCode == null
                                        && c.TypeCode == null
                                        && c.ModelCode == null
                                        && c.BrandCode == null).FirstOrDefault();
                if(category != null)
                {
                    var listItem = itemCategory.ToList();
                    var lineExceptItem = listItem.Remove(category);
                    foreach (var item in listItem)
                    {
                        messageError.Add(new MessageErrorDto()
                        {
                            Line = item.RowNumber,
                            Message = $"Đã tồn tại mã ngành {item.CategoryCode} ở line {category.RowNumber}"
                        });
                    }
                }
            }
            #endregion

            #region Check ngành, loại duy nhất

            var groupByCategoryType = categoryExcelDtos.GroupBy(c => new { c.CategoryCode, c.TypeCode })
                                                      .ToList();

            foreach (var itemCategoryType in groupByCategoryType)
            {
                var categoryType = itemCategoryType.Where(c => c.CategoryCode != null
                                        && c.TypeCode != null
                                        && c.GroupCode == null
                                        && c.ModelCode == null
                                        && c.BrandCode == null).FirstOrDefault();
                if (categoryType != null)
                {
                    var listItem = itemCategoryType.ToList();
                    var lineExceptItem = listItem.Remove(categoryType);
                    foreach (var item in listItem)
                    {
                        messageError.Add(new MessageErrorDto()
                        {
                            Line = item.RowNumber,
                            Message = $"Đã tồn tại mã ngành {item.CategoryCode} và mã loại {item.TypeCode} ở line {categoryType.RowNumber}"
                        });
                    }
                }
            }
            #endregion

            #region Check ngành, loại, nhóm duy nhất

            var groupByCategoryTypeGroup = categoryExcelDtos.GroupBy(c => new { c.CategoryCode, c.TypeCode, c.GroupCode })
                                                      .ToList();

            foreach (var itemCategoryTypeGroup in groupByCategoryTypeGroup)
            {
                var categoryTypeGroup = itemCategoryTypeGroup.Where(c => c.CategoryCode != null
                                        && c.TypeCode != null
                                        && c.GroupCode != null
                                        && c.ModelCode == null
                                        && c.BrandCode == null).FirstOrDefault();
                if (categoryTypeGroup != null)
                {
                    var listItem = itemCategoryTypeGroup.ToList();
                    var lineExceptItem = listItem.Remove(categoryTypeGroup);
                    foreach (var item in listItem)
                    {
                        messageError.Add(new MessageErrorDto()
                        {
                            Line = item.RowNumber,
                            Message = $"Đã tồn tại mã ngành {item.CategoryCode} " +
                            $"và mã loại {item.TypeCode} " +
                            $"và mã nhóm {item.GroupCode} ở line {categoryTypeGroup.RowNumber}"
                        });
                    }
                }
            }
            #endregion
            return messageError;

        }


        private List<DiscountCategoryExcelDto> ReadDiscountCategory(DataTable dataTable, string typeDiscount)
        {
            var dataCategoryDiscount = new List<DiscountCategoryExcelDto>();

            //Scheme mua sản phẩm giảm giá sản phẩm
            if(typeDiscount == ImportExcelConstants.BuyProductGetDiscount)
            {
                int rowNumber = 3;
                //Lấy data từ file excel
                foreach (DataRow dataRow in dataTable.AsEnumerable().Skip(1))
                {
                    var dataCategory = new DiscountCategoryExcelDto()
                    {
                        LineNumber = 1,
                        CategoryCode = dataRow.ItemArray[0].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[0].ToString(),
                        TypeCode = dataRow.ItemArray[1].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[1].ToString(),
                        Quantity = dataRow.ItemArray[2].ToString().IsNullOrEmpty() ? null : Convert.ToInt32(dataRow.ItemArray[2]),
                        UnitName = dataRow.ItemArray[3].ToString().IsNullOrEmpty() ? ImportExcelConstants.MaxUnit : dataRow.ItemArray[3].ToString(),
                        WarehouseName = dataRow.ItemArray[4].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[4].ToString(),
                        QualifierName = dataRow.ItemArray[5].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[5].ToString(),
                        Discount = dataRow.ItemArray[6].ToString().IsNullOrEmpty() ? null : Convert.ToInt32(dataRow.ItemArray[6]),
                        MaxValue = dataRow.ItemArray[7].ToString().IsNullOrEmpty() ? null : Convert.ToInt32(dataRow.ItemArray[7]),
                        MaxQuantity = dataRow.ItemArray[8].ToString().IsNullOrEmpty() ? 0 : Convert.ToInt32(dataRow.ItemArray[8]),
                        Note = dataRow.ItemArray[9].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[9].ToString(),
                        RowNumber = rowNumber
                    };
                    dataCategory.Validate();
                    dataCategoryDiscount.Add(dataCategory);
                    rowNumber++;
                }
            }

            //Scheme mua combo giảm giá sản phẩm
            if (typeDiscount == ImportExcelConstants.BuyComboGetDiscount)
            {
                int rowNumber = 3;
                //Lấy data từ file excel
                foreach (DataRow dataRow in dataTable.AsEnumerable().Skip(1))
                {
                    var dataCategory = new DiscountCategoryExcelDto()
                    {
                        LineNumber = Convert.ToInt32(dataRow.ItemArray[0]),
                        CategoryCode = dataRow.ItemArray[1].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[1].ToString(),
                        TypeCode = dataRow.ItemArray[2].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[2].ToString(),
                        Quantity = dataRow.ItemArray[3].ToString().IsNullOrEmpty() ? null : Convert.ToInt32(dataRow.ItemArray[3]),
                        UnitName = dataRow.ItemArray[4].ToString().IsNullOrEmpty() ? ImportExcelConstants.MaxUnit : dataRow.ItemArray[4].ToString(),
                        WarehouseName = dataRow.ItemArray[5].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[5].ToString(),
                        QualifierName = dataRow.ItemArray[6].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[6].ToString(),
                        Discount = dataRow.ItemArray[7].ToString().IsNullOrEmpty() ? null : Convert.ToInt32(dataRow.ItemArray[7]),
                        MaxValue = dataRow.ItemArray[8].ToString().IsNullOrEmpty() ? null : Convert.ToInt32(dataRow.ItemArray[8]),
                        MaxQuantity = dataRow.ItemArray[9].ToString().IsNullOrEmpty() ? 0 : Convert.ToInt32(dataRow.ItemArray[9]),
                        Note = dataRow.ItemArray[10].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[10].ToString(),
                        RowNumber = rowNumber
                    };
                    dataCategory.Validate();
                    dataCategoryDiscount.Add(dataCategory);
                    rowNumber++;
                }
            }

            return dataCategoryDiscount;
        }

        private List<DiscountProductExcelDto> ReadDiscountProduct(DataTable dataTable, string typeDiscount)
        {
            var discountProducts = new List<DiscountProductExcelDto>();
            if (typeDiscount == ImportExcelConstants.BuyProductGetDiscount)
            {
                int rowNumber = 3;
                //Lấy data từ file excel
                foreach (DataRow dataRow in dataTable.AsEnumerable().Skip(1))
                {
                    if (dataRow.ItemArray[0].ToString().IsNullOrEmpty())
                    {
                        break;
                    }
                    var dataProduct = new DiscountProductExcelDto()
                    {
                        LineNumber = 1,
                        ItemCode = dataRow.ItemArray[0].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[0].ToString(),
                        Quantity = dataRow.ItemArray[1].ToString().IsNullOrEmpty() ? null : Convert.ToInt32(dataRow.ItemArray[1]),
                        WarehouseName = dataRow.ItemArray[2].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[2].ToString(),
                        UnitName = dataRow.ItemArray[3].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[3].ToString(),
                        QualifierName = dataRow.ItemArray[4].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[4].ToString(),
                        Discount = dataRow.ItemArray[5].ToString().IsNullOrEmpty() ? null : Convert.ToInt32(dataRow.ItemArray[5]),
                        MaxValue = dataRow.ItemArray[6].ToString().IsNullOrEmpty() ? null : Convert.ToInt32(dataRow.ItemArray[6]),
                        MaxQuantity = dataRow.ItemArray[7].ToString().IsNullOrEmpty() ? 0 : Convert.ToInt32(dataRow.ItemArray[7]),
                        Note = dataRow.ItemArray[8].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[8].ToString(),
                        RowNumber = rowNumber
                    };
                    dataProduct.Validate();
                    discountProducts.Add(dataProduct);
                    rowNumber++;
                }
            }

            if(typeDiscount == ImportExcelConstants.BuyComboGetDiscount)
            {
                int rowNumber = 3;
                //Lấy data từ file excel
                foreach (DataRow dataRow in dataTable.AsEnumerable().Skip(1))
                {
                    if (dataRow.ItemArray[1].ToString().IsNullOrEmpty())
                    {
                        break;
                    }
                    var dataProduct = new DiscountProductExcelDto()
                    {
                        LineNumber = Convert.ToInt32(dataRow.ItemArray[0]),
                        ItemCode = dataRow.ItemArray[1].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[1].ToString(),
                        Quantity = dataRow.ItemArray[2].ToString().IsNullOrEmpty() ? null : Convert.ToInt32(dataRow.ItemArray[2]),
                        WarehouseName = dataRow.ItemArray[3].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[3].ToString(),
                        UnitName = dataRow.ItemArray[4].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[4].ToString(),
                        QualifierName = dataRow.ItemArray[5].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[5].ToString(),
                        Discount = dataRow.ItemArray[6].ToString().IsNullOrEmpty() ? null : Convert.ToInt32(dataRow.ItemArray[6]),
                        MaxValue = dataRow.ItemArray[7].ToString().IsNullOrEmpty() ? null : Convert.ToInt32(dataRow.ItemArray[7]),
                        MaxQuantity = dataRow.ItemArray[8].ToString().IsNullOrEmpty() ? 0 : Convert.ToInt32(dataRow.ItemArray[8]),
                        Note = dataRow.ItemArray[9].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[9].ToString(),
                        RowNumber = rowNumber
                    };
                    dataProduct.Validate();
                    discountProducts.Add(dataProduct);
                    rowNumber++;
                }
            }
            return discountProducts;
        
        }

        private List<ProductGetProductInputExcelDto> ReadGetProduct(DataTable dataTable, string typeGet)
        {
            var getProducts = new List<ProductGetProductInputExcelDto>();
            if(typeGet == ImportExcelConstants.BuyProductGetProduct 
                || typeGet == ImportExcelConstants.TotalProductAmount 
                || typeGet == ImportExcelConstants.BuyProductGetAscendingDiscount)
            {
                int rowNumber = 3;
                foreach (DataRow dataRow in dataTable.AsEnumerable().Skip(1))
                {
                    if (dataRow.ItemArray[0].ToString().IsNullOrEmpty())
                    {
                        break;
                    }
                    var dataGetProduct = new ProductGetProductInputExcelDto()
                    {
                        LineNumber = 1,
                        ItemCode = dataRow.ItemArray[0].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[0].ToString(),
                        Quantity = (dataRow.ItemArray[1].ToString().IsNullOrEmpty() || typeGet == "TotalProductAmount") ? 1 : Convert.ToInt32(dataRow.ItemArray[1]),
                        WarehouseName = dataRow.ItemArray[2].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[2].ToString(),
                        UnitName = dataRow.ItemArray[3].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[3].ToString(),
                        RowNumber = rowNumber
                    };
                    dataGetProduct.Validate();
                    getProducts.Add(dataGetProduct);
                    rowNumber++;
                }
            }

            if(typeGet == ImportExcelConstants.BuyComboGetProduct || typeGet == ImportExcelConstants.UsePaymentMethod)
            {
                int rowNumber = 3;
                foreach (DataRow dataRow in dataTable.AsEnumerable().Skip(1))
                {
                    if (dataRow.ItemArray[1].ToString().IsNullOrEmpty())
                    {
                        break;
                    }
                    var dataGetProduct = new ProductGetProductInputExcelDto()
                    {
                        LineNumber = Convert.ToInt32(dataRow.ItemArray[0]),
                        ItemCode = dataRow.ItemArray[1].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[1].ToString(),
                        Quantity = (dataRow.ItemArray[2].ToString().IsNullOrEmpty() || typeGet == "TotalProductAmount") ? 1 : Convert.ToInt32(dataRow.ItemArray[2]),
                        WarehouseName = dataRow.ItemArray[3].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[3].ToString(),
                        UnitName = dataRow.ItemArray[4].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[4].ToString(),
                        RowNumber = rowNumber
                    };
                    dataGetProduct.Validate();
                    getProducts.Add(dataGetProduct);
                    rowNumber++;
                }
            }

            return getProducts;
        }

        private List<CategoryGetProductInputExcelDto> ReadCategoryGetProduct(DataTable dataTable, string typeGet)
        {
            var categoryGetProducts = new List<CategoryGetProductInputExcelDto>();
            if (typeGet == ImportExcelConstants.BuyProductGetProduct
                || typeGet == ImportExcelConstants.TotalProductAmount
                || typeGet == ImportExcelConstants.BuyProductGetAscendingDiscount)
            {
                int rowNumber = 3;
                //Lấy data từ file excell
                foreach (DataRow dataRow in dataTable.AsEnumerable().Skip(1))
                {
                    if (dataRow.ItemArray[0].ToString().IsNullOrEmpty())
                    {
                        break;
                    }
                    var dataGetProduct = new CategoryGetProductInputExcelDto()
                    {
                        LineNumber = 1,
                        CategoryCode = dataRow.ItemArray[0].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[0].ToString(),
                        TypeCode = dataRow.ItemArray[1].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[1].ToString(),
                        Quantity = (dataRow.ItemArray[2].ToString().IsNullOrEmpty() || typeGet == "TotalProductAmount") ? 1 : Convert.ToInt32(dataRow.ItemArray[2]),
                        WarehouseName = dataRow.ItemArray[4].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[4].ToString(),
                        UnitName = dataRow.ItemArray[3].ToString().IsNullOrEmpty() ? ImportExcelConstants.MaxUnit : dataRow.ItemArray[3].ToString(),
                        RowNumber = rowNumber
                    };
                    dataGetProduct.Validate();
                    categoryGetProducts.Add(dataGetProduct);
                    rowNumber++;
                }
            }
            if (typeGet == ImportExcelConstants.BuyComboGetProduct || typeGet == ImportExcelConstants.UsePaymentMethod)
            {
                int rowNumber = 3;
                //Lấy data từ file excell
                foreach (DataRow dataRow in dataTable.AsEnumerable().Skip(1))
                {
                    if (dataRow.ItemArray[1].ToString().IsNullOrEmpty())
                    {
                        break;
                    }
                    var dataGetProduct = new CategoryGetProductInputExcelDto()
                    {
                        LineNumber = Convert.ToInt32(dataRow.ItemArray[0]),
                        CategoryCode = dataRow.ItemArray[1].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[1].ToString(),
                        TypeCode = dataRow.ItemArray[2].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[2].ToString(),
                        Quantity = (dataRow.ItemArray[3].ToString().IsNullOrEmpty() || typeGet == "TotalProductAmount") ? 1 : Convert.ToInt32(dataRow.ItemArray[3]),
                        WarehouseName = dataRow.ItemArray[5].ToString().IsNullOrEmpty() ? null : dataRow.ItemArray[5].ToString(),
                        UnitName = dataRow.ItemArray[4].ToString().IsNullOrEmpty() ? ImportExcelConstants.MaxUnit : dataRow.ItemArray[4].ToString(),
                        RowNumber = rowNumber
                    };
                    dataGetProduct.Validate();
                    categoryGetProducts.Add(dataGetProduct);
                    rowNumber++;
                }
            }

            return categoryGetProducts;
        }

        //Validate quantity and qualifier của từng scheme
        private List<MessageErrorDto> ValidateQuantityOutput(ValidateQuantityOutputDto firstLine, List<ValidateQuantityOutputDto> itemOther)
        {
            var listError = new List<MessageErrorDto>();
            if (itemOther.Count > 1)
            {
                itemOther.Remove(firstLine);
                itemOther.ForEach(c =>
                {
                    //Kiểm tra số lượng mua có giống nhau không
                    if(c.Quantity != firstLine.Quantity) 
                    {
                        listError.Add(new MessageErrorDto()
                        {
                            Line = c.RowNumber,
                            Message = $"Số lượng mua phải giống nhau"
                        });
                    }

                    //Kiểm tra khi có đầu tra của từng scheme
                    if (c.QualifierName != null && c.QualifierName != firstLine.QualifierName)
                    {
                        listError.Add(new MessageErrorDto()
                        {
                            Line = c.RowNumber,
                            Message = $"Đầu ra phải cùng một loại giảm giá"
                        });
                    }
                });

                return listError;
            }

            return listError;
        }
    }
}
