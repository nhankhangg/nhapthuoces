﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FRTTMO.CAM.PromotionAPI.DTOs.Insides;
using FRTTMO.CAM.PromotionAPI.Services;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using StackExchange.Redis;
using Volo.Abp.Auditing;

namespace FRTTMO.CAM.PromotionAPI.Applications;

public class InsideAppService : PromotionAPIAppService, IInsideAppService
{
    private readonly IConfiguration _config;
    private readonly IDatabase _databaseRedisCache;
    private readonly IConfiguration _configuration;
    private readonly IApiClientService _iApiClientService;

    public InsideAppService(
        IConfiguration config,
        IDatabase databaseRedisCache,
        IConfiguration configuration,
        IApiClientService iApiClientService)
    {
        _config = config;
        _configuration = configuration;
        _databaseRedisCache = databaseRedisCache;
        _iApiClientService = iApiClientService;
    }

    [DisableAuditing]
    public async Task<List<ListShopOutputDto>> ShopAll(string shopType)
    {
        var keyPrefix = shopType != null 
            ? $"{_configuration["Redis:KeyPrefix"]}:{"Regions"}:{shopType}" 
            : $"{_configuration["Redis:KeyPrefix"]}:{"Regions"}";

        var shopItem = await _databaseRedisCache.StringGetAsync(keyPrefix);
        if (!shopItem.IsNullOrEmpty)
        {
            return JsonConvert.DeserializeObject<List<ListShopOutputDto>>(shopItem);
        }
        else
        {
            var regions = await _iApiClientService.FetchAsync<List<ListShopOutputDto>>(
            _config["RemoteServices:DSMSAPI:BaseUrl"],
            "api/data/nhapthuoc/regionpromotion",
            payload: string.Empty,
            method: "GET",
            authorization: string.Empty);

            await _databaseRedisCache.StringSetAsync(keyPrefix, JsonConvert.SerializeObject(regions));
            SetTimeSpanCacheAsync(keyPrefix, DateTime.Now);

            return regions;
        }

    }

    [DisableAuditing]
    public async Task<List<DepartmentDto>> GetListDepartmentAsync()
    {
        var departments = await _iApiClientService.FetchAsync<List<DepartmentResponseDto>>(
            _config["RemoteServices:InsideICTAPI:BaseUrl"],
            "api/inside/ict/v2/master-data/get-organ-nganh-hang-mkt-bod",
            payload: string.Empty,
            method: "GET",
            authorization: string.Empty
        );

        return ObjectMapper.Map<List<ChildDepartmentDto>, List<DepartmentDto>>(departments.SelectMany(x => x.Children).Distinct().ToList());
    }

    private async void SetTimeSpanCacheAsync(string keyCache, DateTime? dateTime)
    {
        if (dateTime == null)
        {
            await _databaseRedisCache.KeyPersistAsync(keyCache);
        }
        else
        {
            // Tính thời gian hiện tại
            DateTime currentTime = DateTime.Now;
            // Tính thời gian đến cuối ngày (23:59:59) của cùng ngày
            DateTime endOfDay = currentTime.Date.AddDays(1).AddSeconds(-1);
            // Tính thời gian còn lại đến cuối ngày
            TimeSpan timeSpan = endOfDay - currentTime;
            await _databaseRedisCache.KeyExpireAsync(keyCache, timeSpan);
        }
    }
}