﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FRTTMO.CAM.PromotionAPI.Dtos;
using FRTTMO.CAM.PromotionAPI.DTOs;
using FRTTMO.CAM.PromotionAPI.DTOs.PIM;
using FRTTMO.CAM.PromotionAPI.ExceptionCodes;
using FRTTMO.CAM.PromotionAPI.Localization;
using FRTTMO.CAM.PromotionAPI.Options;
using FRTTMO.CAM.PromotionAPI.Paginations;
using FRTTMO.CAM.PromotionAPI.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace FRTTMO.CAM.PromotionAPI.Applications;

public class PIMAppService : PromotionAPIAppService, IPIMAppService
{
    private readonly IApiClientService _iApiClientService;
    private readonly IOptions<RemoteServicesOption> _remoteServicesOption;
    private readonly IConfiguration _configuration;
    private readonly IStringLocalizer<PromotionAPIResource> _stringLocalize;
    public PIMAppService(
        IConfiguration configuration,
        IApiClientService iApiClientService,
        IOptions<RemoteServicesOption> remoteServicesOption,
        IStringLocalizer<PromotionAPIResource> stringLocalize)
    {
        _configuration = configuration;
        _stringLocalize = stringLocalize;
        _iApiClientService = iApiClientService;
        _remoteServicesOption = remoteServicesOption;
    }
    public async Task<PaginationResultDto<CategoryLevelDto>> SearchCategoryLevelAsync(SearchCategoryLevelInputDto input, Pagination pagination)
    {
        switch (input.Type)
        {
            case "Category":
                var queryParamCategory = new Dictionary<string, object>()
                {
                    { "keyword", input.Keyword },
                };
                var resultCategory = await _iApiClientService.FetchAsync<PIMCategoryLevelOutputDto>(
                    _configuration["RemoteServices:PIMAPICache:BaseUrl"],
                        "/promotion/category", "GET", queryParams: queryParamCategory);

                return new PaginationResultDto<CategoryLevelDto>
                {
                    Items = resultCategory.Items.Select(x => new CategoryLevelDto
                    {
                        Name = x.Name,
                        Code = x.Code
                    }).ToList(),
                    SkipCount = pagination.SkipCount ?? 0,
                    MaxResultCount = pagination.MaxResultCount ?? 10,
                    TotalCount = resultCategory.Items?.Count ?? 0
                };
            case "Type":
                var queryParamType = new Dictionary<string, object>()
                {
                    { "keyword", input.Keyword },
                    { "categoryUniqueId", input.ParentCode },
                    { "page", pagination.SkipCount + 1 },
                    { "size", pagination.MaxResultCount },
                };
                var resultType = await _iApiClientService.FetchAsync<PIMTypeLevelOutputDto>(
                    _configuration["RemoteServices:PIMAPICache:BaseUrl"],
                        "/promotion/group", "GET", queryParams: queryParamType);
                return new PaginationResultDto<CategoryLevelDto>
                {
                    Items = resultType.Items.Select(x => new CategoryLevelDto
                    {
                        Name = x.Name,
                        Code = x.Code
                    }).ToList(),
                    SkipCount = pagination.SkipCount ?? 0,
                    MaxResultCount = pagination.MaxResultCount ?? 10,
                    TotalCount = resultType.Items?.Count ?? 0
                };
        }

        return new PaginationResultDto<CategoryLevelDto>();
    }

    public async Task<PaginationResultDto<CategoryLevelDto>> SearchBrandAsync(SearchCategoryLevelInputDto input, Pagination pagination)
    {
        var result = await _iApiClientService.FetchAsync<PIMBrandOutputDto>(
            _remoteServicesOption.Value.PIMAPI.BaseUrl,
            "/api/v1/promotion/brand",
            "GET",
            queryParams: new Dictionary<string, object>
            {
                { "query", input.Keyword },
                { "skipCount", pagination.SkipCount },
                { "maxResultCount", pagination.MaxResultCount }
            }
        );

        return new PaginationResultDto<CategoryLevelDto>
        {
            Items = result.Items.Select(x => new CategoryLevelDto
            {
                Name = x.Name,
                Code = x.UniqueId
            }).ToList(),
            SkipCount = pagination.SkipCount ?? 0,
            MaxResultCount = pagination.MaxResultCount ?? 10,
            TotalCount = result.Items?.Count ?? 0
        };
    }

    public async Task<PaginationResultDto<ProductDetailsDto>> SearchProductAsync(SearchProductInputDto input, Pagination pagination)
    {
        try
        {
            var payload = new ProductInputDto()
            {
                page = pagination.SkipCount + 1,
                size = pagination.MaxResultCount,
            };
            payload.categoryUniqueId = new List<string>();
            payload.groupUniqueId = new List<string>();
            if (!input.CategoryCode.IsNullOrEmpty())
            {
                payload.categoryUniqueId.Add(input.CategoryCode);
            }

            if (!input.TypeCode.IsNullOrEmpty())
            {
                payload.groupUniqueId.Add(input.TypeCode);
            }

            if (!input.Keyword.IsNullOrEmpty())
            {
                payload.keyword = input.Keyword;
            }
            var result = await _iApiClientService.FetchAsync<SearchOutputDto>(
                _configuration["RemoteServices:PIMAPICache:BaseUrl"],
                "promotion/products/search",
                "POST", payload: JsonConvert.SerializeObject(payload));

            result.Data.ForEach(x =>
            {
                x.Measures.Add(new MeasureUnitDto
                {
                    UnitCode = "0",
                    UnitName = "Tất cả",
                    Level = 100
                });
            });

            return new PaginationResultDto<ProductDetailsDto>
            {
                Items = result.Data,
                SkipCount = pagination.SkipCount.GetValueOrDefault(0),
                MaxResultCount = pagination.MaxResultCount.GetValueOrDefault(10),
                TotalCount = result.TotalCount
            };
        }
        catch (Exception)
        {
            return new PaginationResultDto<ProductDetailsDto>();
        }
    }

    //private static Tuple<string, Dictionary<string, object>> SearchCategoryParamsBuilder(SearchCategoryLevelInputDto input, Pagination pagination)
    //{
    //    var queryParams = input.Type switch
    //    {
    //        "Category" => new Dictionary<string, object>
    //        {
    //            { "level", 1 },
    //            { "query", input.Keyword },
    //            { "skipCount", pagination.SkipCount },
    //            { "maxResultCount", pagination.MaxResultCount }
    //        },
    //        "Type" => new Dictionary<string, object>
    //        {
    //            { "parentCategoryId", input.ParentCode },
    //            { "level", 2 },
    //            { "query", input.Keyword },
    //            { "skipCount", pagination.SkipCount },
    //            { "maxResultCount", pagination.MaxResultCount }
    //        },
    //        "Group" => new Dictionary<string, object>
    //        {
    //            { "parentCategoryId", input.ParentCode },
    //            { "level", 3 },
    //            { "type", input.Type },
    //            { "query", input.Keyword },
    //            { "skipCount", pagination.SkipCount },
    //            { "maxResultCount", pagination.MaxResultCount }
    //        },
    //        "Model" => new Dictionary<string, object>
    //        {
    //            { "parentCategoryId", input.ParentCode },
    //            { "level", 3 },
    //            { "type", input.Type },
    //            { "query", input.Keyword },
    //            { "skipCount", pagination.SkipCount },
    //            { "maxResultCount", pagination.MaxResultCount }
    //        },
    //        _ => new Dictionary<string, object>()
    //    };
    //    var endpoint = input.Type switch
    //    {
    //        "Category" => "/api/v1/promotion/search-category-level",
    //        _ => "/api/v1/promotion/search-child-category"
    //    };

    //    return new Tuple<string, Dictionary<string, object>>(endpoint, queryParams);
    //}

    public async Task<List<ItemDetails>> ListProductDetailAsync(List<string> sKus)
    {
        var queryParams = new Dictionary<string, object>();
        if (sKus.Any())
        {
            queryParams.Add("skus", sKus.JoinAsString(","));
        }

        var result = await _iApiClientService.FetchAsync<List<ItemDetails>>(
            _remoteServicesOption.Value.PIMAPI.BaseUrl,
            "/api/v1/promotion/product/list-detail",
            "GET",
            queryParams: queryParams
        );

        return result;
    }

    #region product pim cache
    public async Task<PimProductCacheDto> GetListProductByPimCacheAsync(PimCacheSearch pimCacheSearch)
    {
        var queryParams = new Dictionary<string, object>  // lần 2 chỉ truền ScrollId
        {
            { "scrollId", pimCacheSearch.ScrollId },
        };
        if (pimCacheSearch.ScrollId.IsNullOrEmpty()) // define pay load call lần đầu
        {
            queryParams.Add("size", pimCacheSearch.ScrollSize);

            if (!pimCacheSearch.CategoryCode.IsNullOrEmpty())
            {
                queryParams.Add("categoryUniqueId", pimCacheSearch.CategoryCode);
            }
            if (!pimCacheSearch.TypeCode.IsNullOrEmpty())
            {
                queryParams.Add("groupUniqueId", pimCacheSearch.TypeCode);
            }
        }
        if (pimCacheSearch.IsScroll == true)
        {
            queryParams.Add("isScroll", "true");
        }
        else
        {
            queryParams.Add("isScroll", "false");
        }    

        try
        {
            var products = await _iApiClientService.FetchAsync<PimProductCacheDto>(
                _configuration["RemoteServices:PIMAPICache:BaseUrl"],
                "/promotion/products/scroll", "POST", payload: JsonConvert.SerializeObject(queryParams));

            if (products.Metadata != null)
            {
                products.Total = products.Metadata.Total;
            }
            return products;
        }
        catch (Exception ex)
        {
            throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                _stringLocalize[ExceptionCode.BadRequest], $"Error call pim cache {ex.Message}: {JsonConvert.SerializeObject(queryParams)}");
        }

    }


    public async Task<PimProductCacheDto> GetProductByPimCacheAsync(string sku)
    {
        var configBaseUrl = _configuration["RemoteServices:PIMAPICache:BaseUrl"];
        var operation = "/promotion/products/list";

        var payload = new PimCacheSearchMany()
        {
            sku = new List<string> { sku }
        };
        try
        {
            var products = await _iApiClientService.FetchAsync<PimProductCacheDto>(
                _configuration["RemoteServices:PIMAPICache:BaseUrl"],
                "/promotion/products/list",
                "POST", payload: JsonConvert.SerializeObject(payload));
            return new PimProductCacheDto
            {
                Total = products.Data.Count,
                Data = products.Data,
                ScrollId = null
            };
        }
        catch (Exception ex)
        {
            throw new FrtPromotionValidationException(ExceptionCode.BadRequest, _stringLocalize[ExceptionCode.BadRequest],
                  $"Error call pim cache: {ex.Message},{configBaseUrl},{operation}," +
                  $"{JsonConvert.SerializeObject(sku)}");
        }
    }

    public async Task<CountPimProductCacheDto> CountDataPimCacheAsync(PimCacheSearch pimCacheSearch)
    {
        var queryParams = new Dictionary<string, object>  // lần 2 chỉ truền ScrollId
        {
        };

        if (!pimCacheSearch.CategoryCode.IsNullOrEmpty())
        {
            queryParams.Add("categoryUniqueId", pimCacheSearch.CategoryCode);
        }
        if (!pimCacheSearch.TypeCode.IsNullOrEmpty())
        {
            queryParams.Add("groupUniqueId", pimCacheSearch.TypeCode);
        }

        try
        {
            var products = await _iApiClientService.FetchAsync<CountPimProductCacheDto>(
                _configuration["RemoteServices:PIMAPICache:BaseUrl"],
                "/promotion/products/count", "GET", queryParams: queryParams);
            return products;
        }
        catch (Exception ex)
        {
            throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                _stringLocalize[ExceptionCode.BadRequest], $"Error call pim cache {ex.Message}: {JsonConvert.SerializeObject(queryParams)}");
        }

    }
    #endregion
    public async Task<List<MeasureUnitDto>> MeasureUnitsAsync()
    {
        var measures = await _iApiClientService.FetchAsync<DataUnitDto>(
            _remoteServicesOption.Value.PIMAPICache.BaseUrl,
            "/promotion/measures/search", "GET");
        return measures.Data;
    }

    public async Task<List<CategoryValidationDto>> CategoriesValidationAsync(List<CategoryValidationInputDto> categoryValidationInputs)
    {
        try
        {
            List<CategoryValidationDto> categoryValidations = new();
            foreach (var pimValidate in categoryValidationInputs)
            {
                var queryParams = new Dictionary<string, object>();

                if (!pimValidate.CategoryCode.IsNullOrEmpty())
                {
                    queryParams.Add("categoryUniqueId", pimValidate.CategoryCode);
                }
                if (!pimValidate.TypeCode.IsNullOrEmpty())
                {
                    queryParams.Add("groupUniqueId", pimValidate.TypeCode);
                }

                var test = _configuration["RemoteServices:PIMAPICache:BaseUrl"];
                var results = await _iApiClientService.FetchAsync<CategoryValidationDto>(
                    _configuration["RemoteServices:PIMAPICache:BaseUrl"],
                    "/promotion/products/count", "GET", queryParams: queryParams
                );

                categoryValidations.Add(new CategoryValidationDto()
                {
                    Total = results.Total,
                    CategoryCode = pimValidate.CategoryCode,
                    CategoryName = pimValidate.CategoryName,
                    TypeCode = pimValidate.TypeCode,
                    TypeName = pimValidate.TypeName
                });
            }

            return categoryValidations;
        }
        catch (Exception)
        {
            return new List<CategoryValidationDto>();
        }
    }

    public async Task<List<ItemDetailWithCategoryDto>> GetListProductWithCategoryAsync(List<string> skus)
    {
        try
        {
            var result = await _iApiClientService.FetchAsync<List<PIMItemDetailWithCategoryDto>>(
            _configuration["RemoteServices:PIMAPICache:BaseUrl"],
            "api/v1/promotion/products/many",
            "POST",
            JsonConvert.SerializeObject(new { skus })
        );

            return ObjectMapper.Map<List<PIMItemDetailWithCategoryDto>, List<ItemDetailWithCategoryDto>>(result);
        }
        catch (Exception)
        {
            return new List<ItemDetailWithCategoryDto>();
        }

    }


    public async Task<GetProductCategoryOutputDto> GetGroupByCategoryId(GroupInputDto input)
    {
        try
        {
            var queryParam = new Dictionary<string, object>()
            {
                { "keyword", input.keyword },
                {"categoryUniqueId", input.categoryUniqueId },
                {"size", input.size },
                {"page", input.page }

            };
            var result = await _iApiClientService.FetchAsync<GetProductCategoryOutputDto>(
                _configuration["RemoteServices:PIMAPICache:BaseUrl"],
                "/promotion/group", "GET", queryParams: queryParam);

            return result;

        }

        catch (Exception)
        {

            return new GetProductCategoryOutputDto();
        }
    }


}