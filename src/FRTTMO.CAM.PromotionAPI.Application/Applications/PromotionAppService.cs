﻿using Castle.Core.Internal;
using Elastic.Apm.Api;
using FRTTMO.CAM.PromotionAPI.Documents;
using FRTTMO.CAM.PromotionAPI.DTOs;
using FRTTMO.CAM.PromotionAPI.DTOs.AmountConditions;
using FRTTMO.CAM.PromotionAPI.DTOs.Condition;
using FRTTMO.CAM.PromotionAPI.DTOs.CostDistribution;
using FRTTMO.CAM.PromotionAPI.DTOs.InstallmentConditions;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInput;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInputExclude;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInputReplace;
using FRTTMO.CAM.PromotionAPI.DTOs.Output;
using FRTTMO.CAM.PromotionAPI.DTOs.OutputReplace;
using FRTTMO.CAM.PromotionAPI.DTOs.PaymentConditions;
using FRTTMO.CAM.PromotionAPI.DTOs.PIM;
using FRTTMO.CAM.PromotionAPI.DTOs.Promotion;
using FRTTMO.CAM.PromotionAPI.DTOs.PromotionExcludes;
using FRTTMO.CAM.PromotionAPI.DTOs.Quota;
using FRTTMO.CAM.PromotionAPI.DTOs.RegionCondition;
using FRTTMO.CAM.PromotionAPI.Entities.AmountCondition;
using FRTTMO.CAM.PromotionAPI.Entities.Campaign;
using FRTTMO.CAM.PromotionAPI.Entities.Common;
using FRTTMO.CAM.PromotionAPI.Entities.Condition;
using FRTTMO.CAM.PromotionAPI.Entities.CostDistribution;
using FRTTMO.CAM.PromotionAPI.Entities.FieldConfigure;
using FRTTMO.CAM.PromotionAPI.Entities.InstallmentCondition;
using FRTTMO.CAM.PromotionAPI.Entities.ItemInput;
using FRTTMO.CAM.PromotionAPI.Entities.ItemInputExclude;
using FRTTMO.CAM.PromotionAPI.Entities.ItemInputReplace;
using FRTTMO.CAM.PromotionAPI.Entities.Output;
using FRTTMO.CAM.PromotionAPI.Entities.OutputReplace;
using FRTTMO.CAM.PromotionAPI.Entities.PaymentCondition;
using FRTTMO.CAM.PromotionAPI.Entities.Promotion;
using FRTTMO.CAM.PromotionAPI.Entities.PromotionExclude;
using FRTTMO.CAM.PromotionAPI.Entities.Quota;
using FRTTMO.CAM.PromotionAPI.Entities.RegionCondition;
using FRTTMO.CAM.PromotionAPI.Enum;
using FRTTMO.CAM.PromotionAPI.ETOs;
using FRTTMO.CAM.PromotionAPI.ETOs.Cache;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;
using FRTTMO.CAM.PromotionAPI.ETOs.Sync;
using FRTTMO.CAM.PromotionAPI.ExceptionCodes;
using FRTTMO.CAM.PromotionAPI.Helpers;
using FRTTMO.CAM.PromotionAPI.Localization;
using FRTTMO.CAM.PromotionAPI.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Auditing;
using Volo.Abp.EventBus.Distributed;
using Volo.Abp.Uow;
using ActionType = FRTTMO.CAM.PromotionAPI.Enum.ActionType;

namespace FRTTMO.CAM.PromotionAPI.Applications;

public class PromotionAppService : PromotionAPIAppService, IPromotionAppService
{
    private readonly IDatabase _databaseRedisCache;
    private readonly IConfiguration _configuration;
    private readonly ICounterRepository _counterRepository;
    private readonly IFieldConfigureRepository _fieldConfigureRepository;
    private readonly IItemInputReplaceRepository _itemInputReplaceRepository;
    private readonly IItemInputRepository _itemInputRepository;
    private readonly IOutputReplaceRepository _outputReplaceRepository;
    private readonly IOutputRepository _outputRepository;
    private readonly IPromotionRepository _promotionRepository;
    private readonly IQuotaRepository _quotaRepository;
    private readonly IProvinceConditionRepository _provinceConditionRepository;
    private readonly IStringLocalizer<PromotionAPIResource> _stringLocalize;
    private readonly IUnitOfWorkManager _unitOfWorkManager;
    private readonly IDistributedEventBus _distributedEventBus;
    private readonly ICampaignRepository _campaignRepository;
    private readonly IExtraConditionRepository _extraConditionRepository;
    private readonly IItemInputExcludeRepository _itemInputExcludeRepository;
    private readonly ICostDistributionRepository _costDistributionRepository;
    private readonly IAmountConditionRepository _amountConditionRepository;
    private readonly IInstallmentConditionRepository _installmentConditionRepository;
    private readonly IElasticSearchAppService _elasticSearchAppService;
    private readonly IQuotaCacheService _redisCacheService;
    private readonly IPromotionExcludeRepository _promotionExcludeRepository;
    private readonly IPIMAppService _pIMAppService;
    //private readonly IKafkaProducerService _kafkaProducerService;
    private readonly ISyncDataEcomService _syncDataEcomService;
    private readonly IPaymentConditionRepository _paymentConditionRepository;
    private readonly IHangFireAppService _hangFireAppService;
    private readonly ITracer _tracer;

    public PromotionAppService(
        IPromotionRepository promotionRepository,
        ICounterRepository counterRepository,
        IConfiguration configuration,
        IUnitOfWorkManager unitOfWorkManager,
        IProvinceConditionRepository provinceConditionRepository,
        IQuotaRepository quotaRepository,
        IExtraConditionRepository extraConditionRepository,
        IOutputRepository outputRepository,
        IOutputReplaceRepository outputReplaceRepository,
        IItemInputRepository itemInputRepository,
        IItemInputReplaceRepository itemInputReplaceRepository,
        IFieldConfigureRepository fieldConfigureRepository,
        IStringLocalizer<PromotionAPIResource> stringLocalize,
        IDistributedEventBus distributedEventBus,
        ICampaignRepository campaignRepository,
        IItemInputExcludeRepository itemInputExcludeRepository,
        ICostDistributionRepository costDistributionRepository,
        IAmountConditionRepository amountConditionRepository,
        IInstallmentConditionRepository installmentConditionRepository,
        IElasticSearchAppService elasticSearchAppService,
        IQuotaCacheService redisCacheService,
        IPromotionExcludeRepository promotionExcludeRepository,
        IPIMAppService pIMAppService,
        IDatabase databaseRedisCache,
        IPaymentConditionRepository paymentConditionRepository,
        IHangFireAppService hangFireAppService,
        ISyncDataEcomService syncDataEcomService,
        ITracer tracer)
    {
        _configuration = configuration;
        _counterRepository = counterRepository;
        _promotionRepository = promotionRepository;
        _unitOfWorkManager = unitOfWorkManager;
        _provinceConditionRepository = provinceConditionRepository;
        _quotaRepository = quotaRepository;
        _extraConditionRepository = extraConditionRepository;
        _outputRepository = outputRepository;
        _outputReplaceRepository = outputReplaceRepository;
        _itemInputRepository = itemInputRepository;
        _itemInputReplaceRepository = itemInputReplaceRepository;
        _fieldConfigureRepository = fieldConfigureRepository;
        _stringLocalize = stringLocalize;
        _distributedEventBus = distributedEventBus;
        _campaignRepository = campaignRepository;
        _provinceConditionRepository = provinceConditionRepository;
        _itemInputExcludeRepository = itemInputExcludeRepository;
        _costDistributionRepository = costDistributionRepository;
        _amountConditionRepository = amountConditionRepository;
        _installmentConditionRepository = installmentConditionRepository;
        _elasticSearchAppService = elasticSearchAppService;
        _redisCacheService = redisCacheService;
        _promotionExcludeRepository = promotionExcludeRepository;
        _pIMAppService = pIMAppService;
        _databaseRedisCache = databaseRedisCache;
        _paymentConditionRepository = paymentConditionRepository;
        _hangFireAppService = hangFireAppService;
        _syncDataEcomService = syncDataEcomService;
        _tracer = tracer;
    }

    #region Crud entity

    [DisableAuditing]
    public async Task<List<PromotionDto>> GetListAsync(SearchPromotionDto searchPromotionDto)
    {
        var promotionResponse = new List<PromotionDto>();

        var promotionEntity = await _promotionRepository.GetPagedListAsync(searchPromotionDto.SkipCount, searchPromotionDto.MaxResultCount, "Id");

        if (promotionEntity.Any())
        {
            foreach (var item in promotionEntity)
            {
                var promotionDto = await GetAsync(item.Id);
                promotionResponse.Add(promotionDto);
            }

            return promotionResponse;
        }

        return new List<PromotionDto>();
    }

    [DisableAuditing]
    public async Task<PagedResultDto<PromotionDto>> SearchPromotionAsync(SearchPromotionDto searchPromotionDto)
    {
        var promotionQueryable = await _promotionRepository.GetQueryableAsync();

        if (!searchPromotionDto.Keyword.IsNullOrWhiteSpace())
        {
            promotionQueryable = promotionQueryable.Where(p => p.Code.Contains(searchPromotionDto.Keyword)
                                                                   || p.Name.Contains(searchPromotionDto.Keyword));
        }

        if (searchPromotionDto.CreatedBy.Any())
        {
            promotionQueryable = promotionQueryable.Where(p => searchPromotionDto.CreatedBy.Contains(p.CreatedBy));
        }

        if (searchPromotionDto.Status != null)
        {
            promotionQueryable = promotionQueryable.Where(p => p.Status == (int)searchPromotionDto.Status);
        }

        if (searchPromotionDto.FromDate != null && searchPromotionDto.ToDate == null)
        {
            promotionQueryable = promotionQueryable.Where(p => p.FromDate >= searchPromotionDto.FromDate);
        }

        if (searchPromotionDto.FromDate == null && searchPromotionDto.ToDate != null)
        {
            promotionQueryable = promotionQueryable.Where(p => p.ToDate <= searchPromotionDto.ToDate);
        }

        if (searchPromotionDto.FromDate != null && searchPromotionDto.ToDate != null)
        {
            promotionQueryable = promotionQueryable.Where(p => p.FromDate >= searchPromotionDto.FromDate
                                             && p.ToDate <= searchPromotionDto.ToDate);
        }

        if (searchPromotionDto.CampaignId != null)
        {
            promotionQueryable = promotionQueryable.Where(p => p.CampaignId == Guid.Parse(searchPromotionDto.CampaignId));
        }

        if (searchPromotionDto.Code.Any())
        {
            promotionQueryable = promotionQueryable.Where(p => searchPromotionDto.Code.Contains(p.Code));
        }

        var promotions = promotionQueryable.ToList();
        var pagedResult = SortHelpers.Sort(promotions.ToList(),
                  searchPromotionDto.Sorting.ColumnName,
                  searchPromotionDto.Sorting.SortType)
            .Skip(searchPromotionDto.SkipCount)
            .Take(searchPromotionDto.MaxResultCount)
            .ToList();
        var promotionsMapping = ObjectMapper.Map<List<Promotion>, List<PromotionDto>>(pagedResult);

        var promotionExcludes = await _promotionExcludeRepository.GetListAsync(p => promotionsMapping.Select(x => x.Code).Contains(p.PromotionCode));
        foreach (var promotionItem in promotionsMapping)
        {
            var promotionExcludesSelected = promotionExcludes.Where(p => p.PromotionCode == promotionItem.Code).ToList();
            if (promotionExcludesSelected != null)
            {
                promotionItem.PromotionExcludes = ObjectMapper.Map<List<PromotionExclude>, List<PromotionExcludeDto>>(promotionExcludesSelected);
            }
        }
        return new PagedResultDto<PromotionDto>
        {
            TotalCount = promotions.Count,
            Items = promotionsMapping
        };
    }

    public async Task<PromotionDto> GetAsync(Guid id)
    {
        var promotionEntity = await _promotionRepository.FindAsync(p => p.Id == id);

        if (promotionEntity != null)
        {
            var promotionDto = ObjectMapper.Map<Promotion, PromotionDto>(promotionEntity);
            var itemInputEntity = await _itemInputRepository.GetListAsync(p => p.PromotionId == id);
            if (itemInputEntity != null)
            {
                promotionDto.ItemInputs = ObjectMapper.Map<List<ItemInput>, List<ItemInputDto>>(itemInputEntity)?
                .OrderBy(x => x.LineNumber)
                .ToList();
            }

            var itemInputReplaceEntity = await _itemInputReplaceRepository.GetListAsync(p => p.PromotionId == id);
            if (itemInputReplaceEntity is { Count: > 0 })
            {
                promotionDto.ItemInputs.ForEach(p =>
                    p.ItemInputReplaces = ObjectMapper.Map<List<ItemInputReplace>, List<ItemInputReplaceDto>>(
                        itemInputReplaceEntity.Where(z => z.InputItemId == p.Id).ToList())?
                        .OrderBy(x => x.LineNumber)
                        .ThenBy(x => x.ItemCode)
                        .ThenBy(x => x.CategoryCode)
                        .ToList()
                        );
            }

            var itemInputExcludeRepository = await _itemInputExcludeRepository.GetListAsync(p => p.PromotionId == id);
            if (itemInputExcludeRepository is { Count: > 0 })
            {
                promotionDto.ItemInputExcludes = ObjectMapper.Map<List<ItemInputExclude>, List<ItemInputExcludeDto>>(itemInputExcludeRepository);
            }

            var outPutEntity = await _outputRepository.GetListAsync(p => p.PromotionId == id);
            if (outPutEntity != null)
            {
                promotionDto.Outputs = ObjectMapper.Map<List<Output>, List<OutputDto>>(outPutEntity)?
                .OrderBy(x => x.LineNumber)
                .ToList();
            }

            var outPutReplaceEntity = await _outputReplaceRepository.GetListAsync(p => p.PromotionId == id);
            if (outPutReplaceEntity is { Count: > 0 })
            {
                promotionDto.Outputs.ForEach(p =>
                    p.OutputReplaces = ObjectMapper.Map<List<OutputReplace>, List<OutputReplaceDto>>(
                        outPutReplaceEntity.Where(z => z.OutputId == p.Id).ToList())?
                        .OrderBy(x => x.LineNumber)
                        .ThenBy(x => x.ItemCode)
                        .ThenBy(x => x.CategoryCode)
                        .ToList()
                        );
            }

            var quotaEntity = await _quotaRepository.FindAsync(p => p.PromotionId == id);
            if (quotaEntity != null)
            {
                promotionDto.Quota = ObjectMapper.Map<Quota, QuotaDto>(quotaEntity);
            }

            var shopConditionEntity = await _provinceConditionRepository.GetListAsync(p => p.PromotionId == id);
            if (shopConditionEntity != null)
            {
                promotionDto.ProvinceConditions = ObjectMapper.Map<List<ProvinceCondition>, List<ProvinceConditionDto>>(shopConditionEntity);
            }

            var amountConditionRepository = await _amountConditionRepository.FindAsync(p => p.PromotionId == id);
            if (amountConditionRepository != null)
            {
                promotionDto.AmountCondition = ObjectMapper.Map<AmountCondition, AmountConditionDto>(amountConditionRepository);
            }

            var installmentConditionRepository = await _installmentConditionRepository.GetListAsync(p => p.PromotionId == id);
            if (installmentConditionRepository is { Count: > 0 })
            {
                promotionDto.InstallmentConditions = ObjectMapper.Map<List<InstallmentCondition>, List<InstallmentConditionDto>>(installmentConditionRepository);
            }

            var costDistributionRepository = await _costDistributionRepository.GetListAsync(p => p.PromotionId == id);
            if (costDistributionRepository is { Count: > 0 })
            {
                promotionDto.CostDistributions = GetDistributions(costDistributionRepository);
            }

            var extraConditionEntity = await _extraConditionRepository.GetListAsync(p => p.PromotionId == id);
            if (extraConditionEntity != null)
            {
                promotionDto.ExtraConditions = ObjectMapper.Map<List<ExtraCondition>, List<ExtraConditionDto>>(extraConditionEntity);
            }

            var promotionExcludeEntity = await _promotionExcludeRepository.GetListAsync(p => p.PromotionCode == promotionEntity.Code);
            if (promotionExcludeEntity != null)
            {
                var listPromotion = (await _promotionRepository.GetQueryableAsync())
                                                               .Select(x => new { Code = x.Code, Id = x.Id })
                                                               .Where(x => promotionExcludeEntity.Select(c => c.Code).Contains(x.Code))
                                                               .ToList();
                promotionDto.PromotionExcludes = promotionExcludeEntity.Select(c => new PromotionExcludeDto
                {
                    Id = c.Id,
                    PromotionId = listPromotion.Where(x => x.Code == c.Code).FirstOrDefault()?.Id,
                    Name = c.Name,
                    Code = c.Code,
                    AllowDisplay = c.AllowDisplay,
                    PromotionCode = c.PromotionCode,
                    PromotionExcludeType = c.PromotionExcludeType
                }).ToList();
            }

            var paymentConditionEnity = await _paymentConditionRepository.GetListAsync(p => p.PromotionId == id);
            if (paymentConditionEnity != null)
            {
                promotionDto.PaymentConditions = ObjectMapper.Map<List<PaymentCondition>, List<PaymentConditionDto>>(paymentConditionEnity);
            }

            return promotionDto;
        }

        return new PromotionDto();
    }

    public async Task<PromotionDto> CreateAsync(CreatePromotionDto promotionDto)
    {
        // check số lượng đầu vào và đầu ra của giảm giá
        #region Validate scheme BuyProductGetDiscount
        if (promotionDto.PromotionType == PromotionTypeEnum.BuyProductGetDiscount.ToString())
        {
            var itemInputsTotal = 0;
            foreach (var inPut in promotionDto.ItemInputs.Where(p => p.GiftCode.IsNullOrEmpty()))
            {
                itemInputsTotal++;
                itemInputsTotal += inPut.ItemInputReplaces != null ? inPut.ItemInputReplaces.Count : 0;
            }

            var outPutTotal = 0;
            foreach (var outPut in promotionDto.Outputs.Where(p => p.GiftCode.IsNullOrEmpty()))
            {
                outPutTotal++;
                outPutTotal += outPut.OutputReplaces != null ? outPut.OutputReplaces.Count : 0;
            }

            if (outPutTotal != itemInputsTotal)
            {
                throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                    _stringLocalize[ExceptionCode.BadRequest], $"số lượng sản phẩm đầu vào và đầu ra không bằng nhau");
            }
        }
        #endregion End Validate scheme BuyProductGetDiscount
        // check line đầu tiên không tồn tại itemCode

        var itemInputs = promotionDto?.ItemInputs;
        if (itemInputs.Any() && !itemInputs.FirstOrDefault().CategoryCode.IsNullOrEmpty()
          && itemInputs.FirstOrDefault().ItemCode.IsNullOrEmpty()
          && itemInputs.FirstOrDefault().GiftCode.IsNullOrEmpty())
        {
            var countData = await _pIMAppService.CountDataPimCacheAsync(new PimCacheSearch
            {
                CategoryCode = itemInputs?.FirstOrDefault().CategoryCode,
                TypeCode = itemInputs?.FirstOrDefault().GroupCode
            });
            if (countData.Total == 0)
            {
                throw new FrtPromotionValidationException(ExceptionCode.BadRequest, _stringLocalize[ExceptionCode.BadRequest],
                        $"Không tồn tại sản phẩm với " +
                        $"Ngành hàng : {itemInputs?.FirstOrDefault()?.CategoryCode}" +
                        (itemInputs?.FirstOrDefault()?.TypeCode == null ? "" : $", Loại hàng: {itemInputs?.FirstOrDefault()?.TypeCode}"));
            }
        }

        #region Check ngày không hợp lệ của shop
        var checkDateShop = promotionDto.ProvinceConditions.Where(c => c.FromDate.Date < promotionDto.FromDate.Value.Date || c.ToDate.Date > promotionDto.ToDate.Value.Date).ToList();
        if (checkDateShop.Any())
        {
            throw new FrtPromotionValidationException(ExceptionCode.BadRequest, _stringLocalize[ExceptionCode.BadRequest], 
                $"Thời gian áp dụng của Vùng/Tỉnh phải trong khoảng thời gian áp dụng của chương trình khuyến mãi");
        }
        #endregion


        #region Create Promotion with SQL
        var promotion = ObjectMapper.Map<CreatePromotionDto, Promotion>(promotionDto);
        var promotionResponseDto = new PromotionDto();
        var filedConfigures = new List<FieldConfigure>();
        using (var uow = _unitOfWorkManager.Begin(new AbpUnitOfWorkOptions { IsTransactional = false }, requiresNew: true))
        {
            filedConfigures = await _fieldConfigureRepository.GetListAsync();
            await ValidationAsync(promotionDto, filedConfigures);
            // promotion
            MapAuditedUser(ref promotion, "create");
            promotion.Code = await _counterRepository.GenerateCode(_configuration["PrefixCodeGenerator:Promotion"]);
            await _promotionRepository.InsertAsync(promotion);
            promotionResponseDto = ObjectMapper.Map<Promotion, PromotionDto>(promotion);

            // Quota
            if (promotionDto.Quota is { Type: "Product" or "Order" })
            {
                var quotas = ObjectMapper.Map<CreateQuotaDto, Quota>(promotionDto.Quota);
                quotas.PromotionId = promotion!.Id;
                quotas.PromotionCode = promotion!.Code;
                await _quotaRepository.InsertAsync(quotas);
                promotionResponseDto.Quota = ObjectMapper.Map<Quota, QuotaDto>(quotas);
            }

            // RegionConditions
            if (promotionDto.ProvinceConditions?.Any() ?? false)
            {
                var provinceConditions = ObjectMapper.Map<List<CreateProvinceConditionDto>, List<ProvinceCondition>>(promotionDto.ProvinceConditions);
                promotionResponseDto.ProvinceConditions = await ProcessRegionConditionAsync(promotion, provinceConditions);
            }

            // ExtraCondition
            if (promotionDto.ExtraConditions?.Any() ?? false)
            {
                var conditions = ObjectMapper.Map<List<CreateExtraConditionDto>, List<ExtraCondition>>(promotionDto.ExtraConditions);
                conditions = conditions?.Select(p =>
                {
                    p.PromotionId = promotion!.Id;
                    p.PromotionCode = promotion!.Code;
                    p.CreatedBy = promotion!.CreatedBy;
                    p.CreatedByName = promotion!.CreatedByName;
                    return p;
                }).ToList();
                await _extraConditionRepository.InsertManyAsync(conditions);
                promotionResponseDto.ExtraConditions = ObjectMapper.Map<List<ExtraCondition>, List<ExtraConditionDto>>(conditions);
            }

            // ItemInputs
            if (promotionDto.ItemInputs?.Any() ?? false)
            {
                promotionResponseDto.ItemInputs = await CreateItemInputAsync(promotion, promotionDto.ItemInputs);
                var itemInputReplace = promotionResponseDto.ItemInputs.SelectMany(x => x.ItemInputReplaces).ToList();
            }

            // Outputs
            if (promotionDto.Outputs?.Any() ?? false)
            {
                promotionResponseDto.Outputs = await ProcessOutputItemAsync(promotion, promotionDto.Outputs, promotionResponseDto.ItemInputs);
                var itemOutputReplace = promotionResponseDto.Outputs.SelectMany(x => x.OutputReplaces).ToList();
            }

            // InputExclude
            if (promotionDto.ItemInputExcludes?.Any() ?? false)
            {
                var inputExcludes = ObjectMapper.Map<List<CreateItemInputExcludeDto>, List<ItemInputExclude>>(promotionDto.ItemInputExcludes);
                inputExcludes?.ForEach(p =>
                {
                    p.PromotionId = promotion!.Id;
                    p.CreatedBy = promotion!.CreatedBy;
                    p.CreatedByName = promotion!.CreatedByName;
                });

                await _itemInputExcludeRepository.InsertManyAsync(inputExcludes);
                promotionResponseDto.ItemInputExcludes = ObjectMapper.Map<List<ItemInputExclude>, List<ItemInputExcludeDto>>(inputExcludes);
            }

            // CostDistribution
            var costDistributions = new List<CostDistribution>();
            if (promotionDto.CostDistributions?.Any() ?? false)
            {
                costDistributions = ObjectMapper.Map<List<CreateCostDistributionDto>, List<CostDistribution>>(promotionDto.CostDistributions);
                costDistributions?.ForEach(p =>
                {
                    p.PromotionId = promotion!.Id;
                    p.CreatedBy = promotion!.CreatedBy;
                    p.CreatedByName = promotion!.CreatedByName;
                });

                await _costDistributionRepository.InsertManyAsync(costDistributions);
            }

            // AmountCondition
            if (promotionDto.AmountCondition != null)
            {
                var amountCondition = ObjectMapper.Map<CreateAmountConditionDto, AmountCondition>(promotionDto.AmountCondition);
                amountCondition.PromotionId = promotion!.Id;
                amountCondition.CreatedBy = promotion!.CreatedBy;
                amountCondition.CreatedByName = promotion!.CreatedBy;
                await _amountConditionRepository.InsertAsync(amountCondition);
                promotionResponseDto.AmountCondition = ObjectMapper.Map<AmountCondition, AmountConditionDto>(amountCondition);
            }

            // InstallmentCondition
            if (promotionDto.InstallmentConditions is { Count: > 0 })
            {
                var installmentCondition = ObjectMapper.Map<List<CreateInstallmentConditionDto>, List<InstallmentCondition>>(promotionDto.InstallmentConditions);
                installmentCondition?.ForEach(p =>
                {
                    p.PromotionId = promotion!.Id;
                    p.CreatedBy = promotion!.CreatedBy;
                    p.CreatedByName = promotion!.CreatedByName;
                });
                await _installmentConditionRepository.InsertManyAsync(installmentCondition);
                promotionResponseDto.InstallmentConditions = ObjectMapper.Map<List<InstallmentCondition>, List<InstallmentConditionDto>>(installmentCondition);
            }

            // Promotion Exclude
            if (promotionDto.PromotionExcludes?.Any() ?? false)
            {
                var promotionExcludes = await CreatePromotionExcludeAsync(promotionResponseDto, promotionDto.PromotionExcludes);
                promotionResponseDto.PromotionExcludes = promotionExcludes;
            }

            // PaymentCondition
            if (promotionDto.PaymentConditions is { Count: > 0 })
            {
                var paymentConditions = ObjectMapper.Map<List<CreatePaymentConditionDto>, List<PaymentCondition>>(promotionDto.PaymentConditions);
                paymentConditions?.ForEach(p =>
                {
                    p.PromotionId = promotion!.Id;
                    p.CreatedBy = promotion!.CreatedBy;
                    p.CreatedByName = promotion!.CreatedByName;
                });
                await _paymentConditionRepository.InsertManyAsync(paymentConditions);
                promotionResponseDto.PaymentConditions = ObjectMapper.Map<List<PaymentCondition>, List<PaymentConditionDto>>(paymentConditions);
            }
            promotionResponseDto.CostDistributions = GetDistributions(costDistributions);

            await uow.CompleteAsync();
            await uow.SaveChangesAsync();

            //Update redis promotion exclude and sync
            var promotionCodes = promotionResponseDto.PromotionExcludes.Select(p => p.Code).ToList();
            if (promotionCodes is { Count: > 0 })
            {
                await _hangFireAppService.UpdatePromotionExcludeAndSyncDataAsync(promotionCodes);
                //await PromotionExcludeCacheAndSyncEcomAsync(promotionCodes);
            }
        }
        #endregion Create Promotion with SQL

        #region Create Promotion With Redis and ES
        // Event cache promotion
        if (promotionResponseDto != null)
        {
            if (promotionResponseDto.ProvinceConditions is { Count: > 0 })
            {
                await _databaseRedisCache.StringSetAsync($"SyncPromotionShop:{promotionResponseDto.Code}",
                    JsonConvert.SerializeObject(promotionResponseDto.ProvinceConditions),null, (When)CommandFlags.FireAndForget);
            }

            await BuildPromotionCacheAsync(promotionResponseDto);
            await CreateUpdatePromotionDocumentAsync(promotionResponseDto, promotion, filedConfigures);
            return promotionResponseDto;
        }
        #endregion Create Promotion With Redis and ES
        throw new FrtPromotionValidationException(ExceptionCode.BadRequest, _stringLocalize[ExceptionCode.BadRequest], $"không thể thêm mới");
    }

    public async Task<PromotionDto> UpdateAsync(Guid promotionId, UpdatePromotionDto promotionDto)
    {
        using (var uow = _unitOfWorkManager.Begin(new AbpUnitOfWorkOptions { IsTransactional = false }, requiresNew: true))
        {
            // check số lượng đầu vào và đầu ra của giảm giá
            if (promotionDto.PromotionType == PromotionTypeEnum.BuyProductGetDiscount.ToString())
            {
                var itemInputsTotal = 0;
                foreach (var inPut in promotionDto.ItemInputs.Where(p => p.GiftCode.IsNullOrEmpty()))
                {
                    itemInputsTotal++;
                    itemInputsTotal += inPut.ItemInputReplaces != null ? inPut.ItemInputReplaces.Count : 0;
                }

                var outPutTotal = 0;
                foreach (var outPut in promotionDto.Outputs.Where(p => p.GiftCode.IsNullOrEmpty()))
                {
                    outPutTotal++;
                    outPutTotal += outPut.OutputReplaces != null ? outPut.OutputReplaces.Count : 0;
                }

                if (outPutTotal != itemInputsTotal)
                {
                    throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest], $"số lượng sản phẩm đầu vào và đầu ra không bằng nhau");
                }

            }
            var itemInputsEntityValidate = promotionDto.ItemInputs;
            // check line đầu tiên không tồn tại itemCode
            if (itemInputsEntityValidate.Any() && !itemInputsEntityValidate.FirstOrDefault().CategoryCode.IsNullOrEmpty()
             && itemInputsEntityValidate.FirstOrDefault().ItemCode.IsNullOrEmpty()
             && itemInputsEntityValidate.FirstOrDefault().GiftCode.IsNullOrEmpty())
            {
                var countData = await _pIMAppService.CountDataPimCacheAsync(new PimCacheSearch
                {
                    CategoryCode = itemInputsEntityValidate?.FirstOrDefault().CategoryCode,
                    TypeCode = itemInputsEntityValidate?.FirstOrDefault().GroupCode
                });
                if (countData.Total == 0)
                {
                    throw new FrtPromotionValidationException(ExceptionCode.BadRequest, _stringLocalize[ExceptionCode.BadRequest],
                            $"Không tồn tại sản phẩm với " +
                            $"Ngành hàng : {itemInputsEntityValidate?.FirstOrDefault()?.CategoryCode} " +
                            $"Loại hàng: {itemInputsEntityValidate?.FirstOrDefault()?.TypeCode} ");
                }
            }

            #region Check ngày không hợp lệ của shop
            var checkDateShop = promotionDto.ProvinceConditions.Where(c => c.FromDate.Date < promotionDto.FromDate.Value.Date || c.ToDate.Date > promotionDto.ToDate.Value.Date).ToList();
            if (checkDateShop.Any())
            {
                throw new FrtPromotionValidationException(ExceptionCode.BadRequest, _stringLocalize[ExceptionCode.BadRequest],
                    $"Thời gian áp dụng của Vùng/Tỉnh phải trong khoảng thời gian áp dụng của chương trình khuyến mãi");
            }
            #endregion

            var promotionEntity = await _promotionRepository.FindAsync(p => p.Id == promotionId);
            var fieldConfigures = await _fieldConfigureRepository.GetListAsync();
            if (promotionEntity != null)
            {
                await ValidationAsync(promotionDto, fieldConfigures);
                var promotion = ObjectMapper.Map(promotionDto, promotionEntity);

                if (promotionDto.Status == PromotionStatusEnum.Activated && promotionDto.ToDate < DateTime.Today)
                {
                    throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"Không thể cập nhật trạng thái thành hiệu lực vì " +
                        $"{promotionDto.ToDate.Value.Date:dd/MM/yyyy} bé hơn " +
                        $"{DateTime.Today.Date:dd/MM/yyyy}");
                }
                var campaign = await _campaignRepository.FindAsync(p => p.Id == promotionEntity.CampaignId);
                if (campaign.Status == 3)
                {
                    throw new FrtPromotionValidationException(
                        ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"Không thể cập nhật trạng thái thành hiệu lực vì chiến dịch hết hiệu lục");
                }

                MapAuditedUser(ref promotion, "update");

                await _promotionRepository.UpdateAsync(promotion);

                //Check payload có Item Input không?
                if (promotionDto.ItemInputs is { Count: > 0 })
                {
                    //Xóa Item Input Old
                    await _itemInputRepository.BulkDeleteAsync(promotionId);

                    //Xóa Item Input Replace Old
                    await _itemInputReplaceRepository.BulkDeleteAsync(promotionId);
                }
                //Check payload có Item Output không?
                if (promotionDto.Outputs is { Count: > 0 })
                {
                    //Xóa Item Input Old
                    await _outputRepository.BulkDeleteAsync(promotionId);

                    //Xóa Item Input Replace Old
                    await _outputReplaceRepository.BulkDeleteAsync(promotionId);
                }

                var promotionResponseDto = ObjectMapper.Map<Promotion, PromotionDto>(promotion);

                #region Update Costdistribution
                //Delete Costdistribution
                var deleteCostdistribution = await _costDistributionRepository.GetListAsync(c => c.PromotionId == promotionId);
                if (deleteCostdistribution.Any())
                {
                    await _costDistributionRepository.DeleteManyAsync(deleteCostdistribution);
                }

                //Check payload có Costdistribution không?
                if (promotionDto.CostDistributions is { Count: > 0 })
                {
                    var createCostDistributions = promotionDto.CostDistributions.Where(p => p.Id == null).ToList();
                    var createEntity = ObjectMapper.Map<List<UpdateCostDistributionDto>, List<CostDistribution>>(createCostDistributions);
                    createEntity?.ForEach(p =>
                    {
                        p.PromotionId = promotion!.Id;
                        p.CreatedBy = promotion!.CreatedBy;
                        p.CreatedByName = promotion!.CreatedByName;
                    });
                    await _costDistributionRepository.InsertManyAsync(createEntity);
                    promotionResponseDto.CostDistributions = GetDistributions(createEntity);
                }
                #endregion

                #region Update AmoutCondition
                //Delete AmoutCondition
                var deleteAmoutCondition = await _amountConditionRepository.FindAsync(c => c.PromotionId == promotionId);
                if (deleteAmoutCondition is not null)
                {
                    await _amountConditionRepository.DeleteAsync(deleteAmoutCondition);
                }

                //Check payload có AmoutCondition không?
                if (promotionDto.AmountCondition is not null)
                {
                    var createEntity = ObjectMapper.Map<UpdateAmountConditionDto, AmountCondition>(promotionDto.AmountCondition);
                    createEntity.PromotionId = promotionId;
                    await _amountConditionRepository.InsertAsync(createEntity);
                    promotionResponseDto.AmountCondition = ObjectMapper.Map<AmountCondition, AmountConditionDto>(createEntity);
                }
                #endregion

                #region Update Quota
                //Delete Quota
                var deleteQuota = await _quotaRepository.FindAsync(c => c.PromotionId == promotionId);
                if (deleteQuota is not null)
                {
                    await _quotaRepository.DeleteAsync(deleteQuota);
                }

                //Check payload có Quota không?
                if (promotionDto.Quota is not null)
                {
                    var checkQuota = await CheckRuleQuotaAsync(promotionId, promotionDto.Quota);
                    var createEntity = ObjectMapper.Map<UpdateQuotaDto, Quota>(promotionDto.Quota);
                    createEntity.PromotionCode = promotion.Code;
                    createEntity.PromotionId = promotionId;
                    await _quotaRepository.InsertAsync(createEntity);

                    promotionResponseDto.Quota = ObjectMapper.Map<Quota, QuotaDto>(createEntity);
                }
                #endregion

                #region Update InputExcludes
                //Delete InputExcludes
                var deleteInputExcludes = await _itemInputExcludeRepository.GetListAsync(c => c.PromotionId == promotionId);
                if (deleteInputExcludes.Any())
                {
                    await _itemInputExcludeRepository.BulkDeleteAsync(promotionId);
                }

                //Check payload có InputExcludes không?
                if (promotionDto.InputExcludes is { Count: > 0 })
                {
                    var createInputExcludes = promotionDto.InputExcludes.Where(p => p.Id == null).ToList();
                    var createEntity = ObjectMapper.Map<List<UpdateItemInputExcludeDto>, List<ItemInputExclude>>(createInputExcludes);
                    createEntity?.ForEach(p =>
                    {
                        p.PromotionId = promotion!.Id;
                        p.CreatedBy = promotion!.CreatedBy;
                        p.CreatedByName = promotion!.CreatedByName;
                    });
                    await _itemInputExcludeRepository.InsertManyAsync(createEntity);

                    promotionResponseDto.ItemInputExcludes = ObjectMapper.Map<List<ItemInputExclude>, List<ItemInputExcludeDto>>(createEntity);
                }
                #endregion

                #region Update Input

                //Check payload có Item Input không?
                if (promotionDto.ItemInputs is { Count: > 0 })
                {
                    var itemInputsEntity = ObjectMapper.Map<List<UpdateItemInputDto>, List<ItemInput>>(promotionDto.ItemInputs);

                    itemInputsEntity?.ForEach(p =>
                    {
                        p.PromotionId = promotion!.Id;
                        p.CreatedBy = promotion!.CreatedBy;
                        p.CreatedByName = promotion!.CreatedByName;
                    });

                    await _itemInputRepository.InsertManyAsync(itemInputsEntity);

                    promotionResponseDto.ItemInputs = ObjectMapper.Map<List<ItemInput>, List<ItemInputDto>>(itemInputsEntity);

                    var itemInputReplaceDtos = promotionDto.ItemInputs.SelectMany(c => c.ItemInputReplaces).ToList();
                    var listItemReplaces = new List<ItemInputReplace>();
                    if (itemInputReplaceDtos.Any())
                    {
                        var createItemInputReplaceEntity = ObjectMapper.Map<List<UpdateItemInputReplaceDto>, List<ItemInputReplace>>(itemInputReplaceDtos);
                        var newItemReplaces = itemInputReplaceDtos.Select(x =>
                        {
                            var itemReplace = ObjectMapper.Map<UpdateItemInputReplaceDto, ItemInputReplace>(x);
                            itemReplace.InputItemId = promotionResponseDto.ItemInputs.FirstOrDefault(x => x.LineNumber == itemReplace.LineNumber)?.Id ?? Guid.Empty;
                            itemReplace.PromotionId = promotion!.Id;
                            itemReplace.CreatedBy = promotion!.CreatedBy;
                            itemReplace.CreatedByName = promotion!.CreatedByName;
                            return itemReplace;
                        }).ToList();
                        await _itemInputReplaceRepository.InsertManyAsync(newItemReplaces);
                        listItemReplaces.AddRange(newItemReplaces);
                    }

                    if (listItemReplaces.Any())
                    {
                        promotionResponseDto.ItemInputs.ForEach(p =>
                                    p.ItemInputReplaces = ObjectMapper.Map<List<ItemInputReplace>, List<ItemInputReplaceDto>>(
                                        listItemReplaces.Where(z => z.InputItemId == p.Id).ToList()));
                    }
                }
                #endregion

                #region Update Output

                //Check payload có Item Output không?
                if (promotionDto.Outputs is { Count: > 0 })
                {
                    var createEntity = ObjectMapper.Map<List<UpdateOutputDto>, List<Output>>(promotionDto.Outputs);
                    createEntity?.ForEach(p =>
                    {
                        p.PromotionId = promotion!.Id;
                        p.CreatedBy = promotion!.CreatedBy;
                        p.CreatedByName = promotion!.CreatedByName;
                    });
                    await _outputRepository.InsertManyAsync(createEntity);

                    promotionResponseDto.Outputs = ObjectMapper.Map<List<Output>, List<OutputDto>>(createEntity);

                    var itemOutputReplaceDtos = promotionDto.Outputs.SelectMany(c => c.OutputReplaces).ToList();
                    var listItemOutputReplaces = new List<OutputReplace>();
                    if (itemOutputReplaceDtos.Any())
                    {
                        var createItemOutputReplaceEntity = ObjectMapper.Map<List<UpdateOutputReplaceDto>, List<OutputReplace>>(itemOutputReplaceDtos);
                        var newItemReplaces = itemOutputReplaceDtos.Select(x =>
                        {
                            var itemReplace = ObjectMapper.Map<UpdateOutputReplaceDto, OutputReplace>(x);
                            itemReplace.OutputId = promotionResponseDto.Outputs.FirstOrDefault(x => x.LineNumber == itemReplace.LineNumber)?.Id ?? Guid.Empty;
                            itemReplace.PromotionId = promotion!.Id;
                            itemReplace.CreatedBy = promotion!.CreatedBy;
                            itemReplace.CreatedByName = promotion!.CreatedByName;
                            return itemReplace;
                        }).ToList();
                        await _outputReplaceRepository.InsertManyAsync(newItemReplaces);
                        listItemOutputReplaces.AddRange(newItemReplaces);
                    }

                    if (listItemOutputReplaces.Any())
                    {
                        promotionResponseDto.Outputs.ForEach(p =>
                                    p.OutputReplaces = ObjectMapper.Map<List<OutputReplace>, List<OutputReplaceDto>>(
                                        listItemOutputReplaces.Where(z => z.OutputId == p.Id).ToList()));
                    }
                }
                #endregion

                #region Update InstallmentConditions
                //Delete InstallmentConditions
                var deleteInstallmentCondition = await _installmentConditionRepository.GetListAsync(c => c.PromotionId == promotionId);
                if (deleteInstallmentCondition.Any())
                {
                    await _installmentConditionRepository.BulkDeleteAsync(promotionId);
                }

                //Check payload có InstallmentConditions không?
                if (promotionDto.InstallmentConditions is { Count: > 0 })
                {
                    var createInstallmentConditions = promotionDto.InstallmentConditions.Where(p => p.Id == null).ToList();
                    var createEntity = ObjectMapper.Map<List<UpdateInstallmentConditionDto>, List<InstallmentCondition>>(createInstallmentConditions);
                    createEntity?.ForEach(p =>
                    {
                        p.PromotionId = promotion!.Id;
                        p.CreatedBy = promotion!.CreatedBy;
                        p.CreatedByName = promotion!.CreatedByName;
                    });
                    await _installmentConditionRepository.InsertManyAsync(createEntity);
                    promotionResponseDto.InstallmentConditions = ObjectMapper.Map<List<InstallmentCondition>, List<InstallmentConditionDto>>(createEntity);
                }
                #endregion

                #region Update ShopConditions

                var listRegion = await _provinceConditionRepository.BulkDeleteAsync(promotionId);
                if (listRegion is { Count: > 0 })
                {
                    await _hangFireAppService.ScheduleClearProvinceConditionAsync(listRegion, promotion.Code);
                }
                //Check payload có ProvinceConditions không?
                if (promotionDto.ProvinceConditions is { Count: > 0 })
                {
                    var createShopConditions = promotionDto.ProvinceConditions.Where(p => p.Id == null).ToList();
                    var createEntity = ObjectMapper.Map<List<UpdateProvinceConditionDto>, List<ProvinceCondition>>(createShopConditions);
                    createEntity?.ForEach(p =>
                    {
                        p.PromotionId = promotion!.Id;
                        p.CreatedBy = promotion!.CreatedBy;
                        p.CreatedByName = promotion!.CreatedByName;
                        p.PromotionCode = promotion!.Code;
                    });
                    await _provinceConditionRepository.InsertManyAsync(createEntity);
                    promotionResponseDto.ProvinceConditions = ObjectMapper.Map<List<ProvinceCondition>, List<ProvinceConditionDto>>(createEntity);
                }
                #endregion

                #region Update ExtraConditions
                //Delete RegionConditions
                var deleteExtraCondition = await _extraConditionRepository.GetListAsync(c => c.PromotionId == promotionId);
                if (deleteExtraCondition.Any())
                {
                    await _extraConditionRepository.DeleteManyAsync(deleteExtraCondition);
                }

                //Check payload có RegionConditions không?
                if (promotionDto.ExtraConditions is { Count: > 0 })
                {
                    var createEntity = ObjectMapper.Map<List<UpdateExtraConditionDto>, List<ExtraCondition>>(promotionDto.ExtraConditions);
                    createEntity?.ForEach(p =>
                    {
                        p.PromotionCode = promotion.Code;
                        p.PromotionId = promotion!.Id;
                        p.CreatedBy = promotion!.CreatedBy;
                        p.CreatedByName = promotion!.CreatedByName;
                    });
                    await _extraConditionRepository.InsertManyAsync(createEntity);

                    promotionResponseDto.ExtraConditions = ObjectMapper.Map<List<ExtraCondition>, List<ExtraConditionDto>>(createEntity);
                }
                #endregion

                #region Update Promotion Excludes
                var promotionExcludes = await UpdatePromotionExcludeAsync(promotionResponseDto, promotionDto.PromotionExcludes);
                promotionResponseDto.PromotionExcludes = promotionExcludes;
                #endregion

                #region Update Payment Conditions
                var deletePaymentCondition = await _paymentConditionRepository.GetListAsync(c => c.PromotionId == promotionId);
                if (deletePaymentCondition.Any())
                {
                    await _paymentConditionRepository.BulkDeleteAsync(promotionId);
                }

                //Check payload có PaymentConditions không?
                if (promotionDto.PaymentConditions is { Count: > 0 })
                {
                    var createPaymentConditions = promotionDto.PaymentConditions.Where(p => p.Id == null).ToList();
                    var createEntity = ObjectMapper.Map<List<UpdatePaymentConditionDto>, List<PaymentCondition>>(createPaymentConditions);
                    createEntity?.ForEach(p =>
                    {
                        p.PromotionId = promotion!.Id;
                        p.CreatedBy = promotion!.CreatedBy;
                        p.CreatedByName = promotion!.CreatedByName;
                    });
                    await _paymentConditionRepository.InsertManyAsync(createEntity);
                    promotionResponseDto.PaymentConditions = ObjectMapper.Map<List<PaymentCondition>, List<PaymentConditionDto>>(createEntity);
                }
                #endregion

                await uow.CompleteAsync();
                await uow.SaveChangesAsync();


                await CreateUpdatePromotionDocumentAsync(promotionResponseDto, promotion, fieldConfigures);
                //await _elasticSearchAppService.DeleteInputReplacesAsync(promotion.Code);
                //await _elasticSearchAppService.DeleteOutputReplacesAsync(promotion.Code);

                var costDistributionRepository = await _costDistributionRepository.GetListAsync(p => p.PromotionId == promotionResponseDto.Id);
                promotionResponseDto.CostDistributions = GetDistributions(costDistributionRepository);

                if (promotionResponseDto != null && promotion.Status != (int)PromotionStatusEnum.InActive)
                {
                    if (promotionResponseDto.ProvinceConditions is { Count: > 0 })
                    {
                        await _databaseRedisCache.StringSetAsync($"SyncPromotionShop:{promotionResponseDto.Code}",
                                JsonConvert.SerializeObject(promotionResponseDto.ProvinceConditions),null, (When)CommandFlags.FireAndForget);
                    }

                    await ReCachePromotionDetailAsync(promotionId, ActionType.Update);
                }

                if (promotionResponseDto != null && promotion.Status == (int)PromotionStatusEnum.InActive)
                {
                    await TimeExpireCacheAsync(new List<PromotionTimeExpireEto>
                    {
                        new PromotionTimeExpireEto
                        {
                            PromotionId = promotionId,
                            PromotionCode = promotionResponseDto.Code,
                            PromotionType = promotionResponseDto.PromotionType,
                            ProvinceConditions = promotionResponseDto?.ProvinceConditions?.Select(p => p.ProvinceCode).ToList()
                        }
                    });
                }

                if (promotionResponseDto.Quota != null)
                {
                    await _redisCacheService.UpdateQuotaAsync(promotionResponseDto.Code, promotionResponseDto.Quota.ResetQuotaType);
                }

                return promotionResponseDto;
            }
        }
        throw new FrtPromotionValidationException(ExceptionCode.NotFound, _stringLocalize[ExceptionCode.NotFound], $"promotion : {promotionId}");
    }


    #endregion

    #region Define documents elk

    private async Task CreateUpdatePromotionDocumentAsync(PromotionDto promotionDto, Promotion promotion, List<FieldConfigure> fieldConfigures)
    {
        try
        {
            var promotionDocument = ObjectMapper.Map<PromotionDto, PromotionDocument>(promotionDto);
            promotionDocument.LastModificationTime = promotion.LastModificationTime;
            promotionDocument = SetFieldConfigureDocumentsAsync(promotionDocument, promotion, fieldConfigures);

            //Quota
            promotionDocument.Quota = ObjectMapper.Map<QuotaDto, QuotaDocument>(promotionDto.Quota);
            promotionDocument.AmountCondition = ObjectMapper.Map<AmountConditionDto, AmountConditionDocument>(promotionDto.AmountCondition);
            promotionDocument.ExtraConditions = ObjectMapper.Map<List<ExtraConditionDto>, List<ExtraConditionDocument>>(promotionDto.ExtraConditions);
            promotionDocument.CostDistributions = ObjectMapper.Map<List<CostDistributionDto>, List<CostDistributionDocument>>(promotionDto.CostDistributions);
            promotionDocument.InstallmentConditions = ObjectMapper.Map<List<InstallmentConditionDto>, List<InstallmentConditionDocument>>(promotionDto.InstallmentConditions);
            promotionDocument.PaymentConditions = ObjectMapper.Map<List<PaymentConditionDto>, List<PaymentConditionDocument>>(promotionDto.PaymentConditions);
            promotionDocument.Outputs = ObjectMapper.Map<List<OutputDto>, List<OutputDocument>>(promotionDto.Outputs);
            promotionDocument.Inputs = ObjectMapper.Map<List<ItemInputDto>, List<InputDocument>>(promotionDto.ItemInputs);
            promotionDocument = await SetInputOrOutPutDocumentsAsync(promotionDocument);

            await _elasticSearchAppService.CreateUpdatePromotionAsync(promotionDocument);
            await CreateUpdatePromotionConditionDocumentAsync(promotionDto);
        }
        catch
        {
            throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                _stringLocalize[ExceptionCode.NotFound], $"promotion : {promotionDto.Id}");
        }
    }

    private PromotionDocument SetFieldConfigureDocumentsAsync(PromotionDocument promotionDocument, Promotion promotion, List<FieldConfigure> fieldConfigures)
    {
        var listChannels = JsonConvert.DeserializeObject<List<string>>(promotion.Channels);
        if (listChannels is not null)
        {
            foreach (var channelCode in listChannels)
            {
                promotionDocument.Channels.Add(new BaseMasterDataDocument()
                {
                    Code = channelCode,
                    Name = fieldConfigures?.FirstOrDefault(p => p.Code == channelCode)?.Name,
                });
            }
        }

        var listOrderTypes = JsonConvert.DeserializeObject<List<string>>(promotion.OrderTypes);
        if (listOrderTypes is not null)
        {
            foreach (var orderTypeCode in listOrderTypes)
            {
                promotionDocument.OrderTypes.Add(new BaseMasterDataDocument()
                {
                    Code = orderTypeCode,
                    Name = fieldConfigures?.FirstOrDefault(p => p.Code == orderTypeCode)?.Name,
                });
            }
        }

        var listStoreType = JsonConvert.DeserializeObject<List<string>>(promotion.StoreTypes);
        if (listStoreType is not null)
        {
            foreach (var storeTypeCode in listStoreType)
            {
                promotionDocument.StoreTypes.Add(new BaseMasterDataDocument()
                {
                    Code = storeTypeCode,
                    Name = fieldConfigures?.FirstOrDefault(p => p.Code == storeTypeCode)?.Name,
                });
            }
        }

        var listCustomerGroups = JsonConvert.DeserializeObject<List<string>>(promotion.CustomerGroups);
        if (listCustomerGroups is not null)
        {
            foreach (var customerCode in listCustomerGroups)
            {
                promotionDocument.CustomerGroups.Add(new BaseMasterDataDocument()
                {
                    Code = customerCode,
                    Name = fieldConfigures?.FirstOrDefault(p => p.Code == customerCode)?.Name,
                });
            }
        }

        var listSourceOrders = JsonConvert.DeserializeObject<List<string>>(promotion.SourceOrders);
        if (listSourceOrders is not null)
        {
            foreach (var sourceOrdersCode in listSourceOrders)
            {
                promotionDocument.SourceOrders.Add(new BaseMasterDataDocument()
                {
                    Code = sourceOrdersCode,
                    Name = fieldConfigures?.FirstOrDefault(p => p.Code == sourceOrdersCode)?.Name,
                });
            }
        }

        return promotionDocument;
    }

    private async Task<PromotionDocument> SetInputOrOutPutDocumentsAsync(PromotionDocument promotionDocument)
    {
        var inPutDocuments = new List<InputDocument>();
        foreach (var itemInput in promotionDocument.Inputs)
        {
            // Ngành hàng rã item es
            if (!itemInput.CategoryCode.IsNullOrEmpty() && itemInput.ItemCode.IsNullOrEmpty())
            {
                var pimProductDto = await _pIMAppService.GetListProductByPimCacheAsync(new PimCacheSearch
                {
                    CategoryCode = itemInput.CategoryCode,
                    TypeCode = itemInput.TypeCode
                });

                if (pimProductDto != null)
                {
                    var product = pimProductDto?.Data?.FirstOrDefault();
                    itemInput.ItemCode = product?.ItemCode;
                    itemInput.ItemName = product?.ItemName;
                    itemInput.CategoryCode = product?.CategoryUniqueId;
                    itemInput.CategoryName = product?.CategoryName;
                    itemInput.TypeCode = product?.GroupUniqueId; //=> Group pim cam lc là loại hàng
                    itemInput.TypeName = product?.GroupName; //=> Group pim cam lc là loại hàng
                    inPutDocuments.Add(itemInput);
                }
            }
            // Sản phẩm es
            else if (!itemInput.ItemCode.IsNullOrEmpty())
            {
                if (!itemInput.ItemCode.IsNullOrEmpty())
                {
                    var pimProductDto = await _pIMAppService.GetProductByPimCacheAsync(itemInput.ItemCode);
                    if (pimProductDto != null)
                    {
                        var product = pimProductDto?.Data?.FirstOrDefault();
                        itemInput.ItemCode = product?.ItemCode;
                        itemInput.ItemName = product?.ItemName;
                        itemInput.CategoryCode = product?.CategoryUniqueId;
                        itemInput.CategoryName = product?.CategoryName;
                        itemInput.TypeCode = product?.GroupUniqueId; //=> Group pim cam lc là loại hàng
                        itemInput.TypeName = product?.GroupName; //=> Group pim cam lc là loại hàng
                    }
                    inPutDocuments.Add(itemInput);
                }
                else
                {
                    inPutDocuments.Add(itemInput);
                }
            }
            // Voucher es
            else
            {
                inPutDocuments.Add(itemInput);
            }
        }

        var outPutDocuments = new List<OutputDocument>();
        foreach (var itemOutput in promotionDocument.Outputs)
        {
            // Ngành hàng rã item es
            if (!itemOutput.CategoryCode.IsNullOrEmpty() && itemOutput.ItemCode.IsNullOrEmpty())
            {
                var pimProductDto = await _pIMAppService.GetListProductByPimCacheAsync(new PimCacheSearch
                {
                    CategoryCode = itemOutput.CategoryCode,
                    TypeCode = itemOutput.TypeCode
                });
                if (pimProductDto.Data is { Count: > 0 })
                {
                    var product = pimProductDto?.Data?.FirstOrDefault();
                    itemOutput.ItemCode = product?.ItemCode;
                    itemOutput.ItemName = product?.ItemName;
                    itemOutput.CategoryCode = product?.CategoryUniqueId;
                    itemOutput.CategoryName = product?.CategoryName;
                    itemOutput.TypeCode = product?.GroupUniqueId; //=> Group pim cam lc là loại hàng
                    itemOutput.TypeName = product?.GroupName; //=> Group pim cam lc là loại hàng
                    outPutDocuments.Add(itemOutput);
                }

            }
            // Sản phẩm es
            else if (!itemOutput.ItemCode.IsNullOrEmpty())
            {
                if (!itemOutput.ItemCode.IsNullOrEmpty())
                {
                    var pimProductDto = await _pIMAppService.GetProductByPimCacheAsync(itemOutput.ItemCode);
                    if (pimProductDto.Data is { Count: > 0 })
                    {
                        var product = pimProductDto?.Data?.FirstOrDefault();
                        itemOutput.ItemCode = product?.ItemCode;
                        itemOutput.ItemName = product?.ItemName;
                        itemOutput.CategoryCode = product?.CategoryUniqueId;
                        itemOutput.CategoryName = product?.CategoryName;
                        itemOutput.TypeCode = product?.GroupUniqueId; //=> Group pim cam lc là loại hàng
                        itemOutput.TypeName = product?.GroupName;     //=> Group pim cam lc là loại hàng
                    }
                    outPutDocuments.Add(itemOutput);
                }
                else
                {
                    outPutDocuments.Add(itemOutput);
                }
            }
            // Voucher es
            else
            {
                outPutDocuments.Add(itemOutput);
            }
        }

        promotionDocument.Inputs = inPutDocuments;
        promotionDocument.Outputs = outPutDocuments;

        return promotionDocument;
    }

    private async Task CreateUpdatePromotionConditionDocumentAsync(PromotionDto promotionDto)
    {
        if (promotionDto.ProvinceConditions?.Any() ?? false)
        {
            var shopConditionDocuments = ObjectMapper.Map<List<ProvinceConditionDto>, List<ProvinceConditionDocument>>(promotionDto.ProvinceConditions);
            shopConditionDocuments.ForEach(p => p.PromotionCode = promotionDto.Code);
            await _elasticSearchAppService.CreateUpdatePromotionShopConditionAsync(shopConditionDocuments);
        }
    }

    #endregion

    #region Private funcition

    private void MapAuditedUser(ref Promotion promotion, string action)
    {
        var userId = CurrentUser?.FindClaim("employee_code")?.Value ?? "";
        var userName = CurrentUser?.FindClaim("full_name")?.Value ?? "";
        switch (action)
        {
            case "create":
                {
                    promotion.CreatedBy = userId;
                    promotion.CreatedByName = userName;
                    return;
                }
            case "update":
                {
                    promotion.UpdateBy = userId;
                    promotion.UpdateByName = userName;
                    return;
                }
        }
    }

    private async Task ValidationAsync(PromotionBaseDto promotion, List<FieldConfigure> fieldConfigures)
    {
        if (!fieldConfigures?.Any() ?? false)
        {
            throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                _stringLocalize[ExceptionCode.NotFound], "không tìm thấy table fieldConfigure");
        }

        var channels = fieldConfigures!.Where(p => p.Group == "Channel").Select(p => p.Code).ToList();
        if (promotion.Channels is { Length: > 0 }
            && promotion.Channels.Any(p => !channels.Contains(p)))
        {
            throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                _stringLocalize[ExceptionCode.NotFound],
                $"channel không đúng : {JsonConvert.SerializeObject(promotion.Channels)}");
        }

        var orderType = fieldConfigures.Where(p => p.Group == "OrderType").Select(p => p.Code).ToList();
        if (promotion.OrderTypes is { Length: > 0 }
            && promotion.OrderTypes.Any(p => !orderType.Contains(p)))
        {
            throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                _stringLocalize[ExceptionCode.NotFound],
                $"orderType không đúng : {JsonConvert.SerializeObject(promotion.OrderTypes)}");
        }

        var storeType = fieldConfigures.Where(p => p.Group == "StoreType").Select(p => p.Code).ToList();
        if (promotion.StoreTypes is { Length: > 0 }
            && promotion.StoreTypes.Any(p => !storeType.Contains(p)))
        {
            throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                _stringLocalize[ExceptionCode.NotFound],
                $"orderType không đúng : {JsonConvert.SerializeObject(promotion.StoreTypes)}");
        }

        var campaign = await _campaignRepository.FindAsync(promotion.CampaignId);
        if (campaign == null)
        {
            throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                _stringLocalize[ExceptionCode.BadRequest], "Chiến dịch không tồn tại");
        }
        if (campaign.Status == (int)CampaignStatusEnum.InActive)
        {
            throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                _stringLocalize[ExceptionCode.BadRequest], "Chiến dịch không còn hiệu lực");
        }
        if (campaign.FromDate.Date > promotion.FromDate!.Value.Date
            || campaign.ToDate.Date < promotion.ToDate!.Value.Date)
        {
            throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                _stringLocalize[ExceptionCode.BadRequest], $"CTKM không thuộc khoản thời gian của chiến dịch: " +
                $"{campaign.Code} - Ngày bắt đầu: {campaign.FromDate:dd/MM/yyyy} - Ngày kết thúc: {campaign.ToDate:dd/MM/yyyy}");
        }
    }

    public async Task ReCachePromotionDetailAsync(Guid promotionId, ActionType actionType = ActionType.Create)
    {
        var promotionDto = await GetAsync(promotionId);
        if (promotionDto != null)
        {
            await BuildPromotionCacheAsync(promotionDto, actionType);
        }
    }

    public async Task BuildPromotionCacheAsync(PromotionDto promotionResponseDto, ActionType actionType = ActionType.Create)
    {
        if (promotionResponseDto != null)
        {
            var promotionCache = new EventPromotionEto { ActionType = actionType };
            promotionCache = ObjectMapper.Map(promotionResponseDto, promotionCache);
            await _distributedEventBus.PublishAsync(promotionCache);

            if (promotionResponseDto.ProvinceConditions is { Count: > 0 })
            {
                await RegionConditionCacheAsync(promotionResponseDto);
            }
        }
    }

    public async Task RegionConditionCacheAsync(PromotionDto promotionResponseDto)
    {
        if (promotionResponseDto.ProvinceConditions is { Count: > 0 })
        {
            await _distributedEventBus.PublishAsync(
                new EventConditionEto
                {
                    PromotionCode = promotionResponseDto.Code,
                    ProvinceConditions = promotionResponseDto.ProvinceConditions.Select(p =>
                        new ProvinceConditionEto
                        {
                            ProvinceCode = p.ProvinceCode,
                            ProvinceName = p.ProvinceName,
                            FromDate = p.FromDate,
                            ToDate = p.ToDate
                        }).ToList()
                });
        }
    }

    private async Task<List<ItemInputDto>> CreateItemInputAsync(Promotion promotion, List<CreateItemInputDto> itemInputDto)
    {
        using (var uow = _unitOfWorkManager.Begin(new AbpUnitOfWorkOptions { IsTransactional = false }, requiresNew: true))
        {
            var itemInputs = ObjectMapper.Map<List<CreateItemInputDto>, List<ItemInput>>(itemInputDto);
            itemInputs = itemInputs.Select(p =>
            {
                p.PromotionId = promotion!.Id;
                p.CreatedBy = promotion!.CreatedBy;
                p.CreatedByName = promotion!.CreatedByName;
                if (!promotion.UpdateBy.IsNullOrEmpty())
                {
                    p.UpdateBy = promotion.UpdateBy;
                    p.UpdateByName = promotion!.UpdateByName;
                }
                return p;
            }).ToList();
            await _itemInputRepository.InsertManyAsync(itemInputs);
            var itemInputResponse = ObjectMapper.Map<List<ItemInput>, List<ItemInputDto>>(itemInputs);

            var itemInputsReplaceDto = itemInputDto.SelectMany(p => p.ItemInputReplaces).ToList();
            if (itemInputsReplaceDto?.Any() ?? false)
            {
                var itemInputsReplaceEntity = new List<ItemInputReplace>();
                var itemInputsReplace = ObjectMapper.Map<List<CreateItemInputReplaceDto>, List<ItemInputReplace>>(itemInputsReplaceDto);
                foreach (var inputsReplace in itemInputs!
                             .Select(itemInput => itemInputsReplace!
                                 .Where(p => p.LineNumber == itemInput.LineNumber)
                                 .Select(inputsRep =>
                                 {
                                     inputsRep.InputItemId = itemInput!.Id;
                                     inputsRep.PromotionId = promotion!.Id;
                                     inputsRep.CreatedBy = promotion!.CreatedBy;
                                     inputsRep.CreatedByName = promotion!.CreatedByName;
                                     return inputsRep;
                                 }).ToList()))
                {
                    itemInputsReplaceEntity.AddRange(inputsReplace);
                }

                if (itemInputsReplaceEntity.Any())
                {
                    await _itemInputReplaceRepository.InsertManyAsync(itemInputsReplaceEntity);
                    itemInputResponse.ForEach(p =>
                        p.ItemInputReplaces = ObjectMapper.Map<List<ItemInputReplace>, List<ItemInputReplaceDto>>(
                            itemInputsReplaceEntity.Where(z => z.InputItemId == p.Id).ToList()));
                }
            }

            await uow.CompleteAsync();
            await uow.SaveChangesAsync();
            return itemInputResponse;
        }

    }

    private async Task<List<OutputDto>> ProcessOutputItemAsync(Promotion promotion, List<CreateOutputDto> outputDto, List<ItemInputDto> itemInputs)
    {
        using (var uow = _unitOfWorkManager.Begin(new AbpUnitOfWorkOptions { IsTransactional = false }, requiresNew: true))
        {
            var oldItemInputs = await _outputRepository.GetListAsync(x => x.PromotionId == promotion.Id);
            if (oldItemInputs?.Any() ?? false)
            {
                await _outputRepository.DeleteManyAsync(oldItemInputs.Select(x => x.Id));
                var oldOutputReplaces = await _outputReplaceRepository.GetListAsync(x => x.PromotionId == promotion.Id);
                if (oldOutputReplaces?.Any() ?? false)
                {
                    await _outputReplaceRepository.DeleteManyAsync(oldOutputReplaces.Select(x => x.Id));
                }
            }

            var outputs = ObjectMapper.Map<List<CreateOutputDto>, List<Output>>(outputDto);
            outputs = outputs?.Select(p =>
            {
                p.ItemInputId = itemInputs.FirstOrDefault(z => z.LineNumber == p.LineNumber)?.Id;
                p.PromotionId = promotion!.Id;
                p.CreatedBy = promotion!.CreatedBy;
                p.CreatedByName = promotion!.CreatedByName;
                return p;
            }).ToList();
            await _outputRepository.InsertManyAsync(outputs);
            var outputResponse = ObjectMapper.Map<List<Output>, List<OutputDto>>(outputs);

            var outputsReplaceDto = outputDto.SelectMany(p => p.OutputReplaces).ToList();
            if (outputsReplaceDto?.Any() ?? false)
            {
                var outputReplaceEntity = new List<OutputReplace>();
                var outputsReplaces =
                    ObjectMapper.Map<List<CreateOutputReplaceDto>, List<OutputReplace>>(outputsReplaceDto);
                foreach (var item in outputsReplaces)
                {
                    var outPutReference = outputs!.FirstOrDefault(p => p.LineNumber == item.LineNumber);
                    if (outPutReference != null)
                    {
                        item.PromotionId = promotion!.Id;
                        item.OutputId = outPutReference!.Id;
                        item.ItemInputReplaceId = itemInputs!
                            .FirstOrDefault(z => z.Id == outPutReference.ItemInputId)?.ItemInputReplaces
                            .FirstOrDefault()?.Id;
                        outputReplaceEntity.Add(item);
                    }
                }

                if (outputsReplaceDto?.Any() ?? false)
                {
                    await _outputReplaceRepository.InsertManyAsync(outputReplaceEntity);
                    outputResponse.ForEach(p =>
                       p.OutputReplaces = ObjectMapper.Map<List<OutputReplace>, List<OutputReplaceDto>>(
                           outputReplaceEntity.Where(z => z.OutputId == p.Id).ToList()));
                }
            }

            await uow.CompleteAsync();
            await uow.SaveChangesAsync();

            return outputResponse;
        }
    }

    private async Task<List<ProvinceConditionDto>> ProcessRegionConditionAsync(Promotion promotion, List<ProvinceCondition> provinceConditions)
    {
        using var uow = _unitOfWorkManager.Begin(isTransactional: true);
        {
            var oldShopConditions = await _provinceConditionRepository.GetListAsync(x => x.PromotionId == promotion.Id);
            if (!oldShopConditions?.Any() ?? false)
            {
                provinceConditions?.ForEach(p =>
                {
                    p.PromotionId = promotion!.Id;
                    p.PromotionCode = promotion!.Code;
                    p.CreatedBy = promotion!.CreatedBy;
                    p.CreatedByName = promotion!.CreatedByName;
                });
                await _provinceConditionRepository.InsertManyAsync(provinceConditions);
                return ObjectMapper.Map<List<ProvinceCondition>, List<ProvinceConditionDto>>(provinceConditions);
            }

            var shopResponse = new List<ProvinceConditionDto>();
            var newShopConditions = provinceConditions.Where(a => !oldShopConditions!.Select(x => x.ProvinceCode).Contains(a.ProvinceCode)).ToList();
            if (newShopConditions.Any())
            {
                newShopConditions?.ForEach(p =>
                {
                    p.PromotionId = promotion!.Id;
                    p.PromotionCode = promotion!.Code;
                    p.CreatedBy = promotion!.CreatedBy;
                    p.CreatedByName = promotion!.CreatedByName;
                });
                await _provinceConditionRepository.InsertManyAsync(newShopConditions);
                shopResponse = ObjectMapper.Map<List<ProvinceCondition>, List<ProvinceConditionDto>>(newShopConditions);
            }

            var updateShopConditions = oldShopConditions!.Where(a => provinceConditions.Select(x => x.ProvinceCode).Contains(a.ProvinceCode))
                                                    .Select(x => ObjectMapper.Map(provinceConditions.FirstOrDefault(b => b.ProvinceCode.Equals(x.ProvinceCode)), x)).ToList();
            if (updateShopConditions.Any())
            {
                updateShopConditions.ForEach(x =>
                {
                    x.UpdateBy = promotion.UpdateBy;
                    x.UpdateByName = promotion.UpdateByName;
                });
                await _provinceConditionRepository.UpdateManyAsync(updateShopConditions);
                shopResponse.AddRange(ObjectMapper.Map<List<ProvinceCondition>, List<ProvinceConditionDto>>(updateShopConditions));
            }

            var deleteShopConditions = oldShopConditions.Where(x => !provinceConditions.Select(a => a.ProvinceCode).Contains(x.ProvinceCode)).ToList();
            if (oldShopConditions.Any())
            {
                await _provinceConditionRepository.DeleteManyAsync(deleteShopConditions.Select(x => x.Id));
            }

            return shopResponse;
        }
    }

    #endregion

    #region ReCache Promotion

    public async Task<PromotionDto> GetReCacheAsync(string promotionCode)
    {
        using (var uow = _unitOfWorkManager.Begin(new AbpUnitOfWorkOptions { IsTransactional = false }, requiresNew: true))
        {
            var promotionEntity = await _promotionRepository.FindAsync(p => p.Code == promotionCode);
            var fieldConfigures = await _fieldConfigureRepository.GetListAsync();
            if (promotionEntity != null)
            {
                var promotionDto = ObjectMapper.Map<Promotion, PromotionDto>(promotionEntity);
                var promotionDocument = ObjectMapper.Map<PromotionDto, PromotionDocument>(promotionDto);

                var itemInputEntity = await _itemInputRepository.GetListAsync(p => p.PromotionId == promotionEntity.Id);
                if (itemInputEntity != null)
                {
                    promotionDto.ItemInputs = ObjectMapper.Map<List<ItemInput>, List<ItemInputDto>>(itemInputEntity)?
                    .OrderBy(x => x.LineNumber)
                    .ToList();
                }

                var itemInputReplaceEntity = await _itemInputReplaceRepository.GetListAsync(p => p.PromotionId == promotionEntity.Id);
                if (itemInputReplaceEntity is { Count: > 0 })
                {
                    promotionDto.ItemInputs.ForEach(p =>
                        p.ItemInputReplaces = ObjectMapper.Map<List<ItemInputReplace>, List<ItemInputReplaceDto>>(
                            itemInputReplaceEntity.Where(z => z.InputItemId == p.Id).ToList())?
                            .OrderBy(x => x.LineNumber)
                            .ToList()
                            );
                }

                var itemInputExcludeRepository = await _itemInputExcludeRepository.GetListAsync(p => p.PromotionId == promotionEntity.Id);
                if (itemInputExcludeRepository is { Count: > 0 })
                {
                    promotionDto.ItemInputExcludes = ObjectMapper.Map<List<ItemInputExclude>, List<ItemInputExcludeDto>>(itemInputExcludeRepository);
                }

                var outPutEntity = await _outputRepository.GetListAsync(p => p.PromotionId == promotionEntity.Id);
                if (outPutEntity != null)
                {
                    promotionDto.Outputs = ObjectMapper.Map<List<Output>, List<OutputDto>>(outPutEntity)?
                    .OrderBy(x => x.LineNumber)
                    .ToList();
                }

                var outPutReplaceEntity = await _outputReplaceRepository.GetListAsync(p => p.PromotionId == promotionEntity.Id);
                if (outPutReplaceEntity is { Count: > 0 })
                {
                    promotionDto.Outputs.ForEach(p =>
                        p.OutputReplaces = ObjectMapper.Map<List<OutputReplace>, List<OutputReplaceDto>>(
                            outPutReplaceEntity.Where(z => z.OutputId == p.Id).ToList())?
                            .OrderBy(x => x.LineNumber)
                            .ToList()
                            );
                }

                var quotaEntity = await _quotaRepository.FindAsync(p => p.PromotionId == promotionEntity.Id);
                if (quotaEntity != null)
                {
                    promotionDto.Quota = ObjectMapper.Map<Quota, QuotaDto>(quotaEntity);
                }

                var shopConditionEntity = await _provinceConditionRepository.GetListAsync(p => p.PromotionId == promotionEntity.Id);
                if (shopConditionEntity != null)
                {
                    promotionDto.ProvinceConditions = ObjectMapper.Map<List<ProvinceCondition>, List<ProvinceConditionDto>>(shopConditionEntity);
                }

                var amountConditionRepository = await _amountConditionRepository.FindAsync(p => p.PromotionId == promotionEntity.Id);
                if (amountConditionRepository != null)
                {
                    promotionDto.AmountCondition = ObjectMapper.Map<AmountCondition, AmountConditionDto>(amountConditionRepository);
                }

                var installmentConditionRepository = await _installmentConditionRepository.GetListAsync(p => p.PromotionId == promotionEntity.Id);
                if (installmentConditionRepository is { Count: > 0 })
                {
                    promotionDto.InstallmentConditions = ObjectMapper.Map<List<InstallmentCondition>, List<InstallmentConditionDto>>(installmentConditionRepository);
                }

                var costDistributionRepository = await _costDistributionRepository.GetListAsync(p => p.PromotionId == promotionEntity.Id);
                if (costDistributionRepository is { Count: > 0 })
                {
                    promotionDto.CostDistributions = GetDistributions(costDistributionRepository);
                }

                var extraConditionEntity = await _extraConditionRepository.GetListAsync(p => p.PromotionId == promotionEntity.Id);
                if (extraConditionEntity != null)
                {
                    promotionDto.ExtraConditions = ObjectMapper.Map<List<ExtraCondition>, List<ExtraConditionDto>>(extraConditionEntity);
                }

                var promotionExcludeEntity = await _promotionExcludeRepository.GetListAsync(p => p.PromotionCode == promotionEntity.Code);
                if (extraConditionEntity != null)
                {
                    promotionDto.PromotionExcludes = ObjectMapper.Map<List<PromotionExclude>, List<PromotionExcludeDto>>(promotionExcludeEntity);
                }

                var paymentConditionEnity = await _paymentConditionRepository.GetListAsync(p => p.PromotionId == promotionEntity.Id);
                if (paymentConditionEnity != null)
                {
                    promotionDto.PaymentConditions = ObjectMapper.Map<List<PaymentCondition>, List<PaymentConditionDto>>(paymentConditionEnity);
                }
                //await _elasticSearchAppService.DeleteInputReplacesAsync(promotionEntity.Code);
                //await _elasticSearchAppService.DeleteOutputReplacesAsync(promotionEntity.Code);
                await uow.CompleteAsync();
                await uow.SaveChangesAsync();

                await CreateUpdatePromotionDocumentAsync(promotionDto, promotionEntity, fieldConfigures);
                if (promotionDto.PromotionExcludes?.Any() ?? false)
                {
                    var promotionExcludesDocuments = ObjectMapper.Map<List<PromotionExcludeDto>, List<PromotionExcludeDocument>>(promotionDto.PromotionExcludes);
                    await _elasticSearchAppService.CreateUpdatePromotionExcludesAsync(promotionExcludesDocuments);
                }
                return promotionDto;
            }
        }
        return new PromotionDto();
    }

    public async Task<bool> ChangePricePromotionAsync(PimChangePriceEto pimChangePriceEto)
    {
        try
        {
            //await _kafkaProducerService.ProduceAsync(KafkaTopics.PromotionChangePrice, pimChangePriceEto, "");
            return true;
        }
        catch
        {
            return false;
        }
    }

    public async Task<List<string>> ReCachePromotionAsync(List<string> promotionCodes, ActionType actionType = ActionType.Create)
    {
        if (actionType == ActionType.UpdateHeader)
        {
            return new List<string>();
        }
        else
        {
            var promotionResponse = new List<string>();
            foreach (var promotionCode in promotionCodes)
            {
                try
                {
                    var promotionDto = await GetReCacheAsync(promotionCode);
                    if (promotionDto != null)
                    {
                        //lock promotion cache
                        var keyPromotionSync = $"SyncPromotion:{promotionDto.Code}:Lock";
                        await _databaseRedisCache.StringSetAsync(keyPromotionSync, JsonConvert.SerializeObject(new CacheDataIsLock
                        {
                            IsLock = true
                        }),null,(When)CommandFlags.FireAndForget);

                        if (promotionDto != null)
                        {
                            var promotionCache = new EventPromotionEto { ActionType = actionType };
                            promotionCache = ObjectMapper.Map(promotionDto, promotionCache);
                            await _hangFireAppService.ScheduleCreatePromotionAsync(promotionCache);

                            if (promotionDto.ProvinceConditions is { Count: > 0 })
                            {
                                await _hangFireAppService.ScheduleCacheConditionAsync(new EventConditionEto
                                {
                                    PromotionCode = promotionDto.Code,
                                    ProvinceConditions = promotionDto.ProvinceConditions.Select(p =>
                                        new ProvinceConditionEto
                                        {
                                            ProvinceCode = p.ProvinceCode,
                                            ProvinceName = p.ProvinceName,
                                            FromDate = p.FromDate,
                                            ToDate = p.ToDate
                                        }).ToList()
                                });
                            }
                        }

                    }

                    promotionResponse.Add(promotionDto.Code);
                }
                catch (Exception ex)
                {
                    await _databaseRedisCache.HashSetAsync("ReCachePromotionError", new HashEntry[]
                    {
                         new(promotionCode, $"{ex.Message}")
                    }, CommandFlags.FireAndForget);
                }
                finally
                {
                }
            }

            return promotionResponse;
        }

    }

    public async Task TimeExpireCacheAsync(List<PromotionTimeExpireEto> promotionCodes)
    {
        await _distributedEventBus.PublishAsync(promotionCodes);
    }

    public async Task<List<ResponseQuotaDto>> UseQuotaAsync(List<UseOrCheckQuotaDto> useOrCheckQuota)
    {
        var responseData = await _redisCacheService.UseQuotaAsync(useOrCheckQuota);
        return responseData;
    }

    public async Task<List<ResponseCheckQuotaDto>> CheckQuotaAsync(List<UseOrCheckQuotaDto> useOrCheckQuota)
    {
        var responseData = await _redisCacheService.CheckQuotaAsync(useOrCheckQuota);
        return responseData;
    }

    #endregion

    #region CRUD Promotion Exclude
    public async Task<List<PromotionExcludeDto>> CreatePromotionExcludeAsync(PromotionDto promotion, List<CreatePromotionExcludeDto> createPromotionExcludeDtos)
    {
        var listPromotionExlucdeDto = new List<PromotionExcludeDto>();
        if (createPromotionExcludeDtos.Any())
        {
            //Tạo loại trừ cho Promotion đang khởi tạo
            var listPromotionExclude = new List<PromotionExclude>();
            var listpromotionExcludesDocuments = new List<PromotionExcludeDocument>();

            var promotionExcludes = ObjectMapper.Map<List<CreatePromotionExcludeDto>, List<PromotionExclude>>(createPromotionExcludeDtos);
            promotionExcludes?.ForEach(p =>
            {
                p.PromotionCode = promotion!.Code;
                p.CreatedBy = promotion!.CreatedBy;
                p.CreatedByName = promotion!.CreatedByName;
            });
            listPromotionExclude.AddRange(promotionExcludes);


            var promotionExcludesDocumentsCreate = ObjectMapper.Map<List<PromotionExclude>, List<PromotionExcludeDocument>>(promotionExcludes);
            await _elasticSearchAppService.CreateUpdatePromotionExcludesAsync(promotionExcludesDocumentsCreate);

            //Check và tạo loại trừ cho các Promotion được loại trừ
            foreach (var promotionExclude in createPromotionExcludeDtos)
            {
                var addPromotionExclude = new PromotionExclude
                {
                    PromotionCode = promotionExclude.Code,
                    Code = promotion!.Code,
                    Name = promotion!.Name,
                    PromotionExcludeType = promotionExclude.PromotionExcludeType
                };
                listPromotionExclude.Add(addPromotionExclude);

                //Push ES
                var promotionExcludeNew = await _promotionExcludeRepository.GetListAsync(x => x.PromotionCode == promotionExclude.Code);
                var promotionExcludesDocuments = ObjectMapper.Map<List<PromotionExclude>, List<PromotionExcludeDocument>>(promotionExcludeNew);
                listpromotionExcludesDocuments.AddRange(promotionExcludesDocuments);

            }

            listPromotionExclude.RemoveAll(p => p.PromotionCode == p.Code);
            listpromotionExcludesDocuments.RemoveAll(p => p.PromotionCode == p.Code);

            await _promotionExcludeRepository.InsertManyAsync(listPromotionExclude);

            await _elasticSearchAppService.CreateUpdatePromotionExcludesAsync(listpromotionExcludesDocuments);

            listPromotionExlucdeDto = ObjectMapper.Map<List<PromotionExclude>, List<PromotionExcludeDto>>(promotionExcludes);
        }
        return listPromotionExlucdeDto;
    }

    public async Task UpdatePromotionExcludeRelatedAsync(EventPromotionExcludeEto excludeEto)
    {
        using (var uow = _unitOfWorkManager.Begin(new AbpUnitOfWorkOptions { IsTransactional = false }, requiresNew: true))
        {
            var getListPromotionExcludeMains = await _promotionExcludeRepository.GetListPromotionExcludeNolockAsync(excludeEto.PromotionCodes);

            //Xóa các loại trừ khỏi promotion hiện tại
            var getListPromotionExcludeOfItem = getListPromotionExcludeMains.Where(p => p.Code == excludeEto.Code);
            if (getListPromotionExcludeOfItem.Any())
            {
                var ids = getListPromotionExcludeOfItem.Select(p => p.Id).ToList();
                await _promotionExcludeRepository.DeleteManyAsync(ids);
            }

            var promotionExcludesDocumentExcepts = getListPromotionExcludeMains.Except(getListPromotionExcludeOfItem).ToList();
            //Check và tạo loại trừ cho các Promotion được loại trừ
            var listCreatePromotionExclude = new List<PromotionExclude>();
            foreach (var promotionExclude in excludeEto.PromotionExcludes)
            {
                var addPromotionExclude = new PromotionExclude
                {
                    PromotionCode = promotionExclude.Code,
                    Code = excludeEto.Code,
                    Name = excludeEto.Name,
                    PromotionExcludeType = promotionExclude.PromotionExcludeType
                };
                listCreatePromotionExclude.Add(addPromotionExclude);
                promotionExcludesDocumentExcepts.Add(addPromotionExclude);
            }

            await _promotionExcludeRepository.InsertManyAsync(listCreatePromotionExclude);

            //Update ES
            var listPromtionExclude = new List<PromotionExcludeDocument>();
            foreach (var item in promotionExcludesDocumentExcepts.Select(c => c.PromotionCode).ToHashSet())
            {
                var promotionExcludesDocuments = ObjectMapper.Map<List<PromotionExclude>, List<PromotionExcludeDocument>>(
                    promotionExcludesDocumentExcepts.Where(c => c.PromotionCode == item).ToList());
                listPromtionExclude.AddRange(promotionExcludesDocuments);
            }

            await _elasticSearchAppService.CreateUpdatePromotionExcludesAsync(listPromtionExclude);

            await uow.CompleteAsync();
            await uow.SaveChangesAsync();

            //Update redis promotion exclude and sync
            var promotionExcludes = promotionExcludesDocumentExcepts.Select(c => c.PromotionCode).ToList();
            promotionExcludes.AddRange(excludeEto.PromotionCodes);
            var promotionCodes = promotionExcludes.Distinct().ToList();
            if (promotionCodes is { Count: > 0 })
            {
                await _hangFireAppService.UpdatePromotionExcludeAndSyncDataAsync(promotionCodes);
                //await PromotionExcludeCacheAndSyncEcomAsync(promotionCodes);
            }
        }
    }

    public async Task<List<PromotionExcludeDto>> UpdatePromotionExcludeAsync(PromotionDto promotion, List<UpdatePromotionExcludeDto> updatePromotionExcludeDtos)
    {

        using (var uow = _unitOfWorkManager.Begin(new AbpUnitOfWorkOptions { IsTransactional = false }, requiresNew: true))
        {
            var deletePromotionExclude = await _promotionExcludeRepository.GetListPromotionExcludeNolockAsync(promotion.Code);
            if (deletePromotionExclude.Any())
            {
                var ids = deletePromotionExclude.Select(p => p.Id).ToList();
                await _promotionExcludeRepository.DeleteManyAsync(deletePromotionExclude);
            }
            var promotionExcludeResponse = new List<PromotionExcludeDto>();
            //Check payload có PromotionExclude không?
            if (updatePromotionExcludeDtos.Any())
            {
                var createEntity = ObjectMapper.Map<List<UpdatePromotionExcludeDto>, List<PromotionExclude>>(updatePromotionExcludeDtos);
                createEntity?.ForEach(p =>
                {
                    p.PromotionCode = promotion!.Code;
                    p.CreatedBy = promotion!.CreatedBy;
                    p.CreatedByName = promotion!.CreatedByName;
                });
                createEntity.RemoveAll(p => p.PromotionCode == p.Code);
                await _promotionExcludeRepository.InsertManyAsync(createEntity);
                promotionExcludeResponse = ObjectMapper.Map<List<PromotionExclude>, List<PromotionExcludeDto>>(createEntity);

            }
            var promotionExcludesDocumentsUpdate = ObjectMapper.Map<List<PromotionExcludeDto>, List<PromotionExcludeDocument>>(promotionExcludeResponse);
            await _elasticSearchAppService.CreateUpdatePromotionExcludesAsync(promotionExcludesDocumentsUpdate);

            await uow.CompleteAsync();
            await uow.SaveChangesAsync();
            var promotionExcludeEto = new EventPromotionExcludeEto()
            {
                Code = promotion!.Code,
                Name = promotion!.Name,
                PromotionCodes = deletePromotionExclude.Where(p => p.Code != promotion!.Code).Select(c => c.Code).ToList(),
                PromotionExcludes = ObjectMapper.Map<List<UpdatePromotionExcludeDto>, List<PromotionExcludeEto>>(updatePromotionExcludeDtos)
            };
            await _hangFireAppService.UpdatePromotionExcludeRelatedAsync(promotionExcludeEto);
            return promotionExcludeResponse;
        }

    }

    public async Task PromotionExcludeCacheAndSyncEcomAsync(List<string> promotionCodes)
    {
        foreach (var code in promotionCodes)
        {
            var promotionDto = await GetReCacheAsync(code);
            if (promotionDto != null)
            {
                var promotionCache = new EventPromotionEto
                {
                    ActionType = ActionType.UpdateHeader
                };
                var dataCache = ObjectMapper.Map(promotionDto, promotionCache);
                await _syncDataEcomService.UpdatePromotionExcludeAsync(dataCache);
            }

        }
    }

    #endregion

    #region Check unclock

    public async Task<CheckUpdatePermissionDto> CheckUpdatePermissionAsync(Guid promotionId)
    {
        var promotionEntity = await _promotionRepository.FindAsync(p => p.Id == promotionId);
        bool existsKey = await _databaseRedisCache.KeyExistsAsync($"SyncPromotion:{promotionEntity.Code}:Lock");
        if (existsKey)
        {
            return new CheckUpdatePermissionDto()
            {
                Enabled = false,
                Message = "Hệ thống đang xử lí CTKM, vui lòng thử lại sau ít phút."
            };
        }

        return new CheckUpdatePermissionDto()
        {
            Enabled = true,
            Message = "Được phép cập nhật"
        };
    }

    #endregion

    public async Task<List<SuggestPromotionExcludeDto>> SuggestPromotionExcludeAsync(SuggestPromotionExcludeRequestDto input)
    {
        if (!PromotionExcludeTypeMapping.PromotionExcludeType.TryGetValue(input.PromotionExcludeType, out var promotionTypes))
        {
            return new List<SuggestPromotionExcludeDto>();
        }

        if (promotionTypes.Item2.Contains(input.PromotionType))
        {
            return new List<SuggestPromotionExcludeDto>();
        }

        var promotions = await _elasticSearchAppService.GetListPromotionByTypeAsync(input.Keyword,
            new List<PromotionStatusEnum> { PromotionStatusEnum.Activated }
            , promotionTypes.Item1, input.Excludes);

        if (!promotions.Any())
        {
            return new List<SuggestPromotionExcludeDto>();
        }

        // case filter input item
        if (promotionTypes.Item3 && input.ItemInputs.Any())
        {
            var inputItem = new ItemDetailSuggestPromotionExcludeDto();
            ObjectMapper.Map(input.ItemInputs, inputItem);

            var promotionIncludes = await _elasticSearchAppService.SuggestPromotionExcludeAsync(inputItem, promotions.Select(x => x.Code).ToList());
            if (!promotionIncludes.Any())
            {
                return new List<SuggestPromotionExcludeDto>();
            }

            promotions.RemoveAll(x => !promotionIncludes.Contains(x.Code));
        }

        return ObjectMapper.Map<List<PromotionDocument>, List<SuggestPromotionExcludeDto>>(promotions);
    }

    public async Task<PagedResultDto<PromotionByItemDto>> SearchPromotionWithItemCodeAsync(SearchPromotionByItemDto input)
    {
        var (totalCount, promotions) = await _elasticSearchAppService.GetListPromotionByItemAsync(input.ItemCode, input.Status, input.DisplayArea, input.Pagination);
        return new PagedResultDto<PromotionByItemDto>()
        {
            TotalCount = totalCount,
            Items = ObjectMapper.Map<List<PromotionDocument>, List<PromotionByItemDto>>(promotions)
        };
    }

    public async Task<UsedQuotaDto> GetUsedQuotaAsync(string promotionCode)
    {
        var quota = await _quotaRepository.FindAsync(x => x.PromotionCode == promotionCode);
        if (quota == null)
        {
            return new UsedQuotaDto();
        }

        return await _elasticSearchAppService.GetUsedQuotaAsync(promotionCode, ObjectMapper.Map<Quota, QuotaDto>(quota));
    }

    public async Task<UpdateQuotaDto> CheckRuleQuotaAsync(Guid promotionId, UpdateQuotaDto updateQuotaDto)
    {

        //Check Quantity, limitQuantityShop, limitQuantityPhone  dưới data
        var quota = await _quotaRepository.FindAsync(c => c.PromotionId == promotionId);
        if (quota == null)
        {
            return updateQuotaDto;
        }
        if (updateQuotaDto.Quantity < quota.Quantity)
        {
            throw new FrtPromotionValidationException(ExceptionCode.NotFound,
            _stringLocalize[ExceptionCode.NotFound], $"Số lượng CTKM chỉnh sửa không được bé hơn {quota.Quantity} ban đầu");
        }

        if (updateQuotaDto.LimitQuantityShop < quota.LimitQuantityShop)
        {
            throw new FrtPromotionValidationException(ExceptionCode.NotFound,
            _stringLocalize[ExceptionCode.NotFound], $"Số lượng theo shop chỉnh sửa không được bé hơn {quota.LimitQuantityShop} ban đầu");
        }

        if ((updateQuotaDto.FlagQuantityPhone && quota.FlagQuantityPhone) && (updateQuotaDto.LimitQuantityPhone < quota.LimitQuantityPhone))
        {
            throw new FrtPromotionValidationException(ExceptionCode.NotFound,
            _stringLocalize[ExceptionCode.NotFound], $"Số lượng tối đa (phone) chỉnh sửa không được bé hơn {quota.LimitQuantityPhone} ban đầu");
        }

        return updateQuotaDto;
    }

    private static List<CostDistributionDto> GetDistributions(List<CostDistribution> costDistribution)
    {
        var listCostDistributionDto = new List<CostDistributionDto>();
        foreach (var item in costDistribution)
        {
            var costDistributionDto = new CostDistributionDto();

            //Category
            var categoryCode = JsonConvert.DeserializeObject<List<string>>(item.CategoryCode);
            var categoryName = JsonConvert.DeserializeObject<List<string>>(item.CategoryName);

            //Type
            var typeCode = JsonConvert.DeserializeObject<List<string>>(item.TypeCode);
            var typeName = JsonConvert.DeserializeObject<List<string>>(item.TypeName);

            //Brand
            var brandCode = JsonConvert.DeserializeObject<List<string>>(item.BrandCode);
            var brandName = JsonConvert.DeserializeObject<List<string>>(item.BrandName);

            costDistributionDto.Categories = categoryCode.Zip(categoryName, (code, name) => new DataPIMDto { Code = code.ToString(), Name = name.ToString() }).ToList();

            costDistributionDto.Types = typeCode.Zip(typeName, (code, name) => new DataPIMDto { Code = code.ToString(), Name = name.ToString() }).ToList();

            costDistributionDto.Brands = brandCode.Zip(brandName, (code, name) => new DataPIMDto { Code = code.ToString(), Name = name.ToString() }).ToList();

            costDistributionDto.PromotionId = item.PromotionId;
            costDistributionDto.DepartmentCode = item.DepartmentCode;
            costDistributionDto.DepartmentName = item.DepartmentName;
            costDistributionDto.Percentage = item.Percentage;
            costDistributionDto.Id = item.Id;
            listCostDistributionDto.Add(costDistributionDto);
        }
        return listCostDistributionDto;
    }

    public async Task<PromotionDto> GetByCodeAsync(string promotionCode)
    {
        var promotionEntity = await _promotionRepository.FindAsync(p => p.Code == promotionCode);

        if (promotionEntity != null)
        {
            var promotionDto = ObjectMapper.Map<Promotion, PromotionDto>(promotionEntity);
            var itemInputEntity = await _itemInputRepository.GetListAsync(p => p.PromotionId == promotionEntity.Id);
            if (itemInputEntity != null)
            {
                promotionDto.ItemInputs = ObjectMapper.Map<List<ItemInput>, List<ItemInputDto>>(itemInputEntity)?
                .OrderBy(x => x.LineNumber)
                .ToList();
            }

            var itemInputReplaceEntity = await _itemInputReplaceRepository.GetListAsync(p => p.PromotionId == promotionEntity.Id);
            if (itemInputReplaceEntity is { Count: > 0 })
            {
                promotionDto.ItemInputs.ForEach(p =>
                    p.ItemInputReplaces = ObjectMapper.Map<List<ItemInputReplace>, List<ItemInputReplaceDto>>(
                        itemInputReplaceEntity.Where(z => z.InputItemId == p.Id).ToList())?
                        .OrderBy(x => x.LineNumber)
                        .ToList()
                        );
            }

            var itemInputExcludeRepository = await _itemInputExcludeRepository.GetListAsync(p => p.PromotionId == promotionEntity.Id);
            if (itemInputExcludeRepository is { Count: > 0 })
            {
                promotionDto.ItemInputExcludes = ObjectMapper.Map<List<ItemInputExclude>, List<ItemInputExcludeDto>>(itemInputExcludeRepository);
            }

            var outPutEntity = await _outputRepository.GetListAsync(p => p.PromotionId == promotionEntity.Id);
            if (outPutEntity != null)
            {
                promotionDto.Outputs = ObjectMapper.Map<List<Output>, List<OutputDto>>(outPutEntity)?
                .OrderBy(x => x.LineNumber)
                .ToList();
            }

            var outPutReplaceEntity = await _outputReplaceRepository.GetListAsync(p => p.PromotionId == promotionEntity.Id);
            if (outPutReplaceEntity is { Count: > 0 })
            {
                promotionDto.Outputs.ForEach(p =>
                    p.OutputReplaces = ObjectMapper.Map<List<OutputReplace>, List<OutputReplaceDto>>(
                        outPutReplaceEntity.Where(z => z.OutputId == p.Id).ToList())?
                        .OrderBy(x => x.LineNumber)
                        .ToList()
                        );
            }

            var quotaEntity = await _quotaRepository.FindAsync(p => p.PromotionId == promotionEntity.Id);
            if (quotaEntity != null)
            {
                promotionDto.Quota = ObjectMapper.Map<Quota, QuotaDto>(quotaEntity);
            }

            var shopConditionEntity = await _provinceConditionRepository.GetListAsync(p => p.PromotionId == promotionEntity.Id);
            if (shopConditionEntity != null)
            {
                promotionDto.ProvinceConditions = ObjectMapper.Map<List<ProvinceCondition>, List<ProvinceConditionDto>>(shopConditionEntity);
            }

            var amountConditionRepository = await _amountConditionRepository.FindAsync(p => p.PromotionId == promotionEntity.Id);
            if (amountConditionRepository != null)
            {
                promotionDto.AmountCondition = ObjectMapper.Map<AmountCondition, AmountConditionDto>(amountConditionRepository);
            }

            var installmentConditionRepository = await _installmentConditionRepository.GetListAsync(p => p.PromotionId == promotionEntity.Id);
            if (installmentConditionRepository is { Count: > 0 })
            {
                promotionDto.InstallmentConditions = ObjectMapper.Map<List<InstallmentCondition>, List<InstallmentConditionDto>>(installmentConditionRepository);
            }

            var costDistributionRepository = await _costDistributionRepository.GetListAsync(p => p.PromotionId == promotionEntity.Id);
            if (costDistributionRepository is { Count: > 0 })
            {
                promotionDto.CostDistributions = GetDistributions(costDistributionRepository);
            }

            var extraConditionEntity = await _extraConditionRepository.GetListAsync(p => p.PromotionId == promotionEntity.Id);
            if (extraConditionEntity != null)
            {
                promotionDto.ExtraConditions = ObjectMapper.Map<List<ExtraCondition>, List<ExtraConditionDto>>(extraConditionEntity);
            }

            var promotionExcludeEntity = await _promotionExcludeRepository.GetListAsync(p => p.PromotionCode == promotionEntity.Code);
            if (promotionExcludeEntity != null)
            {
                promotionDto.PromotionExcludes = ObjectMapper.Map<List<PromotionExclude>, List<PromotionExcludeDto>>(promotionExcludeEntity);
            }

            var paymentConditionEnity = await _paymentConditionRepository.GetListAsync(p => p.PromotionId == promotionEntity.Id);
            if (paymentConditionEnity != null)
            {
                promotionDto.PaymentConditions = ObjectMapper.Map<List<PaymentCondition>, List<PaymentConditionDto>>(paymentConditionEnity);
            }

            return promotionDto;
        }

        return new PromotionDto();
    }

    public async Task ScheduleReCacheAsync()
    {
        var transactionTracer = _tracer?.StartTransaction($"ScheduleReCacheAsync", ApiConstants.TypeRequest);
        try
        {
            var promotion = await _promotionRepository.GetPromotionReCacheAsync();
            await ReCachePromotionAsync(promotion, ActionType.Create);
        }
        catch (Exception ex)
        {
            transactionTracer?.CaptureException(ex);
            Logger.LogError(ex, "Lỗi ScheduleReCacheAsync");
        }
        finally
        {
            transactionTracer?.End();
        }
    }

    /// <summary>
    /// tự động bật khuyến mãi(Active) và bắn kaffka theo ActiveDate == ngày hiện tại, nếu khuyến mãi có status = Create
    /// bắn kaffka theo ActiveDate == ngày hiện tại, nếu khuyến mãi hiện đang Active
    /// </summary>
    /// <returns></returns>
    public async Task JobSyncDataDailyFromActiveDateAsync()
    {
        var transactionTracer = _tracer?.StartTransaction($"JobSyncDataDailyFromActiveDateAsync", ApiConstants.TypeRequest);
        try
        {
            var promotionsStatusCreatedOrActive = await _promotionRepository.GetPromotionActiveDateAsync();

            if (promotionsStatusCreatedOrActive is { Count: > 0 })
            {
                var promotionCodes = new List<string>();
                var promotionsCreated = promotionsStatusCreatedOrActive.Where(x => x.Status == (int)PromotionStatusEnum.Created).ToList();
                if (promotionsCreated is { Count: > 0 })
                {
                    promotionsCreated.ForEach(x => x.Status = (int)PromotionStatusEnum.Activated);
                    await _promotionRepository.UpdateManyAsync(promotionsCreated);

                    promotionCodes.AddRange(promotionsCreated.Select(p => p.Code));
                }

                var promotionsStatusActivated = promotionsStatusCreatedOrActive.Where(x => x.Status == (int)PromotionStatusEnum.Activated).ToList();
                if (promotionsStatusActivated is { Count: > 0 })
                {
                    promotionCodes.AddRange(promotionsStatusActivated.Select(p => p.Code));
                }

                await ReCachePromotionAsync(promotionCodes, ActionType.Update);
            }
        }
        catch (Exception ex)
        {
            transactionTracer?.CaptureException(ex);
            Logger.LogError(ex, "Lỗi JobSyncDataDailyFromActiveDateAsync");
        }
        finally
        {
            transactionTracer?.End();
        }
    }

    public async Task ClearPromotionInactiveFromRedisAsync(List<string> promotionCodes)
    {
        var promotions = await _promotionRepository.GetPromotionFromStastusInActiveAsync(promotionCodes);
        if (promotions is { Count: > 0 })
        {
            var promotionIds = promotions.Select(p => p.Id).ToList();
            var shopConditionEntity = await _provinceConditionRepository.GetListAsync(p => promotionIds.Contains(p.PromotionId));
            var promotionTimeExpires = promotions.Select(p => new PromotionTimeExpireEto
            {
                PromotionId = p.Id,
                PromotionCode = p.Code,
                PromotionType = p.PromotionType,
                ProvinceConditions = shopConditionEntity?.Select(p => p.ProvinceCode).ToList()
            }).ToList();

            await _hangFireAppService.UpdateStatusPromotionAsync(promotionTimeExpires);
        }
    }
}