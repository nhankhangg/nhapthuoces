﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using FRTTMO.CAM.PromotionAPI.DTOs.CostDistribution;
using FRTTMO.CAM.PromotionAPI.DTOs.InstallmentConditions;
using FRTTMO.CAM.PromotionAPI.DTOs.Promotion;
using FRTTMO.CAM.PromotionAPI.DTOs.Quota;
using FRTTMO.CAM.PromotionAPI.DTOs.RegionCondition;
using FRTTMO.CAM.PromotionAPI.Services;
using FRTTMO.CAM.PromotionAPI.DTOs.Promotion.PromotionDetails;
using FRTTMO.CAM.PromotionAPI.Entities.Promotion;
using FRTTMO.CAM.PromotionAPI.ExceptionCodes;
using Volo.Abp.Uow;
using FRTTMO.CAM.PromotionAPI.Enum;
using Newtonsoft.Json;
using FRTTMO.CAM.PromotionAPI.Localization;
using Microsoft.Extensions.Localization;
using FRTTMO.CAM.PromotionAPI.Entities.FieldConfigure;
using FRTTMO.CAM.PromotionAPI.Entities.Campaign;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInput;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInputReplace;
using FRTTMO.CAM.PromotionAPI.Entities.ItemInput;
using FRTTMO.CAM.PromotionAPI.Entities.ItemInputReplace;
using FRTTMO.CAM.PromotionAPI.Entities.Output;
using FRTTMO.CAM.PromotionAPI.Entities.OutputReplace;
using Volo.Abp.EventBus.Distributed;
using FRTTMO.CAM.PromotionAPI.ETOs;
using FRTTMO.CAM.PromotionAPI.DTOs.Output;
using FRTTMO.CAM.PromotionAPI.DTOs.OutputReplace;
using FRTTMO.CAM.PromotionAPI.Entities.CostDistribution;
using ActionType = FRTTMO.CAM.PromotionAPI.Enum.ActionType;
using FRTTMO.CAM.PromotionAPI.Entities.Quota;
using FRTTMO.CAM.PromotionAPI.DTOs.AmountConditions;
using FRTTMO.CAM.PromotionAPI.Entities.AmountCondition;
using FRTTMO.CAM.PromotionAPI.Entities.InstallmentCondition;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInputExclude;
using FRTTMO.CAM.PromotionAPI.Entities.ItemInputExclude;
using FRTTMO.CAM.PromotionAPI.DTOs.Condition;
using FRTTMO.CAM.PromotionAPI.Entities.Condition;
using FRTTMO.CAM.PromotionAPI.Entities.RegionCondition;
using FRTTMO.CAM.PromotionAPI.Documents;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;
using FRTTMO.CAM.PromotionAPI.Constants;

namespace FRTTMO.CAM.PromotionAPI.Applications
{
    public class PromotionDetailsAppService : PromotionAPIAppService, IPromotionDetailsAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IStringLocalizer<PromotionAPIResource> _stringLocalize;
        private readonly IFieldConfigureRepository _fieldConfigureRepository;
        private readonly ICampaignRepository _campaignRepository;
        private readonly IPromotionRepository _promotionRepository;
        private readonly IItemInputRepository _itemInputRepository;
        private readonly IItemInputReplaceRepository _itemInputReplaceRepository;
        private readonly IOutputRepository _outputRepository;
        private readonly IOutputReplaceRepository _outputReplaceRepository;
        private readonly IPromotionAppService _promotionAppService;
        private readonly IDistributedEventBus _distributedEventBus;
        private readonly ICostDistributionRepository _costDistributionRepository;
        private readonly IQuotaRepository _quotaRepository;
        private readonly IAmountConditionRepository _amountConditionRepository;
        private readonly IInstallmentConditionRepository _installmentConditionRepository;
        private readonly IItemInputExcludeRepository _itemInputExcludeRepository;
        private readonly IExtraConditionRepository _extraConditionRepository;
        private readonly IProvinceConditionRepository _shopConditionRepository;
        private readonly IElasticSearchAppService _elasticSearchAppService;
        public PromotionDetailsAppService(IUnitOfWorkManager unitOfWorkManager,
            IStringLocalizer<PromotionAPIResource> stringLocalize,
            IFieldConfigureRepository fieldConfigureRepository,
            ICampaignRepository campaignRepository,
            IPromotionRepository promotionRepository,
            IItemInputRepository itemInputRepository,
            IItemInputReplaceRepository itemInputReplaceRepository,
            IOutputRepository outputRepository,
            IOutputReplaceRepository outputReplaceRepository,
            IDistributedEventBus distributedEventBus,
            IPromotionAppService promotionAppService,
            ICostDistributionRepository costDistributionRepository,
            IQuotaRepository quotaRepository,
            IAmountConditionRepository amountConditionRepository,
            IInstallmentConditionRepository installmentConditionRepository,
            IItemInputExcludeRepository itemInputExcludeRepository,
            IExtraConditionRepository extraConditionRepository,
            IProvinceConditionRepository shopConditionRepository,
            IElasticSearchAppService elasticSearchAppService)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _stringLocalize = stringLocalize;
            _fieldConfigureRepository = fieldConfigureRepository;
            _campaignRepository = campaignRepository;
            _promotionRepository = promotionRepository;
            _itemInputRepository = itemInputRepository;
            _itemInputReplaceRepository = itemInputReplaceRepository;
            _outputRepository = outputRepository;
            _outputReplaceRepository = outputReplaceRepository;
            _distributedEventBus = distributedEventBus;
            _promotionAppService = promotionAppService;
            _costDistributionRepository = costDistributionRepository;
            _quotaRepository = quotaRepository;
            _amountConditionRepository = amountConditionRepository;
            _installmentConditionRepository = installmentConditionRepository;
            _itemInputExcludeRepository = itemInputExcludeRepository;
            _extraConditionRepository = extraConditionRepository;
            _shopConditionRepository = shopConditionRepository;
            _elasticSearchAppService = elasticSearchAppService;
        }
        public async Task<QuotaDto> UpdateQuotaAsync(Guid promotionId, UpdateQuotaDto updateQuotaDto)
        {
            using (var uow = _unitOfWorkManager.Begin(new AbpUnitOfWorkOptions { IsTransactional = false }, requiresNew: true))
            {
                var quota = await _quotaRepository.FindAsync(x => x.PromotionId == promotionId);
                var promotion = await _promotionRepository.FindAsync(promotionId);
                if (quota == null)
                {
                    throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                        _stringLocalize[ExceptionCode.NotFound], "Quota không tồn tại");
                }

                ObjectMapper.Map(updateQuotaDto, quota);


                await _quotaRepository.UpdateAsync(quota);


                MapAuditedUser(ref promotion, "update");
                await _promotionRepository.UpdateAsync(promotion);

                await uow.CompleteAsync();
                await uow.SaveChangesAsync();

                //PromotionCache update
                await _promotionAppService.ReCachePromotionDetailAsync(promotionId);

                //Push ES
                var promotionES = await _elasticSearchAppService.GetPromotionByIdAsync(promotionId);
                if (promotionES != null)
                {
                    promotionES.Quota = ObjectMapper.Map<UpdateQuotaDto, QuotaDocument>(updateQuotaDto);

                    await _elasticSearchAppService.UpdateAsync(promotionId, promotionES);
                }

                return ObjectMapper.Map<Quota, QuotaDto>(quota);
            }
        }
        
        public async Task<PromotionUpdateDetailResponseDto> UpdateShopConditionAsync(Guid promotionId, List<UpdateProvinceConditionDto> updateShopConditionDtos)
        {
            using (var uow = _unitOfWorkManager.Begin(new AbpUnitOfWorkOptions { IsTransactional = false }, requiresNew: true))
            {
                var promotion = await _promotionRepository.FindAsync(promotionId);
                if (promotion == null)
                {
                    throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                        _stringLocalize[ExceptionCode.NotFound], "CTKM không tồn tại");
                }

                #region Update Shop Condition
                var shopCondition = new List<UpdateProvinceConditionDto>();
                var newshopConditionDtos = updateShopConditionDtos!.Where(x => x.Id == null).ToList();
                if (newshopConditionDtos.Any())
                {
                    var newshopConditions = ObjectMapper.Map<List<UpdateProvinceConditionDto>, List<ProvinceCondition>>(newshopConditionDtos);
                    newshopConditions = newshopConditions.Select(p =>
                    {
                        p.PromotionId = promotion!.Id;
                        p.CreatedBy = promotion!.CreatedBy;
                        p.CreatedByName = promotion!.CreatedByName;
                        p.PromotionCode = promotion!.Code;
                        if (!promotion.UpdateBy.IsNullOrEmpty())
                        {
                            p.UpdateBy = promotion.UpdateBy;
                            p.UpdateByName = promotion!.UpdateByName;
                        }
                        return p;
                    }).ToList();
                    await _shopConditionRepository.InsertManyAsync(newshopConditions);
                    shopCondition = ObjectMapper.Map<List<ProvinceCondition>, List<UpdateProvinceConditionDto>>(newshopConditions);
                }

                // update shop condition 
                var shopConditions = await _shopConditionRepository.GetListAsync(x => x.PromotionId == promotion.Id);
                var updatedShopConditionDtos = updateShopConditionDtos.Except(newshopConditionDtos).ToList();
                if (updatedShopConditionDtos.Any())
                {
                    var shopConditionsUpdated = await _shopConditionRepository.GetListAsync(x => x.PromotionId == promotionId
                                                        && updatedShopConditionDtos.Select(x => x.Id).Contains(x.Id));

                    var shopsUpdated = new List<ProvinceCondition>();
                    foreach (var itemUpdate in updatedShopConditionDtos)
                    {
                        var shopUpdated = shopConditionsUpdated.Find(x => x.Id == itemUpdate.Id);
                        shopsUpdated.Add(ObjectMapper.Map(itemUpdate, shopUpdated));
                    }

                    await _shopConditionRepository.UpdateManyAsync(shopsUpdated);
                    shopCondition.AddRange(ObjectMapper.Map<List<ProvinceCondition>, List<UpdateProvinceConditionDto>>(shopsUpdated));
                }



                // delete old shop condition
                var deleteShopConditions = shopConditions.Where(x => !shopCondition.Select(x => x.Id).Contains(x.Id)).ToList();
                if (deleteShopConditions.Any())
                {
                    await _shopConditionRepository.DeleteManyAsync(deleteShopConditions.Select(x => x.Id));
                }

                MapAuditedUser(ref promotion, "update");
                await _promotionRepository.UpdateAsync(promotion);

                #endregion

                await uow.CompleteAsync();
                await uow.SaveChangesAsync();

                //PromotionCache update
                //await _promotionAppService.ReCachePromotionDetailAsync(promotionId);

                //Shop condition cache
                await _distributedEventBus.PublishAsync(new EventConditionEto
                {
                    PromotionCode = promotion.Code,
                    ProvinceConditions = shopCondition.Select(p =>
                        new ProvinceConditionEto
                        {
                            ProvinceCode = p.ProvinceCode,
                            ProvinceName = p.ProvinceName,
                            FromDate = p.FromDate,
                            ToDate = p.ToDate
                        }).ToList()
                    
                });

                //Push ES
                var shopConditionsES = ObjectMapper.Map<List<UpdateProvinceConditionDto>, List<ProvinceConditionDto>>(shopCondition);
                var promotionES = await _elasticSearchAppService.GetPromotionByIdAsync(promotionId);
                if (promotionES != null)
                {
                    //promotionES.RegionConditions = ObjectMapper.Map<List<ShopConditionDto>, List<ShopConditionDocument>>(shopConditionsES);

                    //await _elasticSearchAppService.UpdateAsync(promotionId, promotionES);
                }

                return new PromotionUpdateDetailResponseDto
                {
                    PromotionId = promotionId
                };
            }
        }

        public async Task<PromotionUpdateDetailResponseDto> UpdateInstallmentConditionAsync(Guid promotionId, List<UpdateInstallmentConditionDto> updateInstallmentConditionDto)
        {
            using (var uow = _unitOfWorkManager.Begin(new AbpUnitOfWorkOptions { IsTransactional = false }, requiresNew: true))
            {
                var promotion = await _promotionRepository.FindAsync(promotionId);
                if (promotion == null)
                {
                    throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                        _stringLocalize[ExceptionCode.NotFound], "CTKM không tồn tại");
                }

                #region Update Installment Condition
                var installmentCondition = new List<UpdateInstallmentConditionDto>();
                var newinstallmentConditionDtos = updateInstallmentConditionDto!.Where(x => x.Id == null).ToList();
                if (newinstallmentConditionDtos.Any())
                {
                    var newinstallmentConditionts = ObjectMapper.Map<List<UpdateInstallmentConditionDto>, List<InstallmentCondition>>(newinstallmentConditionDtos);
                    newinstallmentConditionts = newinstallmentConditionts.Select(p =>
                    {
                        p.PromotionId = promotion!.Id;
                        p.CreatedBy = promotion!.CreatedBy;
                        p.CreatedByName = promotion!.CreatedByName;
                        if (!promotion.UpdateBy.IsNullOrEmpty())
                        {
                            p.UpdateBy = promotion.UpdateBy;
                            p.UpdateByName = promotion!.UpdateByName;
                        }
                        return p;
                    }).ToList();
                    await _installmentConditionRepository.InsertManyAsync(newinstallmentConditionts);
                    installmentCondition = ObjectMapper.Map<List<InstallmentCondition>, List<UpdateInstallmentConditionDto>>(newinstallmentConditionts);
                }

                // update installment condition (trả góp) 
                var installmentConditions = await _installmentConditionRepository.GetListAsync(x => x.PromotionId == promotion.Id);
                var updateinstallmentConditionDtos = updateInstallmentConditionDto.Except(newinstallmentConditionDtos).ToList();
                if (updateinstallmentConditionDtos.Any())
                {
                    var installmentConditionsUpdated = await _installmentConditionRepository.GetListAsync(x => x.PromotionId == promotionId
                                                        && updateinstallmentConditionDtos.Select(x => x.Id).Contains(x.Id));

                    var installmentsUpdated = new List<InstallmentCondition>();
                    foreach (var itemUpdate in updateinstallmentConditionDtos)
                    {
                        var installmentUpdated = installmentConditionsUpdated.Find(x => x.Id == itemUpdate.Id);
                        installmentsUpdated.Add(ObjectMapper.Map(itemUpdate, installmentUpdated));
                    }

                    await _installmentConditionRepository.UpdateManyAsync(installmentsUpdated);
                    installmentCondition.AddRange(ObjectMapper.Map<List<InstallmentCondition>, List<UpdateInstallmentConditionDto>>(installmentsUpdated));
                }



                // delete old installment condition
                var deleteInstallmentConditions = installmentConditions.Where(x => !installmentCondition.Select(x => x.Id).Contains(x.Id)).ToList();
                if (deleteInstallmentConditions.Any())
                {
                    await _installmentConditionRepository.DeleteManyAsync(deleteInstallmentConditions.Select(x => x.Id));
                }

                MapAuditedUser(ref promotion, "update");
                await _promotionRepository.UpdateAsync(promotion);

                #endregion

                await uow.CompleteAsync();
                await uow.SaveChangesAsync();
                //PromotionCache update
                await _promotionAppService.ReCachePromotionDetailAsync(promotionId);

                //Push ES
                var installmentConditionsES = ObjectMapper.Map<List<UpdateInstallmentConditionDto>, List<InstallmentConditionDto>>(installmentCondition);
                var promotionES = await _elasticSearchAppService.GetPromotionByIdAsync(promotionId);
                if (promotionES != null)
                {
                    promotionES.InstallmentConditions = ObjectMapper.Map<List<InstallmentConditionDto>, List<InstallmentConditionDocument>>(installmentConditionsES);

                    await _elasticSearchAppService.UpdateAsync(promotionId, promotionES);
                }

                return new PromotionUpdateDetailResponseDto
                {
                    PromotionId = promotionId
                };
            }
        }

        public async Task<PromotionHeaderDto> UpdatePromotionHeaderAsync(Guid promotionId, UpdatePromotionHeaderDto updatePromotionHeaderDto)
        {
            using (var uow = _unitOfWorkManager.Begin(new AbpUnitOfWorkOptions { IsTransactional = false }, requiresNew: true))
            {
                //var updateDto = ObjectMapper.Map<UpdatePromotionHeaderDto, PromotionHeaderDto>(updatePromotionHeaderDto);
                await ValidationAsync(updatePromotionHeaderDto);
                var promotion = await _promotionRepository.FindAsync(promotionId);
                if (promotion == null)
                {
                    throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                        _stringLocalize[ExceptionCode.NotFound], "CTKM không tồn tại");
                }

                ObjectMapper.Map(updatePromotionHeaderDto, promotion);
                MapAuditedUser(ref promotion, "update");
                await _promotionRepository.UpdateAsync(promotion);

                await uow.CompleteAsync();
                await uow.SaveChangesAsync();

                //_hangFireAppService.DeleteOnScheduled(promotion.Id);
                //await _hangFireAppService.ScheduleUpdatePromotionStatusExpiredAsync(promotion);

                await _promotionAppService.ReCachePromotionDetailAsync(promotionId);

                var promotionES = await _elasticSearchAppService.GetPromotionByIdAsync(promotionId);
                if (promotionES != null)
                {
                    promotionES.Name = updatePromotionHeaderDto.Name;
                    promotionES.CampaignId = updatePromotionHeaderDto.CampaignId;
                    promotionES.FromDate = updatePromotionHeaderDto.FromDate.Value;
                    promotionES.ToDate = updatePromotionHeaderDto.ToDate.Value;
                    promotionES.ActiveDate = updatePromotionHeaderDto.ActiveDate;
                    promotionES.FromHour = updatePromotionHeaderDto.FromHour;
                    promotionES.ToHour = updatePromotionHeaderDto.ToHour;
                    promotionES.PromotionClass = updatePromotionHeaderDto.PromotionClass;
                    promotionES.PromotionType = updatePromotionHeaderDto.PromotionType;
                    promotionES.ApplicableMethod = updatePromotionHeaderDto.ApplicableMethod;
                    promotionES.TradeIndustryCode = updatePromotionHeaderDto.TradeIndustryCode;
                    //promotionES.PromotionExcludes = updatePromotionHeaderDto.PromotionExcludes;
                    promotionES.AllowDisplayOnBill = updatePromotionHeaderDto.AllowDisplayOnBill;
                    promotionES.Channels = await MasterDataDocumentsAsync(updatePromotionHeaderDto.Channels, FieldConfigureConstants.Channel);
                    promotionES.OrderTypes = await MasterDataDocumentsAsync(updatePromotionHeaderDto.OrderTypes, FieldConfigureConstants.OrderType);
                    promotionES.StoreTypes = await MasterDataDocumentsAsync(updatePromotionHeaderDto.StoreTypes, FieldConfigureConstants.StoreType);
                    promotionES.CustomerGroups = await MasterDataDocumentsAsync(updatePromotionHeaderDto.CustomerGroups, FieldConfigureConstants.CustomerGroup);

                    await _elasticSearchAppService.UpdateAsync(promotionId, promotionES);
                }

                return ObjectMapper.Map<Promotion, PromotionHeaderDto>(promotion);
            }
        }
        
        private async Task<List<BaseMasterDataDocument>> MasterDataDocumentsAsync(string[] common, string fieldConfigure)
        {
            var listMasterData = new List<BaseMasterDataDocument>();

            //Channels
            if (fieldConfigure == FieldConfigureConstants.Channel)
            {
                var listChannels = new List<string>(common);
                foreach (var channel in listChannels)
                {
                    var channelMasterData = await _fieldConfigureRepository.GetAsync(x => x.Group == fieldConfigure && x.Code == channel);
                    listMasterData.Add(new BaseMasterDataDocument()
                    {
                        Code = channelMasterData.Code,
                        Name = channelMasterData.Name,
                    });
                }
            }

            //OrderType
            if (fieldConfigure == FieldConfigureConstants.OrderType)
            {
                var listOrderTypes = new List<string>(common);
                foreach (var orderType in listOrderTypes)
                {
                    var orderTypeMasterData = await _fieldConfigureRepository.GetAsync(x => x.Group == fieldConfigure && x.Code == orderType);
                    listMasterData.Add(new BaseMasterDataDocument()
                    {
                        Code = orderTypeMasterData.Code,
                        Name = orderTypeMasterData.Name,
                    });
                }
            }

            //StoreType
            if (fieldConfigure == FieldConfigureConstants.StoreType)
            {
                var listStoreTypes = new List<string>(common);
                foreach (var storeType in listStoreTypes)
                {
                    var storeTypeMasterData = await _fieldConfigureRepository.GetAsync(x => x.Group == fieldConfigure && x.Code == storeType);
                    listMasterData.Add(new BaseMasterDataDocument()
                    {
                        Code = storeTypeMasterData.Code,
                        Name = storeTypeMasterData.Name,
                    });
                }
            }

            //CustomerGroup
            if (fieldConfigure == FieldConfigureConstants.CustomerGroup)
            {
                var listCustomerGroups = new List<string>(common);
                foreach (var customerGroup in listCustomerGroups)
                {
                    var customerGroupMasterData = await _fieldConfigureRepository.GetAsync(x => x.Group == fieldConfigure && x.Code == customerGroup);
                    listMasterData.Add(new BaseMasterDataDocument()
                    {
                        Code = customerGroupMasterData.Code,
                        Name = customerGroupMasterData.Name,
                    });
                }
            }

            return listMasterData;
        }
        
        private async Task ValidationAsync(UpdatePromotionHeaderDto promotion)
        {
            var fieldConfigures = await _fieldConfigureRepository.GetListAsync();
            if (!fieldConfigures?.Any() ?? false)
            {
                throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                    _stringLocalize[ExceptionCode.NotFound], "không tìm thấy table fieldConfigure");
            }

            var channels = fieldConfigures.Where(p => p.Group == "Channel").Select(p => p.Code).ToList();
            if (promotion.Channels is { Length: > 0 }
                && promotion.Channels.Any(p => !channels.Contains(p)))
            {
                throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                    _stringLocalize[ExceptionCode.NotFound],
                    $"channel không đúng : {JsonConvert.SerializeObject(promotion.Channels)}");
            }

            var orderType = fieldConfigures.Where(p => p.Group == "OrderType").Select(p => p.Code).ToList();
            if (promotion.OrderTypes is { Length: > 0 }
                && promotion.OrderTypes.Any(p => !orderType.Contains(p)))
            {
                throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                    _stringLocalize[ExceptionCode.NotFound],
                    $"orderType không đúng : {JsonConvert.SerializeObject(promotion.OrderTypes)}");
            }

            var storeType = fieldConfigures.Where(p => p.Group == "StoreType").Select(p => p.Code).ToList();
            if (promotion.StoreTypes is { Length: > 0 }
                && promotion.StoreTypes.Any(p => !storeType.Contains(p)))
            {
                throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                    _stringLocalize[ExceptionCode.NotFound],
                    $"orderType không đúng : {JsonConvert.SerializeObject(promotion.StoreTypes)}");
            }

            var campaign = await _campaignRepository.FindAsync(promotion.CampaignId);
            if (campaign == null)
            {
                throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                    _stringLocalize[ExceptionCode.BadRequest], "Chiến dịch không tồn tại");
            }
            if (campaign.Status == (int)CampaignStatusEnum.InActive)
            {
                throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                    _stringLocalize[ExceptionCode.BadRequest], "Chiến dịch không còn hiệu lực");
            }
            if (campaign.FromDate.Date > promotion.FromDate!.Value.Date
                || campaign.ToDate.Date < promotion.ToDate!.Value.Date)
            {
                throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                    _stringLocalize[ExceptionCode.BadRequest], $"CTKM không thuộc khoản thời gian của chiến dịch: {campaign.Code} - From: {campaign.FromDate:dd/MM/yyyy} - To: {campaign.ToDate:dd/MM/yyyy}");
            }
        }
        
        private void MapAuditedUser(ref Promotion promotion, string action)
        {
            var userId = CurrentUser?.FindClaim("employee_code")?.Value ?? "";
            var userName = CurrentUser?.FindClaim("full_name")?.Value ?? "";
            switch (action)
            {
                case "create":
                    {
                        promotion.CreatedBy = userId;
                        promotion.CreatedByName = userName;
                        return;
                    }
                case "update":
                    {
                        promotion.UpdateBy = userId;
                        promotion.UpdateByName = userName;
                        return;
                    }
            }
        }

        public async Task<PromotionWebDto> UpdatePromotionWebAsync(Guid promotionId, UpdatePromotionWebDto updatePromotionWebDto)
        {
            using (var uow = _unitOfWorkManager.Begin(new AbpUnitOfWorkOptions { IsTransactional = false }, requiresNew: true))
            {
                var promotion = await _promotionRepository.FindAsync(promotionId);
                if (promotion == null)
                {
                    throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                        _stringLocalize[ExceptionCode.NotFound], "CTKM không tồn tại");
                }

                ObjectMapper.Map(updatePromotionWebDto, promotion);
                MapAuditedUser(ref promotion, "update");
                await _promotionRepository.UpdateAsync(promotion);

                await uow.CompleteAsync();
                await uow.SaveChangesAsync();

                //_hangFireAppService.DeleteOnScheduled(promotion.Id);
                //await _hangFireAppService.ScheduleUpdatePromotionStatusExpiredAsync(promotion);

                //var promotionResponseDto = ObjectMapper.Map<Promotion, PromotionDto>(promotion);

                await _promotionAppService.ReCachePromotionDetailAsync(promotionId);

                var promotionES = await _elasticSearchAppService.GetPromotionByIdAsync(promotionId);
                if (promotionES != null)
                {
                    promotionES.UrlImage = updatePromotionWebDto.UrlImage;
                    promotionES.UrlPage = updatePromotionWebDto.UrlPage;
                    promotionES.Description = updatePromotionWebDto.Description;
                    promotionES.DisplayArea = updatePromotionWebDto.DisplayArea;
                    promotionES.Priority = updatePromotionWebDto.Priority;
                    promotionES.AllowShowOnline = updatePromotionWebDto.AllowShowOnline;

                    await _elasticSearchAppService.UpdateAsync(promotionId, promotionES);
                }
                return ObjectMapper.Map<Promotion, PromotionWebDto>(promotion);
            }
        }

        private async Task UpdateInputPromotionAsync(Guid promotionId, List<UpdateItemInputDto> updateInputPromotionDtos)
        {
            using (var uow = _unitOfWorkManager.Begin(new AbpUnitOfWorkOptions { IsTransactional = false }, requiresNew: true))
            {
                var promotion = await _promotionRepository.FindAsync(promotionId);
                if (promotion == null)
                {
                    throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                        _stringLocalize[ExceptionCode.NotFound], "CTKM không tồn tại");
                }

                #region Update Item Input
                var itemInput = new List<UpdateItemInputDto>();
                var newItemInputDtos = updateInputPromotionDtos!.Where(x => x.Id == null).ToList();
                if (newItemInputDtos.Any())
                {
                    var newItemInputs = ObjectMapper.Map<List<UpdateItemInputDto>, List<ItemInput>>(newItemInputDtos);
                    newItemInputs = newItemInputs.Select(p =>
                    {
                        p.PromotionId = promotion!.Id;
                        p.CreatedBy = promotion!.CreatedBy;
                        p.CreatedByName = promotion!.CreatedByName;
                        if (!promotion.UpdateBy.IsNullOrEmpty())
                        {
                            p.UpdateBy = promotion.UpdateBy;
                            p.UpdateByName = promotion!.UpdateByName;
                        }
                        return p;
                    }).ToList();
                    await _itemInputRepository.InsertManyAsync(newItemInputs);
                    itemInput = ObjectMapper.Map<List<ItemInput>, List<UpdateItemInputDto>>(newItemInputs);
                }

                // update itemInput 
                var itemInputs = await _itemInputRepository.GetListAsync(x => x.PromotionId == promotion.Id);
                var updateItemInputDtos = itemInputs.Where(x => updateInputPromotionDtos.Where(x => x.Id.HasValue).Select(x => x.Id).Contains(x.Id)).ToList();
                if (updateItemInputDtos.Any())
                {
                    var updateItemInputs = updateItemInputDtos.Select(x => ObjectMapper.Map(updateInputPromotionDtos.FirstOrDefault(a => a.Id == x.Id), x)).ToList();
                    await _itemInputRepository.UpdateManyAsync(updateItemInputs);
                    itemInput.AddRange(ObjectMapper.Map<List<ItemInput>, List<UpdateItemInputDto>>(updateItemInputs));
                }

                var listItemReplaces = new List<ItemInputReplace>();
                var itemInputReplaces = await _itemInputReplaceRepository.GetListAsync(x => x.PromotionId == promotion.Id);
                var itemInputReplaceDtos = updateInputPromotionDtos.SelectMany(x => x.ItemInputReplaces).ToList();
                if (itemInputReplaceDtos.Any())
                {
                    // insert new itemReplace
                    var newItemReplaceDtos = itemInputReplaceDtos.Where(x => x.Id == null).ToList();
                    if (newItemReplaceDtos.Any())
                    {
                        var newItemReplaces = newItemReplaceDtos.Select(x =>
                        {
                            var itemReplace = ObjectMapper.Map<UpdateItemInputReplaceDto, ItemInputReplace>(x);
                            itemReplace.InputItemId = itemInput.FirstOrDefault(x => x.LineNumber == itemReplace.LineNumber)?.Id ?? Guid.Empty;
                            itemReplace.PromotionId = promotion!.Id;
                            itemReplace.CreatedBy = promotion!.CreatedBy;
                            itemReplace.CreatedByName = promotion!.CreatedByName;
                            return itemReplace;
                        }).ToList();

                        await _itemInputReplaceRepository.InsertManyAsync(newItemReplaces);
                        listItemReplaces.AddRange(newItemReplaces);
                    }

                    // update itemInputReplace
                    var updateItemReplaceDtos = itemInputReplaces.Where(x => itemInputReplaceDtos.Where(x => x.Id.HasValue).Select(x => x.Id).Contains(x.Id)).ToList();
                    if (updateItemReplaceDtos.Any())
                    {
                        var promotionInputReplacesUpdated = await _itemInputReplaceRepository.GetListAsync(x => x.PromotionId == promotionId
                                                        && updateItemReplaceDtos.Select(x => x.Id).Contains(x.Id));

                        var inputReplacesUpdated = new List<ItemInputReplace>();
                        foreach (var itemReplaceUpdate in updateItemReplaceDtos)
                        {
                            var promotionInputReplaceUpdated = promotionInputReplacesUpdated.Find(x => x.Id == itemReplaceUpdate.Id);
                            inputReplacesUpdated.Add(ObjectMapper.Map(itemReplaceUpdate, promotionInputReplaceUpdated));
                        }
                        await _itemInputReplaceRepository.UpdateManyAsync(inputReplacesUpdated);
                        listItemReplaces.AddRange(inputReplacesUpdated);
                    }
                }

                // delete old item and itemReplace
                var deleteItemInputs = itemInputs.Where(x => !itemInput.Select(x => x.Id).Contains(x.Id)).ToList();
                if (deleteItemInputs.Any())
                {
                    await _itemInputRepository.DeleteManyAsync(deleteItemInputs.Select(x => x.Id));
                }

                var deleteInputReplaces = itemInputReplaces.Where(x => !listItemReplaces.Select(x => x.Id).Contains(x.Id));
                if (deleteInputReplaces.Any())
                {
                    await _itemInputReplaceRepository.DeleteManyAsync(deleteInputReplaces.Select(x => x.Id));
                }

                if (listItemReplaces.Any())
                {
                    itemInput.ForEach(p =>
                                p.ItemInputReplaces = ObjectMapper.Map<List<ItemInputReplace>, List<UpdateItemInputReplaceDto>>(
                                    listItemReplaces.Where(z => z.InputItemId == p.Id).ToList()));
                }
                var promotionResponseDto = ObjectMapper.Map<Promotion, PromotionDto>(promotion);

                promotionResponseDto.ItemInputs = ObjectMapper.Map<List<UpdateItemInputDto>, List<ItemInputDto>>(itemInput);

                var promotionES = await _elasticSearchAppService.GetPromotionByIdAsync(promotionId);
                if (promotionES != null)
                {
                    promotionES.Inputs = ObjectMapper.Map<List<ItemInputDto>, List<InputDocument>>(promotionResponseDto.ItemInputs);

                    await _elasticSearchAppService.UpdateAsync(promotionId, promotionES);
                }

                MapAuditedUser(ref promotion, "update");
                await _promotionRepository.UpdateAsync(promotion);

                //Delete input cache item old

                //var inputCacheItemEto = new InputCacheItemEto
                //{
                //    PromotionId = promotionId
                //};
                //await _distributedEventBus.PublishAsync(inputCacheItemEto);

                //Insert input cache item new
                //var promotionEto = ObjectMapper.Map<PromotionDto, PromotionEto>(promotionResponseDto);

                //await _distributedEventBus.PublishAsync(promotionEto);

                #endregion

                await uow.CompleteAsync();
                await uow.SaveChangesAsync();

                //PromotionCache update
                await _promotionAppService.ReCachePromotionDetailAsync(promotionId);
            }
        }

        public async Task<PromotionUpdateDetailResponseDto> UpdateStatusPromotionAsync(Guid promotionId, int? status)
        {
            using var uow = _unitOfWorkManager.Begin(isTransactional: true);
            {
                var promotion = await _promotionRepository.FindAsync(promotionId);
                if (promotion == null)
                {
                    throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                        _stringLocalize[ExceptionCode.NotFound], "CTKM không tồn tại");
                }

                if((promotion.ToDate.Date < DateTime.Now.Date) && status == 2)
                {
                    throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                        _stringLocalize[ExceptionCode.NotFound], $"CTKM không thể cập nhật thành có hiệu lực vì ngày kết thúc bé hơn {DateTime.Now.ToString("dd/MM/yyyy")}");
                }

                var campaign = await _campaignRepository.FindAsync(p => p.Id == promotion.CampaignId);
                if (campaign.Status == 3)
                {
                    throw new FrtPromotionValidationException(ExceptionCode.BadRequest,
                        _stringLocalize[ExceptionCode.BadRequest],
                        $"Không thể cập nhật trạng thái thành hiệu lực vì chiến dịch hết hiệu lục");
                }


                promotion.Status = status.Value;
                MapAuditedUser(ref promotion, "update");
                await _promotionRepository.UpdateAsync(promotion);

                await uow.CompleteAsync();

                var promotionResponseDto = ObjectMapper.Map<Promotion, PromotionDto>(promotion);

                if (promotionResponseDto != null && status == (int)PromotionStatusEnum.Created)
                {
                    await _promotionAppService.ReCachePromotionDetailAsync(promotionId, ActionType.Create);
                }

                if (promotionResponseDto != null && status == (int)PromotionStatusEnum.Activated)
                {
                    await _promotionAppService.ReCachePromotionDetailAsync(promotionId, ActionType.Active);
                }

                if (promotionResponseDto != null && status == (int)PromotionStatusEnum.InActive)
                {
                    if(promotionResponseDto.ToDate.Value.AddDays(7) == DateTime.Now)
                    {
                        var shopConditions = await _shopConditionRepository.GetListAsync(x => x.PromotionCode == promotion.Code);
                        await _promotionAppService.TimeExpireCacheAsync(new List<PromotionTimeExpireEto>
                        {
                            new PromotionTimeExpireEto
                            {
                                PromotionId = promotionId,
                                PromotionCode = promotionResponseDto.Code,
                                PromotionType = promotionResponseDto.PromotionType,
                                ProvinceConditions = shopConditions?.Select(p => p.ProvinceCode).ToList()
                            }
                        });
                    }

                    await _promotionAppService.ReCachePromotionDetailAsync(promotionId, ActionType.UpdateHeader);

                }

                var promotionES = await _elasticSearchAppService.GetPromotionByIdAsync(promotionId);
                if (promotionES != null)
                {
                    promotionES.Status = status.Value;
                    await _elasticSearchAppService.UpdateAsync(promotionId, promotionES);
                }

                return new PromotionUpdateDetailResponseDto
                {
                    PromotionId = promotionId
                };
            }
        }

        private async Task UpdateOutputPromotionAsync(Guid promotionId, List<UpdateOutputDto> updateOutputDtos)
        {
            using (var uow = _unitOfWorkManager.Begin(new AbpUnitOfWorkOptions { IsTransactional = false }, requiresNew: true))
            {
                var promotion = await _promotionRepository.FindAsync(promotionId);
                if (promotion == null)
                {
                    throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                        _stringLocalize[ExceptionCode.NotFound], "CTKM không tồn tại");
                }

                #region Update Item Output
                var itemOutput = new List<UpdateOutputDto>();
                var newItemOutputDtos = updateOutputDtos!.Where(x => x.Id == null).ToList();
                if (newItemOutputDtos.Any())
                {
                    var newItemOutputs = ObjectMapper.Map<List<UpdateOutputDto>, List<Output>>(newItemOutputDtos);
                    newItemOutputs = newItemOutputs.Select(p =>
                    {
                        p.PromotionId = promotion!.Id;
                        p.CreatedBy = promotion!.CreatedBy;
                        p.CreatedByName = promotion!.CreatedByName;
                        if (!promotion.UpdateBy.IsNullOrEmpty())
                        {
                            p.UpdateBy = promotion.UpdateBy;
                            p.UpdateByName = promotion!.UpdateByName;
                        }
                        return p;
                    }).ToList();
                    await _outputRepository.InsertManyAsync(newItemOutputs);
                    itemOutput = ObjectMapper.Map<List<Output>, List<UpdateOutputDto>>(newItemOutputs);
                }

                // update itemOutput 
                var itemOutputs = await _outputRepository.GetListAsync(x => x.PromotionId == promotion.Id);
                var updateItemOutputDtos = updateOutputDtos.Except(newItemOutputDtos).ToList();
                if (updateItemOutputDtos.Any())
                {
                     var promotionOutputsUpdated = await _outputRepository.GetListAsync(x => x.PromotionId == promotionId 
                                                        && updateItemOutputDtos.Select(x => x.Id).Contains(x.Id));

                    var outPutsUpdated = new List<Output>();
                    foreach(var itemUpdate in updateItemOutputDtos)
                    {
                        var promotionOutputUpdated = promotionOutputsUpdated.Find(x => x.Id == itemUpdate.Id);
                        outPutsUpdated.Add(ObjectMapper.Map(itemUpdate, promotionOutputUpdated));
                    }

                    await _outputRepository.UpdateManyAsync(outPutsUpdated);
                    itemOutput.AddRange(ObjectMapper.Map<List<Output>, List<UpdateOutputDto>>(outPutsUpdated));
                }

                var listItemReplaces = new List<OutputReplace>();
                var itemOutputReplaces = await _outputReplaceRepository.GetListAsync(x => x.PromotionId == promotion.Id);
                var itemOutputReplaceDtos = updateOutputDtos.SelectMany(x => x.OutputReplaces).ToList();
                if (itemOutputReplaceDtos.Any())
                {
                    // insert new itemReplace
                    var newItemReplaceDtos = itemOutputReplaceDtos.Where(x => x.Id == null).ToList();
                    if (newItemReplaceDtos.Any())
                    {
                        var newItemReplaces = newItemReplaceDtos.Select(x =>
                        {
                            var itemReplace = ObjectMapper.Map<UpdateOutputReplaceDto, OutputReplace>(x);
                            itemReplace.OutputId = itemOutput.FirstOrDefault(x => x.LineNumber == itemReplace.LineNumber)?.Id ?? Guid.Empty;
                            itemReplace.PromotionId = promotion!.Id;
                            itemReplace.CreatedBy = promotion!.CreatedBy;
                            itemReplace.CreatedByName = promotion!.CreatedByName;
                            return itemReplace;
                        }).ToList();

                        await _outputReplaceRepository.InsertManyAsync(newItemReplaces);
                        listItemReplaces.AddRange(newItemReplaces);
                    }

                    // update itemInputReplace
                    var updateItemReplaceDtos = itemOutputReplaceDtos.Except(newItemReplaceDtos).ToList();
                    if (updateItemReplaceDtos.Any())
                    {
                        var promotionOutputReplacesUpdated = await _outputReplaceRepository.GetListAsync(x => x.PromotionId == promotionId
                                                        && updateItemReplaceDtos.Select(x => x.Id).Contains(x.Id));

                        var outPutReplacesUpdated = new List<OutputReplace>();
                        foreach (var itemReplaceUpdate in updateItemReplaceDtos)
                        {
                            var promotionOutputReplaceUpdated = promotionOutputReplacesUpdated.Find(x => x.Id == itemReplaceUpdate.Id);
                            outPutReplacesUpdated.Add(ObjectMapper.Map(itemReplaceUpdate, promotionOutputReplaceUpdated));
                        }
                        await _outputReplaceRepository.UpdateManyAsync(outPutReplacesUpdated);
                        listItemReplaces.AddRange(outPutReplacesUpdated);
                    }
                }

                // delete old item and itemReplace
                var deleteItemOutputs = itemOutputs.Where(x => !itemOutput.Select(x => x.Id).Contains(x.Id)).ToList();
                if (deleteItemOutputs.Any())
                {
                    await _outputRepository.DeleteManyAsync(deleteItemOutputs.Select(x => x.Id));
                }

                var deleteOutputReplaces = itemOutputReplaces.Where(x => !listItemReplaces.Select(x => x.Id).Contains(x.Id));
                if (deleteOutputReplaces.Any())
                {
                    await _outputReplaceRepository.DeleteManyAsync(deleteOutputReplaces.Select(x => x.Id));
                }

                if (listItemReplaces.Any())
                {
                    itemOutput.ForEach(p =>
                                p.OutputReplaces = ObjectMapper.Map<List<OutputReplace>, List<UpdateOutputReplaceDto>>(
                                    listItemReplaces.Where(z => z.OutputId == p.Id).ToList()));
                }

                var promotionES = await _elasticSearchAppService.GetPromotionByIdAsync(promotionId);
                if (promotionES != null)
                {
                    promotionES.Outputs = ObjectMapper.Map<List<UpdateOutputDto>, List<OutputDocument>>(itemOutput);

                    await _elasticSearchAppService.UpdateAsync(promotionId, promotionES);
                }
                MapAuditedUser(ref promotion, "update");
                await _promotionRepository.UpdateAsync(promotion);

                #endregion

                await uow.CompleteAsync();
                await uow.SaveChangesAsync();

                //PromotionCache update
                await _promotionAppService.ReCachePromotionDetailAsync(promotionId);
            }
        }

        public async Task<PromotionUpdateDetailResponseDto> UpdateCostDistributionAsync(Guid promotionId, List<UpdateCostDistributionDto> updateCostDistributionDtos)
        {
            using (var uow = _unitOfWorkManager.Begin(new AbpUnitOfWorkOptions { IsTransactional = false }, requiresNew: true))
            {
                var promotion = await _promotionRepository.FindAsync(promotionId);
                if (promotion == null)
                {
                    throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                        _stringLocalize[ExceptionCode.NotFound], "CTKM không tồn tại");
                }


                //insert cost distribution new
                var costDistribution = new List<UpdateCostDistributionDto>();
                var newCostDistributionDtos = updateCostDistributionDtos!.Where(x => x.Id == null).ToList();
                if (newCostDistributionDtos.Any())
                {
                    var newCostDistributions = ObjectMapper.Map<List<UpdateCostDistributionDto>, List<CostDistribution>>(newCostDistributionDtos);
                    newCostDistributions = newCostDistributions.Select(p =>
                    {
                        p.PromotionId = promotion!.Id;
                        p.CreatedBy = promotion!.CreatedBy;
                        p.CreatedByName = promotion!.CreatedByName;
                        if (!promotion.UpdateBy.IsNullOrEmpty())
                        {
                            p.UpdateBy = promotion.UpdateBy;
                            p.UpdateByName = promotion!.UpdateByName;
                        }
                        return p;
                    }).ToList();
                    await _costDistributionRepository.InsertManyAsync(newCostDistributions);
                    costDistribution = ObjectMapper.Map<List<CostDistribution>, List<UpdateCostDistributionDto>>(newCostDistributions);
                }

                //update cost distribution old
                var costDistributions = await _costDistributionRepository.GetListAsync(x => x.PromotionId == promotion.Id);
                var updateCostDistributions = updateCostDistributionDtos.Except(newCostDistributionDtos).ToList();
                if (updateCostDistributions.Any())
                {
                    var costDistributionsUpdated = await _costDistributionRepository.GetListAsync(x => x.PromotionId == promotionId
                                                        && updateCostDistributions.Select(x => x.Id).Contains(x.Id));

                    var distributionsUpdated = new List<CostDistribution>();
                    foreach (var itemUpdate in updateCostDistributions)
                    {
                        var distributionUpdated = costDistributionsUpdated.Find(x => x.Id == itemUpdate.Id);
                        distributionsUpdated.Add(ObjectMapper.Map(itemUpdate, distributionUpdated));
                    }

                    await _costDistributionRepository.UpdateManyAsync(distributionsUpdated);
                    costDistribution.AddRange(ObjectMapper.Map<List<CostDistribution>, List<UpdateCostDistributionDto>>(distributionsUpdated));
                }

                // delete old cost distribution
                var deleteCostDistributions = costDistributions.Where(x => !costDistribution.Select(x => x.Id).Contains(x.Id)).ToList();
                if (deleteCostDistributions.Any())
                {
                    await _costDistributionRepository.DeleteManyAsync(deleteCostDistributions.Select(x => x.Id));
                }
                var costDistributionES = ObjectMapper.Map<List<UpdateCostDistributionDto>, List<CostDistributionDto>>(costDistribution);
                
                var promotionES = await _elasticSearchAppService.GetPromotionByIdAsync(promotionId);
                if (promotionES != null)
                {
                    promotionES.CostDistributions = ObjectMapper.Map<List<CostDistributionDto>, List<CostDistributionDocument>>(costDistributionES);

                    await _elasticSearchAppService.UpdateAsync(promotionId, promotionES);
                }


                MapAuditedUser(ref promotion, "update");
                await _promotionRepository.UpdateAsync(promotion);

                await uow.CompleteAsync();
                await uow.SaveChangesAsync();

                return new PromotionUpdateDetailResponseDto
                {
                    PromotionId = promotionId
                };
            }
        }

        public async Task<PromotionUpdateDetailResponseDto> UpdateInputAsync(Guid promotionId, UpdateInputDetailsDto updateItemInputDto)
        {
            if (updateItemInputDto.UpdateItemInputs is { Count :> 0 })
            {
                await UpdateInputPromotionAsync(promotionId, updateItemInputDto.UpdateItemInputs);
            }

            if (updateItemInputDto.UpdateItemInputExcludes is { Count:> 0 })
            {
                await UpdateItemInputExcludeAsync(promotionId, updateItemInputDto.UpdateItemInputExcludes);
            }

            return new PromotionUpdateDetailResponseDto
            {
                PromotionId = promotionId
            };
        }

        public async Task<PromotionUpdateDetailResponseDto> UpdateOutputAsync(Guid promotionId, UpdateOutputDetailsDto updateOutputDto)
        {
            if (updateOutputDto.UpdateOutputs is { Count: > 0 })
            {
                await UpdateOutputPromotionAsync(promotionId, updateOutputDto.UpdateOutputs);
            }

            return new PromotionUpdateDetailResponseDto
            {
                PromotionId = promotionId
            };
        }

        public async Task<PromotionUpdateDetailResponseDto> UpdateInputOutputAsync(Guid promotionId, UpdateInputOutputDto updateInputOutputDto)
        {
            if(updateInputOutputDto.UpdateItemInputs.Any() || updateInputOutputDto.UpdateOutputs.Any() || updateInputOutputDto.UpdateItemInputExcludes.Any())
            {
                await UpdateInputPromotionAsync(promotionId, updateInputOutputDto.UpdateItemInputs);
                await UpdateOutputPromotionAsync(promotionId, updateInputOutputDto.UpdateOutputs);
                await UpdateItemInputExcludeAsync(promotionId, updateInputOutputDto.UpdateItemInputExcludes);
            }

            return new PromotionUpdateDetailResponseDto
            {
                PromotionId = promotionId
            };
        }

        public async Task<AmountConditionDto> UpdateAmountConditionAsync(Guid promotionId, UpdateAmountConditionDto updateAmountConditionDto)
        {
            using (var uow = _unitOfWorkManager.Begin(new AbpUnitOfWorkOptions { IsTransactional = false }, requiresNew: true))
            {
                var amountCondition = await _amountConditionRepository.FindAsync(x => x.PromotionId == promotionId);
                if (amountCondition == null)
                {
                    throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                        _stringLocalize[ExceptionCode.NotFound], "Amount Condition không tồn tại");
                }

                ObjectMapper.Map(updateAmountConditionDto, amountCondition);

                await _amountConditionRepository.UpdateAsync(amountCondition);

                var promotion = await _promotionRepository.FindAsync(promotionId);


                MapAuditedUser(ref promotion, "update");
                await _promotionRepository.UpdateAsync(promotion);

                await uow.CompleteAsync();
                await uow.SaveChangesAsync();

                var promotionES = await _elasticSearchAppService.GetPromotionByIdAsync(promotionId);
                if (promotionES != null)
                {
                    promotionES.AmountCondition = ObjectMapper.Map<UpdateAmountConditionDto, AmountConditionDocument>(updateAmountConditionDto);

                    await _elasticSearchAppService.UpdateAsync(promotionId, promotionES);
                }

                //PromotionCache update
                await _promotionAppService.ReCachePromotionDetailAsync(promotionId);

                return ObjectMapper.Map<AmountCondition, AmountConditionDto>(amountCondition);
            }
        }
        
        private async Task UpdateItemInputExcludeAsync(Guid promotionId, List<UpdateItemInputExcludeDto> updateItemInputExcludeDtos)
        {
            using (var uow = _unitOfWorkManager.Begin(new AbpUnitOfWorkOptions { IsTransactional = false }, requiresNew: true))
            {
                var promotion = await _promotionRepository.FindAsync(promotionId);
                if (promotion == null)
                {
                    throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                        _stringLocalize[ExceptionCode.NotFound], "CTKM không tồn tại");
                }

                #region Update Item Input Exclude
                var itemInputExclude = new List<UpdateItemInputExcludeDto>();
                var newItemInputExcludeDtos = updateItemInputExcludeDtos!.Where(x => x.Id == null).ToList();
                if (newItemInputExcludeDtos.Any())
                {
                    var newItemInputExcludes = ObjectMapper.Map<List<UpdateItemInputExcludeDto>, List<ItemInputExclude>>(newItemInputExcludeDtos);
                    newItemInputExcludes = newItemInputExcludes.Select(p =>
                    {
                        p.PromotionId = promotion!.Id;
                        p.CreatedBy = promotion!.CreatedBy;
                        p.CreatedByName = promotion!.CreatedByName;
                        if (!promotion.UpdateBy.IsNullOrEmpty())
                        {
                            p.UpdateBy = promotion.UpdateBy;
                            p.UpdateByName = promotion!.UpdateByName;
                        }
                        return p;
                    }).ToList();
                    await _itemInputExcludeRepository.InsertManyAsync(newItemInputExcludes);
                    itemInputExclude = ObjectMapper.Map<List<ItemInputExclude>, List<UpdateItemInputExcludeDto>>(newItemInputExcludes);
                }

                // update item exclude (item loại trừ)
                var itemInputExcludes = await _itemInputExcludeRepository.GetListAsync(x => x.PromotionId == promotion.Id);
                var updatedItemInputExcludeDtos = updateItemInputExcludeDtos.Except(newItemInputExcludeDtos).ToList();
                if (updatedItemInputExcludeDtos.Any())
                {
                    var itemInputExcludesUpdated = await _itemInputExcludeRepository.GetListAsync(x => x.PromotionId == promotionId
                                                        && updatedItemInputExcludeDtos.Select(x => x.Id).Contains(x.Id));

                    var inputExcludesUpdated = new List<ItemInputExclude>();
                    foreach (var itemUpdate in updatedItemInputExcludeDtos)
                    {
                        var inputExcludeUpdated  = itemInputExcludesUpdated.Find(x => x.Id == itemUpdate.Id);
                        inputExcludesUpdated.Add(ObjectMapper.Map(itemUpdate, inputExcludeUpdated));
                    }

                    await _itemInputExcludeRepository.UpdateManyAsync(inputExcludesUpdated);
                    itemInputExclude.AddRange(ObjectMapper.Map<List<ItemInputExclude>, List<UpdateItemInputExcludeDto>>(inputExcludesUpdated));
                }



                // delete old item exclude
                var deleteItemInputExcludes = itemInputExcludes.Where(x => !itemInputExclude.Select(x => x.Id).Contains(x.Id)).ToList();
                if (deleteItemInputExcludes.Any())
                {
                    await _itemInputExcludeRepository.DeleteManyAsync(deleteItemInputExcludes.Select(x => x.Id));
                }

                var promotionES = await _elasticSearchAppService.GetPromotionByIdAsync(promotionId);
                if (promotionES != null)
                {
                    promotionES.ItemInputExcludes = itemInputExclude.Select(c => c.ItemCode).ToArray();

                    await _elasticSearchAppService.UpdateAsync(promotionId, promotionES);
                }

                MapAuditedUser(ref promotion, "update");
                await _promotionRepository.UpdateAsync(promotion);

                #endregion

                await uow.CompleteAsync();
                await uow.SaveChangesAsync();
                //PromotionCache update
                await _promotionAppService.ReCachePromotionDetailAsync(promotionId);
            }
        }

        public async Task<PromotionUpdateDetailResponseDto> UpdateExtraConditionAsync(Guid promotionId, UpdateExtraConditionDto updateExtraConditionDto)
        {
            using (var uow = _unitOfWorkManager.Begin(new AbpUnitOfWorkOptions { IsTransactional = false }, requiresNew: true))
            {
                var extraCondition = await _extraConditionRepository.FindAsync(x => x.PromotionId == promotionId);
                var promotion = await _promotionRepository.FindAsync(promotionId);
                if (extraCondition == null)
                {
                    throw new FrtPromotionValidationException(ExceptionCode.NotFound,
                        _stringLocalize[ExceptionCode.NotFound], "Extra Condition không tồn tại");
                }

                ObjectMapper.Map(updateExtraConditionDto, extraCondition);


                await _extraConditionRepository.UpdateAsync(extraCondition);


                MapAuditedUser(ref promotion, "update");
                await _promotionRepository.UpdateAsync(promotion);

                await uow.CompleteAsync();
                await uow.SaveChangesAsync();

                //PromotionCache update
                await _promotionAppService.ReCachePromotionDetailAsync(promotionId);

                //Push ES
                //var promotionES = await _elasticSearchAppService.GetPromotionByIdAsync(promotionId);
                //if (promotionES != null)
                //{
                //    promotionES.ExtraConditions = ObjectMapper.Map<UpdateExtraConditionDto, ExtraConditionDocument>(updateExtraConditionDto);

                //    await _elasticSearchAppService.UpdateAsync(promotionId, promotionES);
                //}

                return new PromotionUpdateDetailResponseDto
                {
                    PromotionId = promotionId
                };
            }
        }
    }
}
