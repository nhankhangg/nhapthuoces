﻿using FRTTMO.CAM.PromotionAPI.Enum;
using FRTTMO.CAM.PromotionAPI.ETOs;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;
using FRTTMO.CAM.PromotionAPI.Managers;
using FRTTMO.CAM.PromotionAPI.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using Volo.Abp.EventBus.Distributed;

namespace FRTTMO.CAM.PromotionAPI.Event
{
    public class PromotionEventCacheHandler : 
        IDistributedEventHandler<EventPromotionEto>, 
        IDistributedEventHandler<EventConditionEto>,
        IDistributedEventHandler<List<PromotionTimeExpireEto>>,
        IDistributedEventHandler<EventPromotionExcludeEto>,
        ITransientDependency
    {
        private readonly IHangFireAppService _hangFireAppService;
        public PromotionEventCacheHandler(IHangFireAppService hangFireAppService)
        {
            _hangFireAppService = hangFireAppService;
        }

        public async Task HandleEventAsync(EventPromotionEto eventData)
        {
            switch (eventData.ActionType)
            {
                case ActionType.Create:
                case ActionType.Update:
                case ActionType.Active:
                    await _hangFireAppService.ScheduleCreatePromotionAsync(eventData);
                    break;
                case ActionType.UpdateHeader:
                    await _hangFireAppService.UpdateHeaderPromotionAsync(eventData);
                    break;
                default:
                    break;
            }
        }

        public async Task HandleEventAsync(EventConditionEto eventData)
        {
            await _hangFireAppService.ScheduleCacheConditionAsync(eventData);
        }

        public async Task HandleEventAsync(List<PromotionTimeExpireEto> eventData)
        {
            await _hangFireAppService.UpdateStatusPromotionAsync(eventData);
        }

        public async Task HandleEventAsync(EventPromotionExcludeEto eventData)
        {
            await _hangFireAppService.UpdatePromotionExcludeRelatedAsync(eventData);
        }
    }
}
