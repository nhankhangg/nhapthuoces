﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Elasticsearch.Net;
using FRTTMO.CAM.PromotionAPI.Documents;
using Microsoft.Extensions.DependencyInjection;
using Nest;
using FRTTMO.CAM.PromotionAPI.Options;
using FRTTMO.CAM.PromotionAPI.Documents.Campaign;
using FRTTMO.CAM.PromotionAPI.Documents.PromotionInputReplaces;
using FRTTMO.CAM.PromotionAPI.Documents.PromotionOutputReplaces;

namespace FRTTMO.CAM.PromotionAPI.Extensions
{
    public static class ElasticSearchExtension
    {
        public static void AddElasticSearchService(this IServiceCollection services, IConfiguration configuration)
        {
            var elasticSearchOption = configuration.GetSection("ElasticSearch").Get<ElasticSearchOption>(options =>
            {
                options.BindNonPublicProperties = true;
            });

            var uris = elasticSearchOption.ConnectionStrings.Select(x => new Uri(x)).ToArray();
            var pool = new StaticConnectionPool(uris);
            var settings = new ConnectionSettings(pool)
                .EnableDebugMode()
                .PrettyJson()
                .RequestTimeout(TimeSpan.FromMinutes(2));
            if (!elasticSearchOption.UserName.IsNullOrEmpty())
            {
                settings.ServerCertificateValidationCallback((sender, certificate, chain, errors) => true);
                settings.ServerCertificateValidationCallback(CertificateValidations.AllowAll);
                settings.BasicAuthentication(elasticSearchOption.UserName, elasticSearchOption.Password);
            }

            var client = new ElasticClient(settings);
            services.AddSingleton<IElasticClient>(client);

            ConfigurationCampaign(ref client, elasticSearchOption);
            ConfigurationPromotion(ref client, elasticSearchOption);
            ConfigurationPromotionInPutReplace(ref client, elasticSearchOption);
            ConfigurationPromotionOutPutReplace(ref client, elasticSearchOption);
            ConfigurationQuotaHistory(ref client, elasticSearchOption);
            ConfigurationPromotionShopCondition(ref client, elasticSearchOption);
            ConfigurationPromotionExclude(ref client, elasticSearchOption);
        }

        private static void ConfigurationCampaign(ref ElasticClient client, ElasticSearchOption elasticSearchOption)
        {
            var isExists = client.Indices.Exists(elasticSearchOption.IndexCampaign).Exists;

            if (!isExists)
            {
                client.Indices.Create(elasticSearchOption.IndexCampaign,
                    m => m.Map<CampaignDocument>(
                        cd => cd
                            .AutoMap()
                            .Properties(p => p
                                .Text(t => t.Name(n => n.Id).Fields(f => f.Keyword(ff => ff.Name("keyword"))))
                                .Text(t => t.Name(n => n.Code).Fields(f => f.Keyword(ff => ff.Name("keyword"))))
                            )
                        )
                    );

                client.Indices.Refresh(elasticSearchOption.IndexCampaign);
            }
            else
            {
                var item = client.Indices.PutMapping<CampaignDocument>(descriptor =>
                    descriptor.Index(elasticSearchOption.IndexCampaign)
                        .AutoMap()
                        .Properties(p => p
                                .Text(t => t.Name(n => n.Id).Fields(f => f.Keyword(ff => ff.Name("keyword"))))
                                .Text(t => t.Name(n => n.Code).Fields(f => f.Keyword(ff => ff.Name("keyword"))))
                            )
                    );
            }
        }
        private static void ConfigurationPromotion(ref ElasticClient client, ElasticSearchOption elasticSearchOption)
        {
            var isExists = client.Indices.Exists(elasticSearchOption.IndexPromotion).Exists;
            if (!isExists)
            {
                client.Indices.Create(elasticSearchOption.IndexPromotion,
                    m => m.Map<PromotionDocument>(
                        cd => cd
                            .AutoMap()
                            .Properties(p => p
                                .Text(t => t.Name(n => n.Id).Fields(f => f.Keyword(ff => ff.Name("keyword"))))
                                .Text(t => t.Name(n => n.CampaignId).Fields(f => f.Keyword(ff => ff.Name("keyword"))))
                                .Text(t => t.Name(n => n.ItemInputExcludes).Fields(f => f.Keyword(ff => ff.Name("keyword"))))
                            )
                        )
                    );

                client.Indices.Refresh(elasticSearchOption.IndexPromotion);
            }
            else
            {
                var item = client.Indices.PutMapping<PromotionDocument>(descriptor =>
                    descriptor.Index(elasticSearchOption.IndexPromotion)
                        .AutoMap()
                        .Properties(p => p
                                .Text(t => t.Name(n => n.Id).Fields(f => f.Keyword(ff => ff.Name("keyword"))))
                                .Text(t => t.Name(n => n.CampaignId).Fields(f => f.Keyword(ff => ff.Name("keyword"))))
                                .Text(t => t.Name(n => n.ItemInputExcludes).Fields(f => f.Keyword(ff => ff.Name("keyword"))))
                            )
                    );
            }
        }
        private static void ConfigurationPromotionInPutReplace(ref ElasticClient client, ElasticSearchOption elasticSearchOption)
        {
            var isExists = client.Indices.Exists(elasticSearchOption.IndexPromotionInPutReplace).Exists;
            if (!isExists)
            {
                client.Indices.Create(elasticSearchOption.IndexPromotionInPutReplace,
                    m => m.Map<InputReplaceDocument>(
                        cd => cd
                            .AutoMap()
                        )
                    );

                client.Indices.Refresh(elasticSearchOption.IndexPromotionInPutReplace);
            }
            else
            {
                var item = client.Indices.PutMapping<InputReplaceDocument>(descriptor =>
                    descriptor.Index(elasticSearchOption.IndexPromotionInPutReplace)
                        .AutoMap()
                    );
            }
        }
        private static void ConfigurationPromotionOutPutReplace(ref ElasticClient client, ElasticSearchOption elasticSearchOption)
        {
            var isExists = client.Indices.Exists(elasticSearchOption.IndexPromotionOutPutReplace).Exists;
            if (!isExists)
            {
                client.Indices.Create(elasticSearchOption.IndexPromotionOutPutReplace,
                    m => m.Map<OutputReplaceDocument>(
                        cd => cd
                            .AutoMap()
                        )
                    );

                client.Indices.Refresh(elasticSearchOption.IndexPromotionOutPutReplace);
            }
            else
            {
                var item = client.Indices.PutMapping<OutputReplaceDocument>(descriptor =>
                    descriptor.Index(elasticSearchOption.IndexPromotionOutPutReplace)
                        .AutoMap()
                    );
            }
        }
        private static void ConfigurationQuotaHistory(ref ElasticClient client, ElasticSearchOption elasticSearchOption)
        {
            var isExists = client.Indices.Exists(elasticSearchOption.IndexQuotaHistory).Exists;
            if (!isExists)
            {
                client.Indices.Create(elasticSearchOption.IndexQuotaHistory,
                    descriptor => descriptor.Map<QuotaHistoryDocument>(cd => cd.AutoMap()));

                client.Indices.Refresh(elasticSearchOption.IndexQuotaHistory);
            }
            else
            {
                var item = client.Indices.PutMapping<QuotaHistoryDocument>(
                    descriptor => descriptor.Index(elasticSearchOption.IndexQuotaHistory).AutoMap());
            }
        }
        private static void ConfigurationPromotionShopCondition(ref ElasticClient client, ElasticSearchOption elasticSearchOption)
        {
            var isExists = client.Indices.Exists(elasticSearchOption.IndexPromotionShopCondition).Exists;
            if (!isExists)
            {
                client.Indices.Create(elasticSearchOption.IndexPromotionShopCondition,
                    descriptor => descriptor.Map<ProvinceConditionDocument>(cd => cd.AutoMap()));

                client.Indices.Refresh(elasticSearchOption.IndexPromotionShopCondition);
            }
            else
            {
                var item = client.Indices.PutMapping<ProvinceConditionDocument>(
                    descriptor => descriptor.Index(elasticSearchOption.IndexPromotionShopCondition).AutoMap());
            }
        }
        private static void ConfigurationPromotionExclude(ref ElasticClient client, ElasticSearchOption elasticSearchOption)
        {
            var isExists = client.Indices.Exists(elasticSearchOption.IndexPromotionExclude).Exists;
            if (!isExists)
            {
                client.Indices.Create(elasticSearchOption.IndexPromotionExclude,
                    descriptor => descriptor.Map<PromotionExcludeDocument>(cd => cd.AutoMap()));

                client.Indices.Refresh(elasticSearchOption.IndexPromotionExclude);
            }
            else
            {
                var item = client.Indices.PutMapping<PromotionExcludeDocument>(
                    descriptor => descriptor.Index(elasticSearchOption.IndexPromotionExclude).AutoMap());
            }
        }
    }
}
