﻿using AutoMapper;
using FRTTMO.CAM.PromotionAPI.Documents;
using FRTTMO.CAM.PromotionAPI.Entities;
using Volo.Abp.AutoMapper;

namespace FRTTMO.CAM.PromotionAPI.Extensions;

public static class PromotionAuditedUserAutoMapperExpressionExtensions
{
    public static IMappingExpression<TSource, TDestination>
        IgnorePromotionAuditedUser<TSource, TDestination>(this IMappingExpression<TSource, TDestination> mapping)
        where TDestination : IPromotionBaseEntity
    {
        return mapping
            .Ignore(x => x.CreatedBy)
            .Ignore(x => x.CreatedByName)
            .Ignore(x => x.UpdateBy)
            .Ignore(x => x.UpdateByName);
    }

    //public static IMappingExpression<TSource, TDestination>
    //    IgnorePromotionDocumentAuditedUser<TSource, TDestination>(this IMappingExpression<TSource, TDestination> mapping)
    //    where TDestination : IPromotionBaseDocument
    //{
    //    return mapping
    //        .Ignore(x => x.CreatedBy)
    //        .Ignore(x => x.CreatedByName)
    //        .Ignore(x => x.UpdateBy)
    //        .Ignore(x => x.UpdateByName)
    //        .Ignore(x => x.LastModificationTime)
    //        .Ignore(x => x.LastModifierId)
    //        .Ignore(x => x.CreationTime)
    //        .Ignore(x => x.CreatorId);
    //}
}