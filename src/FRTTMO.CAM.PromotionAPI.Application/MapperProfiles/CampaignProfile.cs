﻿using AutoMapper;
using FRTTMO.CAM.PromotionAPI.Dtos.Campaigns;
using FRTTMO.CAM.PromotionAPI.Entities.Campaign;
using FRTTMO.CAM.PromotionAPI.Extensions;
using Volo.Abp.AutoMapper;

namespace FRTTMO.CAM.PromotionAPI.MapperProfiles;

public class CampaignProfile : Profile
{
    public CampaignProfile()
    {
        CreateMap<Campaign, CampaignDto>();

        CreateMap<CreateCampaignDto, Campaign>()
            .IgnoreAuditedObjectProperties()
            .Ignore(x => x.Id)
            .Ignore(x => x.CreatedByName)
            .Ignore(x => x.UpdateByName)
            .Ignore(x => x.Code)
            .IgnorePromotionAuditedUser()
            .ReverseMap();

        CreateMap<UpdateCampaignDto, Campaign>()
            .IgnoreAuditedObjectProperties()
            .Ignore(x => x.Id)
            .Ignore(x => x.CreatedByName)
            .Ignore(x => x.UpdateByName)
            .Ignore(x => x.Code)
            .IgnorePromotionAuditedUser()
            .ForMember(x => x.Name, opts => opts.Condition(s => !string.IsNullOrEmpty(s.Name)))
            .ForMember(x => x.Description, opts => opts.Condition(s => !string.IsNullOrEmpty(s.Description)));
    }
}