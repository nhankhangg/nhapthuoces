﻿using AutoMapper;
using FRTTMO.CAM.PromotionAPI.DTOs.FieldConfigures;
using FRTTMO.CAM.PromotionAPI.DTOs.Insides;
using FRTTMO.CAM.PromotionAPI.Entities.FieldConfigure;
using FRTTMO.CAM.PromotionAPI.Entities.ImageConfigure;
using Volo.Abp.AutoMapper;

namespace FRTTMO.CAM.PromotionAPI.MapperProfiles;

public class CommonProfile : Profile
{
    public CommonProfile()
    {
        CreateMap<FieldConfigure, CommonDto>();
        CreateMap<CreateCommonDto, FieldConfigure>()
            .Ignore(x => x.Id)
            .Ignore(x => x.Description)
            .Ignore(x => x.LineNumber)
            .Ignore(x => x.ParentGroup)
            .Ignore(x => x.ParentCode)
            .Ignore(x => x.CreationTime)
            .Ignore(x => x.CreatorId);

        CreateMap<CreateCommonDto, ImageConfigure>()
           .Ignore(x => x.Id)
           .Ignore(x => x.CreationTime)
           .Ignore(x => x.CreatorId);


        CreateMap<ImageConfigure, CommonDto>()
            .Ignore(x => x.ParentGroup)
            .Ignore(x => x.ParentCode);

        CreateMap<ChildDepartmentDto, DepartmentDto>()
            .ForMember(x => x.Code, opt => opt.MapFrom(src => src.OrganCode))
            .ForMember(x => x.Name, opt => opt.MapFrom(src => src.OrganName));
    }
}