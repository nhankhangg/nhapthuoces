﻿using AutoMapper;
using FRTTMO.CAM.PromotionAPI.DTOs.AmountConditions;
using FRTTMO.CAM.PromotionAPI.DTOs.Condition;
using FRTTMO.CAM.PromotionAPI.DTOs.InstallmentConditions;
using FRTTMO.CAM.PromotionAPI.Entities.AmountCondition;
using FRTTMO.CAM.PromotionAPI.Entities.Condition;
using FRTTMO.CAM.PromotionAPI.Entities.InstallmentCondition;
using FRTTMO.CAM.PromotionAPI.Extensions;
using Newtonsoft.Json;
using Volo.Abp.AutoMapper;

namespace FRTTMO.CAM.PromotionAPI.MapperProfiles;

public class ConditionProfile : Profile
{
    public ConditionProfile()
    {
        CreateMap<CreateExtraConditionDto, ExtraCondition>()
            .IgnoreAuditedObjectProperties()
            .IgnorePromotionAuditedUser()
            .Ignore(x => x.Id)
            .Ignore(x => x.PromotionId)
            .Ignore(x => x.PromotionCode);

        CreateMap<UpdateExtraConditionDto, ExtraCondition>()
            .IgnoreAuditedObjectProperties()
            .IgnorePromotionAuditedUser()
            .Ignore(x => x.Id)
            .Ignore(x => x.PromotionId)
            .Ignore(x => x.PromotionCode);

        //response
        CreateMap<ExtraCondition, ExtraConditionDto>();

        CreateMap<CreateAmountConditionDto, AmountCondition>()
            .IgnoreAuditedObjectProperties()
            .IgnorePromotionAuditedUser()
            .Ignore(x => x.Id)
            .Ignore(x => x.PromotionId);
        CreateMap<AmountCondition, AmountConditionDto>();


        CreateMap<UpdateAmountConditionDto, AmountCondition>()
            .IgnoreAuditedObjectProperties()
            .IgnorePromotionAuditedUser()
            .Ignore(x => x.Id)
            .Ignore(x => x.PromotionId);
        
    }
}