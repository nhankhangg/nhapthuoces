﻿using AutoMapper;
using FRTTMO.CAM.PromotionAPI.DTOs.CostDistribution;
using FRTTMO.CAM.PromotionAPI.Entities.CostDistribution;
using FRTTMO.CAM.PromotionAPI.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.AutoMapper;

namespace FRTTMO.CAM.PromotionAPI.MapperProfiles
{
    public class CostDistributionProfile : Profile
    {
        public CostDistributionProfile() 
        {
            CreateMap<CreateCostDistributionDto, CostDistribution>()
                .ForMember(x => x.CategoryCode, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.CategoryCode)))
                .ForMember(x => x.CategoryName, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.CategoryName)))
                .ForMember(x => x.TypeCode, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.TypeCode)))
                .ForMember(x => x.TypeName, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.TypeName)))
                .ForMember(x => x.BrandCode, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.BrandCode)))
                .ForMember(x => x.BrandName, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.BrandName)))
                .IgnoreAuditedObjectProperties()
                .IgnorePromotionAuditedUser()
                .Ignore(x => x.Id)
                .Ignore(x => x.PromotionId);
            //CreateMap<CostDistribution, CostDistributionDto>();
                //.ForMember(x => x.CategoryCode, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.CategoryCode)))
                //.ForMember(x => x.CategoryName, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.CategoryName)))
                //.ForMember(x => x.TypeCode, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.TypeCode)))
                //.ForMember(x => x.TypeName, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.TypeName)))
                //.ForMember(x => x.BrandCode, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.BrandCode)))
                //.ForMember(x => x.BrandName, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.BrandName)));


        }
    }
}
