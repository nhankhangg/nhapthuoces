﻿using AutoMapper;
using FRTTMO.CAM.PromotionAPI.DTOs.CustomerFiles;
using FRTTMO.CAM.PromotionAPI.Entities.CustomerTags;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.AutoMapper;

namespace FRTTMO.CAM.PromotionAPI.MapperProfiles
{
    public class CustomerTagProfile : Profile
    {
        public CustomerTagProfile()
        {
            CreateMap<CreateCustomerTagDto, CustomerTags>()
                .IgnoreAuditedObjectProperties()
                .Ignore(c => c.CreatedBy)
                .Ignore(c => c.CreatedByName)
                .Ignore(c => c.UpdateBy)
                .Ignore(c => c.UpdateByName)
                .Ignore(c => c.Id);

            CreateMap<CustomerTags, CustomerTagDto>();

            CreateMap<UpdateCustomerTagDto, CustomerTags>()
                .IgnoreAuditedObjectProperties()
                .Ignore(c => c.CreatedBy)
                .Ignore(c => c.CreatedByName)
                .Ignore(c => c.UpdateBy)
                .Ignore(c => c.UpdateByName)
                .Ignore(c => c.Id);
        }
    }
}
