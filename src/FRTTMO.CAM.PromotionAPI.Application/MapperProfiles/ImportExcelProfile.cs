﻿using AutoMapper;
using FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels;
using FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels.Discounts;
using FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels.GetProducts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.AutoMapper;

namespace FRTTMO.CAM.PromotionAPI.MapperProfiles
{
    public class ImportExcelProfile : Profile
    {
        public ImportExcelProfile() 
        {
            CreateMap<CategoryGetProductInputExcelDto, DiscountCategoryExcelDto>()
                .Ignore(x => x.QualifierName)
                .Ignore(x => x.MaxQuantity)
                .Ignore(x => x.MaxValue)
                .Ignore(x => x.Discount)
                .Ignore(x => x.Note)
                .Ignore(x => x.NoteBoom)
                .Ignore(x => x.BrandCode)
                .Ignore(x => x.ModelCode)
                .Ignore(x => x.GroupCode);

            CreateMap<DiscountProductExcelDto, ValidateQuantityOutputDto>();

            CreateMap<DiscountCategoryExcelDto, ValidateQuantityOutputDto>();

            CreateMap<ProductGetProductInputExcelDto, ValidateQuantityOutputDto>()
                .Ignore(c => c.QualifierName);

            CreateMap<CategoryGetProductInputExcelDto, ValidateQuantityOutputDto>()
                .Ignore(c => c.QualifierName);
        }
    }
}
