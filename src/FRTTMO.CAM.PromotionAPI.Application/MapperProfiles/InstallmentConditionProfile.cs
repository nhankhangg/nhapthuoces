﻿using AutoMapper;
using FRTTMO.CAM.PromotionAPI.DTOs.InstallmentConditions;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInputExclude;
using FRTTMO.CAM.PromotionAPI.Entities.InstallmentCondition;
using FRTTMO.CAM.PromotionAPI.Entities.ItemInputExclude;
using FRTTMO.CAM.PromotionAPI.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.AutoMapper;

namespace FRTTMO.CAM.PromotionAPI.MapperProfiles
{
    public class InstallmentConditionProfile : Profile
    {
        public InstallmentConditionProfile()
        {
            CreateMap<CreateInstallmentConditionDto, InstallmentCondition>()
                .IgnoreAuditedObjectProperties()
                .IgnorePromotionAuditedUser()
                .Ignore(x => x.Id)
                .Ignore(x => x.PromotionId)
                .ForMember(x => x.Financiers, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.Financiers)))
                .ForMember(x => x.CardTypes, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.CardTypes)))
                .ForMember(x => x.PaymentForms, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.PaymentForms)))
                .ForMember(x => x.Tenures, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.Tenures)));
            CreateMap<InstallmentCondition, InstallmentConditionDto>()
                .ForMember(x => x.Financiers, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.Financiers)))
                .ForMember(x => x.CardTypes, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.CardTypes)))
                .ForMember(x => x.PaymentForms, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.PaymentForms)))
                .ForMember(x => x.Tenures, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.Tenures)));
            CreateMap<UpdateInstallmentConditionDto, InstallmentCondition>()
                .IgnoreAuditedObjectProperties()
                .IgnorePromotionAuditedUser()
                .Ignore(x => x.Id)
                .Ignore(x => x.PromotionId)
                .ForMember(x => x.Financiers, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.Financiers)))
                .ForMember(x => x.CardTypes, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.CardTypes)))
                .ForMember(x => x.PaymentForms, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.PaymentForms)))
                .ForMember(x => x.Tenures, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.Tenures)));
        }
    }
}
