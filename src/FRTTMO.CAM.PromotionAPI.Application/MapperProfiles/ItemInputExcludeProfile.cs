﻿using AutoMapper;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInputExclude;
using FRTTMO.CAM.PromotionAPI.Entities.ItemInputExclude;
using FRTTMO.CAM.PromotionAPI.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.AutoMapper;

namespace FRTTMO.CAM.PromotionAPI.MapperProfiles
{
    public class ItemInputExcludeProfile : Profile
    {
        public ItemInputExcludeProfile()
        {
            CreateMap<CreateItemInputExcludeDto, ItemInputExclude>()
                .IgnoreAuditedObjectProperties()
                .IgnorePromotionAuditedUser()
                .Ignore(x => x.Id)
                .Ignore(x => x.PromotionId);

            CreateMap<ItemInputExclude, ItemInputExcludeDto>();
            //.IgnoreAuditedObjectProperties()
            //.IgnorePromotionAuditedUser();

            CreateMap<UpdateItemInputExcludeDto, ItemInputExclude>()
                .IgnoreAuditedObjectProperties()
                .IgnorePromotionAuditedUser()
                .Ignore(x => x.Id)
                .Ignore(x => x.PromotionId);

            CreateMap<ItemInputExclude, UpdateItemInputExcludeDto>();
        }
    }
}
