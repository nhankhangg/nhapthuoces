﻿using AutoMapper;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInput;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInputReplace;
using FRTTMO.CAM.PromotionAPI.DTOs.PIM;
using FRTTMO.CAM.PromotionAPI.Entities.ItemInput;
using FRTTMO.CAM.PromotionAPI.Entities.ItemInputReplace;
using FRTTMO.CAM.PromotionAPI.ETOs;
using FRTTMO.CAM.PromotionAPI.Extensions;
using System.Security.Cryptography.X509Certificates;
using Volo.Abp.AutoMapper;

namespace FRTTMO.CAM.PromotionAPI.MapperProfiles;

public class ItemInputProfile : Profile
{
    public ItemInputProfile()
    {
        CreateMap<CreateItemInputDto, ItemInput>()
            .IgnoreAuditedObjectProperties()
            .IgnorePromotionAuditedUser()
            .Ignore(x => x.Id)
            .Ignore(x => x.PromotionId)
            .Ignore(x => x.DeletionTime)
            .Ignore(x => x.IsDeleted);

        CreateMap<CreateItemInputReplaceDto, ItemInputReplace>()
            .IgnoreAuditedObjectProperties()
            .IgnorePromotionAuditedUser()
            .Ignore(x => x.Id)
            .Ignore(x => x.InputItemId)
            .Ignore(x => x.PromotionId)
            .Ignore(x => x.DeletionTime)
            .Ignore(x => x.IsDeleted);

        //response
        CreateMap<ItemInput, ItemInputDto>()
            .Ignore(x => x.ItemInputReplaces);
        CreateMap<ItemInputReplace, ItemInputReplaceDto>();

        CreateMap<UpdateItemInputDto, ItemInput>()
            .IgnoreAuditedObjectProperties()
            .IgnorePromotionAuditedUser()
            .Ignore(x => x.Id)
            .Ignore(x => x.PromotionId)
            .Ignore(x => x.DeletionTime)
            .Ignore(x => x.IsDeleted);
        CreateMap<UpdateItemInputReplaceDto, ItemInputReplace>()
            .IgnoreAuditedObjectProperties()
            .IgnorePromotionAuditedUser()
            .Ignore(x => x.Id)
            .Ignore(x => x.InputItemId)
            .Ignore(x => x.PromotionId)
            .Ignore(x => x.DeletionTime)
            .Ignore(x => x.IsDeleted);

        //CreateMap<ItemInputReplaceDto, ItemInputReplaceEto>();

        //CreateMap<ItemInputReplace, ItemInputReplaceEto>();

        //CreateMap<ItemInputDto, ItemInputEto>();

        CreateMap<ItemInput, PimCacheSearch>()
            .Ignore(x => x.ScrollId)
            .Ignore(x => x.ScrollSize)
            .Ignore(x=>x.IsScroll);
        CreateMap<ItemInputReplace, ItemInput>();
    }
}