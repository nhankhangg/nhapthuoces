﻿using AutoMapper;
using FRTTMO.CAM.PromotionAPI.DTOs.Output;
using FRTTMO.CAM.PromotionAPI.DTOs.OutputReplace;
using FRTTMO.CAM.PromotionAPI.DTOs.Promotion;
using FRTTMO.CAM.PromotionAPI.Entities.Output;
using FRTTMO.CAM.PromotionAPI.Entities.OutputReplace;
using FRTTMO.CAM.PromotionAPI.Extensions;
using Volo.Abp.AutoMapper;

namespace FRTTMO.CAM.PromotionAPI.MapperProfiles;

public class OutputProfile : Profile
{
    public OutputProfile()
    {
        CreateMap<CreateOutputDto, Output>()
            .IgnoreAuditedObjectProperties()
            .IgnorePromotionAuditedUser()
            .Ignore(x => x.Id)
            .Ignore(x => x.ItemInputId)
            .Ignore(x => x.PromotionId)
            .Ignore(x => x.IsDeleted)
            .Ignore(x => x.DeletionTime);

        CreateMap<CreateOutputReplaceDto, OutputReplace>()
            .IgnoreAuditedObjectProperties()
            .IgnorePromotionAuditedUser()
            .Ignore(x => x.Id)
            .Ignore(x => x.OutputId)
            .Ignore(x => x.PromotionId)
            .Ignore(x => x.ItemInputReplaceId)
            .Ignore(x => x.IsDeleted)
            .Ignore(x => x.DeletionTime);

        //response
        CreateMap<Output, OutputDto>()
            .Ignore(x => x.OutputReplaces);
        CreateMap<OutputReplace, OutputReplaceDto>()
            .Ignore(x => x.SchemeCode)
            .Ignore(x => x.SchemeName)
            .Ignore(x => x.PaymentMethodCode)
            .Ignore(x => x.PaymentMethodName);
        CreateMap<UpdateOutputDto, CreateOutputDto>();
        CreateMap<UpdateOutputReplaceDto, CreateOutputReplaceDto>();

        ////Update output by promotion
        //CreateMap<UpdatePromotionOutputDto, Output>()
        //    .IgnoreAuditedObjectProperties()
        //    .IgnorePromotionAuditedUser()
        //    .Ignore(x => x.PromotionId)
        //    .Ignore(x => x.ItemInputId)
        //    .Ignore(x => x.DeletionTime)
        //    .Ignore(x => x.IsDeleted);

        //CreateMap<UpdatePromotionOutputReplaceDto, OutputReplace>()
        //    .IgnoreAuditedObjectProperties()
        //    .IgnorePromotionAuditedUser()
        //    .Ignore(x => x.PromotionId)
        //    .Ignore(x => x.OutputId)
        //    .Ignore(x => x.DeletionTime)
        //    .Ignore(x => x.IsDeleted)
        //    .Ignore(x => x.ItemInputReplaceId);
    }
}