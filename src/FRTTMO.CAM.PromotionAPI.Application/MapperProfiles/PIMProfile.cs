﻿using AutoMapper;
using Castle.Core.Resource;
using FRTTMO.CAM.PromotionAPI.DTOs;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInput;
using FRTTMO.CAM.PromotionAPI.DTOs.PIM;
using System.Collections.Generic;
using System.Linq;
using Volo.Abp.AutoMapper;

namespace FRTTMO.CAM.PromotionAPI.MapperProfiles
{
    public class PIMProfile : Profile
    {
        public PIMProfile()
        {
            CreateMap<Items, ProductDetailsDto>()
                .ForMember(x => x.ItemCode, opt => opt.MapFrom(src => src.Code))
                .ForMember(x => x.ItemName, opt => opt.MapFrom(src => src.Name))
                .Ignore(x => x.Units).AfterMap(AfterMapMeasureUnitDto);
            CreateMap<PIMMeasure, MeasureUnitDto>()
                .ForMember(x => x.UnitCode, opt => opt.MapFrom(src => src.MeasureUnitId.ToString()))
                .ForMember(x => x.UnitName, opt => opt.MapFrom(src => src.MeasureUnitName.ToString()))
                .Ignore(c => c.Level);
            CreateMap<PIMItemDetailWithCategoryDto, ItemDetailWithCategoryDto>()
                .ForMember(x => x.CategoryCode, opt => opt.MapFrom(src => src.Categories.FirstOrDefault(x => x.Level == 1).UniqueId))
                .ForMember(x => x.TypeCode, opt => opt.MapFrom(src => src.Categories.FirstOrDefault(x => x.Level == 2).UniqueId))
                .ForMember(x => x.ModelCode, opt => opt.MapFrom(src => src.Model.UniqueId))
                .ForMember(x => x.GroupCode, opt => opt.MapFrom(src => src.Group.UniqueId))
                .ForMember(x => x.BrandCode, opt => opt.MapFrom(src => src.Brand.UniqueId));

            CreateMap<CreateItemInputDto, ItemDetailWithCategoryDto>()
                .ForMember(x => x.Code, opt => opt.MapFrom(src => src.ItemCode));
            CreateMap<ProductCache, ItemDetails>()
                .ForMember(x => x.Code, opt => opt.MapFrom(src => src.ItemCode))
                .ForMember(x => x.Name, opt => opt.MapFrom(src => src.ItemName));
            CreateMap<Measure, PIMMeasure>();

        }

        private void AfterMapMeasureUnitDto(Items src, ProductDetailsDto dest, ResolutionContext resolutionContext)
        {
            if (src == null) return;
            dest.Measures = resolutionContext.Mapper.Map<List<MeasureUnitDto>>(src.Measures);
        }
    }
}
