﻿using FRTTMO.CAM.PromotionAPI.DTOs;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInput;
using NUglify.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using Volo.Abp.ObjectMapping;

namespace FRTTMO.CAM.PromotionAPI.MapperProfiles
{
    public class PIMWithCategoryProfile : ITransientDependency,
        IObjectMapper<List<CreateItemInputDto>, ItemDetailSuggestPromotionExcludeDto>
    {
        public ItemDetailSuggestPromotionExcludeDto Map(List<CreateItemInputDto> source)
        {
            return Map(source, new ItemDetailSuggestPromotionExcludeDto());
        }

        public ItemDetailSuggestPromotionExcludeDto Map(List<CreateItemInputDto> source, ItemDetailSuggestPromotionExcludeDto destination)
        {
            destination.Code = source.Where(x => !x.ItemCode.IsNullOrEmpty()).Select(x => x.ItemCode).ToHashSet();

            var categories = source.Where(x => x.ItemCode.IsNullOrEmpty() && x.CouponCode.IsNullOrEmpty()
                                                && x.GiftCode.IsNullOrEmpty());
            foreach (var item in categories)
            {
                if (!item.GroupCode.IsNullOrEmpty())
                {
                    if (item.BrandCode.IsNullOrEmpty() && item.ModelCode.IsNullOrEmpty())
                    {
                        destination.GroupCode.Add(item.GroupCode);
                    }
                    else
                    {
                        destination.CategoryWithBrand.Add(new CategoryWithBrandPromotionExcludeDto()
                        {
                            Code = item.GroupCode,
                            ModelCode = item.ModelCode,
                            BrandCode = item.BrandCode,
                            Level = Enum.PIMCategoryLevelEnum.Group
                        });
                    }
                    continue;
                }
                if (!item.TypeCode.IsNullOrEmpty())
                {
                    if (item.BrandCode.IsNullOrEmpty() && item.ModelCode.IsNullOrEmpty())
                    {
                        destination.TypeCode.Add(item.TypeCode);
                    }
                    else
                    {
                        destination.CategoryWithBrand.Add(new CategoryWithBrandPromotionExcludeDto()
                        {
                            Code = item.TypeCode,
                            ModelCode = item.ModelCode,
                            BrandCode = item.BrandCode,
                            Level = Enum.PIMCategoryLevelEnum.Type
                        });
                    }
                    continue;
                }
                if (!item.CategoryCode.IsNullOrEmpty())
                {
                    if (item.BrandCode.IsNullOrEmpty() && item.ModelCode.IsNullOrEmpty())
                    {
                        destination.CategoryCode.Add(item.CategoryCode);
                    }
                    else
                    {
                        destination.CategoryWithBrand.Add(new CategoryWithBrandPromotionExcludeDto()
                        {
                            Code = item.CategoryCode,
                            ModelCode = item.ModelCode,
                            BrandCode = item.BrandCode,
                            Level = Enum.PIMCategoryLevelEnum.Category
                        });
                    }
                }
            };

            var itemReplaces = source.SelectMany(x => x.ItemInputReplaces);
            if (itemReplaces.Any())
            {
                destination.Code.UnionWith(itemReplaces.Where(x => !x.ItemCode.IsNullOrEmpty()).Select(x => x.ItemCode));
                var replaceCategories = itemReplaces.Where(x => x.ItemCode.IsNullOrEmpty() && x.CouponCode.IsNullOrEmpty()
                                                && x.GiftCode.IsNullOrEmpty());
                foreach (var item in replaceCategories)
                {
                    if (!item.GroupCode.IsNullOrEmpty())
                    {
                        if (item.BrandCode.IsNullOrEmpty() && item.ModelCode.IsNullOrEmpty())
                        {
                            destination.GroupCode.Add(item.GroupCode);
                        }
                        else
                        {
                            destination.CategoryWithBrand.Add(new CategoryWithBrandPromotionExcludeDto()
                            {
                                Code = item.GroupCode,
                                ModelCode = item.ModelCode,
                                BrandCode = item.BrandCode,
                                Level = Enum.PIMCategoryLevelEnum.Group
                            });
                        }
                        continue;
                    }
                    if (!item.TypeCode.IsNullOrEmpty())
                    {
                        if (item.BrandCode.IsNullOrEmpty() && item.ModelCode.IsNullOrEmpty())
                        {
                            destination.TypeCode.Add(item.TypeCode);
                        }
                        else
                        {
                            destination.CategoryWithBrand.Add(new CategoryWithBrandPromotionExcludeDto()
                            {
                                Code = item.TypeCode,
                                ModelCode = item.ModelCode,
                                BrandCode = item.BrandCode,
                                Level = Enum.PIMCategoryLevelEnum.Type
                            });
                        }
                        continue;
                    }
                    if (!item.CategoryCode.IsNullOrEmpty())
                    {
                        if (item.BrandCode.IsNullOrEmpty() && item.ModelCode.IsNullOrEmpty())

                            destination.CategoryCode.Add(item.CategoryCode);
                    }
                    else
                    {
                        destination.CategoryWithBrand.Add(new CategoryWithBrandPromotionExcludeDto()
                        {
                            Code = item.CategoryCode,
                            ModelCode = item.ModelCode,
                            BrandCode = item.BrandCode,
                            Level = Enum.PIMCategoryLevelEnum.Category
                        });
                    }
                }
            };

            return destination;
        }
    }
}
