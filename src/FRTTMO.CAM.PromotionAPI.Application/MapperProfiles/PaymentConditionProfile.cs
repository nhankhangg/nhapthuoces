﻿using AutoMapper;
using FRTTMO.CAM.PromotionAPI.DTOs.PaymentConditions;
using FRTTMO.CAM.PromotionAPI.Entities.PaymentCondition;
using FRTTMO.CAM.PromotionAPI.Extensions;
using Newtonsoft.Json;
using Volo.Abp.AutoMapper;

namespace FRTTMO.CAM.PromotionAPI.MapperProfiles
{
    public class PaymentConditionProfile : Profile
    {
        public PaymentConditionProfile()
        {
            CreateMap<PaymentCondition, PaymentConditionDto>()
                .ForMember(x => x.BankCodes, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.BankCodes)))
                .ForMember(x => x.PINCodes, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.PINCodes)))
                .ForMember(x => x.PaymentForms, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.PaymentForms)))
                .ForMember(x => x.CardTypes, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.CardTypes)));


            CreateMap<CreatePaymentConditionDto, PaymentCondition>()
                .ForMember(x => x.BankCodes, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.BankCodes)))
                .ForMember(x => x.PINCodes, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.PINCodes)))
                .ForMember(x => x.CardTypes, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.CardTypes)))
                .ForMember(x => x.PaymentForms, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.PaymentForms)))
                .IgnoreAuditedObjectProperties()
                .IgnorePromotionAuditedUser()
                .Ignore(x => x.PromotionId)
                .Ignore(x => x.Id);

            CreateMap<UpdatePaymentConditionDto, PaymentCondition>()
                .ForMember(x => x.BankCodes, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.BankCodes)))
                .ForMember(x => x.PINCodes, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.PINCodes)))
                .ForMember(x => x.CardTypes, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.CardTypes)))
                .ForMember(x => x.PaymentForms, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.PaymentForms)))
                .IgnoreAuditedObjectProperties()
                .IgnorePromotionAuditedUser()
                .Ignore(x => x.PromotionId)
                .Ignore(x => x.Id);
        }
    }
}
