﻿using AutoMapper;
using FRTTMO.CAM.PromotionAPI.DTOs.CostDistribution;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInput;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInputReplace;
using FRTTMO.CAM.PromotionAPI.DTOs.Output;
using FRTTMO.CAM.PromotionAPI.DTOs.OutputReplace;
using FRTTMO.CAM.PromotionAPI.DTOs.Promotion.PromotionDetails;
using FRTTMO.CAM.PromotionAPI.Entities.CostDistribution;
using FRTTMO.CAM.PromotionAPI.Entities.ItemInput;
using FRTTMO.CAM.PromotionAPI.Entities.ItemInputReplace;
using FRTTMO.CAM.PromotionAPI.Entities.Output;
using FRTTMO.CAM.PromotionAPI.Entities.OutputReplace;
using FRTTMO.CAM.PromotionAPI.Entities.Promotion;
using FRTTMO.CAM.PromotionAPI.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.AutoMapper;

namespace FRTTMO.CAM.PromotionAPI.MapperProfiles
{
    public class PromotionDetailProfile : Profile
    {
        public PromotionDetailProfile() 
        {
            #region Update Header Promotion
            CreateMap<UpdatePromotionHeaderDto, PromotionHeaderDto>()
                //.Ignore(x => x.PromotionExcludes)
                .Ignore(x => x.Id);

            CreateMap<PromotionHeaderDto, Promotion>()
                .ForMember(x => x.Channels, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.Channels)))
                .ForMember(x => x.OrderTypes, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.OrderTypes)))
                .ForMember(x => x.StoreTypes, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.StoreTypes)))
                .ForMember(x => x.CustomerGroups, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.CustomerGroups)))
                //.ForMember(x => x.PromotionExcludes, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.PromotionExcludes)))
                .ForMember(x => x.SourceOrders, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.SourceOrders)))
                .IgnoreAuditedObjectProperties()
                .Ignore(x => x.Code)
                .Ignore(x => x.Status)
                .Ignore(x => x.Priority)
                .Ignore(x => x.DisplayArea)
                .Ignore(x => x.FlagDebit)
                .Ignore(x => x.UrlImage)
                .Ignore(x => x.UrlPage)
                .Ignore(x => x.NameOnline)
                .Ignore(x => x.AllowShowOnline)
                .Ignore(x => x.Description)
                .Ignore(x => x.Remark)
                .Ignore(x => x.CreatedBy)
                .Ignore(x => x.CreatedByName)
                .Ignore(x => x.UpdateBy)
                .Ignore(x => x.UpdateByName)
                .Ignore(x => x.PromotionExcludes)
                .Ignore(x => x.ProgramType)
                .Ignore(x => x.LabelDescription);

            CreateMap<UpdatePromotionHeaderDto, Promotion>()
                .ForMember(x => x.Channels, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.Channels)))
                .ForMember(x => x.OrderTypes, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.OrderTypes)))
                .ForMember(x => x.StoreTypes, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.StoreTypes)))
                .ForMember(x => x.CustomerGroups, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.CustomerGroups)))
                //.ForMember(x => x.PromotionExcludes, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.PromotionExcludes)))
                .ForMember(x => x.SourceOrders, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.SourceOrders)))
                .IgnoreAuditedObjectProperties()
                .Ignore(x => x.Id)
                .Ignore(x => x.Code)
                .Ignore(x => x.Status)
                .Ignore(x => x.Priority)
                .Ignore(x => x.DisplayArea)
                .Ignore(x => x.FlagDebit)
                .Ignore(x => x.UrlImage)
                .Ignore(x => x.UrlPage)
                .Ignore(x => x.NameOnline)
                .Ignore(x => x.AllowShowOnline)
                .Ignore(x => x.Description)
                .Ignore(x => x.Remark)
                .Ignore(x => x.CreatedBy)
                .Ignore(x => x.CreatedByName)
                .Ignore(x => x.UpdateBy)
                .Ignore(x => x.UpdateByName)
                .Ignore(x => x.PromotionClass)
                .Ignore(x => x.PromotionType)
                .Ignore(x => x.ApplicableMethod)
                .Ignore(x => x.PromotionExcludes)
                .Ignore(x => x.ProgramType)
                .Ignore(x => x.LabelDescription);

            CreateMap<Promotion, PromotionHeaderDto>()
                .ForMember(x => x.Channels, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.Channels)))
                .ForMember(x => x.OrderTypes, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.OrderTypes)))
                .ForMember(x => x.StoreTypes, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.StoreTypes)))
                .ForMember(x => x.CustomerGroups, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.CustomerGroups)))
                //.ForMember(x => x.PromotionExcludes, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.PromotionExcludes)))
                .ForMember(x => x.SourceOrders, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.SourceOrders)));



            CreateMap<UpdatePromotionHeaderDto, PromotionHeaderDto>()
                .Ignore(x => x.PromotionClass)
                .Ignore(x => x.PromotionType)
                .Ignore(x => x.ApplicableMethod)
                .Ignore(x => x.Id);

            #endregion

            #region Update Information Web Promotion
            CreateMap<UpdatePromotionWebDto, PromotionWebDto>()
                .Ignore(x => x.Id);

            CreateMap<PromotionWebDto, Promotion>()
                .IgnoreAuditedObjectProperties()
                .Ignore(x => x.Channels)
                .Ignore(x => x.StoreTypes)
                .Ignore(x => x.CustomerGroups)
                .Ignore(x => x.OrderTypes)
                .Ignore(x => x.Code)
                .Ignore(x => x.Status)
                .Ignore(x => x.PromotionExcludes)
                .Ignore(x => x.AllowDisplayOnBill)
                .Ignore(x => x.FlagDebit)
                .Ignore(x => x.NameOnline)
                .Ignore(x => x.Remark)
                .Ignore(x => x.CreatedBy)
                .Ignore(x => x.CreatedByName)
                .Ignore(x => x.UpdateBy)
                .Ignore(x => x.UpdateByName)
                .Ignore(x => x.Name)
                .Ignore(x => x.CampaignId)
                .Ignore(x => x.FromDate)
                .Ignore(x => x.ToDate)
                .Ignore(x => x.ActiveDate)
                .Ignore(x => x.FromHour)
                .Ignore(x => x.ToHour)
                .Ignore(x => x.PromotionClass)
                .Ignore(x => x.PromotionType)
                .Ignore(x => x.ApplicableMethod)
                .Ignore(x => x.TradeIndustryCode)
                .Ignore(x => x.SourceOrders)
                .Ignore(x => x.VerifyScheme)
                .Ignore(x => x.ProgramType)
                .Ignore(x => x.LabelDescription);

            CreateMap<UpdatePromotionWebDto, Promotion>()
                .IgnoreAuditedObjectProperties()
                .Ignore(x => x.Channels)
                .Ignore(x => x.StoreTypes)
                .Ignore(x => x.CustomerGroups)
                .Ignore(x => x.OrderTypes)
                .Ignore(x => x.Id)
                .Ignore(x => x.Code)
                .Ignore(x => x.Status)
                .Ignore(x => x.PromotionExcludes)
                .Ignore(x => x.AllowDisplayOnBill)
                .Ignore(x => x.FlagDebit)
                .Ignore(x => x.NameOnline)
                .Ignore(x => x.Remark)
                .Ignore(x => x.CreatedBy)
                .Ignore(x => x.CreatedByName)
                .Ignore(x => x.UpdateBy)
                .Ignore(x => x.UpdateByName)
                .Ignore(x => x.Name)
                .Ignore(x => x.CampaignId)
                .Ignore(x => x.FromDate)
                .Ignore(x => x.ToDate)
                .Ignore(x => x.ActiveDate)
                .Ignore(x => x.FromHour)
                .Ignore(x => x.ToHour)
                .Ignore(x => x.PromotionClass)
                .Ignore(x => x.PromotionType)
                .Ignore(x => x.ApplicableMethod)
                .Ignore(x => x.TradeIndustryCode)
                .Ignore(x => x.SourceOrders)
                .Ignore(x => x.VerifyScheme)
                .Ignore(x => x.ProgramType)
                .Ignore(x => x.LabelDescription);

            CreateMap<Promotion, PromotionWebDto>();
            #endregion

            #region Update Input Promotion
            CreateMap<UpdateItemInputReplaceDto, ItemInputReplace>()
                .IgnoreAuditedObjectProperties()
                .IgnorePromotionAuditedUser()
                .Ignore(x => x.PromotionId)
                .Ignore(x => x.InputItemId)
                .Ignore(x => x.DeletionTime)
                .Ignore(x => x.IsDeleted)
                .Ignore(x => x.Id);

            CreateMap<UpdateItemInputDto, ItemInput>()
                .IgnoreAuditedObjectProperties()
                .IgnorePromotionAuditedUser()
                .Ignore(x => x.PromotionId)
                .Ignore(x => x.DeletionTime)
                .Ignore(x => x.IsDeleted)
                .Ignore(x => x.Id);

            CreateMap<ItemInput, UpdateItemInputDto>()
                .Ignore(x => x.ItemInputReplaces);

            CreateMap<ItemInputReplace, UpdateItemInputReplaceDto>();

            CreateMap<UpdateItemInputDto, ItemInputDto>()
                .Ignore(x => x.PromotionId);

            CreateMap<UpdateItemInputReplaceDto, ItemInputReplaceDto>()
                .Ignore(x => x.PromotionId)
                .Ignore(x => x.InputItemId);
            #endregion

            #region Update Output Promotion
            CreateMap<Output, UpdateOutputDto>()
                .Ignore(x => x.OutputReplaces );

            CreateMap<UpdateOutputDto, Output>()
                .IgnoreAuditedObjectProperties()
                .IgnorePromotionAuditedUser()
                .Ignore(x => x.PromotionId)
                .Ignore(x => x.DeletionTime)
                .Ignore(x => x.IsDeleted)
                .Ignore(x => x.ItemInputId)
                .Ignore(x => x.Id);

            CreateMap<UpdateOutputReplaceDto, OutputReplace>()
                .IgnoreAuditedObjectProperties()
                .IgnorePromotionAuditedUser()
                .Ignore(x => x.PromotionId)
                .Ignore(x => x.DeletionTime)
                .Ignore(x => x.IsDeleted)
                .Ignore(x => x.OutputId)
                .Ignore(x => x.ItemInputReplaceId)
                .Ignore(x => x.Id);

            CreateMap<OutputReplace, UpdateOutputReplaceDto>();
            #endregion

            #region Update Cost Distribution Promotion
            //CreateMap<CostDistribution, UpdateCostDistributionDto>()
            //    .ForMember(x => x.CategoryCode, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.CategoryCode)))
            //    .ForMember(x => x.CategoryName, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.CategoryName)))
            //    .ForMember(x => x.TypeCode, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.TypeCode)))
            //    .ForMember(x => x.TypeName, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.TypeName)))
            //    .ForMember(x => x.BrandCode, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.BrandCode)))
            //    .ForMember(x => x.BrandName, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.BrandName)));

            CreateMap<UpdateCostDistributionDto, CostDistribution>()
                .ForMember(x => x.CategoryCode, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.CategoryCode)))
                .ForMember(x => x.CategoryName, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.CategoryName)))
                .ForMember(x => x.TypeCode, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.TypeCode)))
                .ForMember(x => x.TypeName, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.TypeName)))
                .ForMember(x => x.BrandCode, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.BrandCode)))
                .ForMember(x => x.BrandName, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.BrandName)))
                .IgnoreAuditedObjectProperties()
                .IgnorePromotionAuditedUser()
                .Ignore(x => x.PromotionId);
            #endregion
        }
    }
}
