﻿using AutoMapper;
using FRTTMO.CAM.PromotionAPI.Documents;
using FRTTMO.CAM.PromotionAPI.Documents.Campaign;
using FRTTMO.CAM.PromotionAPI.DTOs.AmountConditions;
using FRTTMO.CAM.PromotionAPI.DTOs.Condition;
using FRTTMO.CAM.PromotionAPI.DTOs.CostDistribution;
using FRTTMO.CAM.PromotionAPI.DTOs.InstallmentConditions;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInput;
using FRTTMO.CAM.PromotionAPI.DTOs.Output;
using FRTTMO.CAM.PromotionAPI.DTOs.PaymentConditions;
using FRTTMO.CAM.PromotionAPI.DTOs.Promotion;
using FRTTMO.CAM.PromotionAPI.DTOs.PromotionExcludes;
using FRTTMO.CAM.PromotionAPI.DTOs.RegionCondition;
using FRTTMO.CAM.PromotionAPI.Entities.Campaign;
using FRTTMO.CAM.PromotionAPI.Entities.PromotionExclude;
using Volo.Abp.AutoMapper;

namespace FRTTMO.CAM.PromotionAPI.MapperProfiles
{
    public class PromotionDocumentProfile : Profile
    {
        public PromotionDocumentProfile()
        {
            #region ES CreateAsync

            CreateMap<Campaign, CampaignDocument>();

            CreateMap<PromotionDto, PromotionDocument>()
                 .Ignore(x => x.Channels)
                 .Ignore(x => x.CustomerGroups)
                 .Ignore(x => x.StoreTypes)
                 .Ignore(x => x.OrderTypes)
                 .Ignore(x => x.ItemInputExcludes)
                 .Ignore(x => x.Quota)
                 .Ignore(x => x.AmountCondition)
                 .Ignore(x => x.Inputs)
                 .Ignore(x => x.Outputs)
                 .Ignore(x => x.ExtraConditions)
                 .Ignore(x => x.CostDistributions)
                 .Ignore(x => x.InstallmentConditions)
                 .Ignore(x => x.LastModificationTime)
                 .Ignore(x => x.LastModifierId)
                 .Ignore(x => x.CreatorId)
                 .Ignore(X => X.SourceOrders);

            CreateMap<QuotaDto, QuotaDocument>();

            CreateMap<AmountConditionDto, AmountConditionDocument>();

            CreateMap<ExtraConditionDto, ExtraConditionDocument>()
                .Ignore(x => x.PromotionCode);


            CreateMap<CostDistributionDto, CostDistributionDocument>();


            CreateMap<InstallmentConditionDto, InstallmentConditionDocument>();

            CreateMap<PaymentConditionDto, PaymentConditionDocument>();

            CreateMap<OutputDto, OutputDocument>()
                 .Ignore(x => x.CreatedBy)
                 .Ignore(x => x.CreatedByName)
                 .Ignore(x => x.CreatorId)
                 .Ignore(x => x.CreationTime)
                 .Ignore(x => x.UpdateBy)
                 .Ignore(x => x.UpdateByName)
                 .Ignore(x => x.LastModifierId)
                 .Ignore(x => x.LastModificationTime)
                 .Ignore(x => x.PromotionCode);

            CreateMap<ItemInputDto, InputDocument>()
                 .Ignore(x => x.CreatedBy)
                 .Ignore(x => x.CreatedByName)
                 .Ignore(x => x.CreatorId)
                 .Ignore(x => x.CreationTime)
                 .Ignore(x => x.UpdateBy)
                 .Ignore(x => x.UpdateByName)
                 .Ignore(x => x.LastModifierId)
                 .Ignore(x => x.LastModificationTime)
                 .Ignore(x => x.PromotionCode);

            CreateMap<PromotionExcludeDto, PromotionExcludeDocument>();

            CreateMap<ProvinceConditionDto, ProvinceConditionDocument>()
                .Ignore(x => x.PromotionCode)
                .Ignore(x => x.DeletionTime)
                .Ignore(x => x.IsDeleted);

            CreateMap<PromotionExclude, PromotionExcludeDocument>();

            #endregion

            CreateMap<PromotionDocument, PromotionByItemDto>();
        }
    }
}
