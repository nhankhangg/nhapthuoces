﻿using AutoMapper;
using FRTTMO.CAM.PromotionAPI.Documents;
using FRTTMO.CAM.PromotionAPI.DTOs.PromotionExcludes;
using FRTTMO.CAM.PromotionAPI.Entities.PromotionExclude;
using FRTTMO.CAM.PromotionAPI.ETOs.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.AutoMapper;

namespace FRTTMO.CAM.PromotionAPI.MapperProfiles
{
    public class PromotionExcludeProfile : Profile
    {
        public PromotionExcludeProfile()
        {
            CreateMap<PromotionDocument, SuggestPromotionExcludeDto>();

            CreateMap<CreatePromotionExcludeDto, PromotionExclude>()
                .IgnoreAuditedObjectProperties()
                .IgnoreCreationAuditedObjectProperties()
                .Ignore(x => x.Id)
                .Ignore(x => x.CreatedBy)
                .Ignore(x => x.CreatedByName)
                .Ignore(x => x.UpdateBy)
                .Ignore(x => x.UpdateByName);

            CreateMap<PromotionExclude, PromotionExcludeDto>()
                .Ignore(c => c.PromotionId);
            CreateMap<UpdatePromotionExcludeDto, PromotionExclude>()
                .IgnoreAuditedObjectProperties()
                .IgnoreCreationAuditedObjectProperties()
                .Ignore(x => x.Id)
                .Ignore(x => x.CreatedBy)
                .Ignore(x => x.CreatedByName)
                .Ignore(x => x.UpdateBy)
                .Ignore(x => x.UpdateByName)
                .Ignore(x => x.PromotionCode);

            CreateMap<UpdatePromotionExcludeDto, PromotionExcludeEto>()
                .Ignore(x => x.PromotionCode);

        }
    }
}
