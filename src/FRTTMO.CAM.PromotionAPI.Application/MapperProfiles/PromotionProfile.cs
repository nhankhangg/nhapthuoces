﻿using AutoMapper;
using Newtonsoft.Json;
using Volo.Abp.AutoMapper;
using FRTTMO.CAM.PromotionAPI.Extensions;
using FRTTMO.CAM.PromotionAPI.DTOs.Promotion;
using FRTTMO.CAM.PromotionAPI.Entities.Promotion;

namespace FRTTMO.CAM.PromotionAPI.MapperProfiles;

public class PromotionProfile : Profile
{
    public PromotionProfile()
    {
        CreateMap<CreatePromotionDto, Promotion>()
            .IgnoreAuditedObjectProperties()
            .IgnorePromotionAuditedUser()
            .Ignore(x => x.Id)
            .Ignore(x => x.Code)
            .ForMember(x => x.Channels, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.Channels)))
            .ForMember(x => x.OrderTypes, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.OrderTypes)))
            .ForMember(x => x.StoreTypes, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.StoreTypes)))
            .ForMember(x => x.CustomerGroups, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.CustomerGroups)))
            .ForMember(x => x.PromotionExcludes, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.PromotionExcludes)))
            .ForMember(x => x.SourceOrders, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.SourceOrders)));

        CreateMap<UpdatePromotionDto, Promotion>()
            .IgnoreAuditedObjectProperties()
            .IgnorePromotionAuditedUser()
            .Ignore(x => x.Id)
            .Ignore(x => x.Code)
            .ForMember(x => x.OrderTypes, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.OrderTypes)))
            .ForMember(x => x.StoreTypes, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.StoreTypes)))
            .ForMember(x => x.Channels, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.Channels)))
            .ForMember(x => x.CustomerGroups, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.CustomerGroups)))
            .ForMember(x => x.SourceOrders, opts => opts.MapFrom(s => JsonConvert.SerializeObject(s.SourceOrders)))
            .Ignore(x => x.PromotionExcludes);

        //response
        CreateMap<Promotion, PromotionDto>()
            .Ignore(x => x.Quota)
            .Ignore(x => x.Outputs)
            .Ignore(x => x.ItemInputs)
            .Ignore(x => x.ExtraConditions)
            .Ignore(x => x.ProvinceConditions)
            .Ignore(x => x.ItemInputExcludes)
            .Ignore(x => x.CostDistributions)
            .Ignore(x => x.AmountCondition)
            .Ignore(x => x.InstallmentConditions)
            .Ignore(x => x.PaymentConditions)
            .ForMember(x => x.Channels, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.Channels)))
            .ForMember(x => x.OrderTypes, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.OrderTypes)))
            .ForMember(x => x.StoreTypes, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.StoreTypes)))
            .ForMember(x => x.CustomerGroups, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.CustomerGroups)))
            .ForMember(x => x.PromotionExcludes, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.PromotionExcludes)))
            .ForMember(x => x.SourceOrders, opts => opts.MapFrom(s => JsonConvert.DeserializeObject(s.SourceOrders)));

    }
}