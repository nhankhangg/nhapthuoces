﻿using AutoMapper;
using FRTTMO.CAM.PromotionAPI.Documents;
using FRTTMO.CAM.PromotionAPI.DTOs.Promotion;
using FRTTMO.CAM.PromotionAPI.DTOs.Quota;
using FRTTMO.CAM.PromotionAPI.Entities.Quota;
using FRTTMO.CAM.PromotionAPI.Extensions;
using System.Linq;
using Volo.Abp.AutoMapper;

namespace FRTTMO.CAM.PromotionAPI.MapperProfiles;

public class QuotaProfile : Profile
{
    public QuotaProfile()
    {
        CreateMap<CreateQuotaDto, Quota>()
            .IgnoreAuditedObjectProperties()
            .Ignore(x => x.Id)
            .Ignore(x => x.PromotionId)
            .Ignore(x => x.PromotionCode)
            .IgnoreDeletionAuditedObjectProperties();

        CreateMap<UpdateQuotaDto, Quota>()
           .IgnoreAuditedObjectProperties()
           .Ignore(x => x.Id)
           .Ignore(x => x.PromotionId)
           .Ignore(x => x.PromotionCode)
           .IgnoreDeletionAuditedObjectProperties();

        //response
        CreateMap<Quota, QuotaDto>();
    }
}