﻿using AutoMapper;
using FRTTMO.CAM.PromotionAPI.DTOs.RegionCondition;
using FRTTMO.CAM.PromotionAPI.Entities.RegionCondition;
using FRTTMO.CAM.PromotionAPI.Extensions;
using Volo.Abp.AutoMapper;

namespace FRTTMO.CAM.PromotionAPI.MapperProfiles;

public class ShopConditionProfile : Profile
{
    public ShopConditionProfile()
    {
        CreateMap<CreateProvinceConditionDto, ProvinceCondition>()
            .IgnoreAuditedObjectProperties()
            .IgnorePromotionAuditedUser()
            .Ignore(x => x.Id)
            .Ignore(x => x.PromotionId)
            .Ignore(x => x.PromotionCode)
            .Ignore(x => x.IsDeleted)
            .Ignore(x => x.DeletionTime);

        CreateMap<UpdateProvinceConditionDto, ProvinceCondition>()
            .IgnoreAuditedObjectProperties()
            .IgnorePromotionAuditedUser()
            .Ignore(x => x.Id)
            .Ignore(x => x.PromotionId)
            .Ignore(x => x.PromotionCode)
            .Ignore(x => x.IsDeleted)
            .Ignore(x => x.DeletionTime);

        //response
        CreateMap<ProvinceCondition, ProvinceConditionDto>();
        CreateMap<ProvinceCondition, ProvinceCondition>()
            .IgnoreAuditedObjectProperties()
            .IgnorePromotionAuditedUser()
            .Ignore(x => x.Id)
            .Ignore(x => x.PromotionId)
            .Ignore(x => x.PromotionCode);

        CreateMap<ProvinceCondition, UpdateProvinceConditionDto>();
    }
}