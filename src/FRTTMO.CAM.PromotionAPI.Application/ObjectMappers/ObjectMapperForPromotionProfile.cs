﻿using System.Linq;
using Volo.Abp.ObjectMapping;
using System.Collections.Generic;
using Volo.Abp.DependencyInjection;
using FRTTMO.CAM.PromotionAPI.Enum;
using FRTTMO.CAM.PromotionAPI.ETOs;
using FRTTMO.CAM.PromotionAPI.ETOs.Cache;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;
using FRTTMO.CAM.PromotionAPI.DTOs.Promotion;
using System;

namespace FRTTMO.CAM.PromotionAPI.ObjectMappers;

public class ObjectMapperForPromotionProfile :
    IObjectMapper<PromotionDto, EventPromotionEto>,
    ITransientDependency
{
    public EventPromotionEto Map(PromotionDto source)
    {
        return Map(source, new EventPromotionEto());
    }

    public EventPromotionEto Map(PromotionDto source, EventPromotionEto destination)
    {
        var listQualifierCode = new List<string>()
        {
            "AmountOff",
            "ProductPercentOff",
            "FixedPrice",
            "PercentOff",
            "ProductAmountOff",
            "ProductFixedPrice"
        };
        destination.HeaderCache = new CacheValuePromotionHeader
        {
            Id = source.Id,
            Name = source.Name,
            Code = source.Code,
            Status = source.Status,
            FromDate = source.FromDate,
            ToDate = source.ToDate,
            ActiveDate = source.ActiveDate,
            FromHour = source.FromHour,
            ToHour = source.ToHour,
            PromotionClass = source.PromotionClass,
            PromotionType = source.PromotionType,
            DisplayArea = source.DisplayArea,
            ApplicableMethod = source.ApplicableMethod,
            Channels = source.Channels,
            OrderTypes = source.OrderTypes,
            Priority = source.Priority,
            StoreTypes = source.StoreTypes,
            AllowDisplayOnBill = source.AllowDisplayOnBill,
            FlagDebit = source.FlagDebit,
            UrlImage = source.UrlImage,
            UrlPage = source.UrlPage,
            NameOnline = source.NameOnline,
            AllowShowOnline = source.AllowShowOnline,
            Description = source.Description,
            Remark = source.Remark,
            FlagProvinceCondition = source.ProvinceConditions is { Count: > 0 },
            SourceOrders = source.SourceOrders,
            VerifyScheme = source.VerifyScheme,
            ProgramType = source.ProgramType,
            CustomerGroups = source.CustomerGroups,
            ShortDescription = source.ShortDescription,
            IsBoom = source.IsBoom,
            IsShowDetail = source.IsShowDetail,
            DescriptionBoom = source.DescriptionBoom,
            LabelDescription = source.LabelDescription,
        };

        var FlagGiftCode = false;
        if(source.ItemInputs.Any(p => !p.GiftCode.IsNullOrEmpty()) ||
            source.ItemInputs.SelectMany(p=>p.ItemInputReplaces).Any(p => !p.GiftCode.IsNullOrEmpty()))
        {
            FlagGiftCode = true;
        }    

        var FlagGiftCodeOutput = false;
        if (source.Outputs.Any(p => !p.GiftCode.IsNullOrEmpty()) || 
            source.Outputs.SelectMany(p => p.OutputReplaces).Any(z => !z.GiftCode.IsNullOrEmpty()))
        {
            FlagGiftCodeOutput = true;
        }

        var FlagCouponCodeOutput = false;
        if (source.Outputs.Any(p => !p.CouponCode.IsNullOrEmpty()) ||
            source.Outputs.SelectMany(p => p.OutputReplaces).Any(z => !z.CouponCode.IsNullOrEmpty()))
        {
             FlagCouponCodeOutput = true;
        }

        if (source.ItemInputs is { Count: > 0 })
        {
            destination.InputCache = source.ItemInputs.Select(p =>
                new ItemInputCacheEto
                {
                    PromotionId = source.Id,
                    PromotionCode = source.Code,
                    ExpireTime = source.ToDate,
                    PromotionType = source.PromotionType,
                    IsDiscount = source.Outputs.Any(c => listQualifierCode.Contains(c.QualifierCode)),
                    FlagGiftCode = FlagGiftCode,
                    FlagGiftCodeOutput = FlagGiftCodeOutput,
                    FlagCouponCodeOutput = FlagCouponCodeOutput,
                    Id = p.Id,
                    CategoryCode = p.CategoryCode?.Trim(),
                    CategoryName = p.CategoryName,
                    GroupCode = p.GroupCode?.Trim(),
                    GroupName = p.GroupName,
                    BrandCode = p.BrandCode?.Trim(),
                    BrandName = p.BrandName,
                    ModelCode = p.ModelCode?.Trim(),
                    ModelName = p.ModelName,
                    ItemCode = p.ItemCode?.ToUpper(),
                    ItemName = p.ItemName,
                    TypeCode = p.TypeCode?.Trim(),
                    TypeName = p.TypeName,
                    GiftCode = p.GiftCode?.Trim(),
                    GiftName = p.GiftName,
                    CouponCode = p.CouponCode?.Trim(),
                    CouponName = p.CouponName,
                    Quantity = p.Quantity,
                    UnitCode = p.UnitCode,
                    UnitName = p.UnitName,
                    WarehouseCode = p.WarehouseCode,
                    WarehouseName = p.WarehouseName,
                    CreatedByName = source.CreatedByName,
                    CreationTime = source.CreationTime,
                    UpdateByName = source.UpdateByName,
                    LastModificationTime = DateTime.Now.Date,
                    ItemInputReplaceCaches = p.ItemInputReplaces?.Select(t =>
                        new ItemInputCacheEto
                        {
                            Id = p.Id,
                            PromotionId = source.Id,
                            PromotionCode = source.Code,
                            ExpireTime = source.ToDate,
                            PromotionType = source.PromotionType,
                            IsDiscount = source.Outputs.Any(c => listQualifierCode.Contains(c.QualifierCode)),
                            FlagGiftCode = FlagGiftCode,
                            FlagGiftCodeOutput = FlagGiftCodeOutput,
                            FlagCouponCodeOutput = FlagCouponCodeOutput,
                            CategoryCode = t.CategoryCode?.Trim(),
                            CategoryName = t.CategoryName,
                            GroupCode = t.GroupCode?.Trim(),
                            GroupName = t.GroupName,
                            BrandCode = t.BrandCode?.Trim(),
                            BrandName = t.BrandName,
                            ModelCode = t.ModelCode?.Trim(),
                            ModelName = t.ModelName,
                            ItemCode = t.ItemCode?.Trim().ToUpper(),
                            ItemName = t.ItemName,
                            TypeCode = t.TypeCode?.Trim(),
                            TypeName = t.TypeName,
                            GiftCode = t.GiftCode?.Trim(),
                            GiftName = t.GiftName,
                            CouponCode = t.CouponCode?.Trim(),
                            CouponName = t.CouponName,
                            Quantity = t.Quantity,
                            UnitCode = t.UnitCode,
                            UnitName = t.UnitName,
                            WarehouseCode = t.WarehouseCode,
                            WarehouseName = t.WarehouseName,
                            CreatedByName = source.CreatedByName,
                            CreationTime = source.CreationTime,
                            UpdateByName = source.UpdateByName,
                            LastModificationTime = DateTime.Now.Date,
                        }).ToList()
                }).ToList();
        }

        if (source.Outputs is { Count: > 0 })
        {
            destination.OutputCache = source.Outputs.Select(p =>
                new OutputCacheEto
                {
                    Id = p.Id,
                    PromotionId = source.Id,
                    PromotionCode = source.Code,
                    ExpireTime = source.ToDate,
                    PromotionType = source.PromotionType,
                    IsDiscount = source.Outputs.Any(c => listQualifierCode.Contains(c.QualifierCode)),
                    FlagGiftCode = FlagGiftCode,
                    FlagGiftCodeOutput = FlagGiftCodeOutput,
                    FlagCouponCodeOutput = FlagCouponCodeOutput,
                    CategoryCode = p.CategoryCode?.Trim(),
                    CategoryName = p.CategoryName,
                    GroupCode = p.GroupCode?.Trim(),
                    GroupName = p.GroupName,
                    BrandCode = p.BrandCode?.Trim(),
                    BrandName = p.BrandName,
                    ModelCode = p.ModelCode?.Trim(),
                    ModelName = p.ModelName,
                    ItemCode = p.ItemCode?.Trim().ToUpper(),
                    ItemName = p.ItemName,
                    TypeCode = p.TypeCode?.Trim(),
                    TypeName = p.TypeName,
                    
                    GiftCode = p.GiftCode?.Trim(),
                    GiftName = p.GiftName,
                    CouponCode = p.CouponCode?.Trim(),
                    CouponName = p.CouponName,
                    MinQuantity = p.MinQuantity,
                    MaxQuantity = p.MaxQuantity,
                    QualifierCode = p.QualifierCode,
                    Discount = p.Discount,
                    MaxDiscount = p.MaxValue,
                    Quantity = p.Quantity,
                    UnitCode = p.UnitCode,
                    UnitName = p.UnitName,
                    WarehouseCode = p.WarehouseCode,
                    WarehouseName = p.WarehouseName,
                    Note = p.Note,
                    SchemeCode = p.SchemeCode,
                    SchemeName = p.SchemeName,
                    PaymentMethodCode = p.PaymentMethodCode,
                    PaymentMethodName = p.PaymentMethodName,
                    MaxValue = p.MaxValue,
                    NoteBoom = p.NoteBoom,
                    DiscountPlaceCode = p.DiscountPlaceCode,
                    CreatedByName = source.CreatedByName,
                    CreationTime = source.CreationTime,
                    UpdateByName = source.UpdateByName,
                    LastModificationTime = DateTime.Now.Date,
                    OutputReplaceCaches = p.OutputReplaces.Select(t => new OutputCacheEto
                    {
                        Id = p.Id,
                        PromotionId = source.Id,
                        PromotionCode = source.Code,
                        ExpireTime = source.ToDate,
                        PromotionType = source.PromotionType,
                        IsDiscount = source.Outputs.Any(c => listQualifierCode.Contains(c.QualifierCode)),
                        FlagGiftCode = FlagGiftCode,
                        FlagGiftCodeOutput = FlagGiftCodeOutput,
                        FlagCouponCodeOutput = FlagCouponCodeOutput,
                        CategoryCode = t.CategoryCode?.Trim(),
                        CategoryName = t.CategoryName,
                        GroupCode = t.GroupCode?.Trim(),
                        GroupName = t.GroupName,
                        BrandCode = t.BrandCode?.Trim(),
                        BrandName = t.BrandName,
                        ModelCode = t.ModelCode?.Trim(),
                        ModelName = t.ModelName,
                        ItemCode = t.ItemCode?.Trim().ToUpper(),
                        ItemName = t.ItemName,
                        TypeCode = t.TypeCode?.Trim(),
                        TypeName = t.TypeName,
                        GiftCode = t.GiftCode?.Trim(),
                        GiftName = t.GiftName,
                        CouponCode = t.CouponCode?.Trim(),
                        CouponName = t.CouponName,
                        MinQuantity = t.MinQuantity,
                        MaxQuantity = t.MaxQuantity,
                        QualifierCode = t.QualifierCode,
                        Discount = t.Discount,
                        MaxDiscount = t.MaxValue,
                        Quantity = t.Quantity,
                        UnitCode = t.UnitCode,
                        UnitName = t.UnitName,
                        WarehouseCode = t.WarehouseCode,
                        WarehouseName = t.WarehouseName,
                        Note = t.Note,
                        SchemeCode = t.SchemeCode,
                        SchemeName = t.SchemeName,
                        PaymentMethodCode = t.PaymentMethodCode,
                        PaymentMethodName =  t.PaymentMethodName,
                        MaxValue = t.MaxValue,
                        NoteBoom = t.NoteBoom,
                        DiscountPlaceCode = p.DiscountPlaceCode,
                        CreatedByName = source.CreatedByName,
                        CreationTime = source.CreationTime,
                        UpdateByName = source.UpdateByName,
                        LastModificationTime = DateTime.Now.Date,
                    }).ToList()
                }).ToList();
        }

        if (source.Quota != null && source.Quota.Type != null)
        {
            destination.HeaderCache.Quota = new QuotaEto
            {
                Type = source.Quota.Type,
                Quantity = source.Quota.Quantity,
                LimitQuantityShop = source.Quota.LimitQuantityShop,
                LimitQuantityPhone = source.Quota.LimitQuantityPhone,
                ResetQuotaType = source.Quota.ResetQuotaType,
                FlagQuantityPhone = source.Quota.FlagQuantityPhone,
                FlagQuantityEmail = source.Quota.FlagQuantityEmail,
                LimitQuantityEmail = source.Quota.LimitQuantityEmail,
            };
        }

        if (source.AmountCondition != null && source.AmountCondition.Type != null)
        {
            destination.HeaderCache.AmountCondition = new AmountConditionEto()
            {
                Type = source.AmountCondition.Type,
                MinAmount = source.AmountCondition.MinAmount,
                MaxAmount = source.AmountCondition.MaxAmount
            };
        }

        if (source.InstallmentConditions is { Count: > 0 })
        {
            destination.HeaderCache.InstallmentConditions = source.InstallmentConditions.Select(p => new InstallmentConditionEto
            {
                Type = p.Type,
                Financiers = p.Financiers,
                CardTypes = p.CardTypes,
                Tenures = p.Tenures,
                MaxInterestRate = p.InterestRateType.IsNullOrEmpty() ? 100 : System.Enum.Parse<InterestRateTypeEnum>(p.InterestRateType) switch
                {
                    InterestRateTypeEnum.Zero => 0,
                    InterestRateTypeEnum.NonZero => 100,
                    InterestRateTypeEnum.Optional => p.MaxInterestRate,
                    InterestRateTypeEnum.HalfPercent => 0.5f,
                    InterestRateTypeEnum.NinetyNineHundredthsPercent => 0.99f,
                    _ => 100
                },
                MinInterestRate = p.InterestRateType.IsNullOrEmpty() ? 0 : System.Enum.Parse<InterestRateTypeEnum>(p.InterestRateType) switch
                {
                    InterestRateTypeEnum.Zero => 0,
                    InterestRateTypeEnum.NonZero => 0.01f,
                    InterestRateTypeEnum.Optional => p.MinInterestRate,
                    InterestRateTypeEnum.HalfPercent => 0.5f,
                    InterestRateTypeEnum.NinetyNineHundredthsPercent => 0.99f,
                    _ => 0
                },
                PaymentType = p.PaymentType,
                PaymentForms = p.PaymentForms
            }).ToList();
        }

        if (source.ExtraConditions is { Count: > 0 })
        {
            destination.HeaderCache.ExtraConditions = source.ExtraConditions.Select(p =>
                new ExtraConditionEto
                {
                    QualifierCode = p.QualifierCode,
                    OperatorCode = p.OperatorCode,
                    Values = p.Values,
                    Number = p.Number
                }).ToList();
        }

        if (source.ItemInputExcludes is { Count: > 0 })
        {
            destination.HeaderCache.ItemInputExcludes
                = source.ItemInputExcludes
                    .Select(p => string.Join(",", $"{p.ItemCode}"))
                .ToArray();
        }

        if (source.PromotionExcludes is { Count: > 0})
        {
            destination.HeaderCache.PromotionExcludes = source.PromotionExcludes.Select(p =>
                new PromotionExcludeEto
                {
                    Name = p.Name,
                    Code = p.Code,
                    AllowDisplay = p.AllowDisplay,
                    PromotionExcludeType = p.PromotionExcludeType,
                    PromotionCode = p.PromotionCode
                }).ToList();
        }

        if (source.PaymentConditions is { Count: > 0 })
        {
            destination.HeaderCache.PaymentConditions = source.PaymentConditions.Select(p =>
                new PaymentConditionEto
                {
                    BankCodes = p.BankCodes,
                    CardTypes = p.CardTypes,
                    PaymentType = p.PaymentType,
                    PINCodes = p.PINCodes,
                    PaymentForms = p.PaymentForms
                }).ToList();
        }

        if (source.CostDistributions is { Count: > 0 })
        {
            destination.HeaderCache.CostDistributions = source.CostDistributions.Select(p =>
                new CostDistributionEto
                {
                    DepartmentCode = p.DepartmentCode,
                    DepartmentName = p.DepartmentName,
                    CategoryCode = p.Categories.Select(c => c.Code).ToList(),
                    CategoryName = p.Categories.Select(c => c.Name).ToList(),
                    TypeCode = p.Types.Select(c => c.Code).ToList(),
                    TypeName = p.Types.Select(c => c.Name).ToList(),
                    BrandCode = p.Brands.Select(c => c.Code).ToList(),
                    BrandName = p.Brands.Select(c => c.Name).ToList(),
                    Percentage = p.Percentage
                }).ToList();
        }

        return destination;
    }
}