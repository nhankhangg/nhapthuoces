﻿using System;
using System.Linq;
using Volo.Abp.ObjectMapping;
using Volo.Abp.DependencyInjection;
using FRTTMO.CAM.PromotionAPI.DTOs.PIM;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;
using FRTTMO.CAM.PromotionAPI.Documents;

namespace FRTTMO.CAM.PromotionAPI.ObjectMappers
{
    public class ObjectMapperInPutElasticSearchProfile :
       IObjectMapper<ProductCache, InputDocument>,
       ITransientDependency
    {
        public InputDocument Map(ProductCache source)
        {
            return Map(source, new InputDocument());
        }

        public InputDocument Map(ProductCache source, InputDocument destination)
        {
            return destination;
        }

    }

}
