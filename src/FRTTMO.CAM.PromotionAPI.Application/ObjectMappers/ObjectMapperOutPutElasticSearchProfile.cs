﻿using Volo.Abp.ObjectMapping;
using Volo.Abp.DependencyInjection;
using FRTTMO.CAM.PromotionAPI.DTOs.PIM;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;
using FRTTMO.CAM.PromotionAPI.Documents;

namespace FRTTMO.CAM.PromotionAPI.ObjectMappers
{
    public class ObjectMapperOutPutElasticSearchProfile :
       IObjectMapper<ProductCache, OutputDocument>,
       ITransientDependency
    {
        public OutputDocument Map(ProductCache source)
        {
            return Map(source, new OutputDocument());
        }

        public OutputDocument Map(ProductCache source, OutputDocument destination)
        {
            return destination;
        }
    }
}
