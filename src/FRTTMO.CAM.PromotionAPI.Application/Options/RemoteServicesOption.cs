﻿using System;

namespace FRTTMO.CAM.PromotionAPI.Options;

public class ElasticSearchOption
{
    private string _indexPromotion;
    private string _indexQuotaHistory;
    private string _indexCampaign;
    private string _indexPromotionInPutReplace;
    private string _indexPromotionOutPutReplace;
    private string _indexPromotionShopCondition;
    private string _indexPromotionExclude;

    public string[] ConnectionStrings { get; set; }
    public string UserName { get; set; }
    public string Password { get; set; }

    public string IndexPromotion
    {
        get => $"{_indexPromotion}";
        set => _indexPromotion = value;
    }
    public string IndexQuotaHistory
    {
        get => $"{_indexQuotaHistory}";
        set => _indexQuotaHistory = value;
    }
    public string IndexCampaign
    {
        get => $"{_indexCampaign}";
        set => _indexCampaign = value;
    }
    public string IndexPromotionInPutReplace
    {
        get => $"{_indexPromotionInPutReplace}";
        set => _indexPromotionInPutReplace = value;
    }
    public string IndexPromotionOutPutReplace
    {
        get => $"{_indexPromotionOutPutReplace}";
        set => _indexPromotionOutPutReplace = value;
    }
    public string IndexPromotionShopCondition
    {
        get => $"{_indexPromotionShopCondition}";
        set => _indexPromotionShopCondition = value;
    }
    public string IndexPromotionExclude
    {
        get => $"{_indexPromotionExclude}";
        set => _indexPromotionExclude = value;
    }
}

public class RemoteServicesOption
{
    public PIMAPI PIMAPI { get; set; }
    public PIMAPICache PIMAPICache { get; set; }
}

public class PIMAPI : BaseRemoteServicesOption
{
}

public class PIMAPICache : BaseRemoteServicesOption
{
}

public class BaseRemoteServicesOption
{
    public string BaseUrl { get; set; }
}