﻿using FRTTMO.CAM.PromotionAPI.Localization;
using Volo.Abp.Application.Services;

namespace FRTTMO.CAM.PromotionAPI;

public abstract class PromotionAPIAppService : ApplicationService
{
    protected PromotionAPIAppService()
    {
        LocalizationResource = typeof(PromotionAPIResource);
        ObjectMapperContext = typeof(PromotionAPIApplicationModule);
    }
}