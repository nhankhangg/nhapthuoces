﻿using FRTTMO.CAM.PromotionAPI.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;
using Volo.Abp.Application;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;

namespace FRTTMO.CAM.PromotionAPI;

[DependsOn(
    typeof(PromotionAPIDomainModule),
    typeof(PromotionAPIApplicationContractsModule),
    typeof(AbpDddApplicationModule),
    typeof(AbpAutoMapperModule)
)]
public class PromotionAPIApplicationModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddAutoMapperObjectMapper<PromotionAPIApplicationModule>();

        Configure<AbpAutoMapperOptions>(options => 
        { 
            options.AddMaps<PromotionAPIApplicationModule>(true); 
        });

        Configure<RemoteServicesOption>(options =>
        {
            context.Services.GetConfiguration().GetSection("RemoteServices").Bind(options);
        });

        //context.Services.AddSingleton(cfg =>
        //{
        //    IConnectionMultiplexer multiplexer =
        //        ConnectionMultiplexer.Connect(context.Services.GetConfiguration()["Redis:Configuration"]);
        //    return multiplexer.GetDatabase();
        //});

        IConnectionMultiplexer multiplexer = ConnectionMultiplexer.Connect(context.Services.GetConfiguration()["Redis:Configuration"]);
        context.Services.AddSingleton<IConnectionMultiplexer>(multiplexer);
        context.Services.AddScoped<IDatabase>(cfg => multiplexer.GetDatabase());
    }
}