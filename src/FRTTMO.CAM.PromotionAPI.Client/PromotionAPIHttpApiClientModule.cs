﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;
using Volo.Abp.VirtualFileSystem;

namespace FRTTMO.CAM.PromotionAPI;

[DependsOn(
    typeof(PromotionAPIApplicationContractsModule),
    typeof(AbpHttpClientModule))]
public class PromotionAPIHttpApiClientModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddHttpClientProxies(
            typeof(PromotionAPIApplicationContractsModule).Assembly,
            PromotionAPIRemoteServiceConsts.RemoteServiceName
        );

        Configure<AbpVirtualFileSystemOptions>(options =>
        {
            options.FileSets.AddEmbedded<PromotionAPIHttpApiClientModule>();
        });

    }
}
