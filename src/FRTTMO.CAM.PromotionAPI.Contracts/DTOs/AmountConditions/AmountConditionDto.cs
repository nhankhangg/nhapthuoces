﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace FRTTMO.CAM.PromotionAPI.DTOs.AmountConditions
{
    public class AmountConditionDto : EntityDto<Guid>
    {
        public Guid PromotionId { get; set; }
        public string Type { get; set; }
        public decimal MinAmount { get; set; }
        public decimal MaxAmount { get; set; }
    }
}
