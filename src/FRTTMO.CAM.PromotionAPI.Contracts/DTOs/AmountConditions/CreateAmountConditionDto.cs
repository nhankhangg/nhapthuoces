﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.AmountConditions
{
    public class CreateAmountConditionDto 
    {
        [StringLength(100)]
        [Required(ErrorMessage = "Không được bỏ trống loại giá trị")]
        public string Type { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Giá nhỏ nhất bắt đầu từ 1")]
        public decimal MinAmount { get; set; }
        public decimal MaxAmount { get; set; }
    }
}
