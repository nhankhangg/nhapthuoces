﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace FRTTMO.CAM.PromotionAPI.DTOs.AmountConditions
{
    public class UpdateAmountConditionDto 
    {
        public Guid? Id { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống loại giá trị")]
        public string Type { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Giá nhỏ nhất bắt đầu từ 1")]
        public long MinAmount { get; set; }
        public long MaxAmount { get; set; }
    }
}
