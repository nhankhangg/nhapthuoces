﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FRTTMO.CAM.PromotionAPI.Enum;

namespace FRTTMO.CAM.PromotionAPI.Dtos.Campaigns;

public class BaseCampaignDto : IValidatableObject
{
    [Required(ErrorMessage = "Tên chiến dịch không được để trống")]
    [StringLength(500, ErrorMessage = "Tên chiến dịch không được vượt quá 500 ký tự")]
    public string Name { get; set; }

    public string Description { get; set; }

    [Required(ErrorMessage = "Ngày bắt đầu không được để trống")]
    public DateTime? FromDate { get; set; }

    [Required(ErrorMessage = "Ngày kết thúc không được để trống")]
    public DateTime? ToDate { get; set; }

    [Required(ErrorMessage = "Trạng thái không được để trống")]
    [EnumDataType(typeof(CampaignStatusEnum), ErrorMessage = "Trạng thái không hơp lệ")]
    public int Status { get; set; }

    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
        if (FromDate!.Value.Date >= ToDate!.Value.Date)
        {
            yield return new ValidationResult("Ngày bắt đầu phải nhỏ hơn ngày kết thúc",
                new[] { nameof(FromDate), nameof(ToDate) });
        }
    }
}