﻿using System;
using Volo.Abp.Application.Dtos;

namespace FRTTMO.CAM.PromotionAPI.Dtos.Campaigns;

public class CampaignDto : EntityDto<Guid>
{
    public string Code { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public DateTime FromDate { get; set; }

    public DateTime ToDate { get; set; }

    public int Status { get; set; }

    public DateTime CreationTime { get; set; }

    public DateTime? LastModificationTime { get; set; }

    public string CreatedBy { get; set; }
    public string CreatedByName { get; set; }
}