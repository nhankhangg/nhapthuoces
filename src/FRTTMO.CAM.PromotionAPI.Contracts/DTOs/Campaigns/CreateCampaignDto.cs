﻿using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;

namespace FRTTMO.CAM.PromotionAPI.Dtos.Campaigns;

public class CreateCampaignDto : BaseCampaignDto, IValidatableObject
{
    public new IEnumerable<ValidationResult> Validate(ValidationContext validationContext) 
    {
        if (FromDate!.Value.Date >= ToDate!.Value.Date)
        {
            yield return new ValidationResult("Ngày bắt đầu phải nhỏ hơn ngày kết thúc",
                new[] { nameof(FromDate)});
        }

        if (FromDate.Value.Date < DateTime.Now.Date)
        {
            yield return new ValidationResult("Ngày bắt đầu phải lớn hơn ngày hiện tại",
                new[] { nameof(FromDate) });
        }
    }
}