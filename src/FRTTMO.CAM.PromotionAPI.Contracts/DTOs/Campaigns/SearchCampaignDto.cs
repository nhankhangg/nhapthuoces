﻿using System;
using System.Collections.Generic;
using FRTTMO.CAM.PromotionAPI.Enum;
using Volo.Abp.Application.Dtos;

namespace FRTTMO.CAM.PromotionAPI.DTOs.Campaigns;

public class SearchCampaignDto : PagedResultRequestDto
{
    public string Keyword { get; set; }

    public DateTime? FromDate { get; set; }

    public DateTime? ToDate { get; set; }

    public CampaignStatusEnum? Status { get; set; }

    public Guid? CampaignId { get; set; }

    public List<string> CreatedBy { get; set; } = new();

    public SortByResultDto Sorting { get; set; } = new();
}