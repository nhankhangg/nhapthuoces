﻿namespace FRTTMO.CAM.PromotionAPI.DTOs.Common;

public abstract class CommonBaseDto
{
    public abstract string Code { get; set; }

    public abstract string Name { get; set; }
}