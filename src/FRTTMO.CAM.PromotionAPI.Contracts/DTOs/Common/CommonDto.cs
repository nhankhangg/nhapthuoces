﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Volo.Abp.Application.Dtos;

namespace FRTTMO.CAM.PromotionAPI.DTOs.FieldConfigures;

public class CommonDto
{
    public string Code { get; set; }
    public string Name { get; set; }
    public string Group { get; set; }
    public string ParentCode { get; set; }
    public string ParentGroup { get; set; }
}

public class QualifierConfigureDto
{
    public string Code { get; set; }
    public string Name { get; set; }
    public string PromotionType { get; set; }
    public bool IsOutput { get; set; }
    public bool IsInput { get; set; }
    public bool IsCondition { get; set; }
    public List<CommonDto> Operators { get; set; }
}

public class FieldConfigureFilterDto : IValidatableObject
{
    //[Required(ErrorMessage = "Vui lòng nhập nhóm")]
    public string Group { get; set; }

    public string ParentGroup { get; set; }

    public string ParentCode { get; set; }

    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
        if (ParentCode.IsNullOrEmpty() && !ParentGroup.IsNullOrEmpty())
        {
            yield return new ValidationResult("Vui lòng nhập parentCode");
        }

        if (!ParentCode.IsNullOrEmpty() && ParentGroup.IsNullOrEmpty())
        {
            yield return new ValidationResult("Vui lòng nhập parentGroup");
        }
    }
}

public class QualifierConfigureFilterDto 
// : IValidatableObject
{
    // [Required(ErrorMessage = "Vui lòng nhập promotionClass")]
    public string PromotionClass { get; set; }

    // [Required(ErrorMessage = "Vui lòng nhập promotionType")]
    public string PromotionType { get; set; }

    // [Required(ErrorMessage = "Vui lòng nhập promotionType")]
    public string Type { get; set; }

    // public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    // {
    //     var list = new List<string> { "intput", "output", "condition" };
    //     if (!list.Contains(Type.ToLower()))
    //     {
    //         yield return new ValidationResult("Tham số type không hợp lệ");
    //     }
    // }
}

public class CreateCommonDto
{
    [StringLength(500)]
    public string Code { get; set; }
    [StringLength(500)]
    public string Name { get; set; }
    public string Group { get; set; }
}

public class FilterPromotionImageDto : PagedResultRequestDto
{
    public string Keyword { get; set; }
    public string Group { get; set; }
    public SortImageByResultDto Sorting { get; set; } = new();
}

public class SortImageByResultDto
{
    public string SortType { get; set; } = "Desc";
    public string ColumnName { get; set; } = "CreationTime";
}