﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.Common
{
    public class VoucherDefineDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public string TypeCode { get; set; }
        public int DiscountType { get; set; }
        public decimal? Amount { get; set; }
        public decimal? MaxDiscountAmount { get; set; }
        public decimal? DiscountPercent { get; set; }
        public string Prefix { get; set; }
        public int? ApplicableMethod { get; set; }
        public string ApplicableMethodCode { get; set; }

    }
}
