﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace FRTTMO.CAM.PromotionAPI.DTOs.CostDistribution
{
    public class CostDistributionDto : EntityDto<Guid>
    {
        public Guid? PromotionId { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống phòng ban")]
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }
        public List<DataPIMDto> Categories { get; set; }
        public List<DataPIMDto> Types { get; set; }
        public List<DataPIMDto> Brands { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống tỷ lệ chi phí phòng ban")]
        public decimal Percentage { get; set; }
    }
    
    public class DataPIMDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
