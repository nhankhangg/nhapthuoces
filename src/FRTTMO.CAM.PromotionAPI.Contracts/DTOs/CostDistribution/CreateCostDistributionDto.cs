﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.CostDistribution
{
    public class CreateCostDistributionDto
    {
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }
        public List<string> CategoryCode { get; set; }
        public List<string> CategoryName { get; set; }
        public List<string> TypeCode { get; set; }
        public List<string> TypeName { get; set; }
        public List<string> BrandCode { get; set; }
        public List<string> BrandName { get; set; }
        public decimal Percentage { get; set; }
    } 
}
