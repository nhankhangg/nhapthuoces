﻿using FRTTMO.CAM.PromotionAPI.Enum;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace FRTTMO.CAM.PromotionAPI.DTOs.CustomerFiles
{
    public class SearchCustomerTagDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
        public CustomerFileStatusEnum? Status { get; set; }
        public SortByResultDto Sorting { get; set; } = new();
        public string PromotionCode { get; set; }
    }
}
