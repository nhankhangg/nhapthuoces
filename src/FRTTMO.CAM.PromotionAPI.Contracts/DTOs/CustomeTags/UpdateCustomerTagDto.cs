﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.CustomerFiles
{
    public class UpdateCustomerTagDto
    {
        public string CustomerGroupCode { get; set; }
        public string CustomerGroupName { get; set; }
        public string PromotionCode { get; set; }
        public string PromotionName { get; set; }
        public Guid PromotionId { get; set; }
        public int Status { get; set; }
        public bool Tick { get; set; } // 1 : đánh dấu, 2 : không đánh dấu
    }
}
