﻿
namespace FRTTMO.CAM.PromotionAPI.DTOs.Condition;

public class CreateExtraConditionDto
{
    public string QualifierCode { get; set; }
    public string OperatorCode { get; set; }
    public string Values { get; set; }
    public decimal Number { get; set; }
}