﻿using System;
using Volo.Abp.Application.Dtos;

namespace FRTTMO.CAM.PromotionAPI.DTOs.Condition;

public class ExtraConditionDto : EntityDto<Guid>
{
    public Guid? PromotionId { get; set; }
    public string QualifierCode { get; set; }
    public string OperatorCode { get; set; }
    public string Values { get; set; }
    public decimal Number { get; set; }
}