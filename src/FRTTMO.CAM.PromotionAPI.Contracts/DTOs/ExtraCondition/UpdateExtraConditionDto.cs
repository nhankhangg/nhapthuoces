﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.Condition
{
    public class UpdateExtraConditionDto
    {
        public Guid? Id { get; set; }
        public string QualifierCode { get; set; }
        public string OperatorCode { get; set; }
        public string Values { get; set; }
        public decimal Number { get; set; }
    }
}
