﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels.Discounts
{
    public class DiscountCategoryExcelDto
    {
        public int? LineNumber { get; set; }
        public string CategoryCode { get; set; }
        public string TypeCode { get; set; }
        public string GroupCode { get; set; }
        public string BrandCode { get; set; }
        public string ModelCode { get; set; }
        public int? Quantity { get; set; }
        public string WarehouseName { get; set; }
        public string QualifierName { get; set; }
        public decimal? MaxValue { get; set; }
        public decimal? Discount { get; set; }
        public int? MaxQuantity { get; set; }
        public string Note { get; set; }
        public string NoteBoom { get; set; }
        public string UnitName { get; set; }
        public int RowNumber { get; set; }
        public List<string> Messages { get; set; }
        public bool IsError { get; set; }
        public void Validate()
        {
            Messages = new List<string>();
            //if (CategoryCode.IsNullOrEmpty())
            //{
            //    Messages.Add("Ngành hàng không được truyền null hoặc để rỗng");
            //    IsError = true;
            //}
            //if (CategoryCode.IsNullOrEmpty() && (!TypeCode.IsNullOrEmpty() || !GroupCode.IsNullOrEmpty() || !BrandCode.IsNullOrEmpty() || !ModelCode.IsNullOrEmpty()))
            //{
            //    Messages.Add("Ngành hàng không được để rỗng khi có các loại, nhóm, nhãn");
            //    IsError = true;
            //}
            if (MaxValue < 0)
            {
                Messages.Add("Số tiền giảm tối đa không được bé hơn 0");
                IsError = true;
            }
            if (MaxQuantity < 0)
            {
                Messages.Add("Số lượng mua tối đa    không được bé hơn 0");
                IsError = true;
            }
            if ((Discount < 0 || Discount > 100) && QualifierName == "Giảm giá theo phần trăm %")
            {
                Messages.Add("Giảm theo % không được bé hơn 0 và không được lớn hơn 100");
                IsError = true;
            }
            if (Discount < 0)
            {
                Messages.Add("Số tiền không được bé hơn 0");
                IsError = true;
            }
            if (WarehouseName.IsNullOrEmpty())
            {
                Messages.Add("Tên kho không được truyền null hoặc để rỗng");
                IsError = true;
            }
            if(!GroupCode.IsNullOrEmpty() && !ModelCode.IsNullOrEmpty()) 
            {
                Messages.Add("Nhóm hàng và model không hợp lệ");
                IsError = true;
            }
        }
    }
}
