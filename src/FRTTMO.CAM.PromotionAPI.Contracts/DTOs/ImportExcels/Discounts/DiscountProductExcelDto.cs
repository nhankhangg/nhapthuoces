﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels.Discounts
{
    public class DiscountProductExcelDto
    {
        public int? LineNumber { get; set; }
        public string ItemCode { get; set; }
        public int? Quantity { get; set; }
        public string WarehouseName { get; set; }
        public string QualifierName { get; set; }
        public decimal? MaxValue { get; set; }
        public decimal? Discount { get; set; }
        public int? MaxQuantity { get; set; }
        public string Note { get; set; }
        public string UnitName { get; set; }
        public int RowNumber { get; set; }

        public List<string> Messages { get; set; }
        public bool IsError { get; set; }
        public void Validate()
        {
            Messages = new List<string>();  
            if (ItemCode.IsNullOrEmpty())
            {
                Messages.Add("Mã sản phẩm không được truyền null hoặc để rỗng");
                IsError = true;
            }
            if(MaxValue < 0)
            {
                Messages.Add("Số tiền giảm tối đa không được bé hơn 0");
                IsError = true;
            }
            if (MaxQuantity < 0)
            {
                Messages.Add("Số lượng mua tối đa không được bé hơn 0");
                IsError = true;
            }
            if ((Discount < 0 || Discount > 100) && QualifierName == "Giảm giá sản phẩm theo phần trăm %")
            {
                Messages.Add("Giảm theo % không được bé hơn 0 và không được lớn hơn 100");
                IsError = true;
            }
            if (Discount < 0)
            {
                Messages.Add("Số tiền không được bé hơn 0");
                IsError = true;
            }
            if (WarehouseName.IsNullOrEmpty())
            {
                Messages.Add("Tên kho không được truyền null hoặc để rỗng");
                IsError = true;
            }
        }
    }
}
