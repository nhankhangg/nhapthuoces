﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels.GetProducts
{
    public class CategoryGetProductInputExcelDto
    {
        public int LineNumber { get; set; }
        public string CategoryCode { get; set; }
        public string TypeCode { get; set; }
        public int? Quantity { get; set; }
        public string WarehouseName { get; set; }
        public string UnitName { get; set; }
        public int RowNumber { get; set; }
        public List<string> Messages { get; set; }
        public bool IsError { get; set; }
        public void Validate()
        {
            Messages = new List<string>();
            //if (CategoryCode.IsNullOrEmpty())
            //{
            //    Messages.Add("Ngành hàng không được truyền null hoặc để rỗng");
            //    IsError = true;
            //}
            //if (CategoryCode.IsNullOrEmpty() && (!TypeCode.IsNullOrEmpty() || !GroupCode.IsNullOrEmpty() || !BrandCode.IsNullOrEmpty() || !ModelCode.IsNullOrEmpty()))
            //{
            //    Messages.Add("Ngành hàng không được để rỗng khi có các loại, nhóm, nhãn");
            //    IsError = true;
            //}
            if (WarehouseName.IsNullOrEmpty())
            {
                Messages.Add("Tên kho không được truyền null hoặc để rỗng");
                IsError = true;
            }
            if (Quantity < 0)
            {
                Messages.Add("Số lượng mua không được bé hơn 0");
                IsError = true;
            }
        }
    }
}
