﻿using FRTTMO.CAM.PromotionAPI.DTOs.ItemInput;
using FRTTMO.CAM.PromotionAPI.DTOs.Output;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels
{
    public class ImportExcelDto
    {
        public List<CreateItemInputDto> ItemInputs { get; set; }
        public List<CreateOutputDto> Outputs { get; set; }
        public List<MessageErrorDto> Errors { get; set; }
        public bool IsError
        {
            get 
            {
                return Errors?.Any() ?? false;
            }
            set
            {

            }
        }
    }
}
