﻿using FRTTMO.CAM.PromotionAPI.DTOs.ItemInputExclude;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels.ItemExcludes
{
    public class ImportItemExcludesDto
    {
        public List<CreateItemInputExcludeDto> ItemInputExcludes { get; set; }
        public List<MessageErrorDto> Errors { get; set; }
        public bool IsError
        {
            get
            {
                return Errors?.Any() ?? false;
            }
            set
            {

            }
        }
    }
}
