﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels.ItemExcludes
{
    public class ItemInputExcludesExcelDto
    {
        public string ItemCode { get; set; }
        public int RowNumber { get; set; }
        public List<string> Messages { get; set; }
        public bool IsError { get; set; }
        public void Validate()
        {
            Messages = new List<string>();
            if (ItemCode.IsNullOrEmpty())
            {
                Messages.Add("Mã sản phẩm không được truyền null hoặc để rỗng");
                IsError = true;
            }
        }
    }
}
