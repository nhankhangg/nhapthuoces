﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels
{
    public class MessageErrorDto
    {
        public int Line { get; set; }
        public string Message { get; set; }
        public List<string> Messages { get; set; }
    }
}
