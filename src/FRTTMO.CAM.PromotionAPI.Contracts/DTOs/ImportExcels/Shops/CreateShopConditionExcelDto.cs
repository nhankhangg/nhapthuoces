﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels.Shops
{
    public class CreateShopConditionExcelDto
    {
        public string ShopCode { get; set; }
        public string ShopName { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
