﻿using FRTTMO.CAM.PromotionAPI.DTOs.RegionCondition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels.Shops
{
    public class ImportItemShopDto
    {
        public List<CreateShopConditionExcelDto> ShopConditions { get; set; }
        public List<MessageErrorDto> Errors { get; set; }
        public bool IsError
        {
            get
            {
                return Errors?.Any() ?? false;
            }
            set
            {

            }
        }
    }
}
