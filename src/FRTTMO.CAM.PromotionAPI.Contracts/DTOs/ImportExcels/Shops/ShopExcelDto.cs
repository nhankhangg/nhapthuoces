﻿using FRTTMO.CAM.PromotionAPI.ETOs.Cache;
using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels.Shops
{
    public class ShopExcelDto
    {
        public string ShopCode { get; set; }
        public string ShopName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int RowNumber { get; set; }
        public List<string> Messages { get; set; }
        public bool IsError { get; set; }
        public void Validate()
        {
            Messages = new List<string>();
            if (ShopCode.IsNullOrEmpty())
            {
                Messages.Add("Shop code không được truyền null hoặc để rỗng");
                IsError = true;
            }
            if(!DateTime.TryParse(StartDate, out DateTime dateTime) && StartDate != null)
            {
                Messages.Add($"Ngày bắt đầu không đúng định dạng {StartDate}");
                IsError = true;
            }
            if (!DateTime.TryParse(EndDate, out DateTime dateTime1) && EndDate != null)
            {
                Messages.Add($"Ngày kết thúc không đúng định dạng {EndDate}");
                IsError = true;
            }
        }
    }
}
