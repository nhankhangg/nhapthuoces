﻿using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels.TotalBill
{
    public class TotalBillByOrderExcelDto
    {
        public int? LineNumber { get; set; }
        public string ItemCode { get; set; }
        public int? Quantity { get; set; }
        public string WarehouseName { get; set; }
        public string UnitName { get; set; }
        public int RowNumber { get; set; }
        public List<string> Messages { get; set; }
        public bool IsError { get; set; }
        public void Validate()
        {
            Messages = new List<string>();
            if (ItemCode.IsNullOrEmpty())
            {
                Messages.Add("Mã sản phẩm không được truyền null hoặc để rỗng");
                IsError = true;
            }
        }
    }
}
