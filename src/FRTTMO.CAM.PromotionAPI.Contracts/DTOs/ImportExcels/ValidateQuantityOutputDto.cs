﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels
{
    public class ValidateQuantityOutputDto
    {
        public int? Quantity { get; set; }
        public string QualifierName { get; set; }
        public int RowNumber { get; set; }
    }
}
