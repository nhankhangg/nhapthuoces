﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Data;
using Volo.Abp.Domain.Entities;

namespace FRTTMO.CAM.PromotionAPI.DTOs.InputCacheItem
{
    public class InputCacheItemDto : Entity<Guid>
    {
        public Guid PromotionId { get; set; }
        public Guid InputItemId { get; set; }
        public string PromotionCode { get; set; }
        public string ItemCode { get; set; }
        public int Quantity { get; set; }
        public ExtraPropertyDictionary ExtraProperties { get; set; }
    }
}
