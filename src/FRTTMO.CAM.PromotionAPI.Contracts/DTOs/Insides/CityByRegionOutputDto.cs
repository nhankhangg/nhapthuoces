﻿namespace FRTTMO.CAM.PromotionAPI.Dtos.InsideApi;

public class CityByRegionOutputDto
{
    public string CityValue { get; set; }
    public string CityName { get; set; }
    public string RegionValue { get; set; }
}