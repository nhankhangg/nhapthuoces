﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.Insides
{
    public class DepartmentDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
    public class DepartmentResponseDto
    {
        public string OrganCode { get; set; }
        public string OrganName { get; set; }
        public string ParentCode { get; set; }
        public List<ChildDepartmentDto> Children { get; set; }
    }
    public class ChildDepartmentDto
    {
        public string OrganCode { get; set; }
        public string OrganName { get; set; }
    }
}
