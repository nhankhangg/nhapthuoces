﻿using Newtonsoft.Json;

namespace FRTTMO.CAM.PromotionAPI.Dtos.Inside;

public class InsideEmployeeInfoResultDto
{
    [JsonProperty("employeeCode")] public string InsideCode { get; set; }

    [JsonProperty("employeeName")] public string FullName { get; set; }
}