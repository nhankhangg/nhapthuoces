﻿namespace FRTTMO.CAM.PromotionAPI.Dtos.InsideApi;

public class RegionShopOutputDto
{
    public string RegionCode { get; set; }
    public string RegionName { get; set; }
}