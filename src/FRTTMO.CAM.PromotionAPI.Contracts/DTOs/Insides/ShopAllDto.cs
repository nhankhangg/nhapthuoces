﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.Insides
{
    public class ShopAllDto
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public ProvinceDto Province { get; set; }
        public RegionDto Region { get; set; }
        public List<ShopFeatureDto> ShopFeatures { get; set; }
        public ShopFeatureDto ShopType { get; set; }
    }

    public class ProvinceDto
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }

    public class RegionDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class ShopFeatureDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class ListShopOutputDto
    {
        [JsonProperty("code")]
        public string RegionCode { get; set; }
        [JsonProperty("name")]
        public string RegionName { get; set; }

        [JsonProperty("status")]
        public bool Status { get; set; }

        [JsonProperty("province")]
        public List<ProvinceByRegionDto> Provinces { get; set; } = new List<ProvinceByRegionDto>();
    }

    public class ProvinceByRegionDto
    {
        [JsonProperty("name")]
        public string ProvinceName { get; set; }
        [JsonProperty("code")]
        public string ProvinceCode { get; set; }
    }
}
