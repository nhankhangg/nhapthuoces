﻿namespace FRTTMO.CAM.PromotionAPI.Dtos.InsideApi;

public class WarehouseByCityOutputDto
{
    public string WarehouseCode { get; set; }
    public string WarehouseName { get; set; }
    public string CityCode { get; set; }
}