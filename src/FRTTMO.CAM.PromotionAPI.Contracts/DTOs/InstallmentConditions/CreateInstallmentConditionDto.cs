﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.InstallmentConditions
{
    public class CreateInstallmentConditionDto
    {
        [Required]
        [StringLength(100)]
        public string Type { get; set; }
        public List<string> Financiers { get; set; }
        public List<string> CardTypes { get; set; }
        public List<int> Tenures { get; set; }
        public List<string> PaymentForms { get; set; }
        public string InterestRateType { get; set; }
        public float MinInterestRate { get; set; }
        public float MaxInterestRate { get; set; }
        public string PaymentType { get; set; }
    }
}
