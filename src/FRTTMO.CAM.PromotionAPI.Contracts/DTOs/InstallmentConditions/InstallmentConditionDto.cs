﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace FRTTMO.CAM.PromotionAPI.DTOs.InstallmentConditions
{
    public class InstallmentConditionDto : EntityDto<Guid>
    {
        public Guid PromotionId { get; set; }
        public string Type { get; set; }
        public List<string> Financiers { get; set; }
        public List<string> CardTypes { get; set; }
        public List<string> Tenures { get; set; }
        public List<string> PaymentForms { get; set; }
        public string InterestRateType { get; set; }
        public float MinInterestRate { get; set; }
        public float MaxInterestRate { get; set; }
        public string PaymentType { get; set; }
    }
}
