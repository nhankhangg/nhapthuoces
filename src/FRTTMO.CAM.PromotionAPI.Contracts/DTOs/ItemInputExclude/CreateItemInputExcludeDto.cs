﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.ItemInputExclude
{
    public class CreateItemInputExcludeDto
    {
        [StringLength(100)]
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
    }
}
