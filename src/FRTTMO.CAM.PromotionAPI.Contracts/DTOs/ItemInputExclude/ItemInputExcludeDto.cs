﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace FRTTMO.CAM.PromotionAPI.DTOs.ItemInputExclude
{
    public class ItemInputExcludeDto : EntityDto<Guid>
    {
        public Guid PromotionId { get; set; }
        [StringLength(100)]
        public string ItemCode { get; set; }
        [StringLength(1000)]
        public string ItemName { get; set; }
    }
}
