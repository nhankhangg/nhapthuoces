﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.ItemInputExclude
{
    public class UpdateItemInputExcludeDto
    {
        public Guid? Id { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
    }
}
