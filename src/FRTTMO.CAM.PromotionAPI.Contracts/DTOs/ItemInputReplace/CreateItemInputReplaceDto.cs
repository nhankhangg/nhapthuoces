﻿using System.ComponentModel.DataAnnotations;

namespace FRTTMO.CAM.PromotionAPI.DTOs.ItemInputReplace;

public class CreateItemInputReplaceDto
{
    [StringLength(100)]
    public string CategoryCode { get; set; }
    [StringLength(500)]
    public string CategoryName { get; set; }
    [StringLength(100)]
    public string GroupCode { get; set; }
    [StringLength(500)]
    public string GroupName { get; set; }
    [StringLength(100)]
    public string BrandCode { get; set; }
    [StringLength(500)]
    public string BrandName { get; set; }
    [StringLength(100)]
    public string ModelCode { get; set; }
    [StringLength(500)]
    public string ModelName { get; set; }
    [StringLength(100)]
    public string ItemCode { get; set; }
    [StringLength(1000)]
    public string ItemName { get; set; }
    [StringLength(100)]
    public string TypeCode { get; set; }
    [StringLength(500)]
    public string TypeName { get; set; }
    public string GiftCode { get; set; }
    public string GiftName { get; set; }
    public string CouponCode { get; set; }
    public string CouponName { get; set; }

    [StringLength(100)]
    public string WarehouseCode { get; set; }
    [StringLength(500)]
    public string WarehouseName { get; set; }
    [StringLength(100)]
    public string UnitName { get; set; }
    public int? UnitCode { get; set; }
    public int Quantity { get; set; }
    public int LineNumber { get; set; }
}