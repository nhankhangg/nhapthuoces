﻿using System;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace FRTTMO.CAM.PromotionAPI.DTOs.OutputReplace;

public class OutputReplaceDto : EntityDto<Guid>
{
    public Guid PromotionId { get; set; }
    public Guid OutputId { get; set; }
    public Guid? ItemInputReplaceId { get; set; }
    public string QualifierCode { get; set; }
    public string OperatorCode { get; set; }
    public int Quantity { get; set; }
    public int MinQuantity { get; set; }
    public int MaxQuantity { get; set; }
    public int? UnitCode { get; set; }
    public string UnitName { get; set; }
    public string WarehouseCode { get; set; }
    public string WarehouseName { get; set; }
    public decimal MinValue { get; set; }
    public decimal MaxValue { get; set; }
    public decimal Discount { get; set; }
    public string Note { get; set; }
    public int LineNumber { get; set; }
    public string CategoryCode { get; set; }
    public string CategoryName { get; set; }
    public string GroupCode { get; set; }
    public string GroupName { get; set; }
    public string BrandCode { get; set; }
    public string BrandName { get; set; }
    public string ModelCode { get; set; }
    public string ModelName { get; set; }
    public string ItemCode { get; set; }
    public string ItemName { get; set; }
    public string TypeCode { get; set; }
    public string TypeName { get; set; }
    public string GiftCode { get; set; }
    public string GiftName { get; set; }
    public string CouponCode { get; set; }
    public string CouponName { get; set; }
    public string SchemeCode { get; set; }
    public string SchemeName { get; set; }
    public string PaymentMethodCode { get; set; }
    public string PaymentMethodName { get; set; }
    public string NoteBoom { get; set; }
    public string DiscountPlaceCode { get; set; }
    public string DiscountPlaceName { get; set; }
}