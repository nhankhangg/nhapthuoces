﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FRTTMO.CAM.PromotionAPI.DTOs.OutputReplace;

public class UpdateOutputReplaceDto
{
    public Guid? Id { get; set; }
    public string QualifierCode { get; set; }
    public string OperatorCode { get; set; }
    public int Quantity { get; set; }
    public int? MinQuantity { get; set; } = 0;
    public int? MaxQuantity { get; set; } = 0;
    public int? UnitCode { get; set; }
    public string UnitName { get; set; }
    public string WarehouseCode { get; set; }
    public string WarehouseName { get; set; }
    public decimal? MinValue { get; set; } = decimal.Zero;
    public decimal? MaxValue { get; set; } = decimal.Zero;
    public decimal Discount { get; set; }
    public string Note { get; set; }
    public int LineNumber { get; set; }
    [StringLength(100)]
    public string CategoryCode { get; set; }
    [StringLength(500)]
    public string CategoryName { get; set; }
    [StringLength(100)]
    public string GroupCode { get; set; }
    [StringLength(500)]
    public string GroupName { get; set; }
    [StringLength(100)]
    public string BrandCode { get; set; }
    [StringLength(500)]
    public string BrandName { get; set; }
    [StringLength(100)]
    public string ModelCode { get; set; }
    [StringLength(500)]
    public string ModelName { get; set; }
    [StringLength(100)]
    public string ItemCode { get; set; }
    [StringLength(1000)]
    public string ItemName { get; set; }
    [StringLength(100)]
    public string TypeCode { get; set; }
    [StringLength(500)]
    public string TypeName { get; set; }
    public string GiftCode { get; set; }
    public string GiftName { get; set; }
    public string CouponCode { get; set; }
    public string CouponName { get; set; }
    public string NoteBoom { get; set; }
    public string DiscountPlaceCode { get; set; }
    public string DiscountPlaceName { get; set; }
}