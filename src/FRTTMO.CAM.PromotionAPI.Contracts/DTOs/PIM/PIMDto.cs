﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using FRTTMO.CAM.PromotionAPI.DTOs.Common;
using FRTTMO.CAM.PromotionAPI.Enum;
using Newtonsoft.Json;

namespace FRTTMO.CAM.PromotionAPI.DTOs;

#region Search Input DTO
public class SearchCategoryLevelInputDto
{
    public string Keyword { get; set; }
    public string ParentCode { get; set; }
    public string Type { get; set; }
}
#endregion

#region Master Data PIM Model
public class PIMCategoryLevelOutputDto
{
    public MetaData MetaData { get; set; }
    [JsonProperty("Data")]
    public List<PIMCategoryDto> Items { get; set; }
}
public class PIMCategoryDto
{
    [JsonProperty("categoryName")]
    public string Name { get; set; }
    [JsonProperty("categoryUniqueId")]
    public string Code { get; set; }
}

public class PIMTypeLevelOutputDto
{
    public MetaData MetaData { get; set; }
    [JsonProperty("Data")]
    public List<PIMTypeDto> Items { get; set; }
}

public class PIMTypeDto
{
    [JsonProperty("groupName")]
    public string Name { get; set; }
    [JsonProperty("groupUniqueId")]
    public string Code { get; set; }
}

public class PIMBrandOutputDto
{
    public long Total { get; set; }
    public List<PIMBrandDto> Items { get; set; }
}
public class PIMBrandDto
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string UniqueId { get; set; }
}

#endregion

#region Master Data Model Mapping

public class CategoryLevelDto : CommonBaseDto
{
    public override string Code { get; set; }
    public override string Name { get; set; }
}

public class MeasureUnitDto
{
    [JsonProperty("uniqueId")]
    public string UnitCode { get; set; }
    [JsonProperty("measureUnitName")]
    public string UnitName { get; set; }
    public int Level { get; set; }
}

public class DataUnitDto
{
    public List<MeasureUnitDto> Data { get; set; }
}

#endregion

#region Models PIM Product
public class CategoryCommon
{
    public int Id { get; set; }
    public string CategoryId { get; set; }
    public string CategoryName { get; set; }
    public int Level { get; set; }
    public string UniqueId { get; set; }
    public string Type { get; set; }
}

public class PIMMeasure
{
    [JsonProperty("uniqueId")]
    public int MeasureUnitId { get; set; }
    public string MeasureUnitName { get; set; }
    public int Level { get; set; }
}

public class PIMTypeProduct
{
    public int Id { get; set; }
    [JsonProperty("categoryId")]
    public string TypeId { get; set; }
    [JsonProperty("categoryName")]
    public string TypeName { get; set; }
    public int Level { get; set; }
}

public class PIMGroup
{
    public int Id { get; set; }
    [JsonProperty("categoryId")]
    public string GroupId { get; set; }
    [JsonProperty("categoryName")]
    public string GroupName { get; set; }
    public int Level { get; set; }
}
public class PIMModel
{
    public int Id { get; set; }
    [JsonProperty("categoryId")]
    public string ModelId { get; set; }
    [JsonProperty("categoryName")]
    public string ModelName { get; set; }
    public int Level { get; set; }
}

public class PIMTaxonomies
{
    public int Id { get; set; }
    public string TaxonomyId { get; set; }
    public string TaxonomyName { get; set; }
    public int Level { get; set; }
}

public class TaxonomyDto
{
    public int Id { get; set; }
    public string TaxonomyId { get; set; }
    public string TaxonomyName { get; set; }
    public int Level { get; set; }
}

public class SearchProductInputDto
{
    public string Keyword { get; set; }
    public string TypeCode { get; set; }
    public string CategoryCode { get; set; }
}

public class SearchOutputDto
{
    public int TotalCount { get; set; }
    public List<ProductDetailsDto> Data { get; set; }
}

public class Items
{
    public string Name { get; set; }
    [JsonProperty("sku")]
    public string Code { get; set; }
    public List<PIMMeasure> Measures { get; set; }
}

public class ItemDetails
{
    public string Name { get; set; }
    public string Code { get; set; }
    public List<PIMMeasure> Measures { get; set; }
    //[JsonIgnore]
    //[JsonProperty("productSector")]
    //public CategoryCommon Category { get; set; }
    //[JsonIgnore]
    //[JsonProperty("productType")]
    //public PIMTypeProduct Type { get; set; }
    //[JsonIgnore]
    //[JsonProperty("productGroup")]
    //public PIMGroup Group { get; set; }
    //[JsonIgnore]
    //[JsonProperty("productModel")]
    //public PIMModel Model { get; set; }
    //[JsonIgnore]
    //[JsonProperty("taxonomies")]
    //public List<PIMTaxonomies> Brands { get; set; }
    //public string Images { get; set; }
}

#endregion

#region Models Product Mapping

public class ProductDetailsDto
{
    [JsonProperty("sku")]
    public string ItemCode { get; set; }
    [JsonProperty("name")]
    public string ItemName { get; set; }
    public List<MeasureUnitDto> Measures { get; set; }
    public List<MeasureUnitDto> Units { get { return Measures.OrderBy(c => c.Level).ToList(); } set { Units = value; } }
}

#endregion

#region detail item with category

public class ItemDetailWithCategoryDto
{
    public string Code { get; set; }
    public string CategoryCode { get; set; }
    public string TypeCode { get; set; }
    public string GroupCode { get; set; }
    public string ModelCode { get; set; }
    public string BrandCode { get; set; }
}

public class ItemDetailSuggestPromotionExcludeDto
{
    public HashSet<string> Code { get; set; } = new();
    public HashSet<string> CategoryCode { get; set; } = new();
    public HashSet<string> TypeCode { get; set; } = new();
    public HashSet<string> GroupCode { get; set; } = new();
    public List<CategoryWithBrandPromotionExcludeDto> CategoryWithBrand { get; set; } = new();
}

public class CategoryWithBrandPromotionExcludeDto
{
    public PIMCategoryLevelEnum Level { get; set; }
    public string Code { get; set; }
    public string ModelCode { get; set; }
    public string BrandCode { get; set; }
}

public class PIMItemDetailWithCategoryDto
{
    public string Code { get; set; }
    public List<LevelUniqueDto> Categories { get; set; }
    public UniqueDto Brand { get; set; }
    public UniqueDto Model { get; set; }
    public UniqueDto Group { get; set; }
}

public class UniqueDto
{
    public string UniqueId { get; set; }
}

public class LevelUniqueDto : UniqueDto
{
    public int Level { get; set; }
}

public class ProductInputDto
{
    public string keyword { get; set; }
    public List<string> categoryUniqueId { get; set; }
    public List<string> groupUniqueId { get; set; }
    public int? page { get; set; }
    public int? size { get; set; }
}

public class MetadataDto
{
    public long total { get; set; }
    public string keyword { get; set; }
    public string categoryUniqueId { get; set; }
}

public class GroupInputDto
{
    public string keyword { get; set; }
    public string categoryUniqueId { get; set; }
    public int page { get; set; }
    public int size { get; set; }
}

public class DataDto
{
    public string categoryUniqueId { get; set; }
    public string categoryName { get; set; }
    public string groupUniqueId { get; set; }
    public string groupName { get; set; }
}


public class GetProductCategoryOutputDto
{
    public MetadataDto Metadata { get; set; }
    public List<DataDto> Data { get; set; }
}
public class MetaData
{
    public int? Total { get; set; }
    public string Keyword { get; set; }
}

public class SearchProductScrollDto
{
    public string keyword { get; set; }
    public string categoryUniqueId { get; set; }
    public string groupUniqueId { get; set; }
    public int? size { get; set; }
    public bool isScroll { get; set; }
    public string scrollId { get; set; }
}

#endregion