﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.PIM
{
    public class PIMManyCategoryDto
    {
        public string GroupUniqueId {  get; set; }
        public string GroupName { get; set; }
        public string CategoryName { get; set; }
        public string CategoryUniqueId { get; set; }
        public int? Level { get; set; }
        //public string ParentCategoryId { get; set; }
        //public string ParentUniqueId { get; set; }  
    }

    public class GetManyCategoryDto
    {
        public List<PIMManyCategoryDto> Data { get; set; }
    }
}
