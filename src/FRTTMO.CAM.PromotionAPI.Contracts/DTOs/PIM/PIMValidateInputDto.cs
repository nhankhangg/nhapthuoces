﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.PIM
{
    public class CategoryValidationInputDto
    {
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public string TypeCode { get; set; }
        public string TypeName { get; set; }                     
    }
}
