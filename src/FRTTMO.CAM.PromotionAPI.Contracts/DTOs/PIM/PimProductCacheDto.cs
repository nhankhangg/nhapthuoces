﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace FRTTMO.CAM.PromotionAPI.DTOs.PIM
{
    public class PimCacheSearchMany
    {
        public List<string> sku { get; set; }

    }

    public class PimCacheSearch
    {
        //public string Keyword { get; set; }
        public string CategoryCode { get; set; }
        public string TypeCode { get; set; }
        public int ScrollSize { get; set; } = 1;
        public string ScrollId { get; set; } = "";
        public bool IsScroll { get; set; } = false;
    }
    public class CountPimProductCacheDto
    {
        [JsonProperty("data")]
        public int Total { get; set; }
    }
    public class PimProductCacheDto
    {
        public int Total { get; set; }
        public string ScrollId { get; set; }
        public List<ProductCache> Data { get; set; }
        public Metadata Metadata { get; set; }
    }
    public class Metadata
    {
        public int Total { get; set; }
        public int Size { get; set; }
        public string CategoryUniqueId { get; set; }
        public string ScrollId { get; set; }
        public bool IsScroll { get; set; }
    }

    public class ProductCache
    {
        [JsonProperty("name")]
        public string ItemName { get; set; }

        [JsonProperty("sku")]
        public string ItemCode { get; set; }

        [JsonProperty("measures")]
        public List<Measure> Measures { get; set; }
        public string CategoryUniqueId { get; set; }
        public string CategoryName { get; set; }
        public string GroupUniqueId { get; set; }
        public string GroupName { get; set; }

        //[JsonProperty("categories")]
        //public List<Category> Categories { get; set; }

        //[JsonProperty("brand")]
        //public BrandModelGroupType Brand { get; set; } = new();

        //[JsonProperty("model")]
        //public BrandModelGroupType Model { get; set; } = new();

        //[JsonProperty("group")]
        //public BrandModelGroupType Group { get; set; } = new();

        //[JsonProperty("line")]
        //public BrandModelGroupType Line { get; set; } = new();

        //[JsonProperty("prices")]
        //public List<ProductPrice> Prices { get; set; } = new();
    }

    public class ProductPrice
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("level")]
        public int Level { get; set; }

        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("productMeasureUnitId")]
        public int? ProductMeasureUnitId { get; set; }

        [JsonProperty("measureUnit")]
        public string MeasureUnit { get; set; }

        [JsonProperty("bookName")]
        public string BookName { get; set; }

        [JsonProperty("priceBookId")]
        public int PriceBookId { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("shopCode")]
        public string ShopCode { get; set; }

        [JsonProperty("fromDate")]
        public string FromDate { get; set; }

        [JsonProperty("toDate")]
        public string ToDate { get; set; }

        [JsonProperty("approvedDate")]
        public string ApprovedDate { get; set; }
    }

    public class BrandModelGroupType
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("uniqueId")]
        public string Code { get; set; }
    }

    public class Category
    {
        [JsonProperty("categoryId")]
        public string CategoryId { get; set; }

        [JsonProperty("categoryName")]
        public string Name { get; set; }

        [JsonProperty("level")]
        public int Level { get; set; }

        [JsonProperty("uniqueId")]
        public string Code { get; set; }
    }

    public class Measure
    {
        //[JsonProperty("id")]
        //public int Id { get; set; }

        [JsonProperty("uniqueId")]
        public int MeasureUnitId { get; set; }

        [JsonProperty("measureUnitName")]
        public string MeasureUnitName { get; set; }

        [JsonProperty("level")]
        public int Level { get; set; }

        [JsonProperty("isDefault")]
        public bool IsDefault { get; set; }

        [JsonProperty("isSellDefault")]
        public bool IsSellDefault { get; set; }

        //[JsonProperty("uniqueId")]
        //public int UniqueId { get; set; }

        //[JsonProperty("ratio")]
        //public int Ratio { get; set; }
    }

}