﻿using System.Collections.Generic;
using Volo.Abp.Application.Dtos;

namespace FRTTMO.CAM.PromotionAPI.Dtos;

public sealed class PaginationResultDto<T> : PagedResultDto<T>
{
    public PaginationResultDto(int skipCount, int maxResultCount, long totalCount, IReadOnlyList<T> items) : base(
        totalCount, items)
    {
        SkipCount = skipCount;
        MaxResultCount = maxResultCount;
    }

    public PaginationResultDto()
    {
        SkipCount = 0;
        MaxResultCount = 0;
        Items = new List<T>();
        TotalCount = 0;
    }

    public PaginationResultDto(int skipCount, int maxResultCount, int totalCount, IReadOnlyList<T> items) : base(
        totalCount, items)
    {
        SkipCount = skipCount;
        MaxResultCount = maxResultCount;
    }

    public int SkipCount { get; set; }

    public int MaxResultCount { get; set; }
}