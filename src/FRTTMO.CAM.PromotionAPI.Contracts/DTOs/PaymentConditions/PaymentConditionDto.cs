﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities;

namespace FRTTMO.CAM.PromotionAPI.DTOs.PaymentConditions
{
    public class PaymentConditionDto : Entity<Guid>
    {
        public Guid PromotionId { get; set; }
        public List<string> BankCodes { get; set; }
        public string PaymentType { get; set; }
        public List<string> CardTypes { get; set; }
        public List<string> PINCodes { get; set; }
        public List<string> PaymentForms { get; set; }
    }
}
