﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.PaymentConditions
{
    public class UpdatePaymentConditionDto
    {
        public Guid? Id { get; set; }
        [Required]
        [StringLength(200)]
        public string PaymentType { get; set; }
        [Required]
        public List<string> BankCodes { get; set; }
        [Required]
        public List<string> CardTypes { get; set; }
        public List<string> PINCodes { get; set; }
        public List<string> PaymentForms { get; set; }
    }
}
