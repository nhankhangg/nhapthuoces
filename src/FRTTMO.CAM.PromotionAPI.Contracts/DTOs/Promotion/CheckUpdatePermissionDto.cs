﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.Promotion
{
    public class CheckUpdatePermissionDto
    {
        public bool Enabled { get; set; }
        public string Message { get; set; }
    }
}
