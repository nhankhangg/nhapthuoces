﻿using System.Collections.Generic;
using FluentValidation;
using FRTTMO.CAM.PromotionAPI.DTOs.AmountConditions;
using FRTTMO.CAM.PromotionAPI.DTOs.Condition;
using FRTTMO.CAM.PromotionAPI.DTOs.CostDistribution;
using FRTTMO.CAM.PromotionAPI.DTOs.InstallmentConditions;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInput;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInputExclude;
using FRTTMO.CAM.PromotionAPI.DTOs.Output;
using FRTTMO.CAM.PromotionAPI.DTOs.PaymentConditions;
using FRTTMO.CAM.PromotionAPI.DTOs.PromotionExcludes;
using FRTTMO.CAM.PromotionAPI.DTOs.Quota;
using FRTTMO.CAM.PromotionAPI.DTOs.RegionCondition;

namespace FRTTMO.CAM.PromotionAPI.DTOs.Promotion;

public class CreatePromotionDto : PromotionBaseDto 

{
    public CreateQuotaDto Quota { get; set; } = null;

    public CreateAmountConditionDto AmountCondition { get; set; } = null;

    public List<CreateProvinceConditionDto> ProvinceConditions { get; set; } = new();

    public List<CreateExtraConditionDto> ExtraConditions { get; set; } = new();

    public List<CreateItemInputDto> ItemInputs { get; set; } = new();

    public List<CreateOutputDto> Outputs { get; set; } = new();

    public List<CreateItemInputExcludeDto> ItemInputExcludes { get; set; } = new();

    public List<CreateCostDistributionDto> CostDistributions { get; set; } = new();

    public List<CreateInstallmentConditionDto> InstallmentConditions { get; set; } = new();
    
    public List<CreatePromotionExcludeDto> PromotionExcludes { get; set; } = new();

    public List<CreatePaymentConditionDto> PaymentConditions { get; set; } = new();
}