﻿using System;
using FRTTMO.CAM.PromotionAPI.Enum;

namespace FRTTMO.CAM.PromotionAPI.DTOs.Promotion;

public class PromotionBaseDto 
{
    public string Name { get; set; }
    public PromotionStatusEnum Status { get; set; }
    public int? Priority { get; set; }
    public Guid CampaignId { get; set; }
    public DateTime? FromDate { get; set; }
    public DateTime? ToDate { get; set; }
    public DateTime ActiveDate { get; set; }
    public string FromHour { get; set; }
    public string ToHour { get; set; }
    public string PromotionClass { get; set; }
    public string PromotionType { get; set; }
    public string DisplayArea { get; set; }
    public string ApplicableMethod { get; set; }
    public string[] Channels { get; set; }
    public string[] OrderTypes { get; set; }
    public string[] StoreTypes { get; set; }
    public string[] CustomerGroups { get; set; }
    public bool AllowDisplayOnBill { get; set; }
    public bool FlagDebit { get; set; }
    public string UrlImage { get; set; }
    public string UrlPage { get; set; }
    public string NameOnline { get; set; }
    public bool AllowShowOnline { get; set; }
    public string Description { get; set; }
    public string Remark { get; set; }
    public string TradeIndustryCode { get; set; }
    public string[] SourceOrders {  get; set; }
    public string VerifyScheme { get; set; }
    public string ProgramType { get; set; }
    public string ShortDescription { get; set; }
    public bool IsBoom { get; set; }
    public bool IsShowDetail { get; set; }
    public string DescriptionBoom { get; set; }
    public string LabelDescription { get; set; }
}