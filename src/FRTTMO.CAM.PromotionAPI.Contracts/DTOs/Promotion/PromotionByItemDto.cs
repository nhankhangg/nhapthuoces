﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace FRTTMO.CAM.PromotionAPI.DTOs.Promotion
{
    public class PromotionByItemDto : EntityDto<Guid>
    {
        public string Code { get; set; }
        public int Status { get;set; }
        public string Name { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
