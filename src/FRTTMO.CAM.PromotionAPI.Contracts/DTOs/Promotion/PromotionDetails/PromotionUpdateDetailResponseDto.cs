﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.Promotion.PromotionDetails
{
    public class PromotionUpdateDetailResponseDto
    {
        public Guid PromotionId { get; set; }
    }
}
