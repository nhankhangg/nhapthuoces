﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities;

namespace FRTTMO.CAM.PromotionAPI.DTOs.Promotion.PromotionDetails
{
    public class PromotionWebDto : Entity<Guid>
    {
        public string UrlImage { get; set; }
        public string UrlPage { get; set; }
        public string Description { get; set; }
        public string DisplayArea { get; set; }
        public int? Priority { get; set; }
        public bool AllowShowOnline { get; set; }
        public string ShortDescription { get; set; }
        public bool IsBoom { get; set; }
        public bool IsShowDetail { get; set; }
        public string DescriptionBoom { get; set; }
    }
}
