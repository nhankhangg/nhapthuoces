﻿using FRTTMO.CAM.PromotionAPI.DTOs.ItemInput;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInputExclude;
using FRTTMO.CAM.PromotionAPI.DTOs.Output;
using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.Promotion.PromotionDetails
{
    public class UpdateInputOutputDto
    {
        public List<UpdateItemInputDto> UpdateItemInputs { get; set; } = new();
        public List<UpdateOutputDto> UpdateOutputs { get; set; } = new();
        public List<UpdateItemInputExcludeDto> UpdateItemInputExcludes { get; set; } = new();
    }

    public class UpdateInputDetailsDto
    {
        public List<UpdateItemInputDto> UpdateItemInputs { get; set; } = new();
        public List<UpdateItemInputExcludeDto> UpdateItemInputExcludes { get; set; } = new();
    }

    public class UpdateOutputDetailsDto
    {
        public List<UpdateOutputDto> UpdateOutputs { get; set; } = new();
    }
}
