﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.Promotion.PromotionDetails
{
    public class UpdatePromotionHeaderDto
    {
        public string Name { get; set; }
        public Guid CampaignId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public DateTime ActiveDate { get; set; }
        public string FromHour { get; set; }
        public string ToHour { get; set; }
        public string PromotionClass { get; set; }
        public string PromotionType { get; set; }
        public string ApplicableMethod { get; set; }
        public string[] Channels { get; set; }
        public string[] OrderTypes { get; set; }
        public string[] CustomerGroups { get; set; }
        public string[] StoreTypes { get; set; }
        public string TradeIndustryCode { get; set; }
        //public string[] PromotionExcludes { get; set; }
        public bool AllowDisplayOnBill { get; set; }
        public string[] SourceOrders {  get; set; }
        public string VerifyScheme {  get; set; }
        public string ShortDescription { get; set; }
        public bool IsBoom { get; set; }
        public bool IsShowDetail { get; set; }
        public string DescriptionBoom { get; set; }
    }
}
