﻿using System;
using Volo.Abp.Application.Dtos;
using System.Collections.Generic;
using FRTTMO.CAM.PromotionAPI.DTOs.Condition;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInput;
using FRTTMO.CAM.PromotionAPI.DTOs.Output;
using FRTTMO.CAM.PromotionAPI.DTOs.RegionCondition;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInputExclude;
using FRTTMO.CAM.PromotionAPI.DTOs.CostDistribution;
using FRTTMO.CAM.PromotionAPI.DTOs.AmountConditions;
using FRTTMO.CAM.PromotionAPI.DTOs.InstallmentConditions;
using FRTTMO.CAM.PromotionAPI.DTOs.PromotionExcludes;
using FRTTMO.CAM.PromotionAPI.DTOs.PaymentConditions;

namespace FRTTMO.CAM.PromotionAPI.DTOs.Promotion;

public class PromotionDto : EntityDto<Guid>
{
    public string Name { get; set; }
    public string Code { get; set; }
    public int Status { get; set; }
    public int? Priority { get; set; }
    public Guid CampaignId { get; set; }
    public DateTime? FromDate { get; set; }
    public DateTime? ToDate { get; set; }
    public DateTime ActiveDate { get; set; }
    public string FromHour { get; set; }
    public string ToHour { get; set; }
    public string PromotionClass { get; set; }
    public string PromotionType { get; set; }
    public string DisplayArea { get; set; }
    public string ApplicableMethod { get; set; }
    public string[] Channels { get; set; }
    public string[] OrderTypes { get; set; }
    public string[] StoreTypes { get; set; }
    public string[] CustomerGroups { get; set; }
    public bool AllowDisplayOnBill { get; set; }
    public bool FlagDebit { get; set; }
    public string UrlImage { get; set; }
    public string UrlPage { get; set; }
    public string NameOnline { get; set; }
    public bool AllowShowOnline { get; set; }
    public string Description { get; set; }
    public string Remark { get; set; }
    public string CreatedBy { get; set; }

    public string CreatedByName { get; set; }

    public string UpdateBy { get; set; }

    public string UpdateByName { get; set; }

    public DateTime CreationTime { get; set; }

    public string TradeIndustryCode { get; set; }
    public string[] SourceOrders { get; set; }
    public string VerifyScheme { get; set; }
    public string ProgramType { get; set; }
    public string ShortDescription { get; set; }
    public bool IsBoom { get; set; }
    public bool IsShowDetail { get; set; }
    public string DescriptionBoom { get; set; }
    public string LabelDescription { get; set; }

    public QuotaDto Quota { get; set; }

    public AmountConditionDto AmountCondition { get; set; }

    public List<ItemInputDto> ItemInputs { get; set; } = new();

    public List<OutputDto> Outputs { get; set; } = new();

    public List<ProvinceConditionDto> ProvinceConditions { get; set; } = new();

    public List<ExtraConditionDto> ExtraConditions { get; set; } = new();

    public List<ItemInputExcludeDto> ItemInputExcludes { get; set; } = new();

    public List<CostDistributionDto> CostDistributions { get; set; } = new();

    public List<InstallmentConditionDto> InstallmentConditions { get; set; } = new();

    public List<PromotionExcludeDto> PromotionExcludes { get; set; } = new();

    public List<PaymentConditionDto> PaymentConditions { get; set; } = new();
}