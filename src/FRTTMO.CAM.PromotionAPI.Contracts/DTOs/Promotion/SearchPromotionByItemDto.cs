﻿using FRTTMO.CAM.PromotionAPI.Enum;
using FRTTMO.CAM.PromotionAPI.Paginations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.Promotion
{
    public class SearchPromotionByItemDto
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Vui lòng nhập mã sản phẩm")]
        public string ItemCode { get; set; }
        public string DisplayArea { get; set; }
        public Pagination Pagination { get; set; } = new Pagination();
        public PromotionStatusEnum? Status { get; set; }
    }
}
