﻿using FRTTMO.CAM.PromotionAPI.Enum;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace FRTTMO.CAM.PromotionAPI.DTOs.Promotion
{
    public class SearchPromotionDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public List<string> CreatedBy { get; set; } = new();

        public string CampaignId { get; set; }

        public PromotionStatusEnum? Status { get; set; }

        public SortByResultDto Sorting { get; set; } = new();
        public List<string> Code { get; set; } = new();
    }
}
