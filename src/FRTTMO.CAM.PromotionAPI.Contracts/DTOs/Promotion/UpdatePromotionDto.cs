﻿using System;
using System.Text;
using System.Collections.Generic;
using FRTTMO.CAM.PromotionAPI.DTOs.AmountConditions;
using FRTTMO.CAM.PromotionAPI.DTOs.Condition;
using FRTTMO.CAM.PromotionAPI.DTOs.CostDistribution;
using FRTTMO.CAM.PromotionAPI.DTOs.InstallmentConditions;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInput;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInputExclude;
using FRTTMO.CAM.PromotionAPI.DTOs.Output;
using FRTTMO.CAM.PromotionAPI.DTOs.Quota;
using FRTTMO.CAM.PromotionAPI.DTOs.RegionCondition;
using FRTTMO.CAM.PromotionAPI.DTOs.PromotionExcludes;
using FRTTMO.CAM.PromotionAPI.DTOs.PaymentConditions;

namespace FRTTMO.CAM.PromotionAPI.DTOs.Promotion
{
    public class UpdatePromotionDto : PromotionBaseDto
    {
        public UpdateQuotaDto Quota { get; set; } = null; //3

        public UpdateAmountConditionDto AmountCondition { get; set; } = null; //1

        public List<UpdateProvinceConditionDto> ProvinceConditions { get; set; } = new();

        public List<UpdateExtraConditionDto> ExtraConditions { get; set; } = new();

        public List<UpdateItemInputDto> ItemInputs { get; set; } = new(); //2

        public List<UpdateOutputDto> Outputs { get; set; } = new(); //6

        public List<UpdateItemInputExcludeDto> InputExcludes { get; set; } = new(); //4

        public List<UpdateCostDistributionDto> CostDistributions { get; set; } = new(); //5

        public List<UpdateInstallmentConditionDto> InstallmentConditions { get; set; } = new(); //7

        public List<UpdatePromotionExcludeDto> PromotionExcludes { get; set; } = new();

        public List<UpdatePaymentConditionDto> PaymentConditions { get; set; } = new();
    }
}
