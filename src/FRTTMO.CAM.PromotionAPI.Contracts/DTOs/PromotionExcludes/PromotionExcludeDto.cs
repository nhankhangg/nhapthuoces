﻿using FRTTMO.CAM.PromotionAPI.DTOs.Promotion;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities;

namespace FRTTMO.CAM.PromotionAPI.DTOs.PromotionExcludes
{
    public class PromotionExcludeDto
    {
        public Guid Id { get; set; }
        public Guid? PromotionId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool AllowDisplay { get; set; }
        public string PromotionExcludeType { get; set; }
        public string PromotionCode { get; set; }
    }
}
