﻿using FRTTMO.CAM.PromotionAPI.DTOs.ItemInput;
using FRTTMO.CAM.PromotionAPI.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.PromotionExcludes
{
    public class SuggestPromotionExcludeRequestDto
    {
        public string Keyword { get; set; }
        public PromotionTypeEnum PromotionType { get; set; }
        public PromotionExcludeTypeEnum PromotionExcludeType { get; set; }
        public List<CreateItemInputDto> ItemInputs { get; set; } = new();
        public List<string> Excludes { get; set; } = new();
    }
}
