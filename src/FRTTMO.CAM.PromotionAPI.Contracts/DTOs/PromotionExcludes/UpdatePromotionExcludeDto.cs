﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.PromotionExcludes
{
    public class UpdatePromotionExcludeDto
    {

        public string Name { get; set; }
        public string Code { get; set; }
        public bool AllowDisplay { get; set; }
        public string PromotionExcludeType { get; set; }
    }
}
