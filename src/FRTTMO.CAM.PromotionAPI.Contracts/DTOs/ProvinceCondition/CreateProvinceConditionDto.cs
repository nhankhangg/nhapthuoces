﻿using System;

namespace FRTTMO.CAM.PromotionAPI.DTOs.RegionCondition;

public class CreateProvinceConditionDto
{
    public string ProvinceCode { get; set; }
    public string ProvinceName { get; set; }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
}