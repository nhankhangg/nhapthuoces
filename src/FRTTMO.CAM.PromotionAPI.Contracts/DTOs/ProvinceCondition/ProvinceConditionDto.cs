﻿using System;
using Volo.Abp.Application.Dtos;

namespace FRTTMO.CAM.PromotionAPI.DTOs.RegionCondition;

public class ProvinceConditionDto : EntityDto<Guid>
{
    public Guid PromotionId { get; set; }
    public string ProvinceCode { get; set; }
    public string ProvinceName { get; set; }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
}