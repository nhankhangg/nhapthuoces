﻿using System;
using Volo.Abp.Application.Dtos;

namespace FRTTMO.CAM.PromotionAPI.DTOs.Promotion;

public class QuotaDto : EntityDto<Guid>
{
    //theo san pham or theo đơn hàng
    public string Type { get; set; }   
    // 10000 => quantity ctkm
    public int Quantity { get; set; }     
     // số lượng theo shop
    public int LimitQuantityShop { get; set; }
    // reset(ResetShop) theo SHOP(LimitQuantityShop) hoặc (ResetPromotion)theo toàn cơ cấu (Quantity), (chỉ reset theo ngày)
    public string ResetQuotaType { get; set; }
    // có giới hạng theo sđt
    public bool FlagQuantityPhone { get; set; }
    // số lượng tối đa sđt
    public int LimitQuantityPhone { get; set; }
    // có giới hạng theo email
    public bool FlagQuantityEmail { get; set; }
    // số lượng tối đa email
    public int LimitQuantityEmail { get; set; }
}