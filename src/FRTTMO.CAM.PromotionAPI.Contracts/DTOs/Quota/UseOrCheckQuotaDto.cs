﻿using System.Collections.Generic;
using FRTTMO.CAM.PromotionAPI.ETOs.Cache;

namespace FRTTMO.CAM.PromotionAPI.DTOs.Quota
{
    public class UseOrCheckQuotaDto
    {
        public string PromotionCode { get; set; }
        public int Quantity { get; set; } = 0;
        public string ProvinceCode { get; set; }
        public string ProvinceName { get; set; }
        public string PhoneNumbers { get; set; }
    }

    public class ResponseQuotaDto
    {
        public string PromotionCode { get; set; }
        public int CurrentQuantity { get; set; }
        public List<ResponseShopDto> ResponseShop { get; set; } = new();

        public List<ResponsePhoneDto> ResponsePhone { get; set; } = new();
    }

    public class ResponseShopDto
    {
        public string ShopCode { get; set; }
        public int CurrentQuantity { get; set; }
    }

    public class ResponsePhoneDto
    {
        public string PhoneNumber { get; set; }
        public int CurrentQuantity { get; set; }
    }


    public class ResponseCheckQuotaDto
    {
        public string PromotionCode { get; set; }
        public int CurrentQuantity { get; set; }
        public bool IsValid { get; set; }
        public QuotaEto Quota { get; set; } = new();

        public List<ResponseCheckQuotaShop> ResponseShop { get; set; } = new();

        public List<ResponseCheckQuotaPhone> ResponsePhone { get; set; } = new();
    }


    public class ResponseCheckQuotaShop : ResponseShopDto
    {
        public bool IsValid { get; set; }
    }
    public class ResponseCheckQuotaPhone : ResponsePhoneDto
    {
        public bool IsValid { get; set; }
    }
}
