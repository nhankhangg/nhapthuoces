﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.Quota
{
    public class UsedQuotaDto
    {
        public QuotaAmountDto Quota { get; set; }
        public List<UsedQuotaShopDto> QuotaShops { get; set; } = new();
    }

    public class UsedQuotaShopDto : QuotaAmountDto
    {
        public string ShopCode { get; set; }
        public string ShopName { get; set; }
    }

    public class QuotaAmountDto
    {
        public int Quantity { get; set; }
        public int CurrentQuantity { get; set; }
    }
}
