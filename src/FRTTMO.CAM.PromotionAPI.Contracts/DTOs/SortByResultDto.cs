﻿namespace FRTTMO.CAM.PromotionAPI.DTOs;

public class SortByResultDto
{
    public string SortType { get; set; } = "Desc";
    public string ColumnName { get; set; } = "CreationTime";
}