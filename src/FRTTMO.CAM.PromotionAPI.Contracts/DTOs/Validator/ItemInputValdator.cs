﻿using FluentValidation;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInput;
using System;
using System.Collections.Generic;
using System.Text;


namespace FRTTMO.CAM.PromotionAPI.DTOs.Validator
{
    public class ItemInputValidator : AbstractValidator<CreateItemInputDto>
    {
        public ItemInputValidator()
        {

        }

    }
    public class ItemInputUpdateValidator : AbstractValidator<UpdateItemInputDto>
    {
        public ItemInputUpdateValidator()
        {
        }

    }
}
