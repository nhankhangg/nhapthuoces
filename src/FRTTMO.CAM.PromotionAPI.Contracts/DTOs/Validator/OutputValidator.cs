﻿using FluentValidation;
using FRTTMO.CAM.PromotionAPI.DTOs.Output;
using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.DTOs.Validator
{
    public class OutputValidator : AbstractValidator<CreateOutputDto>
    {
        public OutputValidator()
        {

        }
    }

    public class OutputUpdateValidator : AbstractValidator<UpdateOutputDto>
    {
        public OutputUpdateValidator()
        {
        }
    }
}
