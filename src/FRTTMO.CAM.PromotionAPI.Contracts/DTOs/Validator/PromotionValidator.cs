﻿using System;
using FluentValidation;
using FRTTMO.CAM.PromotionAPI.DTOs.Promotion;
using FRTTMO.CAM.PromotionAPI.Helpers;

namespace FRTTMO.CAM.PromotionAPI.DTOs.Validator
{
    public class PromotionValidator : AbstractValidator<CreatePromotionDto> 
    {
        public PromotionValidator()
        {
            RuleFor(promotion => promotion.Name).NotEmpty().NotNull().MaximumLength(500);

            RuleFor(promotion => promotion.CampaignId).NotEmpty().NotNull();

            RuleFor(promotion => promotion.Priority).NotEmpty();

            RuleFor(promotion => promotion.NameOnline).MaximumLength(1000);

            RuleFor(promotion => promotion.UrlPage).MaximumLength(1000);

            RuleFor(promotion => promotion.ApplicableMethod).NotEmpty().NotNull();

            RuleFor(promotion => promotion.DisplayArea).NotEmpty().NotNull();

            RuleFor(promotion => promotion.PromotionType).NotEmpty().NotNull();

            RuleFor(promotion => promotion.PromotionClass).NotEmpty().NotNull();

            RuleFor(promotion => promotion.ToDate).NotEmpty().NotNull();

            RuleFor(promotion => promotion.FromDate).NotEmpty().NotNull().LessThanOrEqualTo(p => p.ToDate);

            RuleFor(promotion => promotion.ToHour)
                .Must(p => p.IsValidHourFormat())
                .When(p => !p.ToHour.IsNullOrEmpty());

            RuleFor(promotion => promotion.FromHour)
                .Must(p => p.IsValidHourFormat())
                .LessThan(p => p.ToHour)
                .When(p => !p.FromHour.IsNullOrEmpty() && !p.ToHour.IsNullOrEmpty());
                

            RuleFor(promotion => promotion.ItemInputs).
                 ForEach(p => p.SetValidator(new ItemInputValidator()))
                 .When(promotion => promotion.ItemInputs.Count > 0);

            RuleFor(promotion => promotion.Outputs).
                ForEach(p => p.SetValidator(new OutputValidator()))
                .When(promotion => promotion.Outputs.Count > 0);

            
        }
    }

    public class PromotionUpdateValidator : AbstractValidator<UpdatePromotionDto>
    {
        public PromotionUpdateValidator()
        {
            RuleFor(promotion => promotion.Name).NotEmpty().NotNull().MaximumLength(500);

            RuleFor(promotion => promotion.CampaignId).NotEmpty().NotNull();

            RuleFor(promotion => promotion.Priority).NotEmpty();

            RuleFor(promotion => promotion.NameOnline).MaximumLength(1000);

            RuleFor(promotion => promotion.UrlPage).MaximumLength(1000);

            RuleFor(promotion => promotion.ApplicableMethod).NotEmpty().NotNull();

            RuleFor(promotion => promotion.DisplayArea).NotEmpty().NotNull();

            RuleFor(promotion => promotion.PromotionType).NotEmpty().NotNull();

            RuleFor(promotion => promotion.PromotionClass).NotEmpty().NotNull();

            RuleFor(promotion => promotion.ToDate).NotEmpty().NotNull();

            RuleFor(promotion => promotion.FromDate).NotEmpty().NotNull().LessThanOrEqualTo(p => p.ToDate);

            RuleFor(promotion => promotion.ToHour)
                .Must(p => p.IsValidHourFormat())
                .When(p => !p.ToHour.IsNullOrEmpty());

            RuleFor(promotion => promotion.FromHour)
                .Must(p => p.IsValidHourFormat())
                .LessThan(p => p.ToHour)
                .When(p => !p.FromHour.IsNullOrEmpty() && !p.ToHour.IsNullOrEmpty());


            RuleFor(promotion => promotion.ItemInputs).
                 ForEach(p => p.SetValidator(new ItemInputUpdateValidator()))
                 .When(promotion => promotion.ItemInputs.Count > 0);

            RuleFor(promotion => promotion.Outputs).
                ForEach(p => p.SetValidator(new OutputUpdateValidator()))
                .When(promotion => promotion.Outputs.Count > 0);


        }
    }
}
