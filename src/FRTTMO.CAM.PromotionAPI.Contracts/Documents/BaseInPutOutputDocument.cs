﻿using Nest;
using System;

namespace FRTTMO.CAM.PromotionAPI.Documents
{
    public class BaseInPutOutputDocument
    {
        public string Id { get; set; }

        [Keyword] 
        public Guid PromotionId { get; set; }

        [Keyword] 
        public string PromotionCode { get; set; }

        public int Quantity { get; set; }

        [Keyword]
        public string CategoryCode { get; set; }

        public string CategoryName { get; set; }

        [Keyword]
        public string GroupCode { get; set; }
        public string GroupName { get; set; }
        [Keyword]
        public string BrandCode { get; set; }
        public string BrandName { get; set; }
        [Keyword]
        public string ModelCode { get; set; }
        public string ModelName { get; set; }
        [Keyword]
        public string TypeCode { get; set; }
        public string TypeName { get; set; }
        [Keyword]
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        [Keyword]
        public string GiftCode { get; set; }
        public string GiftName { get; set; }
        [Keyword]
        public string CouponCode { get; set; }
        public string CouponName { get; set; }
        public string UnitName { get; set; }
        public int? UnitCode { get; set; }
        public string WarehouseCode { get; set; }
        public string WarehouseName { get; set; }
        public int LineNumber { get; set; }

        [Keyword]
        public string CreatedBy { get; set; }
        [Keyword]
        public string CreatedByName { get; set; }
        [Keyword]
        public string UpdateBy { get; set; }

        public string UpdateByName { get; set; }
        [Keyword]
        public DateTime? LastModificationTime { get; set; }
        public string LastModifierId { get; set; }
        [Keyword]
        public DateTime? CreationTime { get; set; }
        public string CreatorId { get; set; }

    }
}
