﻿using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.Documents
{
    public class BaseMasterDataDocument
    {
        [Text(Name = "code")]
        public string Code { get; set; }

        [Text(Name = "name")]
        public string Name { get; set; }
    }
}
