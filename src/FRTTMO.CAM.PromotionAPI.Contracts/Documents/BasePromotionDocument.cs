﻿using System;

namespace FRTTMO.CAM.PromotionAPI.Documents
{
    public class BasePromotionDocument
    {
        public Guid Id { get; set; }
        public string CreatedBy { get; set; }

        public string CreatedByName { get; set; }

        public string UpdateBy { get; set; }

        public string UpdateByName { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public string LastModifierId { get; set; }
        public DateTime? CreationTime { get; set; }
        public string CreatorId { get; set; }
    }
}
