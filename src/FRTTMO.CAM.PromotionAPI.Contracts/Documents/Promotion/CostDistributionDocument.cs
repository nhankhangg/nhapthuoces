﻿using FRTTMO.CAM.PromotionAPI.DTOs.CostDistribution;
using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.Documents
{
    public class CostDistributionDocument
    {
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }
        public List<DataPIMDto> Categories { get; set; }
        public List<DataPIMDto> Types { get; set; }
        public List<DataPIMDto> Brands { get; set; }
        public decimal Percentage { get; set; }
    }
}
