﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.Documents
{
    public class ExtraConditionDocument
    {
        public string PromotionCode { get; set; }
        public string QualifierCode { get; set; }
        public string OperatorCode { get; set; }
        public string Values { get; set; }
        public decimal Number { get; set; }
    }
}
