﻿using Nest;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities;

namespace FRTTMO.CAM.PromotionAPI.Documents
{
    [ElasticsearchType(IdProperty = nameof(Id))]
    public class InputDocument : BaseInPutOutputDocument
    {
    }
}
