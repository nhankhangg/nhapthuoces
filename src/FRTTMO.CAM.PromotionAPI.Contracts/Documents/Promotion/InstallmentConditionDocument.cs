﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.Documents
{
    public class InstallmentConditionDocument 
    {
        public string Type { get; set; }
        public string[] Financiers { get; set; }
        public string[] CardTypes { get; set; }
        public string[] Tenures { get; set; }
        public string[] PaymentForms { get; set; }
        public string InterestRateType { get; set; }
        public float MinInterestRate { get; set; }
        public float MaxInterestRate { get; set; }
        public string PaymentType { get; set; }
    }
}
