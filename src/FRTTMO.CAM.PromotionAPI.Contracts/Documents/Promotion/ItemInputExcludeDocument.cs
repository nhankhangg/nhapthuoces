﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.Documents
{
    public class ItemInputExcludeDocument
    {
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
    }
}
