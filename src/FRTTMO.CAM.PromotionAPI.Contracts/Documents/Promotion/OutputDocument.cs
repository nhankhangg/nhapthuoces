﻿using Nest;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Volo.Abp.Domain.Entities;

namespace FRTTMO.CAM.PromotionAPI.Documents
{
    [ElasticsearchType(IdProperty = nameof(Id))]
    public class OutputDocument : BaseInPutOutputDocument
    {
        [Keyword]
        public string QualifierCode { get; set; }

        [Keyword]
        public string OperatorCode { get; set; }
        public string Note { get; set; }
        public int MinQuantity { get; set; }
        public int? MaxQuantity { get; set; }
        public decimal MinValue { get; set; }
        public decimal MaxValue { get; set; }
        public decimal Discount { get; set; }
        public string SchemeCode { get; set; }
        public string SchemeName { get; set; }
        public string PaymentMethodCode { get; set; }
        public string PaymentMethodName { get; set; }
        public string NoteBoom { get; set; }
        public string DiscountPlaceCode { get; set; }
    }
   
}
