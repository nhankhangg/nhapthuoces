﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.Documents
{
    public class PaymentConditionDocument
    {
        public List<string> BankCodes { get; set; }
        public string PaymentType { get; set; }
        public List<string> CardTypes { get; set; }
        public List<string> PINCodes { get; set; }
    }
}
