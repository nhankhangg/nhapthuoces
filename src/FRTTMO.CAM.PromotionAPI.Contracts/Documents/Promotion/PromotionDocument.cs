﻿using Nest;
using System;
using System.Collections.Generic;

namespace FRTTMO.CAM.PromotionAPI.Documents
{
    [ElasticsearchType(RelationName = "promotion", IdProperty = nameof(Id))]
    public class PromotionDocument : BasePromotionDocument
    {
        private DateTime ValueFromDate { get; set; }

        private DateTime ValueToDate { get; set; }

        public Guid CampaignId { get; set; }

        [Keyword] 
        public string Code { get; set; }

        [Text(Name = "name")]
        public string Name { get; set; }

        public int Status { get; set; }

        [Date(Name = "fromDate")]
        public DateTime FromDate
        {
            get
            {
                return ValueFromDate;
            }

            set
            {
                ValueFromDate = new DateTime(value.Year, value.Month, value.Day, value.Hour, value.Minute, value.Second, DateTimeKind.Local);
            }
        }

        [Date(Name = "toDate")]
        public DateTime ToDate
        {
            get
            {
                return ValueToDate;
            }

            set
            {
                ValueToDate = new DateTime(value.Year, value.Month, value.Day, value.Hour, value.Minute, value.Second, DateTimeKind.Local);
            }
        }

        [Date(Name = "fromHour", Format = "HH:mm:ss")]
        public string FromHour { get; set; }

        [Date(Name = "toHour", Format = "HH:mm:ss")]
        public string ToHour { get; set; }

        public DateTime ActiveDate { get; set; }

        [Number(NumberType.Integer, Name = "priority")]
        public int? Priority { get; set; }

        [Boolean(Name = "allowDisplayOnBill")]
        public bool AllowDisplayOnBill { get; set; }

        public bool AllowShowOnline { get; set; }
        [Keyword] 
        public string PromotionClass { get; set; }

        public string ApplicableMethod { get; set; }

        [Keyword] 
        public string PromotionType { get; set; }

        public string DisplayArea { get; set; }

        public bool FlagDebit { get; set; }

        public string UrlImage { get; set; }

        public string UrlPage { get; set; }

        public string NameOnline { get; set; }

        public string TradeIndustryCode { get; set; }

        public string Description { get; set; }

        public string Remark { get; set; }

        public string[] ItemInputExcludes { get; set; }

        public string VerifyScheme { get; set; }

        public string ShortDescription { get; set; }

        public bool IsBoom { get; set; }
        public bool IsShowDetail { get; set; }
        public string DescriptionBoom { get; set; }
        public string LabelDescription { get; set; }

        public QuotaDocument Quota { get; set; } = new();

        public AmountConditionDocument AmountCondition { get; set; } = new();

        [Nested(Name = "channels")]
        public List<BaseMasterDataDocument> Channels { get; set; } = new();

        [Nested(Name = "orderTypes")]
        public List<BaseMasterDataDocument> OrderTypes { get; set; } = new();

        [Nested(Name = "storeTypes")]
        public List<BaseMasterDataDocument> StoreTypes { get; set; } = new();

        [Nested(Name = "customerGroups")]
        public List<BaseMasterDataDocument> CustomerGroups { get; set; } = new();

        [Nested(Name = "sourceOrders")]
        public List<BaseMasterDataDocument> SourceOrders { get; set; } = new();

        [Nested(Name = "extraConditions")]
        public List<ExtraConditionDocument> ExtraConditions { get; set; }

        [Nested(Name = "costDistributions")]
        public List<CostDistributionDocument> CostDistributions { get; set; }

        [Nested(Name = "InstallmentConditions")]
        public List<InstallmentConditionDocument> InstallmentConditions { get; set; }

        [Nested(Name = "PaymentConditions")]
        public List<PaymentConditionDocument> PaymentConditions { get; set; }

        [Nested(Name = "inputs")]
        public List<InputDocument> Inputs { get; set; }

        [Nested(Name = "outputs")]
        public List<OutputDocument> Outputs { get; set; }

    }
}
