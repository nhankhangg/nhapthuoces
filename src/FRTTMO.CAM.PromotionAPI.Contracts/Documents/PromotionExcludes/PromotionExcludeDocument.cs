﻿using Nest;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace FRTTMO.CAM.PromotionAPI.Documents
{
    public class PromotionExcludeDocument : EntityDto<Guid>
    {
        public string Name { get; set; }
        [Keyword]
        public string Code { get; set; }
        public bool AllowDisplay { get; set; }
        public string PromotionExcludeType { get; set; }
        [Keyword]
        public string PromotionCode { get; set; }
    }
}
