﻿using Nest;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Volo.Abp.Domain.Entities;

namespace FRTTMO.CAM.PromotionAPI.Documents.PromotionInputReplaces
{
    public class InputReplaceDocument : BaseInPutOutputDocument
    {
        public Guid? InputReplaceId { get; set; }
    }
}
