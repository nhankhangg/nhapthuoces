﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace FRTTMO.CAM.PromotionAPI.Documents
{
    public class ProvinceConditionDocument : EntityDto<Guid>
    {
        public Guid PromotionId { get; set; }
        public string PromotionCode { get; set; }
        public string ProvinceCode { get; set; }
        public string ProvinceName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime? DeletionTime { get; set; }
        public bool IsDeleted { get; set; }
    }
}
