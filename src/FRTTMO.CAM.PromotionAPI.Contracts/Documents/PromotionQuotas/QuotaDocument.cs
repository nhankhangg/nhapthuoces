﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace FRTTMO.CAM.PromotionAPI.Documents
{
    public class QuotaDocument : EntityDto<Guid>
    {
        public string Type { get; set; }
        public int Quantity { get; set; }
        public int LimitQuantityShop { get; set; }
        public string ResetQuotaType { get; set; }
        public bool FlagQuantityPhone { get; set; }
        public int LimitQuantityPhone { get; set; }
        public bool FlagQuantityEmail { get; set; }
        public int LimitQuantityEmail { get; set; }
    }
}
