﻿using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.Documents
{
    public class QuotaHistoryDocument
    {
        [Keyword] public Guid Id { get; set; }
        [Keyword] public string PromotionCode { get; set; }
        [Keyword] public string PhoneNumber { get; set; }
        [Keyword] public string OrderCode { get; set; }
        [Keyword] public string ShopCode { get; set; }
        public string ShopName { get; set; }
        public int Quantity { get; set; }
        private DateTime _valueUsedDateTime { get; set; }
        public DateTime UsedDateTime
        {
            get => _valueUsedDateTime;
            set => _valueUsedDateTime = value.Date;
        }
    }
}
