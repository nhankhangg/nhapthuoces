﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.ETOs.Cache
{
    public class CacheDataIsLock
    {
        public bool IsLock { get; set; } = false;
    }

    public class CacheDataSyncOutput
    {
        public string PromotionCode { get; set; }
        public int TotalItemOutput { get; set; } = 0;
        public int CurrentItemOutput { get; set; } = 0;
        public int TotalAmountCondition { get; set; } = 0;
        public bool FlagIsAmountCondition { get; set; } = false;
    }

}
