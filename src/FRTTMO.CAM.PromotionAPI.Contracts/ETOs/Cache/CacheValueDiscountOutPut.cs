﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.ETOs.Cache
{
    public class CacheValueDiscountOutPut
    {
        public Guid PromotionId { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public int MinQuantity { get; set; }
        public int MaxQuantity { get; set; }
        public string DiscountType { get; set; }
        public decimal Discount { get; set; }
        public decimal MaxDiscount { get; set; }
        public string Note { get; set; }
        public int? UnitCode { get; set; }
        public string UnitName { get; set; }

        public int QuantityReplace { get; set; }
        public string NoteBoom { get; set; }
        public string DiscountPlaceCode { get; set; }
    }
}
