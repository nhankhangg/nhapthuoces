﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.ETOs.Cache
{
    public class CacheValueGetItemOutPut
    {
        public Guid PromotionId { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string VoucherCode { get; set; }
        public int Quantity { get; set; }
        public int MaxQuantity { get; set; }
        public int? UnitCode { get; set; }
        public string UnitName { get; set; }
        public string WarehouseCode { get; set; }
        public string Type { get; set; }
        public int QuantityReplace { get; set; }
        public int QuantityReplaceVoucher { get; set; }
        public string DiscountPlaceCode { get; set; }
        public string Label { get; set; }
    }
}
