﻿namespace FRTTMO.CAM.PromotionAPI.ETOs.Cache
{
    public class CacheValueGetVoucherOutPut : CacheValueGetItemOutPut
    {
         public string ApplicableMethod { get; set; }
         public string DiscountType { get; set; }
         public decimal Discount { get; set; }
         public decimal MaxDiscount { get; set; }
         public decimal Amount { get; set; }
    }
}
