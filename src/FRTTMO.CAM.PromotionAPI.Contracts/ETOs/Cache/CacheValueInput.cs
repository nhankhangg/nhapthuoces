﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.ETOs.Cache
{
    public class CacheValueInput 
    {
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string GiftCode { get; set; }
        public int Quantity { get; set; }
        public string WarehouseCode { get; set; }
        public int? UnitCode { get; set; }
        public string UnitName { get; set; }
        public bool FlagDiscount { get; set; } = false;
    }
    public class CacheValueInputReplace
    {
        public string AlterItemCode { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public int Quantity { get; set; }
        public string WarehouseCode { get; set; }
        public int? UnitCode { get; set; }
        public string UnitName { get; set; }
        public bool FlagDiscount { get; set; } = false;
        public string Label { get; set; }
    }
}
