﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FRTTMO.CAM.PromotionAPI.ETOs.Cache
{
    public class CacheValuePromotionHeader
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int? Priority { get; set; }
        public int Status { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public DateTime ActiveDate { get; set; }
        public string FromHour { get; set; }
        public string ToHour { get; set; }
        public string PromotionClass { get; set; }
        public string PromotionType { get; set; }
        public string DisplayArea { get; set; }
        public string ApplicableMethod { get; set; }
        public string[] Channels { get; set; }
        public string[] OrderTypes { get; set; }
        public string[] ItemInputExcludes { get; set; } = new string[0];
        public string[] StoreTypes { get; set; }
        public bool AllowDisplayOnBill { get; set; }
        public bool FlagDebit { get; set; }
        public string UrlImage { get; set; }
        public string UrlPage { get; set; }
        public string NameOnline { get; set; }
        public bool AllowShowOnline { get; set; }
        public string Description { get; set; }
        public string Remark { get; set; }
        public bool FlagProvinceCondition { get; set; } = false;
        public string[] SourceOrders { get; set; }
        public string VerifyScheme { get; set; }
        public string ProgramType { get; set; }
        public string[] CustomerGroups { get; set; }
        public string ShortDescription { get; set; }
        public bool IsBoom { get; set; }
        public bool IsShowDetail { get; set; }
        public string DescriptionBoom { get; set; }
        public string LabelDescription { get; set; }
        public QuotaEto Quota { get; set; } = null;
        public AmountConditionEto AmountCondition { get; set; } = null;
        public List<InstallmentConditionEto> InstallmentConditions { get; set; } = new();
        public List<ExtraConditionEto> ExtraConditions { get; set; } = new();
        public List<PromotionExcludeEto> PromotionExcludes { get; set; } = new();
        public List<PaymentConditionEto> PaymentConditions { get; set; } = new();
        public List<CostDistributionEto> CostDistributions { get; set; } = new();
    }

    public class QuotaEto
    {
        public string Type { get; set; }
        public int Quantity { get; set; }
        public int LimitQuantityShop { get; set; }
        public string ResetQuotaType { get; set; }
        public bool FlagQuantityPhone { get; set; }
        public int LimitQuantityPhone { get; set; }
        public bool FlagQuantityEmail { get; set; }
        public int LimitQuantityEmail { get; set; }
    }

    public class AmountConditionEto
    {
        public string Type { get; set; }
        public decimal MinAmount { get; set; }
        public decimal MaxAmount { get; set; }
    }

    public class InstallmentConditionEto
    {
        public string Type { get; set; }
        public List<string> Financiers { get; set; }
        public List<string> CardTypes { get; set; }
        public List<string> Tenures { get; set; }
        public float MinInterestRate { get; set; }
        public float MaxInterestRate { get; set; }
        public string PaymentType { get; set; }
        public List<string> PaymentForms { get; set; }
    }

    public class ExtraConditionEto
    {
        public string QualifierCode { get; set; }
        public string OperatorCode { get; set; }
        public string Values { get; set; }
        public decimal Number { get; set; }
    }

    public class PromotionExcludeEto
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public bool AllowDisplay { get; set; }
        public string PromotionExcludeType { get; set; }
        public string PromotionCode { get; set; }
    }

    public class PaymentConditionEto
    {
        public List<string> BankCodes { get; set; }
        public string PaymentType { get; set; }
        public List<string> CardTypes { get; set; }
        public List<string> PINCodes { get; set; }
        public List<string> PaymentForms { get; set; }
    }

    public class CostDistributionEto
    {
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }
        public List<string> CategoryCode { get; set; }
        public List<string> CategoryName { get; set; }
        public List<string> TypeCode { get; set; }
        public List<string> TypeName { get; set; }
        public List<string> BrandCode { get; set; }
        public List<string> BrandName { get; set; }
        public decimal Percentage { get; set; }
    }
}
