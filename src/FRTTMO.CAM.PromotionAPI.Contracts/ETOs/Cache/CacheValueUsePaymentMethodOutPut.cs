﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.ETOs.Cache
{
    public class CacheValueUsePaymentMethodOutPut
    {
        public Guid PromotionId { get; set; }
        public string PaymentMethodCode { get; set; }
        public string PaymentMethodName { get; set; }
        public string SchemeCode { get; set; }
        public decimal Discount { get; set; }
        public string DiscountType { get; set; }
        public decimal MaxDiscount { get; set; }
        public string Note { get; set; }
        public string NoteBoom { get; set; }
        public string DiscountPlaceCode { get; set; }
    }
}
