﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.ETOs.Event
{
    public class BaseEvenEto
    {
        public Guid Id { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string TypeCode { get; set; }
        public string TypeName { get; set; }
        public string ModelCode { get; set; }
        public string ModelName { get; set; }
        public string GroupCode { get; set; }
        public string GroupName { get; set; }
        public string BrandCode { get; set; }
        public string BrandName { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public string GiftCode { get; set; }
        public string GiftName { get; set; }
        public string CouponCode { get; set; }
        public string CouponName { get; set; }
        public string WarehouseCode { get; set; }
        public string WarehouseName { get; set; }
        public int Quantity { get; set; }
        public int? UnitCode { get; set; }
        public string UnitName { get; set; }
        public Guid PromotionId { get; set; }
        public string PromotionCode { get; set; }
        public string PromotionType { get; set; }
        public bool IsDiscount { get; set; }
        public DateTime? ExpireTime { get; set; }
        public int MinQuantity { get; set; }
        public int MaxQuantity { get; set; }
        public string QualifierCode { get; set; }
        public string VoucherDiscountType { get; set; }
        public decimal Discount { get; set; } = decimal.Zero;
        public decimal MaxDiscount { get; set; } = decimal.Zero;
        public decimal Amount { get; set; } = decimal.Zero;
        public string DiscountType { get; set; }
        public string Note { get; set; }
        public string SchemeCode { get; set; }
        public string SchemeName { get; set; }
        public string PaymentMethodCode { get; set; }
        public string PaymentMethodName { get; set; }
        public decimal MaxValue { get; set; } = decimal.Zero;
        public string ApplicableMethod { get; set; }
        public string ActionTypeSync { get; set; }
        public Guid SyncId { get; set; }
        public string NoteBoom { get; set; }
        public string DiscountPlaceCode { get; set; }
        public bool FlagGiftCode { get; set; } = false;
        public bool FlagGiftCodeOutput { get; set; } = false;
        public bool FlagCouponCodeOutput { get; set; } = false;
        public string CreatedByName { get; set; }
        public string UpdateByName { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public DateTime? CreationTime { get; set; }
    }

}
