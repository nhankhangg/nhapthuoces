﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.EventBus;

namespace FRTTMO.CAM.PromotionAPI.ETOs.Event
{
    [EventName("EventInputEto")]
    public class EventInputCacheEto
    {
        public Guid IdInput { get; set; }
        public string ItemCodeInput { get; set; }
        public string[] ItemInputExcludes { get; set; }
        public ItemInputCacheEto InputCache { get; set; }
    }
    public class ItemInputCacheEto : BaseEvenEto
    {
        public List<ItemInputCacheEto> ItemInputReplaceCaches { get; set; }
    }

}
