﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.EventBus;

namespace FRTTMO.CAM.PromotionAPI.ETOs.Event
{

    [EventName("EventOutputEto")]
    public class EventOutputCacheEto
    {
        public OutputCacheEto OutputCache { get; set; }
    }

    public class OutputCacheEto : BaseEvenEto
    {
        public List<OutputCacheEto> OutputReplaceCaches { get; set; }
    }


}
