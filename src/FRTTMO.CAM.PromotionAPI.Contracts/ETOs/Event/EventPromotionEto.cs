﻿using System;
using System.Collections.Generic;
using FRTTMO.CAM.PromotionAPI.Enum;
using FRTTMO.CAM.PromotionAPI.ETOs.Cache;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;
using Volo.Abp.EventBus;

namespace FRTTMO.CAM.PromotionAPI.ETOs
{
    [EventName("EventPromotionEto")]
    public class EventPromotionEto
    {
        public ActionType ActionType { get; set; } = ActionType.Create;
        public CacheValuePromotionHeader HeaderCache { get; set; }
        public List<ItemInputCacheEto> InputCache { get; set; }
        public List<OutputCacheEto> OutputCache { get; set; }
    }

    [EventName("EventConditionEto")]
    public class EventConditionEto
    {
        public string PromotionCode { get; set; }
        public List<ProvinceConditionEto> ProvinceConditions { get; set; }
    }
   
    public class ProvinceConditionEto
    {
        public string ProvinceCode { get; set; }
        public string ProvinceName { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }

}
