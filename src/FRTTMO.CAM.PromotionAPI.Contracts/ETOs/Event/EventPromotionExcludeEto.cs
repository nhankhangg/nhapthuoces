﻿using FRTTMO.CAM.PromotionAPI.ETOs.Cache;
using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.ETOs.Event
{
    public class EventPromotionExcludeEto
    {
        public string Code { get; set; }
        public string Name { get; set; }
        //Danh sách code loại trừ
        public List<string> PromotionCodes { get; set; }
        public List<PromotionExcludeEto> PromotionExcludes { get; set; } = new();
    }
}
