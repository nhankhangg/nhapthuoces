﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.ETOs.Event
{
    public class PromotionTimeExpireEto
    {
        public Guid PromotionId{ get; set; }
        public string PromotionCode { get; set; }
        public string PromotionType { get; set; }
        public List<string> ProvinceConditions { get; set; }
    }
}
