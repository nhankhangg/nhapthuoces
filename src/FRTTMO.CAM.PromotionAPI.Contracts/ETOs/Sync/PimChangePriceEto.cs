﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.ETOs.Sync
{
    public class PimChangePriceEto
    {
        [JsonProperty("EventName")]
        public string EventName { get; set; }

        [JsonProperty("Id")]
        public int? Id { get; set; }

        [JsonProperty("SKU")]
        public string SKU { get; set; }

        [JsonProperty("PriceBookId")]
        public int? PriceBookId { get; set; }

        [JsonProperty("ProductMeasureUnitId")]
        public int? ProductMeasureUnitId { get; set; }

        [JsonProperty("MeasureUnitName")]
        public string MeasureUnitName { get; set; }

        [JsonProperty("MeasureUnitId")]
        public int? MeasureUnitId { get; set; }

        [JsonProperty("Level")]
        public int? Level { get; set; }

        [JsonProperty("Price")]
        public decimal Price { get; set; }

        [JsonProperty("Store")]
        public string Store { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("FromDate")]
        public DateTime? FromDate { get; set; }

        [JsonProperty("ToDate")]
        public DateTime? ToDate { get; set; }
    }
}
