﻿using System;
using System.Collections.Generic;
using FRTTMO.CAM.PromotionAPI.DTOs.PIM;
using FRTTMO.CAM.PromotionAPI.Enum;
using FRTTMO.CAM.PromotionAPI.ETOs;
using FRTTMO.CAM.PromotionAPI.ETOs.Cache;

namespace FRTTMO.CAM.PromotionAPI.Etos
{
    public class EventPromotionSyncEto
    {
        public string PromotionCode { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string GiftCode { get; set; }
        public string GiftName { get; set; }
        public string CouponCode { get; set; }
        public string CouponName { get; set; }
        public string PaymentMethodCode { get; set; }
        public string PaymentMethodName { get; set; }
        public decimal Discount { get; set; }
        public decimal MaxDiscount { get; set; }
        public string QualifierType { get; set; }
        public string PromotionType { get; set; }
        public string OutputNote { get; set; }
        public string NoteBoom { get; set; }
        public string DiscountPlaceCode { get; set; }
        public int Quantity { get; set; }
        public int MaxQuantity { get; set; }
        public string ActionType { get; set; }
        public Guid SyncId { get; set; }
        public CacheDataSyncOutput CacheDataSync { get; set; }
        public List<ProductCache> Products { get; set; }
        public bool FlagGiftCode { get; set; } = false;
        public bool FlagGiftCodeOutput { get; set; } = false;
        public bool FlagCouponCodeOutput { get; set; } = false;
        public string WarehouseCode { get; set; }
        public string WarehouseName { get; set; }
    }
    public class PromotionSyncDetailMessagesEto
    {
        public Guid Id { get; set; }
        public string Action { get; set; }
        public Guid PromotionId { get; set; }
        public int Status { get; set; }
        public string PromotionCode { get; set; }
        public string PromotionName { get; set; }
        public string PromotionType { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; } 
        public string FromHour { get; set; }
        public string ToHour { get; set; }
        public string UrlImage { get; set; }
        public string UrlPage { get; set; }
        public int? Priority { get; set; }
        public string[] Channels { get; set; }
        public string[] StoreTypes { get; set; }
        public string[] OrderTypes { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string GiftCode { get; set; }
        public string GiftName { get; set; }
        public string CouponCode { get; set; }
        public string CouponName { get; set; }
        public string PaymentMethodCode { get; set; }
        public string PaymentMethodName { get; set; }

        public decimal Price { get; set; }
        public decimal DiscountPrice { get; set; }
        public decimal FinalPrice { get; set; }
        public decimal MaxDiscountAmount { get; set; }
        public string DiscountType { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public string NameOnline { get; set; }
        public bool AllowShowOnline { get; set; }
        public int Quantity { get; set; }
        public int MaxQuantity { get; set; }
        public DateTime? DateDisplay { get; set; }
        public DateTime? ActiveDate { get; set; }
        public int? QuotaQuantity { get; set; } = 0;
        public string ProgramType { get; set; }
        public string ShortDescription { get; set; }
        public bool IsBoom { get; set; }
        public bool IsShowDetail { get; set; }
        public string DescriptionBoom { get; set; }
        public string NoteBoom { get; set; }
        public string DiscountPlaceCode { get; set; }
        public string PromotionDisplayName { get; set; }

        public bool FlagGiftCode { get; set; } = false;
        public bool FlagGiftCodeOutput { get; set; } = false;
        public bool FlagCouponCodeOutput { get; set; } = false;
        public string WarehouseCode { get; set; }
        public string WarehouseName { get; set; }
        public string[] ItemInputExcludes { get; set; }
        public string[] SourceOrders { get; set; }
        public string[] CustomerGroups { get; set; }
        public AmountConditionEto AmountCondition { get; set; } = null;
        public List<PaymentConditionEto> PaymentCondition { get; set; } = new();
        public List<InstallmentConditionEto> InstallmentConditions { get; set; } = new();
        public List<PromotionExcludeEto> PromotionExcludes { get; set; } = new();
        public List<CostDistributionEto> CostDistributions { get; set; } = new();
        public List<ExtraConditionEto> ExtraConditions { get; set; } = new();
        public List<ProvinceConditionEto> ProvinceConditions { get; set; } = new();
    }
    public class PromotionFinishEto
    {
        public Guid Id { get; set; }
        public string Action { get; set; }
        public string PromotionCode { get; set; }
        public int? CountMessage { get; set; } = 0;
        public string ProgramType { get; set; }
        public string[] Channels { get; set; }
        public string[] StoreTypes { get; set; }
        public string[] OrderTypes { get; set; }
        public int Status { get; set; }
        public bool FlagGiftCode { get; set; } = false;
        public bool FlagGiftCodeOutput { get; set; } = false;
        public bool FlagCouponCodeOutput { get; set; } = false;
        public List<PromotionExcludeEto> PromotionExcludes { get; set; } = new();
        public List<ProvinceConditionEto> ProvinceConditions { get; set; } = new();
    }
    public class PromotionActiveSyncEto
    {
        public string PromotionCode { get; set; }
        public string ProgramType { get; set; }
        public string[] Channels { get; set; }
        public string[] StoreTypes { get; set; }
        public string[] OrderTypes { get; set; }
    }
    public class ChangePriceSyncEto
    {
        public string ItemCode { get; set; }
        public PromotionChangePriceSyncEto Promotions { get; set; }

    }
    public class PromotionChangePriceSyncEto
    {
        public string PromotionCode { get; set; }
        public decimal Price { get; set; }
        public decimal DiscountPrice { get; set; }
        public decimal FinalPrice { get; set; }
        public string ProgramType { get; set; }
        public string[] Channels { get; set; }
        public string[] StoreTypes { get; set; }
        public string[] OrderTypes { get; set; }
    }
}
