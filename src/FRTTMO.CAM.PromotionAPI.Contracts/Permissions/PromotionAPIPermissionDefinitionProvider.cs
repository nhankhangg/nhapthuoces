﻿using FRTTMO.CAM.PromotionAPI.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace FRTTMO.CAM.PromotionAPI.Permissions;

public class PromotionAPIPermissionDefinitionProvider : PermissionDefinitionProvider
{
    public override void Define(IPermissionDefinitionContext context)
    {
        var myGroup = context.AddGroup(PromotionAPIPermissions.GroupName, L("Permission:PromotionAPI"));
    }

    private static LocalizableString L(string name)
    {
        return LocalizableString.Create<PromotionAPIResource>(name);
    }
}