﻿using Volo.Abp.Reflection;

namespace FRTTMO.CAM.PromotionAPI.Permissions;

public class PromotionAPIPermissions
{
    public const string GroupName = "PromotionAPI";

    public static string[] GetAll()
    {
        return ReflectionHelper.GetPublicConstantsRecursively(typeof(PromotionAPIPermissions));
    }
}