﻿using Volo.Abp.Application;
using Volo.Abp.Authorization;
using Volo.Abp.Modularity;

namespace FRTTMO.CAM.PromotionAPI;

[DependsOn(
    typeof(PromotionAPIDomainSharedModule),
    typeof(AbpDddApplicationContractsModule),
    typeof(AbpAuthorizationModule)
)]
public class PromotionAPIApplicationContractsModule : AbpModule
{
}