﻿namespace FRTTMO.CAM.PromotionAPI;

public class PromotionAPIRemoteServiceConsts
{
    public const string RemoteServiceName = "PromotionAPI";

    public const string ModuleName = "promotionAPI";
}