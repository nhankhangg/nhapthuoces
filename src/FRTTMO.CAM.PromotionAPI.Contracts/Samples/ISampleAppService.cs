﻿using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace FRTTMO.CAM.PromotionAPI.Samples;

public interface ISampleAppService : IApplicationService
{
    Task<SampleDto> GetAsync();

    Task<SampleDto> GetAuthorizedAsync();
}