﻿using System;
using System.Threading.Tasks;
using FRTTMO.CAM.PromotionAPI.Dtos.Campaigns;
using FRTTMO.CAM.PromotionAPI.DTOs.Campaigns;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace FRTTMO.CAM.PromotionAPI.Services;

public interface ICampaignAppService : IApplicationService
{
    Task<CampaignDto> GetAsync(Guid id);
    Task<CampaignDto> CreateAsync(CreateCampaignDto createDto);
    Task<CampaignDto> UpdateAsync(Guid id, UpdateCampaignDto updateDto);
    Task<PagedResultDto<CampaignDto>> SearchCampaignAsync(SearchCampaignDto searchCampaign);
}