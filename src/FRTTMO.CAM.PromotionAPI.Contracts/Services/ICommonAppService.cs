﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FRTTMO.CAM.PromotionAPI.DTOs.Common;
using FRTTMO.CAM.PromotionAPI.DTOs.FieldConfigures;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace FRTTMO.CAM.PromotionAPI.Services;

public interface ICommonAppService : IApplicationService
{
    Task<List<CommonDto>> GetFieldConfiguresAsync(FieldConfigureFilterDto input);
    Task<List<QualifierConfigureDto>> GetQualifierConfigureAsync(QualifierConfigureFilterDto input);
    Task<List<CommonDto>> CreatePromotionImageAsync(List<CreateCommonDto> input);
    Task<bool> DeletePromotionImageAsync(string group, IList<string> code);
    Task<PagedResultDto<CommonDto>> FilterPromotionImageAsync(FilterPromotionImageDto imageDto);
    Task<CommonDto> GetAppVersionAsync();
    Task<CommonDto> UpdateAppVersionAsync(string version);
    Task<List<VoucherDefineDto>> GetVoucherDefineAsync(List<string> voucherCodes);

}