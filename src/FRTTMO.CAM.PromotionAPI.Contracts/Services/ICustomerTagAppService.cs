﻿using FRTTMO.CAM.PromotionAPI.Dtos.Campaigns;
using FRTTMO.CAM.PromotionAPI.DTOs.Campaigns;
using FRTTMO.CAM.PromotionAPI.DTOs.CustomerFiles;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace FRTTMO.CAM.PromotionAPI.Services
{
    public interface ICustomerTagAppService : IApplicationService
    {
        Task<CustomerTagDto> GetAsync(Guid id);
        Task<PagedResultDto<CustomerTagDto>> SearchCustomerFileAsync(SearchCustomerTagDto searchCustomer);
        Task<CustomerTagDto> CreateAsync(CreateCustomerTagDto createCustomerFile);
        Task<CustomerTagDto> UpdateAsync(Guid id, UpdateCustomerTagDto updateCustomerFile);
    }
}
