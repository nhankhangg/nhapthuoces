﻿using System;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using FRTTMO.CAM.PromotionAPI.Documents;
using System.Collections.Generic;
using FRTTMO.CAM.PromotionAPI.Enum;
using FRTTMO.CAM.PromotionAPI.DTOs.PromotionExcludes;
using FRTTMO.CAM.PromotionAPI.DTOs;
using FRTTMO.CAM.PromotionAPI.Paginations;
using FRTTMO.CAM.PromotionAPI.DTOs.Promotion;
using FRTTMO.CAM.PromotionAPI.DTOs.Quota;
using FRTTMO.CAM.PromotionAPI.Documents.Campaign;
using FRTTMO.CAM.PromotionAPI.Documents.PromotionInputReplaces;
using FRTTMO.CAM.PromotionAPI.Documents.PromotionOutputReplaces;

namespace FRTTMO.CAM.PromotionAPI.Services;

public interface IElasticSearchAppService : IApplicationService
{
    Task<bool> DeleteInputReplacesAsync(string promotionCode);

    Task<bool> DeleteOutputReplacesAsync(string promotionCode);

    void CreateUpdateInputReplaces(List<InputReplaceDocument> inputReplaceDocument);

    void CreateUpdateOutputReplacesAsync(List<OutputReplaceDocument> outputReplaceDocument);

    Task<bool> CreateCampaignDocumentAsync(CampaignDocument campaignDocument);

    Task<bool> UpdateCampaignDocumentAsync(Guid id, CampaignDocument campaignDocument);

    Task<bool> CreateUpdatePromotionShopConditionAsync(List<ProvinceConditionDocument> shopConditionDocuments); 
    
    Task CreateUpdatePromotionExcludesAsync(List<PromotionExcludeDocument> promotionExcludeDocument);

    Task<bool> CreateUpdatePromotionAsync(PromotionDocument promotionDocument);

    Task<bool> UpdateAsync(Guid id, PromotionDocument promotionDocument);



    Task<PromotionDocument> GetPromotionByIdAsync(Guid id, bool throwEx = false);

    Task UpdatePromotionStatusAsync(List<Guid> promotionIds, PromotionStatusEnum status);

    Task<bool> CreateQuotaHistoryAsync(QuotaHistoryDocument document);

    Task<UsedQuotaDto> GetUsedQuotaAsync(string promotionCode, QuotaDto quota);

    Task UpdateQuatityQuotaHistoryAsync(string promotionCode, QuotaDocument quota);

    Task<Tuple<long, List<PromotionDocument>>> GetListPromotionByItemAsync(string itemCode, PromotionStatusEnum? status,  string displayArea, Pagination pagination);

    Task<List<PromotionDocument>> GetListPromotionByTypeAsync(string keyword, List<PromotionStatusEnum> statuses,
            List<PromotionTypeEnum> promotionTypes, List<string> promotionExcludes);

    Task<List<string>> SuggestPromotionExcludeAsync(ItemDetailSuggestPromotionExcludeDto input, List<string> promotionIncludes);

    Task UpdateCampaignStatusAsync(List<Guid> campaignIds, CampaignStatusEnum status);
}