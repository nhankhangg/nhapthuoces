﻿using FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels;
using FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels.ItemExcludes;
using FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels.Shops;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace FRTTMO.CAM.PromotionAPI.Services
{
    public interface IImportExcelAppService : IApplicationService
    {
        //Task<ImportExcelDiscountDto> ImportExcelDiscount(CreateImportExcelDiscountDto createImport);
        //Task<ImportExcelDto> ImportExcelInputConditionAsync(IFormFile formFile, string promotionType, string templateType);
        //Task<ImportExcelDto> ImportExcelOutputConditionAsync(IFormFile formFile, string promotionType, string templateType);
        Task<ImportItemExcludesDto> ImportItemExcludesAsync(IFormFile formFile);
        Task<ImportExcelDto> ImportExcelSchemeAsync(IFormFile formFile, string promotionType, string templateType, bool isInput);
        Task<ImportItemShopDto> ImportShopAsync(List<string> storeType, DateTime fromDate, DateTime toDate, IFormFile formFile);
    }
}
