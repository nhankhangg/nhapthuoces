﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FRTTMO.CAM.PromotionAPI.Dtos.InsideApi;
using FRTTMO.CAM.PromotionAPI.DTOs.Insides;
using Volo.Abp.Application.Services;

namespace FRTTMO.CAM.PromotionAPI.Services;

public interface IInsideAppService : IApplicationService
{
    Task<List<ListShopOutputDto>> ShopAll(string typeShop);
    Task<List<DepartmentDto>> GetListDepartmentAsync();
}