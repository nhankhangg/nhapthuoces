﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FRTTMO.CAM.PromotionAPI.Dtos;
using FRTTMO.CAM.PromotionAPI.DTOs;
using FRTTMO.CAM.PromotionAPI.DTOs.PIM;
using FRTTMO.CAM.PromotionAPI.Paginations;
using Volo.Abp.Application.Services;

namespace FRTTMO.CAM.PromotionAPI.Services;

public interface IPIMAppService : IApplicationService
{
    Task<PaginationResultDto<CategoryLevelDto>> SearchCategoryLevelAsync(SearchCategoryLevelInputDto input, Pagination pagination);
    Task<PaginationResultDto<ProductDetailsDto>> SearchProductAsync(SearchProductInputDto input, Pagination pagination);
    Task<List<ItemDetails>> ListProductDetailAsync(List<string> sKus);
    Task<PimProductCacheDto> GetListProductByPimCacheAsync(PimCacheSearch pimCacheSearch);
    Task<List<MeasureUnitDto>> MeasureUnitsAsync();
    Task<List<CategoryValidationDto>> CategoriesValidationAsync(List<CategoryValidationInputDto> categoryValidationInputs);
    Task<List<ItemDetailWithCategoryDto>> GetListProductWithCategoryAsync(List<string> skus);
    Task<PimProductCacheDto> GetProductByPimCacheAsync(string sku);
    Task<CountPimProductCacheDto> CountDataPimCacheAsync(PimCacheSearch pimCacheSearch);
    Task<GetProductCategoryOutputDto> GetGroupByCategoryId(GroupInputDto input);

}