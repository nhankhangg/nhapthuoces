﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using FRTTMO.CAM.PromotionAPI.DTOs.Promotion;
using FRTTMO.CAM.PromotionAPI.DTOs.Quota;
using FRTTMO.CAM.PromotionAPI.Enum;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;
using FRTTMO.CAM.PromotionAPI.DTOs.PromotionExcludes;
using FRTTMO.CAM.PromotionAPI.ETOs.Sync;

namespace FRTTMO.CAM.PromotionAPI.Services;

public interface IPromotionAppService : IApplicationService
{
    Task BuildPromotionCacheAsync(PromotionDto promotionResponseDto, ActionType actionType);

    Task ReCachePromotionDetailAsync(Guid promotionId, ActionType actionType = ActionType.Create);

    Task<List<string>> ReCachePromotionAsync(List<string> promotionCodes, ActionType actionType = ActionType.Create);

    Task<List<PromotionDto>> GetListAsync(SearchPromotionDto searchPromotionDto);

    Task<PromotionDto> GetAsync(Guid id);

    Task<PromotionDto> CreateAsync(CreatePromotionDto createDto);

    Task<PromotionDto> UpdateAsync(Guid promotionId, UpdatePromotionDto updateDto);

    Task TimeExpireCacheAsync(List<PromotionTimeExpireEto> promotionTimeExpire);

    Task<PagedResultDto<PromotionDto>> SearchPromotionAsync(SearchPromotionDto searchPromotionDto);

    Task<List<ResponseQuotaDto>> UseQuotaAsync(List<UseOrCheckQuotaDto> useOrCheckQuota);

    Task<List<ResponseCheckQuotaDto>> CheckQuotaAsync(List<UseOrCheckQuotaDto> useOrCheckQuota);

    Task<List<SuggestPromotionExcludeDto>> SuggestPromotionExcludeAsync(SuggestPromotionExcludeRequestDto input);

    Task<CheckUpdatePermissionDto> CheckUpdatePermissionAsync(Guid promotionId);

    Task<PagedResultDto<PromotionByItemDto>> SearchPromotionWithItemCodeAsync(SearchPromotionByItemDto input);

    Task<bool> ChangePricePromotionAsync(PimChangePriceEto pimChangePriceEto);
    Task<UsedQuotaDto> GetUsedQuotaAsync(string promotionCode);

    Task UpdatePromotionExcludeRelatedAsync(EventPromotionExcludeEto excludeEto);
    Task PromotionExcludeCacheAndSyncEcomAsync(List<string> promotionCodes);
    Task<PromotionDto> GetByCodeAsync(string promotionCode);

    Task ScheduleReCacheAsync();

    Task JobSyncDataDailyFromActiveDateAsync();

    Task ClearPromotionInactiveFromRedisAsync(List<string> promotionCodes);
}