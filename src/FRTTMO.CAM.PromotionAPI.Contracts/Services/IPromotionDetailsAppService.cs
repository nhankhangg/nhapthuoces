﻿using FRTTMO.CAM.PromotionAPI.DTOs.AmountConditions;
using FRTTMO.CAM.PromotionAPI.DTOs.Condition;
using FRTTMO.CAM.PromotionAPI.DTOs.CostDistribution;
using FRTTMO.CAM.PromotionAPI.DTOs.InstallmentConditions;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInput;
using FRTTMO.CAM.PromotionAPI.DTOs.Output;
using FRTTMO.CAM.PromotionAPI.DTOs.Promotion;
using FRTTMO.CAM.PromotionAPI.DTOs.Promotion.PromotionDetails;
using FRTTMO.CAM.PromotionAPI.DTOs.Quota;
using FRTTMO.CAM.PromotionAPI.DTOs.RegionCondition;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using UpdatePromotionHeaderDto = FRTTMO.CAM.PromotionAPI.DTOs.Promotion.PromotionDetails.UpdatePromotionHeaderDto;

namespace FRTTMO.CAM.PromotionAPI.Services
{
    public interface IPromotionDetailsAppService : IApplicationService
    {
        Task<QuotaDto> UpdateQuotaAsync(Guid promotionId, UpdateQuotaDto updateQuotaDto);
        Task<PromotionUpdateDetailResponseDto> UpdateShopConditionAsync(Guid promotionId, List<UpdateProvinceConditionDto> updateShopConditionDtos);
        Task<PromotionUpdateDetailResponseDto> UpdateInstallmentConditionAsync(Guid promotionId, List<UpdateInstallmentConditionDto> updateInstallmentConditionDto);
        Task<PromotionHeaderDto> UpdatePromotionHeaderAsync(Guid promotionId, UpdatePromotionHeaderDto updatePromotionHeaderDto);
        Task<PromotionWebDto> UpdatePromotionWebAsync(Guid promotionId, UpdatePromotionWebDto updatePromotionWebDto);
        //Task<PromotionUpdateDetailResponseDto> UpdateInputPromotionAsync(Guid promotionId, List<UpdateItemInputDto> updateInputPromotionDtos);
        Task<PromotionUpdateDetailResponseDto> UpdateStatusPromotionAsync(Guid promotionId, int? status);
        //Task<PromotionUpdateDetailResponseDto> UpdateOutputPromotionAsync(Guid promotionId, List<UpdateOutputDto> updateOutputDtos);
        Task<PromotionUpdateDetailResponseDto> UpdateCostDistributionAsync(Guid promotionId, List<UpdateCostDistributionDto> updateCostDistributionDtos);
        Task<PromotionUpdateDetailResponseDto> UpdateInputOutputAsync(Guid promotionId, UpdateInputOutputDto updateInputOutputDto);
        Task<AmountConditionDto> UpdateAmountConditionAsync(Guid promotionId, UpdateAmountConditionDto updateAmountConditionDto);
        Task<PromotionUpdateDetailResponseDto> UpdateExtraConditionAsync(Guid promotionId, UpdateExtraConditionDto updateExtraConditionDto);

        Task<PromotionUpdateDetailResponseDto> UpdateInputAsync(Guid promotionId, UpdateInputDetailsDto updateItemInputDto);
        Task<PromotionUpdateDetailResponseDto> UpdateOutputAsync(Guid promotionId, UpdateOutputDetailsDto updateOutputDto);
    }
}
