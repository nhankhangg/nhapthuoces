﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.Entities.AmountCondition
{
    public class AmountCondition : PromotionBaseEntity
    {
        public Guid PromotionId { get; set; }
        [Required]
        [StringLength(100)]
        public string Type { get; set; }
        public decimal MinAmount { get; set; }
        public decimal MaxAmount { get; set; }
    }
}
