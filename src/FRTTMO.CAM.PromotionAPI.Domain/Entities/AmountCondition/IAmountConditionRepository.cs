﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.AmountCondition
{
    public interface IAmountConditionRepository : IRepository<AmountCondition, Guid>
    {
    }
}
