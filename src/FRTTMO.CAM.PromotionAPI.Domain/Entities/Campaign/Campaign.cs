﻿using System;

namespace FRTTMO.CAM.PromotionAPI.Entities.Campaign;

public class Campaign : PromotionBaseEntity
{
    public string Name { get; set; }

    public string Code { get; set; }

    public string Description { get; set; }

    public DateTime FromDate { get; set; }

    public DateTime ToDate { get; set; }

    public int Status { get; set; }
}