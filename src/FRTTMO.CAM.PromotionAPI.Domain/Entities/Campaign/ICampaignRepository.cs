﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FRTTMO.CAM.PromotionAPI.DTOs.Campaigns;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.Campaign;

public interface ICampaignRepository : IRepository<Campaign, Guid>
{
    Task<List<Campaign>> SearchCampaignAsync(SearchCampaignDto searchCampaignDto);
    Task UpdateCampaignStatusExpiredAsync(Guid id);
}