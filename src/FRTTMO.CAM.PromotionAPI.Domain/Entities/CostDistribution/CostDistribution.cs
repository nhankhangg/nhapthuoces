﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.Entities.CostDistribution
{
    public class CostDistribution : PromotionBaseEntity
    {
        public Guid PromotionId { get; set; }
        [Required]
        [StringLength(100)]
        public string DepartmentCode { get; set; }
        [StringLength(500)]
        public string DepartmentName { get; set; }
        [StringLength(100)]
        public string CategoryCode { get; set; }
        [StringLength(500)]
        public string CategoryName { get; set; }
        [StringLength(100)]
        public string TypeCode { get; set; }
        [StringLength(500)]
        public string TypeName { get; set; }
        [StringLength(100)]
        public string BrandCode { get; set; }
        [StringLength(500)]
        public string BrandName { get; set; }
        public decimal Percentage { get; set; }
    }
}
