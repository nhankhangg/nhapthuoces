﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.CostDistribution
{
    public interface ICostDistributionRepository : IRepository<CostDistribution, Guid>
    {
        Task BulkDeleteAsync(Guid promotionId);
    }
}
