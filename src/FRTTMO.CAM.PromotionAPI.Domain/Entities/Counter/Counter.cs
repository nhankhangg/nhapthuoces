﻿namespace FRTTMO.CAM.PromotionAPI.Entities.Common;

public class Counter : PromotionBaseEntity
{
    public int Value { get; set; }
}