﻿using System;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.Common;

public interface ICounterRepository : IRepository<Counter, Guid>
{
    Task ResetCounter();
    Task<string> GenerateCode(string prefix);
}