﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities;

namespace FRTTMO.CAM.PromotionAPI.Entities.CustomerTags
{
    public class CustomerTags : PromotionBaseEntity
    {
        public string CustomerGroupCode { get; set; }
        public string CustomerGroupName { get; set; }
        public string PromotionCode { get; set; }
        public string PromotionName { get; set; }
        public Guid PromotionId { get; set; }
        public int Status { get; set; }
        public bool Tick { get; set; }
    }
}
