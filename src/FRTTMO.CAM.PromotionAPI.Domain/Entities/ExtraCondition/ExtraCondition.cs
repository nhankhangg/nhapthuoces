﻿using System;

namespace FRTTMO.CAM.PromotionAPI.Entities.Condition;

public class ExtraCondition : PromotionBaseEntity
{
    public Guid PromotionId { get; set; }
    public string PromotionCode { get; set; }
    public string QualifierCode { get; set; }
    public string OperatorCode { get; set; }
    public string Values { get; set; }
    public decimal Number { get; set; }
}