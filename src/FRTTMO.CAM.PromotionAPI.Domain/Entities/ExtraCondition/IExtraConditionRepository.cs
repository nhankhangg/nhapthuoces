﻿using System;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.Condition;

public interface IExtraConditionRepository : IRepository<ExtraCondition, Guid>
{
}