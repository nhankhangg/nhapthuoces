﻿using System;
using Volo.Abp.Auditing;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Entities.Auditing;

namespace FRTTMO.CAM.PromotionAPI.Entities.FieldConfigure;

public class FieldConfigure : CreationAuditedEntity<Guid>
{
    public string Code { get; set; }
    public string Name { get; set; }
    public string Group { get; set; }
    public string ParentCode { get; set; }
    public string ParentGroup { get; set; }
    public string Description { get; set; }
    public int? LineNumber { get; set; }
}