﻿using System;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.FieldConfigure;

public interface IFieldConfigureRepository : IRepository<FieldConfigure, Guid>
{
}