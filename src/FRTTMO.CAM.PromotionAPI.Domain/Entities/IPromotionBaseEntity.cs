﻿namespace FRTTMO.CAM.PromotionAPI.Entities;

public interface IPromotionBaseEntity
{
    string CreatedBy { get; set; }
    string CreatedByName { get; set; }
    string UpdateBy { get; set; }
    string UpdateByName { get; set; }
}