﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;

namespace FRTTMO.CAM.PromotionAPI.Entities.ImageConfigure
{
    public class ImageConfigure : CreationAuditedEntity<Guid>
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Group { get; set; }
    }
}
