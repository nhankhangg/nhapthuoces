﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.InputCacheItem;

public interface IInputCacheItemRepository : IRepository<InputCacheItem, Guid>
{
    Task BulkDeleteAsync(Guid promotionId);
    Task BulkCopyAsync(List<InputCacheItem> inputCacheItems);
    Task<List<InputCacheItem>> GetListInputCacheItemNolockAsync(string promotionCode);
}