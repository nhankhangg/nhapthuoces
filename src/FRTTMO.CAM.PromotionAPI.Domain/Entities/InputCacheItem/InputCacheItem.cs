﻿using System;
using Volo.Abp.Data;
using Volo.Abp.Domain.Entities;

namespace FRTTMO.CAM.PromotionAPI.Entities.InputCacheItem;

public class InputCacheItem : Entity<Guid>, IHasExtraProperties
{
    public InputCacheItem(Guid id)
    {
        Id = id;
    }

    public InputCacheItem() { }
    public Guid PromotionId { get; set; }
    public Guid InputItemId { get; set; }
    public string PromotionCode { get; set; }
    public string ItemCode { get; set; }
    public string GiftCode { get; set; }
    public int Quantity { get; set; }
    public string UnitCode { get; set; }
    public ExtraPropertyDictionary ExtraProperties { get; set; }
}