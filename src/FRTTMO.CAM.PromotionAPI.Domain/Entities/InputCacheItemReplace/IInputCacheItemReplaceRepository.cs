﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.InputCacheItemReplace;

public interface IInputCacheItemReplaceRepository : IRepository<InputCacheItemReplace, Guid>
{
    Task BulkDeleteAsync(Guid inputCacheItemId);
    Task BulkCopyAsync(List<InputCacheItemReplace> inputCacheItemReplaces);

    Task<double> CountInputCacheItemReplaceNolockAsync(Guid promotionId);
    Task<List<InputCacheItemReplace>> GetListInputCacheItemReplaceNolockAsync(Guid promotionId);
    Task<List<InputCacheItemReplace>> GetListSkipTakeInputCacheItemReplaceNolockAsync(Guid promotionId, int fromSize);
}