﻿using System;
using Volo.Abp.Data;
using Volo.Abp.Domain.Entities;

namespace FRTTMO.CAM.PromotionAPI.Entities.InputCacheItemReplace;

public class InputCacheItemReplace : Entity<Guid>, IHasExtraProperties
{
    public InputCacheItemReplace(Guid id)
    {
        Id = id;
    }
    public Guid InputCacheItemId { get; set; }
    public string ItemCode { get; set; }
    public string GiftCode { get; set; }
    public int Quantity { get; set; }
    public Guid PromotionId { get; set; }
    public string PromotionCode { get; set; }
    public string UnitCode { get; set; }
    public ExtraPropertyDictionary ExtraProperties { get; set; }
}