﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.InstallmentCondition
{
    public interface IInstallmentConditionRepository : IRepository<InstallmentCondition, Guid>
    {
        Task BulkDeleteAsync(Guid promotionId);
    }
}
