﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.Entities.InstallmentCondition
{
    public class InstallmentCondition : PromotionBaseEntity
    {
        public Guid PromotionId { get; set; }
        [Required]
        [StringLength(100)]
        public string Type { get; set; }
        [StringLength(500)]
        public string Financiers { get; set; }
        [StringLength(500)]
        public string CardTypes { get; set; }
        [StringLength(500)]
        public string Tenures { get; set; }
        public string PaymentForms { get; set; }
        [StringLength(100)]
        public string InterestRateType { get; set; }
        public float MinInterestRate { get; set; }
        public float MaxInterestRate { get; set; }
        public string PaymentType { get; set; }
    }
    
}
