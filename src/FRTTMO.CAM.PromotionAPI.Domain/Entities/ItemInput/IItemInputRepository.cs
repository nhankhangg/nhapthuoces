﻿using System;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.ItemInput;

public interface IItemInputRepository : IRepository<ItemInput, Guid>
{
    Task BulkDeleteAsync(Guid promotionId);
}