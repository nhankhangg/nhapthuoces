﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.ItemInputExclude
{
    public interface IItemInputExcludeRepository : IRepository<ItemInputExclude, Guid>
    {
        Task BulkDeleteAsync(Guid promotionId);
    }
}
