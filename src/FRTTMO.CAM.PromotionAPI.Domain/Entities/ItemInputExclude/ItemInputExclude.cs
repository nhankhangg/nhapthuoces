﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.Entities.ItemInputExclude
{
    public class ItemInputExclude : PromotionBaseEntity
    {
        public Guid PromotionId { get; set; }
        [StringLength(100)]
        public string ItemCode { get; set; }
        [StringLength(1000)]
        public string ItemName { get; set; }
    }
}
