﻿using System;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.ItemInputReplace;

public interface IItemInputReplaceRepository : IRepository<ItemInputReplace, Guid>
{
    Task BulkDeleteAsync(Guid promotionId);
}