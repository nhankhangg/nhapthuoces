﻿using System;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.Output;

public interface IOutputRepository : IRepository<Output, Guid>
{
    Task BulkDeleteAsync(Guid promotionId);
}