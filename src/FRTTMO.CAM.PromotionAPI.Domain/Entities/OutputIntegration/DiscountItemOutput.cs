﻿using System;

namespace FRTTMO.CAM.PromotionAPI.Entities.OutputIntegration;

public sealed class DiscountItemOutput : PromotionBaseEntity
{
    public DiscountItemOutput(Guid id)
    {
        Id = id;
        CreationTime = DateTime.Now;
        LastModificationTime = DateTime.Now;
    }
    public Guid PromotionId { get; set; }
    public string PromotionCode { get; set; }
    public string ItemCode { get; set; }
    public string ItemName { get; set; }
    public int MinQuantity { get; set; }
    public int MaxQuantity { get; set; }
    public int? UnitCode { get; set; }
    public string UnitName { get; set; }
    public string DiscountType { get; set; }
    public decimal Discount { get; set; }
    public decimal MaxDiscount { get; set; }
    public string PromotionType { get; set; }
    public string OutPutType { get; set; }
    public string OutPutNote { get; set; }
    public string NoteBoom { get; set; } //Ghi chú Boom giá
}