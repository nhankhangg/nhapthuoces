﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.Entities.OutputIntegration
{
    public sealed class GetAscendingDiscountOutPut : PromotionBaseEntity
    {
        public GetAscendingDiscountOutPut(Guid id)
        {
            Id = id;
            CreationTime = DateTime.Now;
            LastModificationTime = DateTime.Now;
        }
        public Guid PromotionId { get; set; }
        public string PrmotionCode { get; set; }
        public int MinQuantity { get; set; }
        public int MaxQuantity { get; set; }
        public string DiscountType { get; set; }
        public decimal Discount { get; set; }
        public decimal MaxDiscount { get; set; }
        public int? UnitCode { get; set; }
        public string UnitName { get; set; }

    }
}
