﻿using System;

namespace FRTTMO.CAM.PromotionAPI.Entities.OutputIntegration;

public sealed class GetItemOutput : PromotionBaseEntity
{
    public GetItemOutput(Guid id)
    {
        Id = id;
        CreationTime = DateTime.Now;
        LastModificationTime = DateTime.Now;
    }
    public Guid PromotionId { get; set; }
    public string PromotionCode { get; set; }
    public string ItemCode { get; set; }
    public string ItemName { get; set; }
    public string VoucherCode { get; set; }
    public int Quantity { get; set; }
    public int? UnitCode { get; set; }
    public string UnitName { get; set; }
    public int MaxQuantity { get; set; }
    public string WarehouseCode { get; set; }
    public string PromotionType { get; set; }
    public string OutPutType { get; set; }
    public string OutPutNote { get; set; }
    public string QualifierCode { get; set; }
    public string NoteBoom { get; set; } //Ghi chú Boom giá
}