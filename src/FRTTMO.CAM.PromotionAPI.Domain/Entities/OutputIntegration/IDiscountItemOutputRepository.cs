﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.OutputIntegration;

public interface IDiscountItemOutputRepository : IRepository<DiscountItemOutput, Guid>
{
    Task BulkCopyAsync(List<DiscountItemOutput> discountItemOutput);
    Task BulkDeleteAsync(Guid promotionId);
}