﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.OutputIntegration
{
    public interface IGetAscendingDiscountOutPutRepository : IRepository<GetAscendingDiscountOutPut, Guid>
    {
        Task BulkCopyAsync(List<GetAscendingDiscountOutPut> buyProductGetAscendingDiscountOutPut);
        Task BulkDeleteAsync(Guid promotionId);
    }
}
