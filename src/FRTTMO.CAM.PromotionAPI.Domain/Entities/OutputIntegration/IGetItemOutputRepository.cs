﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.OutputIntegration;

public interface IGetItemOutputRepository : IRepository<GetItemOutput, Guid>
{
    Task BulkCopyAsync(List<GetItemOutput> getItemOutput);
    Task BulkDeleteAsync(Guid promotionId);
}