﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FRTTMO.CAM.PromotionAPI.Entities.OutputIntegration
{
    public sealed class PaymentMethodOutput : PromotionBaseEntity
    {
        public PaymentMethodOutput(Guid id)
        {
            Id = id;
            CreationTime = DateTime.Now;
            LastModificationTime = DateTime.Now;
        }
        public Guid PromotionId { get; set; }
        public string PromotionCode { get; set; }
        public int? UnitCode { get; set; }
        public string UnitName { get; set; }
        public string ItemName { get; set; }
        public string ItemCode { get; set; }
        public string PaymentMethodCode { get; set; }
        public string PaymentMethodName { get; set; }
        public string SchemeCode { get; set; } // Mã đối tác
        public decimal Discount { get; set; }
        public string DiscountType { get; set; }
        public decimal MaxDiscount { get; set; }
        public string Note { get; set; }
        public string NoteBoom { get; set; } //Ghi chú Boom giá
        public string DiscountPlaceCode { get; set; } // Chọn Nơi giảm giá (Đối tác or fptshop)
    }
}
