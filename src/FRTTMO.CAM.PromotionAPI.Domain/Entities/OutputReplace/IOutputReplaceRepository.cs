﻿using System;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.OutputReplace;

public interface IOutputReplaceRepository : IRepository<OutputReplace, Guid>
{
    Task BulkDeleteAsync(Guid promotionId);
}