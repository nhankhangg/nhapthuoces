﻿using StackExchange.Redis;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Auditing;

namespace FRTTMO.CAM.PromotionAPI.Entities.OutputReplace;

public class OutputReplace : PromotionBaseEntity, IHasDeletionTime
{
    public Guid PromotionId { get; set; }
    public Guid OutputId { get; set; }
    [StringLength(100)]
    public string QualifierCode { get; set; }
    [StringLength(100)]
    public string OperatorCode { get; set; }
    public int Quantity { get; set; }
    public int MinQuantity { get; set; }
    public int? MaxQuantity { get; set; }
    public decimal MinValue { get; set; }
    public decimal MaxValue { get; set; }
    public decimal Discount { get; set; }
    public Guid? ItemInputReplaceId { get; set; }
    [StringLength(100)]
    public string ItemCode { get; set; }
    [StringLength(1000)]
    public string ItemName { get; set; }
    [StringLength(100)]
    public string UnitCode { get; set; }
    [StringLength(500)]
    public string UnitName { get; set; }
    [StringLength(100)]
    public string ModelCode { get; set; }
    [StringLength(500)]
    public string ModelName { get; set; }
    [StringLength(100)]
    public string TypeCode { get; set; }
    [StringLength(500)]
    public string TypeName { get; set; }
    [StringLength(100)]
    public string CategoryCode { get; set; }
    [StringLength(500)]
    public string CategoryName { get; set; }
    [StringLength(100)]
    public string GroupCode { get; set; }
    [StringLength(500)]
    public string GroupName { get; set; }
    [StringLength(100)]
    public string BrandCode { get; set; }
    [StringLength(500)]
    public string BrandName { get; set; }

    [StringLength(100)]
    public string GiftCode { get; set; }
    [StringLength(500)]
    public string GiftName { get; set; }

    [StringLength(100)]
    public string CouponCode { get; set; }
    [StringLength(500)]
    public string CouponName { get; set; }

    [StringLength(100)]
    public string WarehouseCode { get; set; }
    public string WarehouseName { get; set; }
    public DateTime? DeletionTime { get; set; }
    public bool IsDeleted { get; set; }
    public string Note { get; set; }
    public int LineNumber { get; set; }
    public string NoteBoom { get; set; } //Ghi chú Boom giá
    public string DiscountPlaceCode { get; set; }// Chọn Nơi giảm giá (Đối tác or fptshop)
    public string DiscountPlaceName { get; set; }// Chọn Nơi giảm giá (Đối tác or fptshop)
}