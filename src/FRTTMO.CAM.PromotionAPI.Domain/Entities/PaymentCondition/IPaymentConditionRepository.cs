﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.PaymentCondition
{
    public interface IPaymentConditionRepository : IRepository<PaymentCondition, Guid>
    {
        Task BulkDeleteAsync(Guid promotionId);
    }
}
