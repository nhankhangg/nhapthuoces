﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities;

namespace FRTTMO.CAM.PromotionAPI.Entities.PaymentCondition
{
    public class PaymentCondition : PromotionBaseEntity
    {
        public Guid PromotionId { get; set; }
        public string BankCodes { get; set; }
        public string PaymentType { get; set; }
        public string PaymentForms { get; set; }
        public string CardTypes { get; set; }
        public string PINCodes { get; set; }
    }
}
