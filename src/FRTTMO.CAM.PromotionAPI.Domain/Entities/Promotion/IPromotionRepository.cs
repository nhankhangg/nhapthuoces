﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.Promotion;

public interface IPromotionRepository : IRepository<Promotion, Guid>
{
    Task UpdatePromotionStatusExpiredAsync(Guid id);
    Task<List<string>> GetPromotionReCacheAsync();
    Task<List<Promotion>> GetPromotionActiveDateAsync();
    Task<List<Promotion>> GetPromotionFromStastusInActiveAsync(List<string> promotionCodes);
}