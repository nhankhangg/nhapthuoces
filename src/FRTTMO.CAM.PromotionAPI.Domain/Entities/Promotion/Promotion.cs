﻿using Nest;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FRTTMO.CAM.PromotionAPI.Entities.Promotion;

/// <summary>
///     Lớp đại diện thông tin cơ cấu khuyến mãi
/// </summary>
public class Promotion : PromotionBaseEntity
{
    public string Code { get; set; }
    public string Name { get; set; }
    public int Status { get; set; }
    public Guid CampaignId { get; set; }
    public string PromotionClass { get; set; }
    public string PromotionType { get; set; }
    //[Column(TypeName = "Date")] 
    public DateTime FromDate { get; set; }
    //[Column(TypeName = "Date")] 
    public DateTime ToDate { get; set; }
    public TimeSpan FromHour { get; set; }
    public TimeSpan ToHour { get; set; }
    public string CustomerGroups { get; set; }  // Đối tượng hưởng khuyến mãi Flc,ict
    public string StoreTypes { get; set; }      // Hệ thống áp dụng FStudio, FPTShop
    public string ApplicableMethod { get; set; } // Hình thức áp dụng khuyến mãi (Trong đơn hàng, Sau khi hoàn tất đơn hàng)
    public string OrderTypes { get; set; }       //Đơn hàng trả góp, Đơn hàng đặt cọc, Đơn hàng tại quầy, Đơn hàng bán nợ
    public string PromotionExcludes { get; set; }
    public string Channels { get; set; }
    public string TradeIndustryCode { get; set; } // Mã đăng kí với bộ
    public DateTime ActiveDate { get; set; }
    public int? Priority { get; set; }
    public string DisplayArea { get; set; }
    public string NameOnline { get; set; }
    public string UrlImage { get; set; }
    public string UrlPage { get; set; }

    public bool AllowDisplayOnBill { get; set; } //Hiển thị trên bill
    public bool AllowShowOnline { get; set; } //Thông tin hiển thị web
    public bool FlagDebit { get; set; }
    public string Description { get; set; }
    public string Remark { get; set; }
    public string SourceOrders { get; set; } // Nguồn đơn hàng
    public string VerifyScheme { get; set; } // Xác thực scheme
    public string ProgramType { get; set; } //Loại chương trình
    public string ShortDescription { get; set; } //Mô tả ngắn
    public bool IsBoom { get; set; } //Hiển thị Boom giá
    public bool IsShowDetail { get; set; } //Hiển thị trang chi tiết
    public string DescriptionBoom { get; set; } //Mô tả Boom giá
    public string LabelDescription { get; set; } //Mô tả ngắn Khuyến Mãi
}