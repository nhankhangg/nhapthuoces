﻿using System;
using Volo.Abp.Domain.Entities.Auditing;

namespace FRTTMO.CAM.PromotionAPI.Entities;

public abstract class PromotionBaseEntity : AuditedEntity<Guid>, IPromotionBaseEntity
{
    public virtual string CreatedBy { get; set; }

    public virtual string CreatedByName { get; set; }

    public virtual string UpdateBy { get; set; }

    public virtual string UpdateByName { get; set; }
}