﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.PromotionExclude
{
    public interface IPromotionExcludeRepository : IRepository<PromotionExclude, Guid>
    {
        Task BulkCopyAsync(List<PromotionExclude> promotionExcludes);
        Task BulkDeleteAsync(string promotionCode);
        Task BulkDeleteAsync(List<PromotionExclude> promotionCodes);
        Task<List<PromotionExclude>> GetListPromotionExcludeNolockAsync(string promotionCode);
        Task<List<PromotionExclude>> GetListPromotionExcludeNolockAsync(List<string> promotionCodes);
    }
}
