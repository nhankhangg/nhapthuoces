﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FRTTMO.CAM.PromotionAPI.Entities.PromotionExclude
{
    public class PromotionExclude : PromotionBaseEntity
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public bool AllowDisplay { get; set; }
        public string PromotionExcludeType { get; set; }
        public string PromotionCode { get; set; }
    }
}
