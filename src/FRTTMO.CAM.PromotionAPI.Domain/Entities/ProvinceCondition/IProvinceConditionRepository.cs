﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.RegionCondition;

public interface IProvinceConditionRepository : IRepository<ProvinceCondition, Guid>
{
    Task<List<string>> BulkDeleteAsync(Guid promotionId);
    Task BulkCopyAsync(List<ProvinceCondition> regionConditions);
}