﻿using System;
using Volo.Abp.Auditing;

namespace FRTTMO.CAM.PromotionAPI.Entities.RegionCondition;

public class ProvinceCondition : PromotionBaseEntity, IHasDeletionTime
{
    public ProvinceCondition(Guid id)
    {
        Id = id;
    }

    public ProvinceCondition() { }
    public Guid PromotionId { get; set; }
    public string PromotionCode { get; set; }
    public string ProvinceCode { get; set; }
    public string ProvinceName { get; set; }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public DateTime? DeletionTime { get; set; }
    public bool IsDeleted { get; set; }
}