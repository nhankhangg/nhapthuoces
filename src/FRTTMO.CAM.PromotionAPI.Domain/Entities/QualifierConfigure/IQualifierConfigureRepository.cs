﻿using System;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.QualifierConfigure;

public interface IQualifierConfigureRepository : IRepository<QualifierConfigure, Guid>
{
}