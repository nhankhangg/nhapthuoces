﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Volo.Abp.Domain.Entities;

namespace FRTTMO.CAM.PromotionAPI.Entities.QualifierConfigure;

public class QualifierConfigure : Entity<Guid>
{
    public string PromotionClass { get; set; }
    public string PromotionType { get; set; }
    public string QualifierCode { get; set; }
    public string OperatorCode { get; set; }
    public bool IsInput { get; set; }
    public bool IsOutput { get; set; }
    public bool IsCondition { get; set; }

    public List<string> GetListOperator()
    {
        if (OperatorCode.StartsWith("[") && OperatorCode.EndsWith("]"))
        {
            return JsonConvert.DeserializeObject<List<string>>(OperatorCode);
        }

        return new List<string>();
    }
}