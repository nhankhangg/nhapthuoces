﻿using System;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.Quota;

public interface IQuotaRepository : IRepository<Quota, Guid>
{
}