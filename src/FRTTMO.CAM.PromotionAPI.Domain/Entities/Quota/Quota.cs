﻿using System;
using Volo.Abp.Domain.Entities.Auditing;

namespace FRTTMO.CAM.PromotionAPI.Entities.Quota;

public class Quota : FullAuditedEntity<Guid>
{
    public string Type { get; set; }
    public int Quantity { get; set; }
    public int LimitQuantityShop { get; set; }
    public string ResetQuotaType { get; set; }
    public bool FlagQuantityPhone { get; set; }
    public int LimitQuantityPhone { get; set; }
    public bool FlagQuantityEmail { get; set; }
    public int LimitQuantityEmail { get; set; }

    public Guid PromotionId { get; set; }
    public string PromotionCode { get; set; }
}