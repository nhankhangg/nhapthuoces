﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace FRTTMO.CAM.PromotionAPI.Entities.QuotaHistories
{
    public interface IQuotaHistoriesRepository : IRepository<QuotaHistories, Guid>
    {
    }
}
