﻿using System;

namespace FRTTMO.CAM.PromotionAPI.Entities.QuotaHistories
{
    public class QuotaHistories : PromotionBaseEntity
    {
        public string PromotionCode { get; set; }
        public string ProvinceCode { get; set; }
        public string PhoneNumber { get; set; }
        public int Quantity { get; set; }
        public DateTime UseDateTime { get; set; }

    }
}
