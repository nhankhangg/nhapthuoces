﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace Customize.Logging
{
    public class HttpApiDelegatingHandler : DelegatingHandler, ITransientDependency
    {
        private readonly ILogger _logger;
        private readonly bool _isRequestResponseLoggingEnabled;
        private readonly string[] _urlIgnoreLoggings;
        private readonly bool _isIgnoreGetRequestRemoteService;

        public HttpApiDelegatingHandler(ILogger<HttpApiDelegatingHandler> logger, IConfiguration configuration)
        {
            _logger = logger;
            _isRequestResponseLoggingEnabled = configuration.GetValue("EnableRequestResponseLogging", false);
            _urlIgnoreLoggings = configuration.GetSection("UrlIgnoreLoggings").Get<string[]>() ?? new string[] { };
            _isIgnoreGetRequestRemoteService = configuration.GetValue("IsIgnoreGetRequestRemoteService", false);
        }

        protected override async Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            if (_isRequestResponseLoggingEnabled)
            {
                if (_isIgnoreGetRequestRemoteService)
                {
                    return await base.SendAsync(request, cancellationToken);
                }

                bool isLogging = true;
                if (_urlIgnoreLoggings.Length > 0)
                {
                    isLogging = !_urlIgnoreLoggings.Any(w => MatchWildcardString(w, request.RequestUri.LocalPath));
                }

                if (_urlIgnoreLoggings.Length == 0 || isLogging)
                {
                    var payloadRequest = request.Content != null
                        ? await request.Content.ReadAsStringAsync()
                        : string.Empty;
                    var startDate = DateTime.Now;
                    var response = await base.SendAsync(request, cancellationToken);
                    var endDate = DateTime.Now;
                    var payloadResponse = response.Content != null
                        ? await response.Content.ReadAsStringAsync()
                        : string.Empty;
                    _logger.LogInformation(
                        "HttpClient logger middleware {StartDate} {Host} {Path} {QueryString} {FullPath} {Method} {BodyRequest} {StatusCode} {BodyResponse} {ExecutionTime}",
                        startDate,
                        request.RequestUri.Scheme + "://" + request.RequestUri.Host,
                        request.RequestUri.LocalPath,
                        request.RequestUri.Query,
                        request.RequestUri.AbsoluteUri,
                        request.Method,
                        payloadRequest,
                        (int) response.StatusCode,
                        payloadResponse,
                        (endDate - startDate).TotalMilliseconds);
                    return response;
                }
                else
                {
                    return await base.SendAsync(request, cancellationToken);
                }
            }
            else
            {
                return await base.SendAsync(request, cancellationToken);
            }
        }

        private Boolean MatchWildcardString(String pattern, String input)
        {
            String regexPattern = "^";

            foreach (Char c in pattern)
            {
                switch (c)
                {
                    case '*':
                        regexPattern += ".*";
                        break;
                    case '?':
                        regexPattern += ".";
                        break;
                    default:
                        regexPattern += "[" + c + "]";
                        break;
                }
            }

            return new Regex(regexPattern + "$").IsMatch(input);
        }
    }
}