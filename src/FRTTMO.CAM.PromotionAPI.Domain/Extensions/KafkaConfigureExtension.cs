﻿//using System;
//using System.Net;
//using Confluent.Kafka;
//using FRTTMO.PromotionAPI.Handlers;
//using Microsoft.Extensions.Configuration;
//using Microsoft.Extensions.DependencyInjection;

//namespace FRTTMO.CAM.PromotionAPI.Extensions
//{
//    public static class KafkaConfigureExtension
//    {
//        public static void ConfigureKafkaEventBus(this IServiceCollection services, IConfiguration configuration)
//        {
//            var config = new ProducerConfig
//            {
//                BootstrapServers = configuration["Kafka:Connections:Default:BootstrapServers"],
//                ClientId = Dns.GetHostName(),
//                ApiVersionRequest = true
//            };

//            if (!configuration["Kafka:Connections:Default:Username"].IsNullOrEmpty())
//            {
//                config.SaslUsername = configuration["Kafka:Connections:Default:Username"];
//                config.SaslPassword = configuration["Kafka:Connections:Default:Password"];
//                config.SaslMechanism = SaslMechanism.Plain;
//                config.SecurityProtocol = SecurityProtocol.SaslPlaintext;
//            }

//            services.AddSingleton(_ => new ProducerBuilder<Null, string>(config).Build());

//            services.AddSingleton(_ => new ProducerBuilder<string, string>(config).Build());

//            services.AddSingleton(_ =>
//            {
//                var consumerConfig = new ConsumerConfig
//                {
//                    BootstrapServers = configuration["Kafka:Connections:Default:BootstrapServers"]
//                };

//                if (!configuration["Kafka:Connections:Default:Username"].IsNullOrEmpty())
//                {
//                    consumerConfig.SaslUsername = configuration["Kafka:Connections:Default:Username"];
//                    consumerConfig.SaslPassword = configuration["Kafka:Connections:Default:Password"];
//                    consumerConfig.SaslMechanism = SaslMechanism.Plain;
//                    consumerConfig.SecurityProtocol = SecurityProtocol.SaslPlaintext;
//                }

//                consumerConfig.GroupId = configuration["Kafka:EventBus:GroupId"];
//                consumerConfig.AutoOffsetReset = AutoOffsetReset.Earliest;
//                consumerConfig.AllowAutoCreateTopics = true;
//                consumerConfig.EnableAutoCommit = true;
//                consumerConfig.PartitionAssignmentStrategy = PartitionAssignmentStrategy.RoundRobin;
//                return consumerConfig;
//            });

//            services.AddHostedService<ConsumerHandler>();
//        }
//    }
//}
