﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Confluent.Kafka;
using FRTTMO.CAM.PromotionAPI.Etos;
using FRTTMO.CAM.PromotionAPI.ETOs.Sync;
using FRTTMO.CAM.PromotionAPI.Kafka;
using FRTTMO.CAM.PromotionAPI.Services;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace FRTTMO.PromotionAPI.Handlers;

public class ConsumerHandler : IHostedService
{
    private readonly ConsumerConfig _consumerConfig;
    private readonly ILogger<ConsumerHandler> _logger;
    private readonly ISyncDataEcomService _iSyncDataEcomService;
    public ConsumerHandler(
        ConsumerConfig consumerConfig,
        ILogger<ConsumerHandler> logger,
        ISyncDataEcomService iSyncDataEcomService
        )
    {
        _logger = logger;
        _consumerConfig = consumerConfig;
        _iSyncDataEcomService = iSyncDataEcomService;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        //Task.Factory.StartNew(async () =>
        //{
        //    using var consumer = new ConsumerBuilder<Ignore, string>(_consumerConfig).Build();
        //    consumer.Subscribe(KafkaTopics.PromotionChangePrice);
        //    while (!cancellationToken.IsCancellationRequested) 
        //    {
        //        try
        //        {
        //            var cr = consumer.Consume(cancellationToken);
        //            var promotionSyncEto = JsonConvert.DeserializeObject<PimChangePriceEto>(cr.Message.Value);

        //            if (promotionSyncEto != null)
        //            {
        //                await _iSyncDataEcomService.PimChangePriceAsync(promotionSyncEto);
        //            }

        //        }
        //        catch (Exception ex)
        //        {
        //            _logger.LogError(ex.Message);
        //        }
        //    }
        //}, cancellationToken);

        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }

}