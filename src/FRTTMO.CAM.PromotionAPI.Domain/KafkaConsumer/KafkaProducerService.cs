﻿//using System;
//using System.Text;
//using System.Threading.Tasks;
//using Confluent.Kafka;
//using Elastic.Apm.Api;
//using Newtonsoft.Json;
//using Volo.Abp.Domain.Services;
//using FRTTMO.CAM.PromotionAPI.KafkaConsumer.Interfaces;
//using Microsoft.Extensions.DependencyInjection;
//using Volo.Abp.DependencyInjection;

//namespace FRTTMO.CAM.PromotionAPI.KafkaConsumer;

//[Dependency(ServiceLifetime.Singleton, ReplaceServices = true)]
//public class KafkaProducerService : IKafkaProducerService
//{
//    private readonly IProducer<string, string> _producer;
//    private readonly ITracer _tracer;

//    public KafkaProducerService(
//        ITracer tracer, 
//        IProducer<string, string> producer)
//    {
//        _tracer = tracer;
//        _producer = producer;
//    }

//    //public async Task ProduceAsync(string topicName, object data, string keyMessage)
//    //{
//    //    var span = _tracer.CurrentTransaction?.StartSpan($"ProduceAsync ({topicName}) ",
//    //        ApiConstants.ActionExec,
//    //        ApiConstants.TypeExternal);
//    //    string key = DateTimeOffset.Now.ToUnixTimeMilliseconds().ToString();
//    //    var header = new Headers
//    //    {
//    //        { "cap-msg-name", Encoding.UTF8.GetBytes(topicName) },
//    //        { "cap-msg-type", Encoding.UTF8.GetBytes("BaseETO") },
//    //        { "cap-msg-id", Encoding.UTF8.GetBytes(key) },
//    //        { "cap-corr-id", Encoding.UTF8.GetBytes(key) },
//    //        { "cap-corr-seq", Encoding.UTF8.GetBytes("0") },
//    //        { "cap-callback-name", null },
//    //        { "cap-senttime", Encoding.UTF8.GetBytes(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff")) }
//    //    };
//    //    await _producer.ProduceAsync(topicName, new Message<string, string>
//    //    {
//    //        Key = key,
//    //        Value = JsonConvert.SerializeObject(data),
//    //        Headers = header
//    //    });

//    //    span?.End();
//    //}
//}