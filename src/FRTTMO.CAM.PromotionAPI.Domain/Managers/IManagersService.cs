﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FRTTMO.CAM.PromotionAPI.Enum;
using FRTTMO.CAM.PromotionAPI.Etos;
using FRTTMO.CAM.PromotionAPI.ETOs;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;
using Hangfire;
using Volo.Abp.Domain.Services;

namespace FRTTMO.CAM.PromotionAPI.Managers
{
    public interface IManagersService : IDomainService
    {
        [AutomaticRetry(Attempts = 0)]
        Task UpSetConditionCachingAsync(EventConditionEto dataCache);
        [AutomaticRetry(Attempts = 0)]
        Task UpSetHeaderCachingAsync(EventPromotionEto dataCache);

        #region Hangfire sub

        //Mua sản phẩm được tặng sản phẩm
        [AutomaticRetry(Attempts = 0)]
        [JobDisplayName("GetItemOutputCachingAsync:{0}")]
        Task GetItemOutputCachingAsync(string promotionCode, EventPromotionEto dataCache);

        // Mua sản phẩm giảm giá
        [AutomaticRetry(Attempts = 0)]
        [JobDisplayName("DiscountItemOutputCachingAsync:{0}")]
        Task DiscountItemOutputCachingAsync(string promotionCode, EventPromotionEto dataCache);

        //Càng mua càng rẻ
        [AutomaticRetry(Attempts = 0)]
        [JobDisplayName("GetAscendingDiscountCachingAsync:{0}")]
        Task GetAscendingDiscountCachingAsync(string promotionCode, EventPromotionEto dataCache);

        //Tổng tiền đơn hàng
        [AutomaticRetry(Attempts = 0)]
        [JobDisplayName("TotalBillAmountCachingAsync:{0}")]
        Task TotalBillAmountCachingAsync(string promotionCode, EventPromotionEto dataCache);

        //Tổng tiền sản phẩm
        [AutomaticRetry(Attempts = 0)]
        [JobDisplayName("TotalProductAmountCachingAsync:{0}")]
        Task TotalProductAmountCachingAsync(string promotionCode, EventPromotionEto dataCache);

        //phuong thuc ap dung
        [AutomaticRetry(Attempts = 0)]
        [JobDisplayName("PaymenMethodCachingAsync:{0}")]
        Task PaymenMethodCachingAsync(string promotionCode, EventPromotionEto dataCache);

        #endregion

        #region Event cache managers

        Task DefinePimItemCodeInputAsync(EventInputCacheEto eventData);

        //Task DefinePimItemCodeOutputAsync(EventOutputCacheEto eventData);

        #endregion
        
        [AutomaticRetry(Attempts = 0)]
        Task TimeExpireCacheAsync(List<PromotionTimeExpireEto> promotionTimeExpire, bool isReCache = false);
        
        [AutomaticRetry(Attempts = 0)]
        [JobDisplayName("ClearProvinceConditionAsync:{1}")]
        Task ClearProvinceConditionAsync(List<string> shopCodes, string promotionCode);
    }
}
