﻿using System;
using System.Linq;
using Volo.Abp.Guids;
using Elastic.Apm.Api;
using Newtonsoft.Json;
using StackExchange.Redis;
using Volo.Abp.ObjectMapping;
using System.Threading.Tasks;
using Volo.Abp.Domain.Services;
using Microsoft.Extensions.Configuration;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;
using System.Collections.Generic;
using FRTTMO.CAM.PromotionAPI.DTOs.PIM;
using FRTTMO.CAM.PromotionAPI.Enum;
using FRTTMO.CAM.PromotionAPI.ETOs;
using FRTTMO.CAM.PromotionAPI.ETOs.Cache;
using FRTTMO.CAM.PromotionAPI.Services;
using FRTTMO.CAM.PromotionAPI.ExceptionCodes;
using FRTTMO.CAM.PromotionAPI.Entities.OutputIntegration;
using FRTTMO.CAM.PromotionAPI.Entities.InputCacheItem;
using FRTTMO.CAM.PromotionAPI.Entities.InputCacheItemReplace;
using FRTTMO.CAM.PromotionAPI.Documents.PromotionInputReplaces;
using FRTTMO.CAM.PromotionAPI.Documents.PromotionOutputReplaces;
using FRTTMO.CAM.PromotionAPI.Entities.Output;
using FRTTMO.CAM.PromotionAPI.Constants;
using FRTTMO.CAM.PromotionAPI.Etos;
using FRTTMO.CAM.PromotionAPI.Kafka;
using FRTTMO.CAM.PromotionAPI.Entities.Promotion;

namespace FRTTMO.CAM.PromotionAPI.Managers
{
    public class ManagersService : DomainService, IManagersService
    {
        private readonly ITracer _tracer;
        private readonly IObjectMapper _objectMapper;
        private readonly IConfiguration _configuration;
        private readonly IDatabase _databaseRedisCache;
        private readonly IGuidGenerator _guidGenerator;
        private readonly IPIMAppService _iPimAppService;
        //private readonly IKafkaProducerService _kafkaProducerService;
        //private readonly ISyncDataEcomService _syncDataEcomService;
        private readonly ICommonAppService _commonAppService;
        private readonly IInputCacheItemRepository _inputCacheItemRepository;
        private readonly IGetItemOutputRepository _getItemOutputRepository;
        private readonly IDiscountItemOutputRepository _discountItemOutputRepository;
        private readonly IInputCacheItemReplaceRepository _inputCacheItemReplaceRepository;
        private readonly IGetAscendingDiscountOutPutRepository _getAscendingDiscountOutPutRepository;
        private readonly IPaymentMethodOutputRepository _paymentMethodOutputRepository;
        private readonly IElasticSearchAppService _elasticSearchAppService;
        private static readonly string _keyEror = "PromotionError";
        private static readonly string _keyOutput = "Output";
        private static readonly string _keyOutputInProcess = "Output_InProcess";
        private static readonly string _keyOutputReplace = "OutputReplace";
        private static readonly string _keyOutputReplaceInProcess = "OutputReplace_InProcess";
        private static readonly string _keyOutputReplaceDeleteSuccess = "OutputReplace_DeleteSuccess";
        public ManagersService(
            ITracer tracer,
            IObjectMapper objectMapper,
            IConfiguration configuration,
            IGuidGenerator guidGenerator,
            IDatabase databaseRedisCache,
            IPIMAppService iPimAppService,
            IGetItemOutputRepository getItemOutputRepository,
            IInputCacheItemRepository inputCacheItemRepository,
            ICommonAppService commonAppService,
            IDiscountItemOutputRepository discountItemOutputRepository,
            IInputCacheItemReplaceRepository inputCacheItemReplaceRepository,
            IGetAscendingDiscountOutPutRepository getAscendingDiscountOutPutRepository,
            IPaymentMethodOutputRepository paymentMethodOutputRepository,
            //ISyncDataEcomService syncDataEcomService,
            IElasticSearchAppService elasticSearchAppService
            )
        {
            _tracer = tracer;
            _objectMapper = objectMapper;
            _configuration = configuration;
            _guidGenerator = guidGenerator;
            _iPimAppService = iPimAppService;
            _databaseRedisCache = databaseRedisCache;
            _getItemOutputRepository = getItemOutputRepository;
            _inputCacheItemRepository = inputCacheItemRepository;
            _commonAppService = commonAppService;
            _discountItemOutputRepository = discountItemOutputRepository;
            _inputCacheItemReplaceRepository = inputCacheItemReplaceRepository;
            _getAscendingDiscountOutPutRepository = getAscendingDiscountOutPutRepository;
            _paymentMethodOutputRepository = paymentMethodOutputRepository;
            _elasticSearchAppService = elasticSearchAppService;
            //_syncDataEcomService = syncDataEcomService;
        }
        
        
        /// <summary>
        /// đổi tên key redis cho key outPut
        /// </summary>
        /// <param name="keyOutputInProcess"></param>
        /// <returns></returns>
        private async Task KeyRenameCacheOutPutAsync(IEnumerable<string> keyOutputInProcess)
        {
            foreach (var oldKeyInProcess in keyOutputInProcess)
            {
                var newkey = oldKeyInProcess.Replace(_keyOutputInProcess, _keyOutput);
                await _databaseRedisCache.KeyDeleteAsync(newkey, CommandFlags.FireAndForget);
                await _databaseRedisCache.KeyRenameAsync(oldKeyInProcess, newkey, (When)CommandFlags.FireAndForget);
            }
        }

        /// <summary>
        /// đổi tên key redis cho key outputReplace
        /// </summary>
        /// <param name="keyOutputInProcess"></param>
        /// <returns></returns>
        private async Task KeyRenameCacheOutputReplaceAsync(IEnumerable<string> keyOutputReplaceInProcess, string promotionCode)
        {
            var keyOutputReplaceDeleteSuccess = $"{_keyOutputReplaceDeleteSuccess}:{promotionCode}";
            var valueOutputReplaceDeleteSuccess = await _databaseRedisCache.StringGetAsync(keyOutputReplaceDeleteSuccess);
            if (valueOutputReplaceDeleteSuccess.HasValue)
            {
                var outputReplace = JsonConvert.DeserializeObject<List<string>>(valueOutputReplaceDeleteSuccess.ToString());
                outputReplace.AddFirst(keyOutputReplaceDeleteSuccess);
                var redisValuePromotions = outputReplace.Select(key => new RedisKey(key)).ToArray();
                await _databaseRedisCache.KeyDeleteAsync(redisValuePromotions, CommandFlags.FireAndForget);

            }
            foreach (var oldKeyInProcess in keyOutputReplaceInProcess)
            {
                var newkey = oldKeyInProcess.Replace(_keyOutputReplaceInProcess, _keyOutputReplace);
                await _databaseRedisCache.KeyDeleteAsync(newkey, CommandFlags.FireAndForget);
                if (await _databaseRedisCache.KeyExistsAsync(oldKeyInProcess))
                {
                    await _databaseRedisCache.KeyRenameAsync(oldKeyInProcess, newkey, (When)CommandFlags.FireAndForget);
                }
            }
        }

        #region Switch promotion from hangfire

        private async Task UpSetPromotionErrorCachingAsync(string promotionCode, string message)
        {
            await _databaseRedisCache.HashSetAsync(_keyEror, new HashEntry[]
            {
               new(promotionCode, $"{message}")
            }, CommandFlags.FireAndForget);
        }
        
        [Obsolete("Obsolete")]
        public async Task UpSetHeaderCachingAsync(EventPromotionEto dataCache)
        {
            var transactionTracer = _tracer?.StartTransaction($"UpSetHeaderCachingAsync", ApiConstants.TypeRequest);
            var span = _tracer?.CurrentTransaction?.StartSpan($"{dataCache.HeaderCache.Code} : {dataCache.HeaderCache.PromotionType}", ApiConstants.ActionExec);
            try
            {
                if (dataCache.ActionType != ActionType.UpdateHeader)
                {
                    await CommonClearDataAndCachingHeaderAsync(dataCache.HeaderCache,false);
                }

                if (dataCache.ActionType == ActionType.UpdateHeader)
                {
                    await CommonCachingHeaderPromotionExcludeAsync(dataCache.HeaderCache);
                }
            }
            catch (Exception ex)
            {
                transactionTracer?.CaptureException(ex);
                await UpSetPromotionErrorCachingAsync(dataCache.HeaderCache.Code, ex.Message);
                throw new Exception(message: ex.Message);
            }
            finally
            {
                span?.End();
                transactionTracer?.End();
            }
        }

        [Obsolete("Obsolete")]
        public async Task UpSetConditionCachingAsync(EventConditionEto conditionCache)
        {
            var transactionTracer = _tracer?.StartTransaction($"UpSetConditionCachingAsync", ApiConstants.TypeRequest);
            var span = _tracer?.CurrentTransaction?.StartSpan($"{conditionCache.PromotionCode}", ApiConstants.ActionExec);
            try
            {
                foreach (var provinceCondition in conditionCache.ProvinceConditions)
                {
                    var keyPrefixCondition = GetKeyPrefixCaching(provinceCondition.ProvinceCode, "ProvinceCondition");

                    var inputRedisValue = new List<HashEntry>
                    {
                        new($"{conditionCache.PromotionCode}", JsonConvert.SerializeObject(new CacheValueProvinceCondition
                        {
                            FromDate = provinceCondition.FromDate,
                            ToDate = provinceCondition.ToDate
                        }))
                    };

                    await _databaseRedisCache.HashSetAsync(keyPrefixCondition, inputRedisValue.ToArray(), CommandFlags.FireAndForget);
                }
            }
            catch (Exception ex)
            {
                transactionTracer?.CaptureException(ex);
                await UpSetPromotionErrorCachingAsync(conditionCache.PromotionCode, ex.Message);
                throw new Exception(message: ex.Message);
            }
            finally
            {
                span?.End();
                transactionTracer?.End();
            }
        }

        [Obsolete("Obsolete")]
        public async Task ClearProvinceConditionAsync(List<string> provinceCodes, string promotionCode)
        {
            foreach (var keyProvinceCondition in provinceCodes.Select(province => GetKeyPrefixCaching(province, "ProvinceCondition")))
            {
                await _databaseRedisCache.HashDeleteAsync(keyProvinceCondition, promotionCode, CommandFlags.FireAndForget);
            }
        }

        #endregion

        #region Mua sản phẩm tặng sản phẩm. Mua Combo sản phẩm tặng sản phẩm

        /// <summary>
        ///  Mua sản phẩm tặng sản phẩm. 
        ///  Mua Combo sản phẩm tặng sản phẩm.
        /// </summary>
        /// <param name="getProductOutPut"></param>
        /// <returns></returns>
        public async Task GetItemOutputCachingAsync(string promotionCode,EventPromotionEto getProductOutPut)
        {
            var transactionTracer = _tracer?.StartTransaction($"GetItemOutputCachingAsync", ApiConstants.TypeRequest);
            var span = _tracer?.CurrentTransaction?.StartSpan($"{promotionCode} : {getProductOutPut.HeaderCache.PromotionType}", ApiConstants.ActionExec);

            try
            {
                await PromotionLockByDefineInputAsync(getProductOutPut);

                await CommonClearDataAndCachingHeaderAsync(getProductOutPut.HeaderCache);

                if (getProductOutPut.OutputCache is { Count: > 0 })
                {
                    await OutPutCacheGetProductAsync(getProductOutPut);
                }

                if (getProductOutPut.InputCache is { Count: > 0 })
                {
                    await CommonCachingInputAsync(getProductOutPut);
                }

            }
            catch (Exception ex)
            {
                transactionTracer?.CaptureException(ex);
                await UpSetPromotionErrorCachingAsync(getProductOutPut.HeaderCache.Code, ex.Message);
                throw new Exception(message: ex.Message);
            }
            finally
            {
                span?.End();
                transactionTracer?.End();
            }
            await _databaseRedisCache.KeyDeleteAsync($"SyncPromotion:{promotionCode}:Lock", CommandFlags.FireAndForget);
        }

        /// <summary>
        /// Out put chỉ tang sản phẩm (Không cho tặng ngành hàng(20/06/2023))
        /// điều kiện tặng cả (And và Or)
        /// 1/ Tặng sản phẩm (GetItemCode) => điều kiện And, Or
        /// 2/ Tặng Mã ưu đãi (GetGiftCard) => điều kiện And
        /// 3/ Tặng phiêu mua hàng (GetCoupon) => điều kiện And
        /// </summary>
        /// <param name="getProductOutPut"></param>
        /// <returns></returns>
        private async Task OutPutCacheGetProductAsync(EventPromotionEto getProductOutPut)
        {
            var keyOutputInProcess = new List<string>();
            var keyOutputReplaceInProcess = new List<string>();
            var hashEntryLists = new List<HashEntry>();
            var getItemOutputEntity = new List<GetItemOutput>();
            await PromotionLockAndDefineCacheOutPutAsync(getProductOutPut);
            foreach (var outPut in getProductOutPut.OutputCache)
            {
                if (System.Enum.TryParse(getProductOutPut.HeaderCache.PromotionType, out PromotionTypeEnum promotionType))
                {
                    if (promotionType != PromotionTypeEnum.BuyProductGetProduct
                     && promotionType != PromotionTypeEnum.UsePaymentMethod)
                    {
                        //bắn event sync data to ecom
                        await EventSyncDataToEcomAsync(outPut);
                    }
                }

                if (System.Enum.TryParse(outPut.QualifierCode, out QualifierTypeEnum qualifierCode))
                {
                    var keyOutput = $"{GetKeyPrefixCaching(outPut.PromotionCode, _keyOutputInProcess)}:{outPut.PromotionType}";
                    switch (qualifierCode)
                    {
                        case QualifierTypeEnum.GetItemCode:
                            //cache Output 
                            var dataGetItemCode = _objectMapper.Map(outPut, new CacheValueGetItemOutPut());
                            dataGetItemCode.QuantityReplace = outPut.OutputReplaceCaches.Count(p => !p.ItemCode.IsNullOrEmpty());
                            dataGetItemCode.QuantityReplaceVoucher = outPut.OutputReplaceCaches.Count(p => !p.CouponCode.IsNullOrEmpty());
                            hashEntryLists.Add(new HashEntry(outPut.ItemCode, JsonConvert.SerializeObject(dataGetItemCode)));

                            //database insert Output
                            getItemOutputEntity.Add(_objectMapper.Map(outPut, new GetItemOutput(_guidGenerator.Create())));
                            if (outPut.OutputReplaceCaches is { Count: > 0 })
                            {
                                var hashEntryOutputReplaceCaches = new List<HashEntry>();
                                var keyOutputReplace = $"{GetKeyPrefixCaching(outPut.PromotionCode, _keyOutputReplaceInProcess)}:{outPut.ItemCode}";
                                //cache replace điều kiện or chỉ có ở qualifierCode GetItemCode
                                foreach (var outPutReplace in outPut.OutputReplaceCaches)
                                {
                                    //cache Output Replace 
                                    if (!outPutReplace.CouponCode.IsNullOrEmpty())
                                    {
                                        var outputGetCouponReplace = await BuildVoucherOutPut(outPutReplace, outPutReplace.CouponCode, QualifierTypeEnum.GetCoupon);
                                        var getCouponOutputReplace = _objectMapper.Map(outputGetCouponReplace, new CacheValueGetVoucherOutPut());
                                        hashEntryOutputReplaceCaches.Add(new HashEntry(outPutReplace.CouponCode, JsonConvert.SerializeObject(getCouponOutputReplace)));
                                    }
                                    else if (!outPutReplace.ItemCode.IsNullOrEmpty())
                                    {
                                        var dataGetItemCodeReplace = _objectMapper.Map(outPutReplace, new CacheValueGetItemOutPut());
                                        hashEntryOutputReplaceCaches.Add(new HashEntry(outPutReplace.ItemCode, JsonConvert.SerializeObject(dataGetItemCodeReplace)));
                                    }
                                    //database insert Output Replace 
                                    var outputReplaceEntity = _objectMapper.Map(outPutReplace, new GetItemOutput(_guidGenerator.Create()));
                                    outputReplaceEntity.OutPutType = "OutputReplace";
                                    getItemOutputEntity.Add(outputReplaceEntity);


                                    if (promotionType != PromotionTypeEnum.BuyProductGetProduct
                                     && promotionType != PromotionTypeEnum.UsePaymentMethod)
                                    {
                                        //bắn event sync data to ecom
                                        await EventSyncDataToEcomAsync(outPutReplace);
                                    }

                                    await CreateUpdateOutPutReplacesElacticAsync(new List<ProductCache>(), outPutReplace);
                                }

                                await _databaseRedisCache.HashSetAsync(keyOutputReplace, hashEntryOutputReplaceCaches.ToArray(), CommandFlags.FireAndForget);
                                keyOutputReplaceInProcess.Add(keyOutputReplace);
                            }
                            break;
                        case QualifierTypeEnum.GetCoupon:
                            //cache Output 
                            var outputGetCoupon = await BuildVoucherOutPut(outPut, outPut.CouponCode, QualifierTypeEnum.GetCoupon);
                            var getCouponOutput = _objectMapper.Map(outputGetCoupon, new CacheValueGetVoucherOutPut());
                            hashEntryLists.Add(new HashEntry(outPut.CouponCode, JsonConvert.SerializeObject(getCouponOutput)));
                            //database insert Output
                            getItemOutputEntity.Add(_objectMapper.Map(outputGetCoupon, new GetItemOutput(_guidGenerator.Create())));
                            keyOutputReplaceInProcess.Add($"{GetKeyPrefixCaching(outPut.PromotionCode, _keyOutputReplaceInProcess)}:{outPut.CouponCode}");
                            break;

                        case QualifierTypeEnum.GetGiftCard:
                            //cache Output 
                            var outputGetGiftCard = await BuildVoucherOutPut(outPut, outPut.GiftCode, QualifierTypeEnum.GetGiftCard);
                            var getGiftCardOutputCache = _objectMapper.Map(outputGetGiftCard, new CacheValueGetVoucherOutPut());
                            hashEntryLists.Add(new HashEntry(outPut.GiftCode, JsonConvert.SerializeObject(getGiftCardOutputCache)));
                            //database insert Output
                            getItemOutputEntity.Add(_objectMapper.Map(outputGetGiftCard, new GetItemOutput(_guidGenerator.Create())));
                            keyOutputReplaceInProcess.Add($"{GetKeyPrefixCaching(outPut.PromotionCode, _keyOutputReplaceInProcess)}:{outPut.GiftCode}");
                            break;
                    }
                    //cache output 
                    await _databaseRedisCache.HashSetAsync(keyOutput, hashEntryLists.ToArray(), CommandFlags.FireAndForget);
                    //key Out put in process tmp
                    keyOutputInProcess.Add(keyOutput);
                }

                await CreateUpdateOutPutReplacesElacticAsync(new List<ProductCache>(), outPut);
            }

            if (getItemOutputEntity is { Count: > 0 })
            {
                await _getItemOutputRepository.BulkCopyAsync(getItemOutputEntity);
            }

            if (keyOutputInProcess is { Count: > 0 })
            {
                await KeyRenameCacheOutPutAsync(keyOutputInProcess.Distinct());
            }

            await KeyRenameCacheOutputReplaceAsync(keyOutputReplaceInProcess.Distinct(), getProductOutPut.HeaderCache.Code);
            
        }

        #endregion

        #region Mua sản phẩm giảm giá sản phẩm. Mua combo giảm giá sản phẩm.

        /// <summary>
        ///  Mua sản phẩm giảm giá sản phẩm. 
        ///  Mua combo giảm giá sản phẩm.
        /// </summary>
        /// <param name="dataCache"></param>
        /// <returns></returns>
        public async Task DiscountItemOutputCachingAsync(string promotionCode, EventPromotionEto dataCache)
        {
            var transactionTracer = _tracer?.StartTransaction($"DiscountItemOutputCachingAsync", ApiConstants.TypeRequest);
            var span = _tracer?.CurrentTransaction?.StartSpan($"{promotionCode} : {dataCache.HeaderCache.PromotionType}", ApiConstants.ActionExec);
            try
            {
                await PromotionLockByDefineInputAsync(dataCache);

                await CommonClearDataAndCachingHeaderAsync(dataCache.HeaderCache);

                if (dataCache.InputCache is { Count: > 0 })
                {
                    await CommonCachingInputAsync(dataCache);
                }

                if (dataCache.OutputCache is { Count: > 0 })
                {
                    await OutPutCacheGetDiscountAsync(dataCache);
                }
            }
            catch (Exception ex)
            {
                transactionTracer?.CaptureException(ex);
                await UpSetPromotionErrorCachingAsync(dataCache.HeaderCache.Code, ex.Message);
                throw new Exception(message: ex.Message);
            }
            finally
            {
                span?.End();
                transactionTracer?.End();
            }
            await _databaseRedisCache.KeyDeleteAsync($"SyncPromotion:{promotionCode}:Lock", CommandFlags.FireAndForget);
        }

        /// <summary>
        /// Output định nghĩa giảm sản phẩm hoặc giảm ngành hàng (rã item output) (20/06/2023)
        /// Điều kiện tặng chỉ có thể định nghĩa điều kiện Or
        /// 1/ Giảm số tiền sản phẩm (ProductAmountOff) => điều kiện Or
        /// 2/ Giảm số tiền cố định sản phẩm (ProductFixedPrice) => điều kiện Or
        /// 3/ Giảm phần trăm theo sản phẩm (ProductPercentOff) => điều kiện Or
        /// </summary>
        /// <param name="getDiscountOutPut"></param>
        /// <returns></returns>
        private async Task OutPutCacheGetDiscountAsync(EventPromotionEto getDiscountOutPut)
        {
            var itemCodeOutPut = string.Empty;
            int? itemUnitCodeOutput = 0;
            string itemUnitNameOutput = string.Empty;
            var hashOutputLists = new List<HashEntry>();
            var keyOutputInProcess = new List<string>();
            var keyOutputReplaceInProcess = new List<string>();
            var discountItemOutputsEntity = new List<DiscountItemOutput>();
            await PromotionLockAndDefineCacheOutPutAsync(getDiscountOutPut);
            foreach (var outPut in getDiscountOutPut.OutputCache)
            {
                //cache output schem giảm giá chí có 1 out put, còn lại toàn bộ output replace
                if (outPut.ItemCode.IsNullOrEmpty())
                {
                    //cache replace từ out put thuộc ngành hàng
                    var pimCacheSearch = _objectMapper.Map(outPut, new PimCacheSearch());
                    var pimProductCache = await _iPimAppService.GetListProductByPimCacheAsync(pimCacheSearch);
                    if (outPut.UnitCode == 1)
                    {
                        pimProductCache.Data.ForEach(a =>
                        {
                            var mesureMax = a.Measures.FirstOrDefault(b => b.Level == a.Measures.Min(c => c.Level));
                            a.Measures.RemoveAll(a => a.Level != mesureMax.Level);
                        });
                    }

                    if (outPut.UnitCode == 2)
                    {
                        pimProductCache.Data.ForEach(a =>
                        {
                            var mesureMax = a.Measures.FirstOrDefault(b => b.Level == a.Measures.Max(c => c.Level));
                            a.Measures.RemoveAll(a => a.Level != mesureMax.Level);
                        });
                    }
                    itemUnitCodeOutput = pimProductCache?.Data?.FirstOrDefault()?.Measures.FirstOrDefault()?.MeasureUnitId;
                    itemUnitNameOutput = pimProductCache?.Data?.FirstOrDefault()?.Measures.FirstOrDefault()?.MeasureUnitName;
                    itemCodeOutPut = pimProductCache?.Data?.FirstOrDefault()?.ItemCode;
                    if (itemCodeOutPut.IsNullOrEmpty())
                    {
                        if (pimProductCache.Total == 0)
                        {
                            foreach (var item in outPut.OutputReplaceCaches)
                            {
                                pimCacheSearch = _objectMapper.Map(item, new PimCacheSearch());
                                pimProductCache = await _iPimAppService.GetListProductByPimCacheAsync(pimCacheSearch);
                                
                                if (pimProductCache.Total > 0)
                                {
                                    itemCodeOutPut = pimProductCache?.Data?.FirstOrDefault()?.ItemCode;
                                    outPut.OutputReplaceCaches.Add(outPut);
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        outPut.OutputReplaceCaches.Add(outPut);
                    }
                }
                else
                {
                    itemCodeOutPut = outPut.ItemCode;
                }
                var disCountOutputProduct = _objectMapper.Map(outPut, new CacheValueDiscountOutPut());
                if (outPut.OutputReplaceCaches is { Count: > 0 })
                {
                    //cache replace điều kiện or và của output chính thuộc ngành hàng
                    foreach (var outputReplace in outPut.OutputReplaceCaches)
                    {
                        var keyOutputReplace = $"{GetKeyPrefixCaching(outPut.PromotionCode, _keyOutputReplaceInProcess)}:{itemCodeOutPut}";
                        if (!outputReplace.ItemCode.IsNullOrEmpty())
                        {
                            //Hash set outPut replace
                            var hashOutputReplaceLists = new List<HashEntry>();
                            var disCountOutputReplace = _objectMapper.Map(outputReplace, new CacheValueDiscountOutPut());
                            hashOutputReplaceLists.Add(new HashEntry(outputReplace.ItemCode, JsonConvert.SerializeObject(disCountOutputReplace)));
                            await _databaseRedisCache.HashSetAsync(keyOutputReplace, hashOutputReplaceLists.ToArray(), CommandFlags.FireAndForget);
                            disCountOutputProduct.QuantityReplace++;

                            //insert database
                            var outputReplaceEntity = _objectMapper.Map(outputReplace, new DiscountItemOutput(_guidGenerator.Create()));
                            outputReplaceEntity.OutPutType = "OutputReplace";
                            discountItemOutputsEntity.Add(outputReplaceEntity);
                            if (getDiscountOutPut.HeaderCache.ItemInputExcludes is { Length: > 0 })
                            {
                                if (!getDiscountOutPut.HeaderCache.ItemInputExcludes.Any(p => p.Equals(outputReplace.ItemCode)))// thuộc loại trừ sản phẩm ko sync
                                {
                                    //bắn event sync data to ecom
                                    await EventSyncDataToEcomAsync(outputReplace);
                                }
                            }
                            else
                            {
                                await EventSyncDataToEcomAsync(outputReplace);
                            }

                            await CreateUpdateOutPutReplacesElacticAsync(new List<ProductCache>(), outputReplace);
                        }
                        else
                        {

                            var pimCacheSearch = _objectMapper.Map(outputReplace, new PimCacheSearch());
                            var dataCount = await _iPimAppService.CountDataPimCacheAsync(pimCacheSearch);
                            if (dataCount.Total > 2000)
                            {
                                pimCacheSearch.IsScroll = true;
                            }
                            const int scrollSize = 2000;
                            var scrollId = string.Empty;

                            for (int i = 0; i <= dataCount.Total; i += scrollSize)
                            {
                                pimCacheSearch.ScrollId = scrollId;
                                pimCacheSearch.ScrollSize = scrollSize;
                                var pimProductCache = await _iPimAppService.GetListProductByPimCacheAsync(pimCacheSearch);
                                // remove all product thuộc sản phẩm loại trừ
                                if (getDiscountOutPut.HeaderCache.ItemInputExcludes is { Length: > 0 })
                                {
                                    pimProductCache.Data.RemoveAll(p => getDiscountOutPut.HeaderCache.ItemInputExcludes.Contains(p.ItemCode));
                                }


                                if (outPut.UnitCode == 1)
                                {
                                    pimProductCache.Data.ForEach(a =>
                                    {
                                        var mesureMax = a.Measures.FirstOrDefault(b => b.Level == a.Measures.Min(c => c.Level));
                                        a.Measures.RemoveAll(a => a.Level != mesureMax.Level);
                                    });
                                }

                                if (outPut.UnitCode == 2)
                                {
                                    pimProductCache.Data.ForEach(a =>
                                    {
                                        var mesureMax = a.Measures.FirstOrDefault(b => b.Level == a.Measures.Max(c => c.Level));
                                        a.Measures.RemoveAll(a => a.Level != mesureMax.Level);
                                    });
                                }

                                if (pimProductCache.Data is { Count: > 0 })
                                {
                                    foreach (var product in pimProductCache.Data)
                                    {
                                        if (itemCodeOutPut != product.ItemCode)
                                        {
                                            //Hash set outPut replace
                                            var hashOutputReplaceLists = new List<HashEntry>();
                                            outputReplace.ItemCode = product.ItemCode;
                                            outputReplace.UnitCode = product.Measures?.FirstOrDefault()?.MeasureUnitId;
                                            outputReplace.UnitName = product.Measures?.FirstOrDefault()?.MeasureUnitName;
                                            var disCountOutputReplace = _objectMapper.Map(outputReplace, new CacheValueDiscountOutPut());
                                            hashOutputReplaceLists.Add(new HashEntry(product.ItemCode, JsonConvert.SerializeObject(disCountOutputReplace)));
                                            await _databaseRedisCache.HashSetAsync(keyOutputReplace, hashOutputReplaceLists.ToArray(), CommandFlags.FireAndForget);

                                            //insert database
                                            var outputReplaceEntity = _objectMapper.Map(outputReplace, new DiscountItemOutput(_guidGenerator.Create()));
                                            outputReplaceEntity.OutPutType = "OutputReplace";
                                            discountItemOutputsEntity.Add(outputReplaceEntity);
                                        }
                                    }

                                    //bắn event sync data to ecom
                                    pimProductCache.Data.RemoveAll(p => p.ItemCode == itemCodeOutPut);
                                    await CreateUpdateOutPutReplacesElacticAsync(pimProductCache.Data, outputReplace);
                                    await EventSyncDataToEcomAsync(outputReplace, pimProductCache);
                                    disCountOutputProduct.QuantityReplace += pimProductCache.Data.Count;
                                }

                                scrollId = pimProductCache.ScrollId;
                            }
                        }
                        keyOutputReplaceInProcess.Add(keyOutputReplace);
                    }
                }
                outPut.ItemCode = itemCodeOutPut;
                disCountOutputProduct.ItemCode = itemCodeOutPut;
                disCountOutputProduct.UnitCode = itemUnitCodeOutput;
                disCountOutputProduct.UnitName = itemUnitNameOutput;
                await CreateUpdateOutPutReplacesElacticAsync(new List<ProductCache>(), outPut);

                if (getDiscountOutPut.HeaderCache.ItemInputExcludes is { Length: > 0 })
                {
                    if (!getDiscountOutPut.HeaderCache.ItemInputExcludes.Any(p => p.Equals(outPut.ItemCode)))// thuộc loại trừ sản phẩm ko sync
                    {
                        //bắn event sync data to
                        await EventSyncDataToEcomAsync(outPut);
                    }
                }
                else
                {
                    await EventSyncDataToEcomAsync(outPut);
                }

                // cache output chính
                var keyOutput = $"{GetKeyPrefixCaching(outPut.PromotionCode, _keyOutputInProcess)}:{outPut.PromotionType}";

                hashOutputLists.Add(new HashEntry(itemCodeOutPut, JsonConvert.SerializeObject(disCountOutputProduct)));
                await _databaseRedisCache.HashSetAsync(keyOutput, hashOutputLists.ToArray(), CommandFlags.FireAndForget);

                //database insert output
                discountItemOutputsEntity.Add(_objectMapper.Map(outPut, new DiscountItemOutput(_guidGenerator.Create())));

                //key Out put in process tmp
                keyOutputInProcess.Add(keyOutput);
            }

            if (discountItemOutputsEntity is { Count: > 0 })
            {
                await _discountItemOutputRepository.BulkCopyAsync(discountItemOutputsEntity);
            }

            if (keyOutputInProcess is { Count: > 0 })
            {
                await KeyRenameCacheOutPutAsync(keyOutputInProcess.Distinct());
            }
           
            await KeyRenameCacheOutputReplaceAsync(keyOutputReplaceInProcess.Distinct(), getDiscountOutPut.HeaderCache.Code);
            
        }

        #endregion

        #region Càng mua càng rẽ

        /// <summary>
        ///  Càng mua càng rẻ giảm giá. 
        /// </summary>
        /// <param name="dataCache"></param>
        /// <returns></returns>
        public async Task GetAscendingDiscountCachingAsync(string promotionCode,EventPromotionEto dataCache)
        {
            var transactionTracer = _tracer?.StartTransaction($"GetAscendingDiscountCachingAsync ", ApiConstants.TypeRequest);
            var span = _tracer?.CurrentTransaction?.StartSpan($"{promotionCode} : {dataCache.HeaderCache.PromotionType}", ApiConstants.ActionExec);
            try
            {
                if (dataCache != null)
                {
                    await PromotionLockByDefineInputAsync(dataCache);

                    await CommonClearDataAndCachingHeaderAsync(dataCache.HeaderCache);

                    if (dataCache.InputCache is { Count: > 0 })
                    {
                        await CommonCachingInputAsync(dataCache);
                    }

                    if (dataCache.OutputCache is { Count: > 0 })
                    {
                        await OutputGetAscendingDiscountAsync(dataCache);
                    }
                }
            }
            catch (Exception ex)
            {
                transactionTracer?.CaptureException(ex);
                await UpSetPromotionErrorCachingAsync(dataCache.HeaderCache.Code, ex.Message);
                throw new Exception(message: ex.Message);
            }
            finally
            {
                span?.End();
                transactionTracer?.End();
            }
            await _databaseRedisCache.KeyDeleteAsync($"SyncPromotion:{promotionCode}:Lock", CommandFlags.FireAndForget);
        }
        /// <summary>
        /// Càng mua càng rẽ out put
        /// </summary>
        /// <param name="eventPromotion"></param>
        /// <returns></returns>
        public async Task OutputGetAscendingDiscountAsync(EventPromotionEto eventPromotion)
        {
            var keyGetAscendingDiscount = GetKeyPrefixCaching(eventPromotion.HeaderCache.Code, "Output");
            keyGetAscendingDiscount = $"{keyGetAscendingDiscount}:{eventPromotion.HeaderCache.PromotionType}";

            await _databaseRedisCache.KeyDeleteAsync(keyGetAscendingDiscount, CommandFlags.FireAndForget);

            //cache
            var ascendingDiscount = eventPromotion.OutputCache.
                Select(outputCache => _objectMapper.Map(outputCache, new CacheValueDiscountOutPut())).ToList();

            ascendingDiscount.AddRange(eventPromotion.OutputCache.FirstOrDefault()!.OutputReplaceCaches.
                    Select(outputReplace => _objectMapper.Map(outputReplace, new CacheValueDiscountOutPut())).ToList());

            await _databaseRedisCache.StringSetAsync(keyGetAscendingDiscount, JsonConvert.SerializeObject(ascendingDiscount),null, (When)CommandFlags.FireAndForget);

            //define insert db
            var getAscendingDiscountOutPut = eventPromotion.OutputCache.Select(output =>
                 new GetAscendingDiscountOutPut(_guidGenerator.Create())
                 {
                     Discount = output.Discount,
                     MaxDiscount = output.MaxDiscount,
                     PromotionId = output.PromotionId,
                     MinQuantity = output.MinQuantity,
                     MaxQuantity = output.MaxQuantity,
                     DiscountType = output.QualifierCode,
                     UnitCode = output.UnitCode,
                     UnitName = output.UnitName,
                     PrmotionCode = output.PromotionCode
                     
                 }).ToList();

            //define insert db
            getAscendingDiscountOutPut.AddRange(
                eventPromotion.OutputCache.FirstOrDefault()?.OutputReplaceCaches.
                    Select(output => new GetAscendingDiscountOutPut(_guidGenerator.Create())
                    {
                        Discount = output.Discount,
                        MaxDiscount = output.MaxDiscount,
                        PromotionId = output.PromotionId,
                        MinQuantity = output.MinQuantity,
                        MaxQuantity = output.MaxQuantity,
                        DiscountType = output.QualifierCode,
                        UnitCode = output.UnitCode,
                        UnitName = output.UnitName,
                        PrmotionCode = output.PromotionCode
                    }));

            foreach (var output in eventPromotion.OutputCache)
            {
                await CreateUpdateOutPutReplacesElacticAsync(new List<ProductCache>(), output);
                foreach (var replace in output.OutputReplaceCaches)
                {
                    await CreateUpdateOutPutReplacesElacticAsync(new List<ProductCache>(), replace);
                }
            }


            await _getAscendingDiscountOutPutRepository.BulkCopyAsync(getAscendingDiscountOutPut);
            await _databaseRedisCache.KeyDeleteAsync($"SyncPromotion:{eventPromotion.HeaderCache.Code}:Lock", CommandFlags.FireAndForget);

        }

        #endregion

        #region Phương thức thanh toán

        /// <summary>
        /// phương thức thanh toán
        /// </summary>
        /// <param name="dataCache"></param>
        /// <returns></returns>
        public async Task PaymenMethodCachingAsync(string promotionCode,EventPromotionEto dataCache)
        {
            var transactionTracer = _tracer?.StartTransaction($"PaymenMethodCachingAsync", ApiConstants.TypeRequest);
            var span = _tracer?.CurrentTransaction?.StartSpan($"{promotionCode} : {dataCache.HeaderCache.PromotionType}", ApiConstants.ActionExec);
            try
            {
                await PromotionLockByDefineInputAsync(dataCache);

                await CommonClearDataAndCachingHeaderAsync(dataCache.HeaderCache);

                await PromotionLockAndDefineCacheOutPutAsync(dataCache);

                await CommonCachingPaymentMethodAsync(dataCache);

                if (dataCache.InputCache is { Count: > 0 })
                {
                    await CommonCachingInputAsync(dataCache);
                }

                if (dataCache.OutputCache is { Count: > 0 })
                {
                    await OutputPaymentMethodCachingAsync(dataCache);
                }
            }
            catch (Exception ex)
            {
                transactionTracer?.CaptureException(ex);
                await UpSetPromotionErrorCachingAsync(dataCache.HeaderCache.Code, ex.Message);
                throw new Exception(message: ex.Message);
            }
            finally
            {
                span?.End();
                transactionTracer?.End();
            }
            await _databaseRedisCache.KeyDeleteAsync($"SyncPromotion:{promotionCode}:Lock", CommandFlags.FireAndForget);
        }

        public async Task CommonCachingPaymentMethodAsync(EventPromotionEto dataCache)
        {
            //Promotion:UsePaymentMethod
            var inputPaymenyMethod = dataCache.OutputCache.FirstOrDefault(p => p.PaymentMethodCode != null);
            var keyinputPaymenyMethodCode = GetKeyPrefixCaching("", dataCache.HeaderCache.PromotionType);
            await _databaseRedisCache.HashSetAsync(keyinputPaymenyMethodCode, new HashEntry[]
            {
                new(dataCache.HeaderCache.Code, $"{inputPaymenyMethod?.PaymentMethodCode}")
            }, CommandFlags.FireAndForget);
        }

        /// <summary>
        /// Phương Thức Thanh Toán 
        /// </summary>
        /// <param name="paymentMethodOutPut"></param>
        /// <returns></returns>
        public async Task OutputPaymentMethodCachingAsync(EventPromotionEto paymentMethodOutPut)
        {

            var keyPaymentMethodCaching = GetKeyPrefixCaching(paymentMethodOutPut.HeaderCache.Code, "Output");
            keyPaymentMethodCaching = $"{keyPaymentMethodCaching}:{paymentMethodOutPut.HeaderCache.PromotionType}";

            await _databaseRedisCache.KeyDeleteAsync(keyPaymentMethodCaching);

            //cache paymentMethod Output
            var paymentMethod = paymentMethodOutPut.OutputCache.
                Select(outputCache => _objectMapper.Map(outputCache, new CacheValueUsePaymentMethodOutPut())).ToList();

            await _databaseRedisCache.StringSetAsync(keyPaymentMethodCaching, JsonConvert.SerializeObject(paymentMethod),null, (When)CommandFlags.FireAndForget);

            //insert database paymentMethod Output
            var getPaymentMethodOutputs = paymentMethodOutPut.OutputCache.Select(output =>
                 new PaymentMethodOutput(_guidGenerator.Create())
                 {
                     Note = output.Note,
                     NoteBoom = output.NoteBoom,
                     UnitCode = output.UnitCode,
                     UnitName = output.UnitName,
                     Discount = output.Discount,
                     MaxDiscount = output.MaxValue,
                     SchemeCode = output.SchemeCode,
                     DiscountType = output.QualifierCode,
                     PromotionId = output.PromotionId,
                     PromotionCode = output.PromotionCode,
                     PaymentMethodCode = output.PaymentMethodCode,
                     PaymentMethodName = output.PaymentMethodName,
                     DiscountPlaceCode = output.DiscountPlaceCode,
                 }).ToList();

            await _paymentMethodOutputRepository.InsertManyAsync(getPaymentMethodOutputs);

            foreach (var output in paymentMethodOutPut.OutputCache)
            {
                await CreateUpdateOutPutReplacesElacticAsync(new List<ProductCache>(), output);
                foreach (var replace in output.OutputReplaceCaches)
                {
                    await CreateUpdateOutPutReplacesElacticAsync(new List<ProductCache>(), replace);
                }
            }

        }

        #endregion

        #region Tổng tiền theo hóa đơn. Tổng tiền theo sản phẩm

        /// <summary>
        /// Tổng tiền theo hóa đơn
        /// </summary>
        /// <param name="dataCache"></param>
        /// <returns></returns>
        public async Task TotalBillAmountCachingAsync(string promotionCode,EventPromotionEto dataCache)
        {
            var transactionTracer = _tracer?.StartTransaction($"TotalBillAmountCachingAsync", ApiConstants.TypeRequest);
            var span = _tracer?.CurrentTransaction?.StartSpan($"{promotionCode} : {dataCache.HeaderCache.PromotionType}", ApiConstants.ActionExec);
            try
            {
                await PromotionLockByDefineInputAsync(dataCache);

                await CommonClearDataAndCachingHeaderAsync(dataCache.HeaderCache);

                //Promotion:TotalCost
                await CommonCachingTotalCostAsync(dataCache.HeaderCache);

                //item cache
                if (dataCache.InputCache is { Count: > 0 })
                {
                    await CommonCachingInputAsync(dataCache);
                }

                if (dataCache.OutputCache is { Count: > 0 })
                {
                    await OutPutCacheTotalAsync(dataCache);
                }
                await _databaseRedisCache.KeyDeleteAsync($"SyncPromotion:{dataCache.HeaderCache.Code}:Lock", CommandFlags.FireAndForget);
            }
            catch (Exception ex)
            {
                transactionTracer?.CaptureException(ex);
                await UpSetPromotionErrorCachingAsync(dataCache.HeaderCache.Code, ex.Message);
                throw new Exception(message: ex.Message);
            }
            finally
            {
                span?.End();
                transactionTracer?.End();
            }
            await _databaseRedisCache.KeyDeleteAsync($"SyncPromotion:{promotionCode}:Lock", CommandFlags.FireAndForget);

        }

        /// <summary>
        /// Tổng tiền theo sản phẩm
        /// </summary>
        /// <param name="dataCache"></param>
        /// <returns></returns>
        public async Task TotalProductAmountCachingAsync(string promotionCode, EventPromotionEto dataCache)
        {
            var transactionTracer = _tracer?.StartTransaction($"TotalProductAmountCachingAsync", ApiConstants.TypeRequest);
            var span = _tracer?.CurrentTransaction?.StartSpan($"{promotionCode} : {dataCache.HeaderCache.PromotionType}", ApiConstants.ActionExec);
            try
            {

                await CommonClearDataAndCachingHeaderAsync(dataCache.HeaderCache);

                //Promotion:TotalCost
                await CommonCachingTotalCostAsync(dataCache.HeaderCache);

                //item cache
                if (dataCache.InputCache is { Count: > 0 })
                {
                    await CommonCachingInputAsync(dataCache);
                }

                if (dataCache.OutputCache is { Count: > 0 })
                {
                    await OutPutCacheTotalAsync(dataCache);
                }

                await _databaseRedisCache.KeyDeleteAsync($"SyncPromotion:{dataCache.HeaderCache.Code}:Lock", CommandFlags.FireAndForget);

            }
            catch (Exception ex)
            {
                transactionTracer?.CaptureException(ex);
                throw new Exception(message: ex.Message);
            }
            finally
            {
                span?.End();
                transactionTracer?.End();
            }
            await _databaseRedisCache.KeyDeleteAsync($"SyncPromotion:{promotionCode}:Lock", CommandFlags.FireAndForget);

        }

        /// <summary>
        /// Out put chỉ tang sản phẩm (Không cho tặng ngành hàng (20/06/2023))
        /// 1/ Tặng sản phẩm (GetItemCode) => điều kiện And, Or
        /// 2/ Tặng Mã ưu đãi (GetGiftCard) => điều kiện And
        /// 3/ Tặng phiêu mua hàng (GetCoupon) => điều kiện And
        /// 4/ Giảm số tiền theo tổng bill (AmountOff) => chỉ duy nhất 1 line
        /// 5/ Giảm phần trăm theo tổng bill (PercentOff) => chỉ duy nhất 1 line
        /// </summary>
        /// <param name="getProductOutPut"></param>
        /// <returns></returns>
        private async Task OutPutCacheTotalAsync(EventPromotionEto TotalBillAmountOutPut)
        {
            var outPutFisrt = TotalBillAmountOutPut.OutputCache.FirstOrDefault();
            if (System.Enum.TryParse(outPutFisrt.QualifierCode, out QualifierTypeEnum qualifierCode))
            {
                switch (qualifierCode)
                {
                    case QualifierTypeEnum.GetCoupon:
                    case QualifierTypeEnum.GetGiftCard:
                    case QualifierTypeEnum.GetItemCode:
                        await OutPutCacheGetProductAsync(TotalBillAmountOutPut);
                        break;
                    case QualifierTypeEnum.PercentOff:
                    case QualifierTypeEnum.AmountOff:
                        await OutPutCacheTotalBillByDiscountAsync(TotalBillAmountOutPut);
                        break;
                }
            }

        }

        /// <summary>
        /// Out put chỉ tang sản phẩm (Không cho tặng ngành hàng (20/06/2023))
        /// 1/ Tặng sản phẩm (GetItemCode) => điều kiện And, Or
        /// 2/ Tặng Mã ưu đãi (GetGiftCard) => điều kiện And
        /// 3/ Tặng phiêu mua hàng (GetCoupon) => điều kiện And
        /// 4/ Giảm số tiền theo tổng bill (AmountOff) => chỉ duy nhất 1 line
        /// 5/ Giảm phần trăm theo tổng bill (PercentOff) => chỉ duy nhất 1 line
        /// </summary>
        /// <param name="totalBillAmountOutPut"></param>
        /// <returns></returns>
        private async Task OutPutCacheTotalBillByDiscountAsync(EventPromotionEto totalBillAmountOutPut)
        {
            var hashEntryLists = new List<HashEntry>();
            var discountItemOutputsEntity = new List<DiscountItemOutput>();

            foreach (var outPut in totalBillAmountOutPut.OutputCache)
            {
                if (System.Enum.TryParse(outPut.QualifierCode, out QualifierTypeEnum qualifierCode))
                {
                    var keyOutput = $"{GetKeyPrefixCaching(outPut.PromotionCode, "Output")}:{outPut.PromotionType}";
                    switch (qualifierCode)
                    {
                        case QualifierTypeEnum.AmountOff:
                        case QualifierTypeEnum.PercentOff:
                            var disCountOutput = _objectMapper.Map(outPut, new CacheValueDiscountOutPut());
                            var keyDiscountTotal = outPut.ItemCode.IsNullOrEmpty() ? "DiscountTotalBill" : outPut.ItemCode;
                            hashEntryLists.Add(new HashEntry(keyDiscountTotal, JsonConvert.SerializeObject(disCountOutput)));

                            //database insert Output
                            discountItemOutputsEntity.Add(_objectMapper.Map(outPut, new DiscountItemOutput(_guidGenerator.Create())));
                            break;
                    }
                    //cache output 
                    await _databaseRedisCache.HashSetAsync(keyOutput, hashEntryLists.ToArray(), CommandFlags.FireAndForget);

                    await CreateUpdateOutPutReplacesElacticAsync(new List<ProductCache>(), outPut);
                }
            }

            if (discountItemOutputsEntity is { Count: > 0 })
            {
                await _discountItemOutputRepository.BulkCopyAsync(discountItemOutputsEntity);
            }
        }

        #endregion

        #region bắn event sync data to ecom remove lock
        /// <summary>
        /// bắn event sync data to ecom
        /// </summary>
        /// <param name="outPutCache"></param>
        /// <param name="pimProductCache"></param>
        /// <returns></returns>
        private async Task EventSyncDataToEcomAsync(
            OutputCacheEto outPutCache = null,
            PimProductCacheDto pimProductCache = null,
            ItemInputCacheEto inPutCache = null)
        {
            if (pimProductCache == null)
            {
                string promtionCode = inPutCache != null ?  inPutCache.PromotionCode : outPutCache.PromotionCode ;
                var keyPromotionSync = $"SyncPromotion:{promtionCode}";
                var syncPromotion = await _databaseRedisCache.StringGetAsync(keyPromotionSync);
                if (syncPromotion.HasValue)
                {
                    var cacheDataSyncOutput = JsonConvert.DeserializeObject<CacheDataSyncOutput>(syncPromotion);
                    cacheDataSyncOutput.CurrentItemOutput += 1;
                    await _databaseRedisCache.StringSetAsync(keyPromotionSync, JsonConvert.SerializeObject(cacheDataSyncOutput), TimeSpan.FromMinutes(30));
                    if (cacheDataSyncOutput.TotalItemOutput <= cacheDataSyncOutput.CurrentItemOutput)
                    {
                        await _databaseRedisCache.KeyDeleteAsync($"SyncPromotion:{promtionCode}:Lock", CommandFlags.FireAndForget);
                        return;
                    }
                }
                else
                {
                    await _databaseRedisCache.StringSetAsync(keyPromotionSync, JsonConvert.SerializeObject(syncPromotion), TimeSpan.FromMinutes(30), (When)CommandFlags.FireAndForget);
                }
            }
            else
            {
                string promtionCode = inPutCache != null ? inPutCache.PromotionCode : outPutCache.PromotionCode;
                var keyPromotionSync = $"SyncPromotion:{promtionCode}";
                var syncPromotion = await _databaseRedisCache.StringGetAsync(keyPromotionSync);
                if (syncPromotion.HasValue)
                {
                    var cacheDataSyncOutput = JsonConvert.DeserializeObject<CacheDataSyncOutput>(syncPromotion);
                    cacheDataSyncOutput.CurrentItemOutput += pimProductCache.Data.Count;

                    await _databaseRedisCache.StringSetAsync(keyPromotionSync, 
                        JsonConvert.SerializeObject(cacheDataSyncOutput), 
                        TimeSpan.FromMinutes(30), 
                        (When)CommandFlags.FireAndForget);

                    if (cacheDataSyncOutput.TotalItemOutput <= cacheDataSyncOutput.CurrentItemOutput)
                    {
                        await _databaseRedisCache.KeyDeleteAsync($"SyncPromotion:{promtionCode}:Lock", CommandFlags.FireAndForget);
                        return;
                    }
                }
                else
                {
                    await _databaseRedisCache.StringSetAsync(
                        keyPromotionSync, 
                        JsonConvert.SerializeObject(syncPromotion), 
                        TimeSpan.FromMinutes(30), 
                        (When)CommandFlags.FireAndForget);
                }
            } 
        }

        #endregion

        #region lock cache && sync data
        public async Task PromotionLockByDefineInputAsync(EventPromotionEto dataCache)
        {
            var keyPromotionSync = $"SyncPromotion:{dataCache.HeaderCache.Code}:Lock";
            var lockData = new CacheDataIsLock
            {
                IsLock = true
            };
            await _databaseRedisCache.StringSetAsync(keyPromotionSync, JsonConvert.SerializeObject(lockData), null, (When)CommandFlags.FireAndForget);
        }

        /// <summary>
        /// cache lock rã item và dịnh nghĩa out put trước khi rã
        /// </summary>
        /// <param name="dataCache"></param>
        /// <returns></returns>
        public async Task PromotionLockAndDefineCacheOutPutAsync(EventPromotionEto dataCache)
        {
            var syncId = Guid.NewGuid();
            var outPuts = dataCache.OutputCache;
            var inPuts = dataCache.InputCache;
            var keyPromotionSync = $"SyncPromotion:{dataCache.HeaderCache.Code}";
            var syncOutput = new CacheDataSyncOutput { PromotionCode = dataCache.HeaderCache.Code };
            if (System.Enum.TryParse(dataCache.HeaderCache.PromotionType, out PromotionTypeEnum PromotionType))
            {
                if (PromotionType is PromotionTypeEnum.BuyProductGetProduct or PromotionTypeEnum.UsePaymentMethod)
                {
                    foreach (var inPut in inPuts)
                    {
                        inPut.SyncId = syncId;
                        inPut.ActionTypeSync = dataCache.ActionType.ToString();
                        inPut.Note = dataCache?.OutputCache?.FirstOrDefault()?.Note;
                        if (inPut.ItemCode.IsNullOrEmpty() && inPut.GiftCode.IsNullOrEmpty() && !inPut.CategoryCode.IsNullOrEmpty())
                        {
                            var pimCacheSearch = _objectMapper.Map(inPut, new PimCacheSearch());
                            var pimProductCacheDto = await _iPimAppService.GetListProductByPimCacheAsync(pimCacheSearch);

                            // remove all product thuộc sản phẩm loại trừ
                            var countItemExclude = 0;
                            if (dataCache.HeaderCache.ItemInputExcludes is { Length: > 0 })
                            {
                                var itemExcludes = dataCache.HeaderCache.ItemInputExcludes.ToList();
                                var listProductWithCategory = await _iPimAppService.GetListProductWithCategoryAsync(itemExcludes);
                                countItemExclude = listProductWithCategory
                                    .Where(p => p.CategoryCode == inPut.CategoryCode)
                                    .WhereIf(!inPut.TypeCode.IsNullOrEmpty(), p => p.TypeCode == inPut.TypeCode)
                                    .WhereIf(!inPut.ModelCode.IsNullOrEmpty(), p => p.ModelCode == inPut.ModelCode)
                                    .WhereIf(!inPut.BrandCode.IsNullOrEmpty(), p => p.BrandCode == inPut.BrandCode)
                                    .WhereIf(!inPut.GroupCode.IsNullOrEmpty(), p => p.GroupCode == inPut.GroupCode)
                                    .Count();
                            }

                            syncOutput.TotalItemOutput += pimProductCacheDto.Total - countItemExclude;
                        }
                        // không sync input là MUD
                        else if (inPut.GiftCode.IsNullOrEmpty())
                        {
                            syncOutput.TotalItemOutput += 1;
                            if (dataCache.HeaderCache.ItemInputExcludes is { Length: > 0 })
                            {
                                var countItemExclude = dataCache.HeaderCache.ItemInputExcludes.Count(p => p.Equals(inPut.ItemCode));
                                if (countItemExclude > 0)
                                {
                                    syncOutput.TotalItemOutput--;
                                }
                            }
                        }

                        foreach (var inPutReplace in inPut.ItemInputReplaceCaches)
                        {
                            inPutReplace.SyncId = syncId;
                            inPutReplace.ActionTypeSync = dataCache.ActionType.ToString();
                            inPutReplace.Note = dataCache?.OutputCache?.FirstOrDefault()?.Note;
                            if (inPutReplace.ItemCode.IsNullOrEmpty() && inPutReplace.GiftCode.IsNullOrEmpty() && !inPutReplace.CategoryCode.IsNullOrEmpty())
                            {
                                var pimCacheSearch = _objectMapper.Map(inPutReplace, new PimCacheSearch());
                                var pimProductCacheDto = await _iPimAppService.GetListProductByPimCacheAsync(pimCacheSearch);

                                // remove all product thuộc sản phẩm loại trừ
                                var countItemExclude = 0;
                                if (dataCache.HeaderCache.ItemInputExcludes is { Length: > 0 })
                                {
                                    var itemExcludes = dataCache.HeaderCache.ItemInputExcludes.ToList();
                                    var listProductWithCategory = await _iPimAppService.GetListProductWithCategoryAsync(itemExcludes);
                                    countItemExclude = listProductWithCategory
                                                .Where(p => p.CategoryCode == inPutReplace.CategoryCode)
                                                .WhereIf(!inPutReplace.TypeCode.IsNullOrEmpty(), p => p.TypeCode == inPutReplace.TypeCode)
                                                .WhereIf(!inPutReplace.ModelCode.IsNullOrEmpty(), p => p.ModelCode == inPutReplace.ModelCode)
                                                .WhereIf(!inPutReplace.BrandCode.IsNullOrEmpty(), p => p.BrandCode == inPutReplace.BrandCode)
                                                .WhereIf(!inPutReplace.GroupCode.IsNullOrEmpty(), p => p.GroupCode == inPutReplace.GroupCode)
                                                .Count();
                                }
                                syncOutput.TotalItemOutput += pimProductCacheDto.Total - countItemExclude;
                            }
                            // không sync input là MUD
                            else if (inPutReplace.GiftCode.IsNullOrEmpty()) 
                            {
                                syncOutput.TotalItemOutput += 1;
                                if (dataCache.HeaderCache.ItemInputExcludes is { Length: > 0 })
                                {
                                  var countItemExclude = dataCache.HeaderCache.ItemInputExcludes.Count(p => p.Equals(inPutReplace.ItemCode));
                                  if(countItemExclude > 0)
                                  {
                                      syncOutput.TotalItemOutput--;
                                  }    
                                }  
                            }
                        }
                    }
                    await _databaseRedisCache.StringSetAsync(keyPromotionSync, 
                        JsonConvert.SerializeObject(syncOutput), 
                        TimeSpan.FromMinutes(60),
                        (When)CommandFlags.FireAndForget);
                }
                else if (outPuts is { Count: > 0 })
                {
                    foreach (var outPut in outPuts)
                    {
                        outPut.SyncId = syncId;
                        outPut.ActionTypeSync = dataCache.ActionType.ToString();
                        //Ngành hàng [outPut tặng quà không tặng theo ngành hàng, => chỉ tặng sản phẩm (itemCode)]
                        if (outPut.ItemCode.IsNullOrEmpty() && outPut.GiftCode.IsNullOrEmpty() && !outPut.CategoryCode.IsNullOrEmpty())
                        {
                            var pimCacheSearch = _objectMapper.Map(outPut, new PimCacheSearch());
                            var pimProductCacheDto = await _iPimAppService.GetListProductByPimCacheAsync(pimCacheSearch);

                            // remove all product thuộc sản phẩm loại trừ
                            var countItemExclude = 0;
                            if (dataCache.HeaderCache.ItemInputExcludes is { Length: > 0 })
                            {
                                var itemExcludes = dataCache.HeaderCache.ItemInputExcludes.ToList();
                                var listProductWithCategory = await _iPimAppService.GetListProductWithCategoryAsync(itemExcludes);
                                countItemExclude = listProductWithCategory
                                               .Where(p => p.CategoryCode == outPut.CategoryCode)
                                               .WhereIf(!outPut.TypeCode.IsNullOrEmpty(), p => p.TypeCode == outPut.TypeCode)
                                               .WhereIf(!outPut.ModelCode.IsNullOrEmpty(), p => p.ModelCode == outPut.ModelCode)
                                               .WhereIf(!outPut.BrandCode.IsNullOrEmpty(), p => p.BrandCode == outPut.BrandCode)
                                               .WhereIf(!outPut.GroupCode.IsNullOrEmpty(), p => p.GroupCode == outPut.GroupCode)
                                               .Count();
                                
                            }

                            syncOutput.TotalItemOutput += pimProductCacheDto.Total - countItemExclude;
                        }
                        else
                        {
                            syncOutput.TotalItemOutput += 1;
                            if (dataCache.HeaderCache.ItemInputExcludes is { Length: > 0 })
                            {
                                var countItemExclude = dataCache.HeaderCache.ItemInputExcludes.Count(p => p.Equals(outPut.ItemCode));
                                if (countItemExclude > 0)
                                {
                                    syncOutput.TotalItemOutput--;
                                }
                            }
                        }

                        foreach (var outputReplace in outPut.OutputReplaceCaches)
                        {
                            outputReplace.SyncId = syncId;
                            outputReplace.ActionTypeSync = dataCache.ActionType.ToString();
                            if (outputReplace.ItemCode.IsNullOrEmpty() && outputReplace.GiftCode.IsNullOrEmpty() && !outputReplace.CategoryCode.IsNullOrEmpty())
                            {
                                var pimCacheSearch = _objectMapper.Map(outputReplace, new PimCacheSearch());
                                var pimProductCacheDto = await _iPimAppService.GetListProductByPimCacheAsync(pimCacheSearch);

                                // remove all product thuộc sản phẩm loại trừ
                                var countItemExclude = 0;
                                if (dataCache.HeaderCache.ItemInputExcludes is { Length: > 0 })
                                {
                                    var itemExcludes = dataCache?.HeaderCache?.ItemInputExcludes.ToList();
                                    var listProductWithCategory = await _iPimAppService.GetListProductWithCategoryAsync(itemExcludes);
                                    countItemExclude = listProductWithCategory
                                               .Where(p => p.CategoryCode == outputReplace.CategoryCode)
                                               .WhereIf(!outputReplace.TypeCode.IsNullOrEmpty(), p => p.TypeCode == outputReplace.TypeCode)
                                               .WhereIf(!outputReplace.ModelCode.IsNullOrEmpty(), p => p.ModelCode == outputReplace.ModelCode)
                                               .WhereIf(!outputReplace.BrandCode.IsNullOrEmpty(), p => p.BrandCode == outputReplace.BrandCode)
                                               .WhereIf(!outputReplace.GroupCode.IsNullOrEmpty(), p => p.GroupCode == outputReplace.GroupCode)
                                               .Count();
                                    
                                }
                                syncOutput.TotalItemOutput += pimProductCacheDto.Total - countItemExclude;
                            }
                            else
                            {
                                syncOutput.TotalItemOutput += 1;
                                if (dataCache.HeaderCache.ItemInputExcludes is { Length: > 0 })
                                {
                                    var countItemExclude = dataCache.HeaderCache?.ItemInputExcludes?.Count(p => p.Equals(outputReplace.ItemCode));
                                    if (countItemExclude > 0)
                                    {
                                        syncOutput.TotalItemOutput--;
                                    }
                                }
                            }
                        }
                    }
                    await _databaseRedisCache.StringSetAsync(keyPromotionSync, JsonConvert.SerializeObject(syncOutput), TimeSpan.FromMinutes(60), (When)CommandFlags.FireAndForget);
                }
            }

        }

        #endregion

        #region caching header, common

        /// <summary>
        /// cache promotion header
        /// </summary>
        /// <param name="dataCache"></param>
        /// <returns></returns>
        public async Task CommonClearDataAndCachingHeaderAsync(CacheValuePromotionHeader dataCache, bool flagDeleteElk = true)
        {
            if (flagDeleteElk)
            {
                await _elasticSearchAppService.DeleteInputReplacesAsync(dataCache.Code);
                await _elasticSearchAppService.DeleteOutputReplacesAsync(dataCache.Code);
            }
            if (dataCache != null)
            {
                await TimeExpireCacheAsync(new List<PromotionTimeExpireEto>
                    {
                        new ()
                        {
                            PromotionId = dataCache.Id,
                            PromotionCode = dataCache.Code,
                            PromotionType = dataCache.PromotionType,
                            ProvinceConditions = new List<string>()
                        }
                    },
                        isReCache: true);

                var keyCache = GetKeyPrefixCaching(dataCache.Code, "Header");
                await _databaseRedisCache.StringSetAsync(keyCache, JsonConvert.SerializeObject(dataCache));
            }
        }

        /// <summary>
        /// cache promotion header cho trường hợp tạo promotion exlcude (cơ cấu loại trừ)
        /// </summary>
        /// <param name="dataCache"></param>
        /// <returns></returns>
        public async Task CommonCachingHeaderPromotionExcludeAsync(CacheValuePromotionHeader dataCache)
        {

            if (dataCache != null)
            {
                var keyCache = GetKeyPrefixCaching(dataCache.Code, "Header");
                await _databaseRedisCache.StringSetAsync(keyCache, JsonConvert.SerializeObject(dataCache));
            }
        }

        /// <summary>
        /// cache promotion code của khuyến mãi tổng tiền
        /// </summary>
        /// <param name="headerCache"></param>
        /// <returns></returns>
        public async Task CommonCachingTotalCostAsync(CacheValuePromotionHeader headerCache)
        {
            //Promotion:TotalCost
            var keyTotalCost = GetKeyPrefixCaching("", "TotalCost");
            await _databaseRedisCache.HashSetAsync(keyTotalCost, new HashEntry[]
            {
                new(headerCache.Code, $"{headerCache.Code}")
            }, CommandFlags.FireAndForget);
        }

        #endregion

        #region index elactic InputReplaces && OutputReplace

        /// <summary>
        /// index InputReplaces docucment
        /// </summary>
        /// <param name="products"></param>
        /// <param name="eventInput"></param>
        /// <returns></returns>
        public async Task CreateUpdateInputReplacesElacticAsync(List<ProductCache> products, EventInputCacheEto eventInput)
        {
            try
            {
                var itemReplace = _objectMapper.Map(eventInput, new InputReplaceDocument());
                if (products is { Count: > 0 }) // ngành hàng
                {
                    var inputReplaceDocuments = new List<InputReplaceDocument>();
                    foreach (var product in products)
                    {
                        var newitemReplace = JsonConvert.DeserializeObject<InputReplaceDocument>(JsonConvert.SerializeObject(itemReplace));
                        var itemReplaceDocument = _objectMapper.Map(product, newitemReplace);
                        inputReplaceDocuments.Add(itemReplaceDocument);
                    }
                    _elasticSearchAppService.CreateUpdateInputReplaces(inputReplaceDocuments);
                }
                else if (!eventInput.InputCache.ItemCode.IsNullOrEmpty()) // sản phẩm
                {
                    var getProduct = await _iPimAppService.GetProductByPimCacheAsync(eventInput.InputCache.ItemCode);
                    var inputReplaceDocuments = new List<InputReplaceDocument>();
                    foreach (var product in getProduct.Data)
                    {
                        if (product.ItemCode == eventInput.InputCache.ItemCode)
                        {
                            var newitemReplace = JsonConvert.DeserializeObject<InputReplaceDocument>(JsonConvert.SerializeObject(itemReplace));
                            var itemReplaceDocument = _objectMapper.Map(product, newitemReplace);
                            inputReplaceDocuments.Add(itemReplaceDocument);
                        }
                    }
                    _elasticSearchAppService.CreateUpdateInputReplaces(inputReplaceDocuments);
                }
                else
                {
                    _elasticSearchAppService.CreateUpdateInputReplaces(new List<InputReplaceDocument> { itemReplace });
                }
            }
            catch
            { }
        }

        /// <summary>
        ///  index OutPutReplaces docucment
        /// </summary>
        /// <param name="products"></param>
        /// <param name="eventInput"></param>
        /// <returns></returns>
        public async Task CreateUpdateOutPutReplacesElacticAsync(List<ProductCache> products, OutputCacheEto outputCache)
        {
            try
            {
                var itemReplace = _objectMapper.Map(outputCache, new OutputReplaceDocument());
                if (products is { Count: > 0 }) // ngành hàng
                {
                    var outputReplaceDocuments = new List<OutputReplaceDocument>();
                    foreach (var product in products)
                    {
                        var newitemReplace = JsonConvert.DeserializeObject<OutputReplaceDocument>(JsonConvert.SerializeObject(itemReplace));
                        var itemReplaceDocument = _objectMapper.Map(product, newitemReplace);
                        outputReplaceDocuments.Add(itemReplaceDocument);
                    }
                    _elasticSearchAppService.CreateUpdateOutputReplacesAsync(outputReplaceDocuments);
                }
                else if (!outputCache.ItemCode.IsNullOrEmpty()) // sản phẩm
                {
                    var getProduct = await _iPimAppService.GetProductByPimCacheAsync(outputCache.ItemCode);
                    var outputReplaceDocuments = new List<OutputReplaceDocument>();
                    foreach (var product in getProduct.Data)
                    {
                        if (product.ItemCode == outputCache.ItemCode)
                        {
                            var newitemReplace = JsonConvert.DeserializeObject<OutputReplaceDocument>(JsonConvert.SerializeObject(itemReplace));
                            var itemReplaceDocument = _objectMapper.Map(product, newitemReplace);
                            outputReplaceDocuments.Add(itemReplaceDocument);
                        }
                    }
                    _elasticSearchAppService.CreateUpdateOutputReplacesAsync(outputReplaceDocuments);
                }
                else // voucher, cmcr
                {
                    _elasticSearchAppService.CreateUpdateOutputReplacesAsync(new List<OutputReplaceDocument> { itemReplace });
                }
            }
            catch
            { }
        }

        #endregion

        #region cache Input 

        /// <summary>
        /// cache input promotion và input replace promotion nếu có nganh,loai,nhan,san pham
        /// </summary>
        /// <param name="dataCache"></param>
        /// <returns></returns>
        public async Task CommonCachingInputAsync(EventPromotionEto dataCache)
        {
            foreach (var inputCache in dataCache.InputCache)
            {
                await DefinePimItemCodeInputAsync(new EventInputCacheEto
                {
                    InputCache = inputCache,
                    ItemInputExcludes = dataCache.HeaderCache.ItemInputExcludes
                });
            }
        }

        /// <summary>
        /// decay item input pim
        /// </summary>
        /// <param name="eventInput"></param>
        /// <returns></returns>
        public async Task DefinePimItemCodeInputAsync(EventInputCacheEto eventInput)
        {
            var itemInputFirst = string.Empty;
            var inputCacheItemId = Guid.Empty;
            // Caching theo nganh hàng
            if (eventInput.InputCache.ItemCode.IsNullOrEmpty()
             && eventInput.InputCache.GiftCode.IsNullOrEmpty()
             && !eventInput.InputCache.CategoryCode.IsNullOrEmpty())
            {
                var pimCacheSearch = _objectMapper.Map(eventInput, new PimCacheSearch());
                var dataCount = await _iPimAppService.CountDataPimCacheAsync(pimCacheSearch);
                if (dataCount.Total == 0)
                {
                    foreach (var item in eventInput.InputCache.ItemInputReplaceCaches)
                    {
                        pimCacheSearch = _objectMapper.Map(item, new PimCacheSearch());
                        dataCount = await _iPimAppService.CountDataPimCacheAsync(pimCacheSearch);
                        if (dataCount.Total > 0)
                        {
                            eventInput.InputCache.ItemInputReplaceCaches.Remove(item);
                            break;
                        }
                    }
                }
                if (dataCount.Total > 2000)
                {
                    pimCacheSearch.IsScroll = true;
                }
                const int scrollSize = 2000;
                var scrollId = string.Empty;

                for (int i = 0; i <= dataCount.Total; i += scrollSize)
                {
                    pimCacheSearch.ScrollId = scrollId;
                    pimCacheSearch.ScrollSize = scrollSize;
                    var pimProductCacheDto = await _iPimAppService.GetListProductByPimCacheAsync(pimCacheSearch);
                    var products = pimProductCacheDto.Data;

                    //#region bắn event sync data to ecom
                    //if (eventInput.InputCache.PromotionType == "BuyProductGetProduct" || eventInput.InputCache.PromotionType == "UsePaymentMethod")
                    //{
                    //    // remove all product thuộc sản phẩm loại trừ
                    //    if (eventInput.ItemInputExcludes is { Length: > 0 })
                    //    {
                    //        pimProductCacheDto.Data.RemoveAll(p => eventInput.ItemInputExcludes.Contains(p.ItemCode));
                    //        pimProductCacheDto.Total = pimProductCacheDto.Data.Count;
                    //    }
                    //    //bắn event sync data to ecom
                    //    await EventSyncDataToEcomAsync(pimProductCache: pimProductCacheDto, inPutCache: eventInput.InputCache);
                    //}
                    //#endregion
                     

                    //Lấy đơn vị lớn nhất của sản phẩm
                    if(eventInput.InputCache.UnitCode == 1)
                    {
                        products.ForEach(a =>
                        {
                            var mesureMax = a.Measures.FirstOrDefault(b => b.Level == a.Measures.Min(c => c.Level));
                            a.Measures.RemoveAll(a => a.Level != mesureMax.Level);
                        });
                    }

                    if (eventInput.InputCache.UnitCode == 2)
                    {
                        products.ForEach(a =>
                        {
                            var mesureMax = a.Measures.FirstOrDefault(b => b.Level == a.Measures.Max(c => c.Level));
                            a.Measures.RemoveAll(a => a.Level != mesureMax.Level);
                        });
                    }
                    // remove all product thuộc sản phẩm loại trừ
                    if (eventInput.ItemInputExcludes is { Length: > 0 })
                    {
                        products.RemoveAll(p => eventInput.ItemInputExcludes.Contains(p.ItemCode));
                        pimProductCacheDto.Total = products.Count;
                    }
                    if (scrollId.IsNullOrEmpty()) // lần đầu rả item
                    {
                        itemInputFirst = eventInput.InputCache.ItemCode = products.FirstOrDefault()?.ItemCode; //lấy sản phẩm đầu tiên của ngành hàng
                        eventInput.InputCache.ItemName = products.FirstOrDefault()?.ItemName;
                        if(eventInput.InputCache.UnitCode != 0)
                        {
                            eventInput.InputCache.UnitCode = products.FirstOrDefault()?.Measures.FirstOrDefault()?.MeasureUnitId;
                            eventInput.InputCache.UnitName = products.FirstOrDefault()?.Measures.FirstOrDefault()?.MeasureUnitName;
                        }
                        await CachingProductInputAsync(eventInput); // caching input
                        inputCacheItemId = await InsertInputCacheItemAsync(eventInput); // insert db

                        await CreateUpdateInputReplacesElacticAsync(products, eventInput);  // index input Replace es
                        eventInput.IdInput = inputCacheItemId;
                        eventInput.ItemCodeInput = products.FirstOrDefault()?.ItemCode;
                        products.Remove(products.FirstOrDefault());                         // lấy all sản phẩm còn lại
                        await CachingProductInputReplaceAsync(products, eventInput);        // caching input Replace redis
                        await InsertInputCacheItemReplaceAsync(products, eventInput);       // insert input Replace db

                    }
                    else // các lần còn lại caching Replace
                    {
                        eventInput.IdInput = inputCacheItemId;
                        await CachingProductInputReplaceAsync(products, eventInput);      // caching input Replace redis
                        await InsertInputCacheItemReplaceAsync(products, eventInput);     // insert input Replace db
                        await CreateUpdateInputReplacesElacticAsync(products, eventInput);// index input Replace es
                    }

                    scrollId = pimProductCacheDto.ScrollId;

                }
            }
            // Caching theo item
            else
            {
                await CachingProductInputAsync(eventInput);
                inputCacheItemId = await InsertInputCacheItemAsync(eventInput); // insert db
                await CreateUpdateInputReplacesElacticAsync(new List<ProductCache>(), eventInput);// index input Replace es
                #region bắn event sync data to ecom
                if (eventInput.InputCache.PromotionType == "BuyProductGetProduct" || eventInput.InputCache.PromotionType == "UsePaymentMethod")
                {
                    if (eventInput.ItemInputExcludes is { Length: > 0 })
                    {
                        if (!eventInput.ItemInputExcludes.Any(p => p.Equals(eventInput.InputCache.ItemCode)))// thuộc loại trừ sản phẩm ko sync
                        {
                            await EventSyncDataToEcomAsync(inPutCache: eventInput.InputCache);
                        }
                    }
                    else
                    { 
                        await EventSyncDataToEcomAsync(inPutCache: eventInput.InputCache);
                    }    
                }
                #endregion
            }
            //event caching inputReplace
            if (eventInput.InputCache.ItemInputReplaceCaches is { Count: > 0 })
            {
                foreach (var itemInputReplace in eventInput.InputCache.ItemInputReplaceCaches)
                {
                    await DefinePimCacheInputReplaceAsync(
                         new EventInputCacheEto
                         {
                             IdInput = inputCacheItemId,
                             ItemCodeInput = itemInputFirst.IsNullOrEmpty()
                                ? eventInput.InputCache.ItemCode : itemInputFirst,
                             InputCache = itemInputReplace,
                             ItemInputExcludes = eventInput.ItemInputExcludes
                         });
                }
            }
        }

        public async Task DefinePimCacheInputReplaceAsync(EventInputCacheEto eventInput)
        {
            // Caching theo nganh hàng
            if (eventInput.InputCache.ItemCode.IsNullOrEmpty()
             && eventInput.InputCache.GiftCode.IsNullOrEmpty()
             && !eventInput.InputCache.CategoryCode.IsNullOrEmpty())
            {
                var pimCacheSearch = _objectMapper.Map(eventInput, new PimCacheSearch());
                var dataCount = await _iPimAppService.CountDataPimCacheAsync(pimCacheSearch);
                if (dataCount.Total > 2000)
                {
                    pimCacheSearch.IsScroll = true;
                }
                const int scrollSize = 2000;
                var scrollId = string.Empty;

                for (int i = 0; i <= dataCount.Total; i += scrollSize)
                {
                    pimCacheSearch.ScrollId = scrollId;
                    pimCacheSearch.ScrollSize = scrollSize;
                    var pimProductCacheDto = await _iPimAppService.GetListProductByPimCacheAsync(pimCacheSearch);
                    var products = pimProductCacheDto.Data;

                    // remove all product thuộc sản phẩm loại trừ
                    if (eventInput.ItemInputExcludes is { Length: > 0 })
                    {
                        pimProductCacheDto.Data.RemoveAll(p => eventInput.ItemInputExcludes.Contains(p.ItemCode));
                        pimProductCacheDto.Total = pimProductCacheDto.Data.Count;
                    }

                    // remove all sản phẩm thuộc input
                    if (!eventInput.ItemCodeInput.IsNullOrEmpty())
                    {
                        pimProductCacheDto.Data.RemoveAll(p => p.ItemCode == eventInput.ItemCodeInput);
                        pimProductCacheDto.Total = pimProductCacheDto.Data.Count;
                    }

                    #region bắn event sync data to ecom
                    if (eventInput.InputCache.PromotionType == "BuyProductGetProduct" || eventInput.InputCache.PromotionType == "UsePaymentMethod")
                    {
                        //bắn event sync data to ecom
                        await EventSyncDataToEcomAsync(pimProductCache: pimProductCacheDto, inPutCache: eventInput.InputCache);
                    }
                    #endregion
                    await CachingProductInputReplaceAsync(products, eventInput);      // insert redis
                    await InsertInputCacheItemReplaceAsync(products, eventInput);     // insert db
                    await CreateUpdateInputReplacesElacticAsync(products, eventInput);// index es

                    scrollId = pimProductCacheDto.ScrollId;
                }    
            }
            // Caching theo item
            else
            {
                #region bắn event sync data to ecom
                if (eventInput.InputCache.PromotionType == "BuyProductGetProduct" || eventInput.InputCache.PromotionType == "UsePaymentMethod")
                {
                    if (eventInput.ItemInputExcludes is { Length: > 0 })
                    {
                        if (!eventInput.ItemInputExcludes.Any(p => p.Equals(eventInput.InputCache.ItemCode)))// thuộc loại trừ sản phẩm ko sync
                        {
                            await EventSyncDataToEcomAsync(inPutCache: eventInput.InputCache);
                        }
                    }
                    else
                    {
                        await EventSyncDataToEcomAsync(inPutCache: eventInput.InputCache);
                    }    
                }
                #endregion 
                await CachingProductInputReplaceAsync(new List<ProductCache>(), eventInput);         // insert redis
                await InsertInputCacheItemReplaceAsync(new List<ProductCache>(), eventInput);        // insert db
                await CreateUpdateInputReplacesElacticAsync(new List<ProductCache>(), eventInput);    // index es
            }
        }

        /// <summary>
        /// cache item input replace pim
        /// </summary>
        /// <param name="products"></param>
        /// <param name="eventInput"></param>
        /// <returns></returns>
        public async Task CachingProductInputReplaceAsync(List<ProductCache> products, EventInputCacheEto eventInput)
        {
            // caching rả theo ngành hàng
            if (products is { Count: > 0 })
            {
                foreach (var replace in products)
                {
                    var keyItemInputReplace = GetKeyPrefixCaching(replace.ItemCode, "ItemCode");
                    var inputReplaceRedisValue = new List<HashEntry>
                    {
                        new($"{eventInput.InputCache.PromotionCode}", JsonConvert.SerializeObject(new CacheValueInputReplace
                        {
                            AlterItemCode = eventInput.ItemCodeInput,
                            ItemCode = replace.ItemCode,
                            ItemName = replace.ItemName,
                            Quantity = eventInput.InputCache.Quantity,
                            UnitCode = replace.Measures.FirstOrDefault()?.MeasureUnitId,
                            UnitName = replace.Measures.FirstOrDefault()?.MeasureUnitName,
                            WarehouseCode = eventInput.InputCache.WarehouseCode,
                            FlagDiscount = eventInput.InputCache.IsDiscount,
                            Label = eventInput.InputCache.PromotionType == LabelPromotionConstants.BuyProductGetAscendingDiscount
                                    ? LabelPromotionConstants.BigDeal 
                                    : eventInput.InputCache.PromotionType == LabelPromotionConstants.BuyProductGetProduct 
                                    ? LabelPromotionConstants.GetProduct : ""
                        }))
                    };
                    if (eventInput.ItemCodeInput != null)
                    {
                        var inputDataCache = await _databaseRedisCache.HashGetAllAsync(keyItemInputReplace);
                        if (inputDataCache.Length > 0)
                        {
                            inputReplaceRedisValue.AddRange(inputDataCache);
                        }
                    }
                    await _databaseRedisCache.HashSetAsync(keyItemInputReplace, inputReplaceRedisValue.ToArray());
                }
            }
            // caching theo item code
            else
            {
                var inputReplaceRedisValue = new List<HashEntry>
                {
                    new($"{eventInput.InputCache.PromotionCode}", JsonConvert.SerializeObject(new CacheValueInputReplace
                    {
                        AlterItemCode = eventInput.ItemCodeInput,
                        ItemCode = eventInput.InputCache.ItemCode,
                        ItemName = eventInput.InputCache.ItemName,
                        Quantity = eventInput.InputCache.Quantity,
                        UnitCode = eventInput.InputCache.UnitCode,
                        UnitName = eventInput.InputCache.UnitName,
                        WarehouseCode = eventInput.InputCache.WarehouseCode,
                        FlagDiscount = eventInput.InputCache.IsDiscount,
                        Label = eventInput.InputCache.PromotionType == LabelPromotionConstants.BuyProductGetAscendingDiscount
                                    ? LabelPromotionConstants.BigDeal
                                    : eventInput.InputCache.PromotionType == LabelPromotionConstants.BuyProductGetProduct
                                    ? LabelPromotionConstants.GetProduct : ""
                    }))
                };
                var keyItemInputReplace = GetKeyPrefixCaching(eventInput.InputCache.ItemCode, "ItemCode");
                if (eventInput.InputCache.ItemCode != null && eventInput.ItemCodeInput != null)
                {
                    var inputDataCache = await _databaseRedisCache.HashGetAllAsync(keyItemInputReplace);
                    if (inputDataCache.Length > 0)
                    {
                        inputReplaceRedisValue.AddRange(inputDataCache);
                    }
                }
                await _databaseRedisCache.HashSetAsync(keyItemInputReplace, inputReplaceRedisValue.ToArray());
            }
        }

        /// <summary>
        /// cache item input pim
        /// </summary>
        /// <param name="eventInput"></param>
        /// <returns></returns>
        public async Task CachingProductInputAsync(EventInputCacheEto eventInput)
        {
            //cache input
            var promotionCodeInput = GetKeyPrefixCaching(eventInput.InputCache.PromotionCode, "Input");
            var cacheValueInput = _objectMapper.Map(eventInput, new CacheValueInput());
            var inputRedisValue = new List<HashEntry>();
            //cache itemCode
            if (!eventInput.InputCache.ItemCode.IsNullOrEmpty())
            { 
                //cache input
                inputRedisValue = new List<HashEntry>
                {
                    new($"{eventInput.InputCache.ItemCode}", JsonConvert.SerializeObject(cacheValueInput))
                };
                var promotionItemCode = GetKeyPrefixCaching(eventInput.InputCache.ItemCode, "ItemCode");
                var inputDataCacheReplace = await _databaseRedisCache.HashGetAllAsync(promotionItemCode);
                var inputReplaceRedisValue = new List<HashEntry>
                {
                    new($"{eventInput.InputCache.PromotionCode}", JsonConvert.SerializeObject(
                        new CacheValueInputReplace
                        {
                            ItemCode = eventInput.InputCache.ItemCode,
                            ItemName = eventInput.InputCache.ItemName,
                            Quantity = eventInput.InputCache.Quantity,
                            UnitCode = eventInput.InputCache.UnitCode,
                             UnitName = eventInput.InputCache.UnitName,
                            AlterItemCode = eventInput.InputCache.ItemCode,
                            FlagDiscount = eventInput.InputCache.IsDiscount,
                            WarehouseCode = eventInput.InputCache.WarehouseCode,
                            Label = eventInput.InputCache.PromotionType == LabelPromotionConstants.BuyProductGetAscendingDiscount
                                    ? LabelPromotionConstants.BigDeal
                                    : eventInput.InputCache.PromotionType == LabelPromotionConstants.BuyProductGetProduct
                                    ? LabelPromotionConstants.GetProduct : ""
                        }))
                };
                if (inputDataCacheReplace.Length > 0)
                {
                    inputReplaceRedisValue.AddRange(inputDataCacheReplace);
                }

                await _databaseRedisCache.HashSetAsync(promotionItemCode, inputReplaceRedisValue.ToArray());
            }
            //cache giftCode
            if (!eventInput.InputCache.GiftCode.IsNullOrEmpty())
            {
                //cache input
                inputRedisValue = new List<HashEntry>
                {
                    new($"{eventInput.InputCache.GiftCode}", JsonConvert.SerializeObject(cacheValueInput))
                };
            }

            var inputDataCache = await _databaseRedisCache.HashGetAllAsync(promotionCodeInput);
            if (inputDataCache.Length > 0)
            {
                inputRedisValue.AddRange(inputDataCache);
            }

            await _databaseRedisCache.HashSetAsync(promotionCodeInput, inputRedisValue.ToArray(), CommandFlags.FireAndForget);
        }

        #endregion

        #region private funtion

        /// <summary>
        /// định nghĩa cache outPut voucher
        /// </summary>
        /// <param name="eventOutput"></param>
        /// <param name="searchVoucher"></param>
        /// <returns></returns>
        private async Task<OutputCacheEto> BuildVoucherOutPut(OutputCacheEto eventOutput, string voucherCode, QualifierTypeEnum voucherType)
        {
            try
            {

                var vouchers = await _commonAppService.GetVoucherDefineAsync(new List<string> { voucherCode });
                if (vouchers is { Count: > 0 })
                {
                    eventOutput.Amount = vouchers?.FirstOrDefault().Amount ?? 0;
                    eventOutput.Discount = vouchers?.FirstOrDefault().DiscountPercent ?? 0;
                    eventOutput.MaxDiscount = vouchers?.FirstOrDefault().MaxDiscountAmount ?? 0;
                    eventOutput.VoucherDiscountType = vouchers?.FirstOrDefault().DiscountType.ToString();
                    eventOutput.ApplicableMethod = vouchers?.FirstOrDefault().ApplicableMethodCode;
                }
            }
            catch (Exception e)
            {
                throw new FrtPromotionValidationException(ExceptionCode.BadRequest, e.Message + "GetVoucherRuleDefine");
            }

            return eventOutput;
        }
        
        /// <summary>
        /// get key redis
        /// </summary>
        /// <param name="keyName"></param>
        /// <param name="keyPromotion"></param>
        /// <returns></returns>
        private string GetKeyPrefixCaching(string keyName, string keyPromotion)
        {
            return keyPromotion switch
            {
                "UsePaymentMethod" => $"{_configuration["Redis:KeyPrefix"]}:{keyPromotion}",
                "TotalCost" => $"{_configuration["Redis:KeyPrefix"]}:{keyPromotion}",
                "ItemCode" => $"{_configuration["Redis:KeyPrefixItemCode"]}:{keyName}",

                "Header" => $"{_configuration["Redis:KeyPrefixPromotion"]}:{keyName}:{keyPromotion}",
                "Input" => $"{_configuration["Redis:KeyPrefixPromotion"]}:{keyName}:{keyPromotion}",
                "Output" => $"{_configuration["Redis:KeyPrefixPromotion"]}:{keyName}:{keyPromotion}",
                "Output_InProcess" => $"{_configuration["Redis:KeyPrefixPromotion"]}:{keyName}:{keyPromotion}",
                "OutputReplace" => $"{_configuration["Redis:KeyPrefixPromotion"]}:{keyName}:{keyPromotion}",
                "OutputReplace_InProcess" => $"{_configuration["Redis:KeyPrefixPromotion"]}:{keyName}:{keyPromotion}",
                "ProvinceCondition" => $"{_configuration["Redis:KeyPrefix"]}:{keyPromotion}:{keyName}",
                _ => ""
            };
        }

        #endregion

        #region clear data cache
        private async Task ClearItemCodeCacheInputAsync(List<PromotionTimeExpireEto> promotionTimeExpire)
        {
            #region input && input Replace
            foreach (var promotion in promotionTimeExpire)
            {
                var itemPromotionCode = promotion.PromotionCode;

                //remove cache input reddis
                var inputCacheItems = await _inputCacheItemRepository.GetListInputCacheItemNolockAsync(itemPromotionCode);
                foreach (var input in inputCacheItems)
                {
                    var keyItemCode = GetKeyPrefixCaching(input.ItemCode, "ItemCode");
                    var redisValues = promotionTimeExpire.Select(p => new RedisValue(p.PromotionCode)).ToArray();
                    await _databaseRedisCache.HashDeleteAsync(keyItemCode, redisValues, CommandFlags.FireAndForget);
                }

                //remove cache input Replace reddis
                if (inputCacheItems is { Count: > 0 })
                {
                    var fromSize = 0;
                    var promotionId = inputCacheItems.FirstOrDefault().PromotionId;
                    var totalRecords = await _inputCacheItemReplaceRepository.CountInputCacheItemReplaceNolockAsync(promotionId);
                    while (totalRecords >= 0)
                    {
                        var inputCacheItemReplaces = await _inputCacheItemReplaceRepository.
                            GetListSkipTakeInputCacheItemReplaceNolockAsync(promotionId, fromSize);

                        if (inputCacheItemReplaces.Count == 0)
                        {
                            break;
                        }

                        foreach (var inputReplaces in inputCacheItemReplaces)
                        {
                            var keyItemCodeReplace = GetKeyPrefixCaching(inputReplaces.ItemCode, "ItemCode");
                            var redisValues = promotionTimeExpire.Select(p => new RedisValue(p.PromotionCode)).ToArray();
                            await _databaseRedisCache.HashDeleteAsync(keyItemCodeReplace, redisValues, CommandFlags.FireAndForget);
                        }

                        fromSize += inputCacheItemReplaces.Count;
                        totalRecords -= 5000;
                    }
                }

            }
            #endregion
        }

        /// <summary>
        /// clear data cache
        /// </summary>
        /// <param name="promotionTimeExpire"></param>
        /// <param name="isReCache"></param>
        /// <returns></returns>
        public async Task TimeExpireCacheAsync(List<PromotionTimeExpireEto> promotionTimeExpire, bool isReCache = false)
        {
            if (promotionTimeExpire is { Count: > 0 })
            {
                await ClearItemCodeCacheInputAsync(promotionTimeExpire);

                var redisValuePromotions = new List<RedisKey>();
                var fieldToDelete_TotalCosts = new List<RedisValue>();
                var fieldToDelete_keyUsePaymentMethods = new List<RedisValue>();

                foreach (var promotion in promotionTimeExpire)
                {
                    // cleae key Header
                    var keyCacheHeader = GetKeyPrefixCaching(promotion.PromotionCode, "Header");
                    var headerValue = await _databaseRedisCache.StringGetAsync(keyCacheHeader);
                    redisValuePromotions.Add(keyCacheHeader);

                    // kaffka to ecom promotoin finish InActive
                    //await InactivePromotionSyncEcomAsync(headerValue, promotion.PromotionCode, isReCache);

                    //clear shop condition
                    await ClearCacheShopConditionAsync(promotion.ProvinceConditions, promotion.PromotionCode);

                    // clear key Input
                    redisValuePromotions.Add(GetKeyPrefixCaching(promotion.PromotionCode, "Input"));

                    if (System.Enum.TryParse(promotion.PromotionType, out PromotionTypeEnum promotionTypeEnum))
                    {
                        switch (promotionTypeEnum)
                        {
                            case PromotionTypeEnum.BuyProductGetDiscount:
                            case PromotionTypeEnum.BuyComboGetDiscount:
                            //case PromotionTypeEnum.BuyProductComboGetDiscount:
                                var outPutDiscount = await ClearOutputGetDiscountAsync(promotion, isReCache);
                                redisValuePromotions.AddRange(outPutDiscount.Item1);
                                break;
                            case PromotionTypeEnum.BuyProductGetProduct:
                            case PromotionTypeEnum.BuyComboGetProduct:
                            //case PromotionTypeEnum.BuyProductComboGetProduct:
                                var outPutGetProduct = await ClearOutputGetProductAsync(promotion, isReCache);
                                redisValuePromotions.AddRange(outPutGetProduct.Item1);
                                break;
                            case PromotionTypeEnum.TotalBillAmount:
                            case PromotionTypeEnum.TotalProductAmount:
                            //case PromotionTypeEnum.TotalQuantityProduct:
                                var (valuePromotions, valueTotalCosts) = ClearOutputTotalBill(promotion, isReCache);
                                redisValuePromotions.AddRange(valuePromotions);
                                fieldToDelete_TotalCosts.AddRange(valueTotalCosts);
                                break;
                            case PromotionTypeEnum.UsePaymentMethod:
                                var (valuePromotionsPayment, valueUsePaymentMethods) = ClearOutputUsePaymentMethods(promotion, isReCache);
                                redisValuePromotions.AddRange(valuePromotionsPayment);
                                fieldToDelete_keyUsePaymentMethods.AddRange(valueUsePaymentMethods);
                                break;
                            case PromotionTypeEnum.BuyProductGetAscendingDiscount:
                                redisValuePromotions.Add($"{GetKeyPrefixCaching(promotion.PromotionCode, "Output")}:{promotion.PromotionType}");
                                break;
                        }
                    }
                }

                //remove cache Header,Input,Output, Output replace
                await _databaseRedisCache.KeyDeleteAsync(redisValuePromotions.ToArray(), CommandFlags.FireAndForget);

                //remove cache TotalCost 
                if (fieldToDelete_TotalCosts is { Count: > 0 })
                {
                    var keyTotalCost = GetKeyPrefixCaching("", "TotalCost");
                    await _databaseRedisCache.HashDeleteAsync(keyTotalCost, fieldToDelete_TotalCosts.ToArray(), CommandFlags.FireAndForget);
                }

                //remove cache UsePaymentMethod, 
                if (fieldToDelete_keyUsePaymentMethods is { Count: > 0 })
                {
                    var keyUsePaymentMethod = GetKeyPrefixCaching("", "UsePaymentMethod");
                    await _databaseRedisCache.HashDeleteAsync(keyUsePaymentMethod, fieldToDelete_keyUsePaymentMethods.ToArray(), CommandFlags.FireAndForget);
                }
            }

            foreach (var promotion in promotionTimeExpire)
            {
                // delete cache item input in db
                await DeleteInputCacheItemAsync(promotion.PromotionId);

                // delete cache item output in db
                await DeleteOutPutCacheItemAsync(promotion.PromotionId, promotion.PromotionType);
            }
        }

        /// <summary>
        /// clear out giảm giá sản phẩm
        /// </summary>
        /// <param name="promotion"></param>
        /// <param name="isReCache"></param>
        /// <returns></returns>
        private async Task<Tuple<List<RedisKey>>> ClearOutputGetDiscountAsync(PromotionTimeExpireEto promotion, bool isReCache = false)
        {
            var keySuccessDelete = new List<string>();
            var redisValuePromotions = new List<RedisKey>();

            if (isReCache == false)//key Output discount,GetItem // nếu là job update status InActive //không phải recache thì xóa luôn ko lên bản tạm
            {
                var keyCacheOutput = $"{GetKeyPrefixCaching(promotion.PromotionCode, "Output")}:{promotion.PromotionType}";
                redisValuePromotions.Add(keyCacheOutput);
            }

            //delete OutputReplace giảm giá  sản phẩm
            var discountItemOutput = await _discountItemOutputRepository.GetListAsync(p => p.PromotionId == promotion.PromotionId && p.OutPutType != "OutputReplace");
            if (discountItemOutput is { Count: > 0 })
            {
                foreach (var outPut in discountItemOutput)
                {
                    var keyOutputReplace = $"{GetKeyPrefixCaching(outPut.PromotionCode, "OutputReplace")}:{outPut.ItemCode}";
                    if (isReCache)
                    {
                        keySuccessDelete.Add(keyOutputReplace);
                    }
                    else // nếu là job update status InActive => delete
                    {
                        redisValuePromotions.Add(keyOutputReplace);
                    }
                }
            }

            //cache lên bản tạm để xóa sau khi end process
            if (isReCache)
            {
                await _databaseRedisCache.StringSetAsync($"{_keyOutputReplaceDeleteSuccess}:{promotion.PromotionCode}",
                    JsonConvert.SerializeObject(keySuccessDelete), null, (When)CommandFlags.FireAndForget);
            }

            return new Tuple<List<RedisKey>>(redisValuePromotions);
        }

        /// <summary>
        /// clear out tặng sản phẩm
        /// </summary>
        /// <param name="promotion"></param>
        /// <param name="isReCache"></param>
        /// <returns></returns>
        private async Task<Tuple<List<RedisKey>>> ClearOutputGetProductAsync(PromotionTimeExpireEto promotion, bool isReCache = false)
        {
            var keySuccessDelete = new List<string>();
            var redisValuePromotions = new List<RedisKey>();

            if (isReCache == false)//key Output discount,GetItem // nếu là job update status InActive //không phải recache thì xóa luôn ko lên bản tạm
            {
                var keyCacheOutput = $"{GetKeyPrefixCaching(promotion.PromotionCode, "Output")}:{promotion.PromotionType}";
                redisValuePromotions.Add(keyCacheOutput);
            }

            //delete OutputReplace giảm giá  sản phẩm
            var getItemOutput = await _getItemOutputRepository.GetListAsync(p => p.PromotionId == promotion.PromotionId && p.OutPutType != "OutputReplace");
            if (getItemOutput is { Count: > 0 })
            {
                foreach (var outPut in getItemOutput)
                {
                    var keyOutputReplace = $"{GetKeyPrefixCaching(outPut.PromotionCode, "OutputReplace")}:{outPut.ItemCode}";
                    if (isReCache)
                    {
                        keySuccessDelete.Add(keyOutputReplace);
                    }
                    else // nếu là job update status InActive => delete
                    {
                        redisValuePromotions.Add(keyOutputReplace);
                    }
                }
            }

            //cache lên bản tạm để xóa sau khi end process
            if (isReCache)
            {
                await _databaseRedisCache.StringSetAsync($"{_keyOutputReplaceDeleteSuccess}:{promotion.PromotionCode}",
                    JsonConvert.SerializeObject(keySuccessDelete), null, (When)CommandFlags.FireAndForget);
            }

            return new Tuple<List<RedisKey>>(redisValuePromotions);
        }

        /// <summary>
        /// clear out tổng tiền đơn hàng
        /// </summary>
        /// <param name="promotion"></param>
        /// <param name="isReCache"></param>
        /// <returns></returns>
        private Tuple<List<RedisKey>, List<RedisValue>> ClearOutputTotalBill(PromotionTimeExpireEto promotion, bool isReCache = false)
        {
            var totalCosts = new List<RedisValue>();
            var redisValuePromotions = new List<RedisKey>();

            //nếu là job update status InActive //không phải recache thì xóa luôn ko lên bản tạm
            if (isReCache == false)
            {
                redisValuePromotions.Add($"{GetKeyPrefixCaching(promotion.PromotionCode, "Output")}:{promotion.PromotionType}");
            }
            //else//cache lên bản tạm để xóa sau khi end process
            //{
            //    await _databaseRedisCache.StringSetAsync($"{_keyOutputReplaceDeleteSuccess}:{promotion.PromotionCode}",
            //       JsonConvert.SerializeObject(keySuccessDelete), null, (When)CommandFlags.FireAndForget);
            //}    

            totalCosts.Add(promotion.PromotionCode);

            return new Tuple<List<RedisKey>, List<RedisValue>>(redisValuePromotions, totalCosts);
        }

        /// <summary>
        /// clear out phương thức thanh toán
        /// </summary>
        /// <param name="promotion"></param>
        /// <param name="isReCache"></param>
        /// <returns></returns>
        private Tuple<List<RedisKey>, List<RedisValue>> ClearOutputUsePaymentMethods(PromotionTimeExpireEto promotion, bool isReCache = false)
        {
            var redisValuePromotions = new List<RedisKey>();
            var keyUsePaymentMethods = new List<RedisValue>();

            //key Output discount,GetItem // nếu là job update status InActive //không phải recache thì xóa luôn ko lên bản tạm
            if (isReCache == false)
            {
                var keyCacheOutput = $"{GetKeyPrefixCaching(promotion.PromotionCode, "Output")}:{promotion.PromotionType}";
                redisValuePromotions.Add(keyCacheOutput);
            }

            keyUsePaymentMethods.Add(promotion.PromotionCode);

            return new Tuple<List<RedisKey>, List<RedisValue>>(redisValuePromotions, keyUsePaymentMethods);
        }

        /// <summary>
        /// clear shop condition
        /// </summary>
        /// <param name="shopCondition"></param>
        /// <param name="promotionCode"></param>
        /// <returns></returns>
        private async Task ClearCacheShopConditionAsync(List<string> provinceConditions, string promotionCode)
        {
            //Delete RegionCondition
            foreach (var keyRegionCondition in provinceConditions
                .Select(shopCondition => GetKeyPrefixCaching(shopCondition, "ProvinceCondition")))
            {
                await _databaseRedisCache.HashDeleteAsync(keyRegionCondition, promotionCode, CommandFlags.FireAndForget);
            }
        }

        #endregion

        #region insert db InputCacheItem, InputCacheItemReplace

        public async Task<Guid> InsertInputCacheItemAsync(EventInputCacheEto inputCache)
        {
            if (!inputCache.InputCache.ItemCode.IsNullOrEmpty())
            {
                var inputCacheItem = new InputCacheItem(_guidGenerator.Create())
                {
                    ItemCode = inputCache.InputCache.ItemCode,
                    InputItemId = inputCache.InputCache.Id,
                    Quantity = inputCache.InputCache.Quantity,
                    PromotionId = inputCache.InputCache.PromotionId,
                    PromotionCode = inputCache.InputCache.PromotionCode,
                    UnitCode = inputCache.InputCache.UnitCode.ToString(),
                };
                await _inputCacheItemRepository.InsertAsync(inputCacheItem);
                return inputCacheItem.Id;

            }
            else // GiftCode
            {
                var inputCacheItem = new InputCacheItem(_guidGenerator.Create())
                {
                    GiftCode = inputCache.InputCache.GiftCode,
                    InputItemId = inputCache.InputCache.Id,
                    Quantity = inputCache.InputCache.Quantity,
                    PromotionId = inputCache.InputCache.PromotionId,
                    PromotionCode = inputCache.InputCache.PromotionCode,
                };
                await _inputCacheItemRepository.InsertAsync(inputCacheItem);
                return inputCacheItem.Id;
            }
        }

        public async Task InsertInputCacheItemReplaceAsync(List<ProductCache> products, EventInputCacheEto inputCache)
        {
            var inputCacheItemReplaces = new List<InputCacheItemReplace>();
            if (products is { Count: > 0 })
            {
                inputCacheItemReplaces.AddRange(products
                    .Where(p => !p.ItemCode.IsNullOrEmpty())
                    .Select(p => new InputCacheItemReplace(_guidGenerator.Create())
                    {
                        ItemCode = p.ItemCode,
                        Quantity = inputCache.InputCache.Quantity,
                        PromotionId = inputCache.InputCache.PromotionId,
                        PromotionCode = inputCache.InputCache.PromotionCode,
                        InputCacheItemId = inputCache.IdInput,
                        UnitCode = inputCache.InputCache.UnitCode.ToString(),
                    }).ToList());
            }
            else
            {
                if (!inputCache.InputCache.ItemCode.IsNullOrEmpty())
                {
                    inputCacheItemReplaces.Add(
                        new InputCacheItemReplace(_guidGenerator.Create())
                        {
                            ItemCode = inputCache.InputCache.ItemCode,
                            Quantity = inputCache.InputCache.Quantity,
                            PromotionId = inputCache.InputCache.PromotionId,
                            PromotionCode = inputCache.InputCache.PromotionCode,
                            InputCacheItemId = inputCache.IdInput
                        });
                }
                else //GiftCode
                {
                    inputCacheItemReplaces.Add(
                        new InputCacheItemReplace(_guidGenerator.Create())
                        {
                            GiftCode = inputCache.InputCache.GiftCode,
                            Quantity = inputCache.InputCache.Quantity,
                            PromotionId = inputCache.InputCache.PromotionId,
                            PromotionCode = inputCache.InputCache.PromotionCode,
                            InputCacheItemId = inputCache.IdInput
                        });
                }
            }

            if (inputCacheItemReplaces is { Count: > 0 })
            {
                inputCacheItemReplaces = inputCacheItemReplaces.Where(p => !p.ItemCode.IsNullOrEmpty()).Distinct().ToList();
                await _inputCacheItemReplaceRepository.BulkCopyAsync(inputCacheItemReplaces);
            }
        }

        public async Task DeleteInputCacheItemAsync(Guid promotionId)
        {
            await _inputCacheItemRepository.BulkDeleteAsync(promotionId);
            await _inputCacheItemReplaceRepository.BulkDeleteAsync(promotionId);
        }

        public async Task DeleteOutPutCacheItemAsync(Guid promotionId, string type)
        {
            if (System.Enum.TryParse(type, out PromotionTypeEnum promotionType))
            {
                switch (promotionType)
                {
                    case PromotionTypeEnum.BuyProductGetProduct:
                    case PromotionTypeEnum.BuyComboGetProduct:
                        await _discountItemOutputRepository.BulkDeleteAsync(promotionId);
                        break;
                    case PromotionTypeEnum.BuyProductGetDiscount:
                    case PromotionTypeEnum.BuyComboGetDiscount:
                        await _discountItemOutputRepository.BulkDeleteAsync(promotionId);
                        break;
                    case PromotionTypeEnum.BuyProductGetAscendingDiscount:
                        await _getAscendingDiscountOutPutRepository.BulkDeleteAsync(promotionId);
                        break;
                    case PromotionTypeEnum.UsePaymentMethod:
                        var usePaymentMethod = await _paymentMethodOutputRepository.GetListAsync(p => p.PromotionId == promotionId);
                        await _paymentMethodOutputRepository.DeleteManyAsync(usePaymentMethod);
                        break;
                }
            }

        }

        #endregion

    }
}
