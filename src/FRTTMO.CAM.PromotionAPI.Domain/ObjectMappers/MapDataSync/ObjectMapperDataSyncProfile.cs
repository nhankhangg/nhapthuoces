﻿using Volo.Abp.ObjectMapping;
using FRTTMO.CAM.PromotionAPI.Etos;
using FRTTMO.CAM.PromotionAPI.ETOs.Cache;
using Volo.Abp.DependencyInjection;

namespace FRTTMO.CAM.PromotionAPI.ObjectMappers.MapDataSync
{
    public class ObjectMapperDataSyncProfile :
        IObjectMapper<CacheValuePromotionHeader, PromotionSyncDetailMessagesEto>,
        IObjectMapper<EventPromotionSyncEto, PromotionSyncDetailMessagesEto>,
        ITransientDependency
    {
        public PromotionSyncDetailMessagesEto Map(CacheValuePromotionHeader source)
        {
            return Map(source, new PromotionSyncDetailMessagesEto());
        }

        public PromotionSyncDetailMessagesEto Map(CacheValuePromotionHeader source, PromotionSyncDetailMessagesEto destination)
        {
            destination.FromDate = source.FromDate;
            destination.ToDate = source.ToDate;
            destination.FromHour = source.FromHour;
            destination.ToHour = source.ToHour;
            destination.UrlImage = source.UrlImage;
            destination.UrlPage = source.UrlPage;
            destination.Priority = source.Priority;
            destination.PromotionCode = source.Code;
            destination.PromotionName = source.Name;
            destination.NameOnline = source.NameOnline;
            destination.Description = source.Description;
            destination.PromotionType = source.PromotionType;
            destination.DateDisplay = source.FromDate; // ngày hiển thị
            destination.ActiveDate = source.ActiveDate;// ngày áp dụng
            destination.Channels = source.Channels;
            destination.StoreTypes = source.StoreTypes;
            destination.Status = source.Status;
            destination.PromotionId = source.Id;
            destination.ProgramType = source.ProgramType;
            destination.AllowShowOnline = source.AllowShowOnline;
            destination.QuotaQuantity = source.Quota?.Quantity;
            destination.ShortDescription = source.ShortDescription;
            destination.IsBoom = source.IsBoom;
            destination.IsShowDetail = source.IsShowDetail;
            destination.InstallmentConditions = source.InstallmentConditions;
            destination.PromotionExcludes = source.PromotionExcludes;
            destination.DescriptionBoom = source.DescriptionBoom;
            destination.OrderTypes = source.OrderTypes;
            destination.StoreTypes = source.StoreTypes;
            destination.ItemInputExcludes = source.ItemInputExcludes;
            destination.SourceOrders = source.SourceOrders;
            destination.CustomerGroups = source.CustomerGroups;
            destination.AmountCondition = source.AmountCondition;
            destination.CostDistributions = source.CostDistributions;
            destination.ExtraConditions = source.ExtraConditions;
            destination.PaymentCondition = source.PaymentConditions;
            return destination;
        }

        public PromotionSyncDetailMessagesEto Map(EventPromotionSyncEto source)
        {
            return Map(source, new PromotionSyncDetailMessagesEto());
        }

        public PromotionSyncDetailMessagesEto Map(EventPromotionSyncEto source, PromotionSyncDetailMessagesEto destination)
        {
            destination.Id = source.SyncId;
            destination.Action = source.ActionType;
            destination.Note = source.OutputNote;
            destination.NoteBoom = source.NoteBoom;
            destination.DiscountPlaceCode = source.DiscountPlaceCode;
            destination.DiscountType = source.QualifierType;
            destination.Quantity = source.Quantity;
            destination.MaxQuantity = source.MaxQuantity;
            destination.ItemCode = source.ItemCode;
            destination.ItemName = source.ItemName;
            destination.CouponCode = source.CouponCode;
            destination.CouponName = source.CouponName;
            destination.GiftCode = source.GiftCode;
            destination.GiftName = source.GiftName;
            destination.PaymentMethodCode = source.PaymentMethodCode;
            destination.PaymentMethodName = source.PaymentMethodName;
            destination.DiscountType = source.QualifierType;
            destination.MaxDiscountAmount = source.MaxDiscount;
            destination.FlagGiftCode = source.FlagGiftCode;
            destination.FlagGiftCodeOutput = source.FlagGiftCodeOutput;
            destination.FlagCouponCodeOutput = source.FlagCouponCodeOutput;
            destination.WarehouseCode = source.WarehouseCode;
            destination.WarehouseName = source.WarehouseName;
            return destination;
        }
    }
}
