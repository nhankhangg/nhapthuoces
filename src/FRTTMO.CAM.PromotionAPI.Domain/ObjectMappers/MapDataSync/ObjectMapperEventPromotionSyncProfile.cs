﻿using Volo.Abp.ObjectMapping;
using FRTTMO.CAM.PromotionAPI.Etos;
using Volo.Abp.DependencyInjection;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;

namespace FRTTMO.CAM.PromotionAPI.ObjectMappers.MapDataSync
{
    public class ObjectMapperEventPromotionSyncProfile : 
        IObjectMapper<OutputCacheEto, EventPromotionSyncEto>,
        IObjectMapper<ItemInputCacheEto, EventPromotionSyncEto>, ITransientDependency
    {
        public EventPromotionSyncEto Map(OutputCacheEto source)
        {
            return Map(source, new EventPromotionSyncEto());
        }

        public EventPromotionSyncEto Map(OutputCacheEto source, EventPromotionSyncEto destination)
        {
            destination.Discount = source.Discount;
            destination.ItemCode = source.ItemCode;
            destination.ItemName = source.ItemName;
            destination.GiftCode = source.GiftCode;
            destination.GiftName = source.GiftName;
            destination.CouponCode = source.CouponCode;
            destination.CouponName = source.CouponName;
            destination.ActionType = source.ActionTypeSync;
            destination.Quantity = source.Quantity;
            destination.MaxQuantity = source.MaxQuantity;
            destination.MaxDiscount = source.MaxDiscount;
            destination.PromotionCode = source.PromotionCode;
            destination.QualifierType = source.QualifierCode;
            destination.PromotionType = source.PromotionType;
            destination.PaymentMethodCode = source.PaymentMethodCode;
            destination.PaymentMethodName = source.PaymentMethodName;
            destination.OutputNote = source.Note;
            destination.NoteBoom = source.NoteBoom;
            destination.DiscountPlaceCode = source.DiscountPlaceCode;
            destination.FlagGiftCode = source.FlagGiftCode;
            destination.FlagGiftCodeOutput = source.FlagGiftCodeOutput;
            destination.FlagCouponCodeOutput = source.FlagCouponCodeOutput;
            destination.WarehouseCode = source.WarehouseCode;
            destination.WarehouseName = source.WarehouseName;
            return destination;
        }

        public EventPromotionSyncEto Map(ItemInputCacheEto source)
        {
            return Map(source, new EventPromotionSyncEto());
        }

        public EventPromotionSyncEto Map(ItemInputCacheEto source, EventPromotionSyncEto destination)
        {
  
            destination.Discount = source.Discount;
            destination.ItemCode = source.ItemCode;
            destination.ItemName = source.ItemName;
            destination.GiftCode = source.GiftCode;
            destination.GiftName = source.GiftName;
            destination.CouponCode = source.CouponCode;
            destination.CouponName = source.CouponName;
            destination.ActionType = source.ActionTypeSync;
            destination.Quantity = source.Quantity;
            destination.MaxQuantity = source.MaxQuantity;
            destination.MaxDiscount = source.MaxDiscount;
            destination.PromotionCode = source.PromotionCode;
            destination.QualifierType = source.QualifierCode;
            destination.PromotionType = source.PromotionType;
            destination.PaymentMethodCode = source.PaymentMethodCode;
            destination.PaymentMethodName = source.PaymentMethodName;
            destination.OutputNote = source.Note;
            destination.NoteBoom = source.NoteBoom;
            destination.NoteBoom = source.NoteBoom;
            destination.DiscountPlaceCode = source.DiscountPlaceCode;
            destination.FlagGiftCode = source.FlagGiftCode;
            destination.FlagGiftCodeOutput = source.FlagGiftCodeOutput;
            destination.FlagCouponCodeOutput = source.FlagCouponCodeOutput;
            destination.WarehouseCode = source.WarehouseCode;
            destination.WarehouseName = source.WarehouseName;
            return destination;
        }
    }
}
