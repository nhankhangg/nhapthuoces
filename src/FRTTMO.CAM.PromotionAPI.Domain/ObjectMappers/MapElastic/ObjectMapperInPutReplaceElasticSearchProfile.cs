﻿using System;
using System.Linq;
using Volo.Abp.ObjectMapping;
using Volo.Abp.DependencyInjection;
using FRTTMO.CAM.PromotionAPI.DTOs.PIM;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;
using FRTTMO.CAM.PromotionAPI.Documents.PromotionInputReplaces;
using FRTTMO.CAM.PromotionAPI.Enum;

namespace FRTTMO.CAM.PromotionAPI.ObjectMappers.MapElastic
{
    public class ObjectMapperInputReplaceElasticSearchProfile :
       IObjectMapper<EventInputCacheEto, InputReplaceDocument>,
       IObjectMapper<ProductCache, InputReplaceDocument>,
       ITransientDependency
    {
        public InputReplaceDocument Map(EventInputCacheEto source)
        {
            return Map(source, new InputReplaceDocument());
        }

        public InputReplaceDocument Map(EventInputCacheEto source, InputReplaceDocument destination)
        {
            destination.InputReplaceId = source.InputCache.Id;
            destination.PromotionId = source.InputCache.PromotionId;
            destination.PromotionCode = source.InputCache.PromotionCode;
            destination.Quantity = source.InputCache.Quantity;
            destination.CategoryCode = source.InputCache.CategoryCode;
            destination.CategoryName = source.InputCache.CategoryName;
            destination.GroupCode = source.InputCache.GroupCode;
            destination.GroupName = source.InputCache.GroupName;
            destination.BrandCode = source.InputCache.BrandCode;
            destination.BrandName = source.InputCache.BrandName;
            destination.ModelCode = source.InputCache.ModelCode;
            destination.ModelName = source.InputCache.ModelName;
            destination.TypeCode = source.InputCache.TypeCode;
            destination.TypeName = source.InputCache.TypeName;
            destination.ItemCode = source.InputCache.ItemCode;
            destination.ItemName = source.InputCache.ItemName;
            destination.Quantity = source.InputCache.Quantity;
            destination.UnitCode = source.InputCache.UnitCode;
            destination.UnitName = source.InputCache.UnitName;
            destination.GiftCode = source.InputCache.GiftCode;
            destination.GiftName = source.InputCache.GiftName;
            destination.CouponCode = source.InputCache.CouponCode;
            destination.CouponName = source.InputCache.CouponName;
            destination.WarehouseCode = source.InputCache.WarehouseCode;
            destination.WarehouseName = source.InputCache.WarehouseName;
            destination.CreatedByName = source.InputCache.CreatedByName;
            destination.CreationTime = source.InputCache.CreationTime;
            destination.UpdateByName = source.InputCache.UpdateByName;
            destination.LastModificationTime = source.InputCache.LastModificationTime;
            var id = $"{source.InputCache.PromotionCode}";
            if (!destination.ItemCode.IsNullOrEmpty())
            {
                id += $":ItemCode:{destination.ItemCode}";
            }
            if (!destination.GiftCode.IsNullOrEmpty())
            {
                id += $":GiftCode:{destination.GiftCode}";
            }
            destination.Id = id;
            return destination;
        }
        
        public InputReplaceDocument Map(ProductCache source)
        {
            return Map(source, new InputReplaceDocument());
        }

        public InputReplaceDocument Map(ProductCache source, InputReplaceDocument destination)
        {
            destination.ItemCode =  source?.ItemCode;
            destination.ItemName =  source?.ItemName;
            destination.CategoryCode = source?.CategoryUniqueId;
            destination.CategoryName = source?.CategoryName;
            destination.TypeCode = source?.GroupUniqueId; //=> Group pim cam lc là loại hàng
            destination.TypeName = source?.GroupName; //=> Group pim cam lc là loại hàng
            var id = $"{destination.PromotionCode}";
            if (!destination.ItemCode.IsNullOrEmpty())
            {
                id += $":ItemCode:{destination.ItemCode}";
            }
            if (!destination.GiftCode.IsNullOrEmpty())
            {
                id += $":GiftCode:{destination.GiftCode}";
            }
            destination.Id = id;
            return destination;
        }
    }
}
