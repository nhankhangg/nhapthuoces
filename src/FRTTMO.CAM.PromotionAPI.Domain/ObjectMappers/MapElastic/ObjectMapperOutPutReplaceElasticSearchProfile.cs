﻿using System;
using System.Linq;
using Volo.Abp.ObjectMapping;
using Volo.Abp.DependencyInjection;
using FRTTMO.CAM.PromotionAPI.DTOs.PIM;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;
using FRTTMO.CAM.PromotionAPI.Documents.PromotionOutputReplaces;
using FRTTMO.CAM.PromotionAPI.Enum;

namespace FRTTMO.CAM.PromotionAPI.ObjectMappers.MapElastic
{
    public class ObjectMapperOutPutReplaceElasticSearchProfile :
       IObjectMapper<OutputCacheEto, OutputReplaceDocument>,
       IObjectMapper<ProductCache, OutputReplaceDocument>,
       ITransientDependency
    {
        public OutputReplaceDocument Map(OutputCacheEto source)
        {
            return Map(source, new OutputReplaceDocument());
        }

        public OutputReplaceDocument Map(OutputCacheEto source, OutputReplaceDocument destination)
        {
            destination.OutputReplaceId = source.Id;
            destination.PromotionId = source.PromotionId;
            destination.PromotionCode = source.PromotionCode;
            destination.CategoryCode = source.CategoryCode;
            destination.CategoryName = source.CategoryName;
            destination.GroupCode = source.GroupCode;
            destination.GroupName = source.GroupName;
            destination.BrandCode = source.BrandCode;
            destination.BrandName = source.BrandName;
            destination.ModelCode = source.ModelCode;
            destination.ModelName = source.ModelName;
            destination.TypeCode = source.TypeCode;
            destination.TypeName = source.TypeName;
            destination.ItemCode = source.ItemCode;
            destination.ItemName = source.ItemName;
            destination.Quantity = source.Quantity;
            destination.UnitCode = source.UnitCode;
            destination.UnitName = source.UnitName;
            destination.GiftCode = source.GiftCode;
            destination.GiftName = source.GiftName;
            destination.CouponCode = source.CouponCode;
            destination.CouponName = source.CouponName;
            destination.WarehouseCode = source.WarehouseCode;
            destination.WarehouseName = source.WarehouseName;
            destination.QualifierCode = source.QualifierCode;
            destination.Quantity = source.Quantity;
            destination.MinQuantity = source.MinQuantity;
            destination.MaxQuantity = source.MaxQuantity;
            destination.MaxValue = source.MaxValue;
            destination.Discount = source.Discount;
            destination.SchemeCode = source.SchemeCode;
            destination.SchemeName = source.SchemeName;
            destination.PaymentMethodCode = source.PaymentMethodCode;
            destination.PaymentMethodName = source.PaymentMethodName;
            destination.Note = source.Note;
            destination.NoteBoom = source.NoteBoom;
            destination.DiscountPlaceCode = source.DiscountPlaceCode;
            destination.CreatedByName = source.CreatedByName;
            destination.CreationTime = source.CreationTime;
            destination.UpdateByName = source.UpdateByName;
            destination.LastModificationTime = source.LastModificationTime;
            
            var id = $"{source.PromotionCode}";
            if (!destination.ItemCode.IsNullOrEmpty())
            {
                id += $":ItemCode:{destination.ItemCode}";
            }
            if (!destination.GiftCode.IsNullOrEmpty())
            {
                id += $":GiftCode:{destination.GiftCode}";
            }
            if (!destination.CouponCode.IsNullOrEmpty())
            {
                id += $":CouponCode:{destination.CouponCode}";
            }
            destination.Id = id;
            return destination;
        }

        public OutputReplaceDocument Map(ProductCache source)
        {
            return Map(source, new OutputReplaceDocument());
        }

        public OutputReplaceDocument Map(ProductCache source, OutputReplaceDocument destination)
        {
            destination.ItemCode =  source?.ItemCode;
            destination.ItemName =  source?.ItemName;
            destination.CategoryCode = source?.CategoryUniqueId;
            destination.CategoryName = source?.CategoryName;
            destination.TypeCode = source?.GroupUniqueId; //=> Group pim cam lc là loại hàng
            destination.TypeName = source?.GroupName; //=> Group pim cam lc là loại hàng
            var id = $"{destination.PromotionCode}";
            if (!destination.ItemCode.IsNullOrEmpty())
            {
                id += $":ItemCode:{destination.ItemCode}";
            }
            if (!destination.GiftCode.IsNullOrEmpty())
            {
                id += $":GiftCode:{destination.GiftCode}";
            }
            if (!destination.CouponCode.IsNullOrEmpty())
            {
                id += $":CouponCode:{destination.CouponCode}";
            }
            destination.Id = id;
            return destination;
        }

  
    }
}
