﻿using Volo.Abp.Guids;
using Volo.Abp.ObjectMapping;
using Volo.Abp.DependencyInjection;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;
using FRTTMO.CAM.PromotionAPI.Entities.OutputIntegration;

namespace FRTTMO.CAM.PromotionAPI.ObjectMappers.MapEntities
{
    class ObjectMapperEntitiesDiscountItemOutputProfile :
        IObjectMapper<OutputCacheEto, DiscountItemOutput>,
        ITransientDependency
    {
        private readonly IGuidGenerator _guidGenerator;
        public ObjectMapperEntitiesDiscountItemOutputProfile(IGuidGenerator guidGenerator)
        {
            _guidGenerator = guidGenerator;
        }

        public DiscountItemOutput Map(OutputCacheEto source)
        {
            return Map(source, new DiscountItemOutput(_guidGenerator.Create()));
        }

        public DiscountItemOutput Map(OutputCacheEto source, DiscountItemOutput destination)
        {
            destination.ItemCode = source.ItemCode;
            destination.ItemName = source.ItemName;
            destination.UnitCode = source.UnitCode;
            destination.UnitName = source.UnitName;
            destination.PromotionCode = source.PromotionCode;
            destination.PromotionId = source.PromotionId;
            destination.MinQuantity = source.MinQuantity;
            destination.MaxQuantity = source.MaxQuantity;
            destination.Discount = source.Discount;
            destination.MaxDiscount = source.MaxDiscount;
            destination.DiscountType = source.QualifierCode;
            destination.OutPutNote = source.Note;
            destination.NoteBoom = source.NoteBoom;
            destination.PromotionType = source.PromotionType;
            return destination;
        }

    }
}
