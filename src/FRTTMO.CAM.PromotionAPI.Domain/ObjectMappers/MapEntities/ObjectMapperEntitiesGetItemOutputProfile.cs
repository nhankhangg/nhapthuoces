﻿using FRTTMO.CAM.PromotionAPI.Entities.OutputIntegration;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Guids;
using Volo.Abp.ObjectMapping;

namespace FRTTMO.CAM.PromotionAPI.ObjectMappers.MapEvent
{
    public class ObjectMapperEntitiesGetItemOutputProfile :
        IObjectMapper<OutputCacheEto, GetItemOutput>,
        ITransientDependency
    {
        private readonly IGuidGenerator _guidGenerator;
        public ObjectMapperEntitiesGetItemOutputProfile(IGuidGenerator guidGenerator)
        {
            _guidGenerator = guidGenerator;
        }

        public GetItemOutput Map(OutputCacheEto source)
        {
            return Map(source, new GetItemOutput(_guidGenerator.Create()));
        }

        public GetItemOutput Map(OutputCacheEto source, GetItemOutput destination)
        {
            destination.ItemCode = source.ItemCode;
            destination.ItemName = source.ItemName;
            destination.VoucherCode = source.CouponCode ?? source.GiftCode;
            destination.PromotionId = source.PromotionId;
            destination.PromotionCode = source.PromotionCode;
            destination.Quantity = source.Quantity;
            destination.MaxQuantity = source.MaxQuantity;
            destination.UnitCode = source.UnitCode;
            destination.UnitName = source.UnitName;
            destination.WarehouseCode = source.WarehouseCode;
            destination.OutPutNote = source.Note;
            destination.PromotionType = source.PromotionType;
            destination.QualifierCode =  source.QualifierCode;
            destination.NoteBoom = source.NoteBoom;
            return destination;
        }
    }
}
