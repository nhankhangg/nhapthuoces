﻿using FRTTMO.CAM.PromotionAPI.ETOs.Cache;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;
using Volo.Abp.DependencyInjection;
using Volo.Abp.ObjectMapping;

namespace FRTTMO.CAM.PromotionAPI.ObjectMappers.MapInPutCache
{
    public class ObjectMapperCachingForInputProfile :
        IObjectMapper<EventInputCacheEto, CacheValueInput>,
        ITransientDependency
    {
        public CacheValueInput Map(EventInputCacheEto source)
        {
            return Map(source, new CacheValueInput());
        }

        public CacheValueInput Map(EventInputCacheEto source, CacheValueInput destination)
        {
            destination.ItemCode = source.InputCache.ItemCode;
            destination.ItemName = source.InputCache.ItemName;
            destination.Quantity = source.InputCache.Quantity;
            destination.UnitCode = source.InputCache.UnitCode;
            destination.UnitName = source.InputCache.UnitName;
            destination.GiftCode = source.InputCache.GiftCode;
            destination.WarehouseCode = source.InputCache.WarehouseCode;
            destination.FlagDiscount = source.InputCache.IsDiscount;
            return destination;
        }
    }
}
