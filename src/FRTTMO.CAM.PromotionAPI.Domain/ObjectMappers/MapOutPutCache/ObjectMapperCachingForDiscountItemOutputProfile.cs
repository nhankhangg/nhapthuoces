﻿using FRTTMO.CAM.PromotionAPI.ETOs.Cache;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;
using Volo.Abp.DependencyInjection;
using Volo.Abp.ObjectMapping;

namespace FRTTMO.CAM.PromotionAPI.ObjectMappers.MapOutPutCache
{
    public class ObjectMapperCachingForDiscountItemOutputProfile : 
        IObjectMapper<OutputCacheEto, CacheValueDiscountOutPut>,
        ITransientDependency
    {
        public CacheValueDiscountOutPut Map(OutputCacheEto source)
        {
            return Map(source, new CacheValueDiscountOutPut());
        }

        public CacheValueDiscountOutPut Map(OutputCacheEto source, CacheValueDiscountOutPut destination)
        {
            destination.ItemCode = source.ItemCode;
            destination.ItemName = source.ItemName;
            destination.PromotionId = source.PromotionId;
            destination.MinQuantity = source.MinQuantity;
            destination.MaxQuantity = source.MaxQuantity;
            destination.DiscountType = source.QualifierCode;
            destination.Discount = source.Discount;
            destination.MaxDiscount = source.MaxDiscount;
            destination.Note = source.Note;
            destination.UnitCode = source.UnitCode;
            destination.UnitName = source.UnitName;
            destination.NoteBoom = source.NoteBoom;
            destination.DiscountPlaceCode = source.DiscountPlaceCode;
            return destination;
        }
    }
}
 