﻿using System;
using FRTTMO.CAM.PromotionAPI.ETOs.Cache;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;
using Volo.Abp.DependencyInjection;
using Volo.Abp.ObjectMapping;

namespace FRTTMO.CAM.PromotionAPI.ObjectMappers.MapOutPutCache
{
    public class ObjectMapperCachingForGetItemOutputProfile :
        IObjectMapper<OutputCacheEto, CacheValueGetItemOutPut>,
        ITransientDependency
    {
       
        public CacheValueGetItemOutPut Map(OutputCacheEto source)
        {
            return Map(source, new CacheValueGetItemOutPut());
        }

        public CacheValueGetItemOutPut Map(OutputCacheEto source, CacheValueGetItemOutPut destination)
        {
            destination.PromotionId = source.PromotionId;
            destination.ItemCode = source.ItemCode;
            destination.ItemName = source.ItemName;
            destination.UnitCode = source.UnitCode;
            destination.UnitName = source.UnitName;
            destination.WarehouseCode = source.WarehouseCode;
            destination.Quantity = source.Quantity;
            destination.MaxQuantity = source.MaxQuantity;
            destination.VoucherCode = source.CouponCode.IsNullOrEmpty() ? source.GiftCode : source.CouponCode;
            destination.Type = source.QualifierCode;
            destination.DiscountPlaceCode = source.DiscountPlaceCode;
            return destination;
        }

    }
}
