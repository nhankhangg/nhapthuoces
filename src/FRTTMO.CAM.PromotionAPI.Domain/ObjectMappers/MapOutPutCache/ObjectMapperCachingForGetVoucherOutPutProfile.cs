﻿using FRTTMO.CAM.PromotionAPI.ETOs.Cache;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.DependencyInjection;
using Volo.Abp.ObjectMapping;

namespace FRTTMO.CAM.PromotionAPI.ObjectMappers.MapOutPutCache
{
    public class ObjectMapperCachingForGetVoucherOutPutProfile :
        IObjectMapper<OutputCacheEto, CacheValueGetVoucherOutPut>,
        ITransientDependency
    {

        public CacheValueGetVoucherOutPut Map(OutputCacheEto source)
        {
            return Map(source, new CacheValueGetVoucherOutPut());
        }

        public CacheValueGetVoucherOutPut Map(OutputCacheEto source, CacheValueGetVoucherOutPut destination)
        {
            destination.PromotionId = source.PromotionId;
            destination.ItemCode = source.ItemCode;
            destination.ItemName = source.ItemName;
            destination.UnitCode = source.UnitCode;
            destination.UnitName = source.UnitName;
            destination.WarehouseCode = source.WarehouseCode;
            destination.Quantity = source.Quantity;
            destination.MaxQuantity = source.MaxQuantity;
            destination.VoucherCode = source.CouponCode.IsNullOrEmpty() ? source.GiftCode : source.CouponCode;
            destination.Type = source.QualifierCode;

            //voucher
            destination.Amount = source.Amount;
            destination.Discount = source.Discount;
            destination.MaxDiscount = source.MaxDiscount;
            destination.DiscountType = source.VoucherDiscountType;
            destination.ApplicableMethod = source.ApplicableMethod;
            return destination;
        }

    }
}
