﻿using FRTTMO.CAM.PromotionAPI.ETOs.Cache;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using Volo.Abp.ObjectMapping;

namespace FRTTMO.CAM.PromotionAPI.ObjectMappers.MapOutPutCache
{
    public class ObjectMapperCachingForPaymenyMethodOutputProfile : IObjectMapper<OutputCacheEto, CacheValueUsePaymentMethodOutPut>,
        ITransientDependency
    {
        public CacheValueUsePaymentMethodOutPut Map(OutputCacheEto source)
        {
            return Map(source, new CacheValueUsePaymentMethodOutPut());
        }

        public CacheValueUsePaymentMethodOutPut Map(OutputCacheEto source, CacheValueUsePaymentMethodOutPut destination)
        {
            destination.PromotionId = source.PromotionId;
            destination.PaymentMethodCode = source.PaymentMethodCode;
            destination.PaymentMethodName = source.PaymentMethodName;
            destination.SchemeCode = source.SchemeCode;
            destination.Discount = source.Discount;
            destination.DiscountType = source.QualifierCode;
            destination.MaxDiscount = source.MaxValue;
            destination.Note = source.Note;
            destination.NoteBoom = source.NoteBoom;
            destination.DiscountPlaceCode = source.DiscountPlaceCode;
            return destination;
        }
    }
}
