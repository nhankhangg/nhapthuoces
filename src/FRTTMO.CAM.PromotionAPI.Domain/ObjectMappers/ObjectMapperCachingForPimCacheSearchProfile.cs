﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using Volo.Abp.DependencyInjection;
using Volo.Abp.ObjectMapping;
using FRTTMO.CAM.PromotionAPI.DTOs.PIM;
using FRTTMO.CAM.PromotionAPI.ETOs.Cache;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;

namespace FRTTMO.CAM.PromotionAPI.ObjectMappers
{

    public class ObjectMapperCachingForPimCacheSearchProfile :
        IObjectMapper<EventInputCacheEto, PimCacheSearch>,
        IObjectMapper<EventOutputCacheEto, PimCacheSearch>,
        IObjectMapper<OutputCacheEto, PimCacheSearch>,
        IObjectMapper<ItemInputCacheEto, PimCacheSearch>,
        ITransientDependency
    {
        public PimCacheSearch Map(EventInputCacheEto source)
        {
            return Map(source, new PimCacheSearch());
        }

        public PimCacheSearch Map(EventInputCacheEto source, PimCacheSearch destination)
        {
            destination.CategoryCode = source.InputCache.CategoryCode;
            destination.TypeCode = source.InputCache.TypeCode;
            return destination;
        }

        public PimCacheSearch Map(EventOutputCacheEto source)
        {
            return Map(source, new PimCacheSearch());
        }

        public PimCacheSearch Map(EventOutputCacheEto source, PimCacheSearch destination)
        {
            destination.CategoryCode = source.OutputCache.CategoryCode;
            destination.TypeCode = source.OutputCache.TypeCode;
            return destination;
        }

        public PimCacheSearch Map(OutputCacheEto source)
        {
            return Map(source, new PimCacheSearch());
        }

        public PimCacheSearch Map(OutputCacheEto source, PimCacheSearch destination)
        {

            destination.CategoryCode = source.CategoryCode;
            destination.TypeCode = source.TypeCode;
            return destination;
        }

        public PimCacheSearch Map(ItemInputCacheEto source)
        {
            return Map(source, new PimCacheSearch());
        }

        public PimCacheSearch Map(ItemInputCacheEto source, PimCacheSearch destination)
        {
            destination.CategoryCode = source.CategoryCode;
            destination.TypeCode = source.TypeCode;
            return destination;
        }
    }
}
