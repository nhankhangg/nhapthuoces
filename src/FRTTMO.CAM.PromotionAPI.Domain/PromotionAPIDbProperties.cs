﻿namespace FRTTMO.CAM.PromotionAPI;

public static class PromotionAPIDbProperties
{
    public const string ConnectionStringName = "PromotionAPI";
    public static string DbTablePrefix { get; set; } = "";

    public static string DbSchema { get; set; } = null;
}