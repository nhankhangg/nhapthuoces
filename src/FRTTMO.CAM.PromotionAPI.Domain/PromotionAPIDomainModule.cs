﻿using Customize.Logging;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.AutoMapper;
using Volo.Abp.Domain;
using Volo.Abp.FluentValidation;
using Volo.Abp.Modularity;

namespace FRTTMO.CAM.PromotionAPI;

[DependsOn(
    typeof(AbpDddDomainModule),
    typeof(PromotionAPIDomainSharedModule),
    typeof(AbpAutoMapperModule),
    typeof(AbpFluentValidationModule)
)]
public class PromotionAPIDomainModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddAutoMapperObjectMapper<PromotionAPIDomainModule>();
        Configure<AbpAutoMapperOptions>(options => { options.AddMaps<PromotionAPIDomainModule>(validate: true); });

        context.Services.AddHttpClient();
        context.Services.AddHttpClient("ApiClient").AddHttpMessageHandler<HttpApiDelegatingHandler>();
    }
}