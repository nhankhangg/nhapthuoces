﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FRTTMO.CAM.PromotionAPI.Helpers;
using Newtonsoft.Json;
using Volo.Abp.Domain.Services;

namespace FRTTMO.CAM.PromotionAPI.Services;

public class ApiClientService : DomainService, IApiClientService
{
    private readonly IHttpClientFactory _iHttpClientFactory;

    public ApiClientService(IHttpClientFactory iHttpClientFactory)
    {
        _iHttpClientFactory = iHttpClientFactory;
    }

    public async Task<T> FetchAsync<T>(
        string baseUrl,
        string operation,
        string method,
        string payload = "",
        Dictionary<string, object> queryParams = null,
        string authorization = "")
    {
        HttpResponseMessage response = null;
        using (var client = _iHttpClientFactory.CreateClient("ApiClient"))
        using (var httpContent = new StringContent(payload, Encoding.UTF8, "application/json"))
        {
            if (!string.IsNullOrEmpty(authorization))
            {
                client.DefaultRequestHeaders.Add("Authorization", authorization);
            }

            client.BaseAddress = new Uri(baseUrl.EnsureEndsWith('/'));
            switch (method.ToUpper())
            {
                case "GET":
                    response = await client.GetAsync(operation.RemovePreFix("/").BuildUrl(queryParams));
                    break;
                case "POST":
                    response = await client.PostAsync(operation.RemovePreFix("/"), httpContent);
                    break;
                case "PUT":
                    response = await client.PutAsync(operation.RemovePreFix("/").BuildUrl(queryParams), httpContent);
                    break;
            }
            //response = method.ToUpper() switch
            //{
            //    "GET" => await client.GetAsync(operation.RemovePreFix("/").BuildUrl(queryParams)),
            //    "POST" => await client.PostAsync(operation.RemovePreFix("/"), httpContent),
            //    "PUT" => await client.PutAsync(operation.RemovePreFix("/"), httpContent),
            //    _ => response
            //};
        }

        response!.EnsureSuccessStatusCode();
        var jsonResult = await response.Content.ReadAsStringAsync();
        var result = JsonConvert.DeserializeObject<T>(jsonResult);
        return result;
    }
}