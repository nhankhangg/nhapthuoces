﻿using System;
using Hangfire;
using System.Linq;
using Hangfire.States;
using System.Threading.Tasks;
using Volo.Abp.Domain.Services;
using Microsoft.Extensions.Configuration;
using FRTTMO.CAM.PromotionAPI.Enum;
using FRTTMO.CAM.PromotionAPI.ETOs;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;
using FRTTMO.CAM.PromotionAPI.Managers;
using FRTTMO.CAM.PromotionAPI.Entities.Campaign;
using FRTTMO.CAM.PromotionAPI.Entities.Promotion;
using FRTTMO.CAM.PromotionAPI.Entities.RegionCondition;
using System.Collections.Generic;
using Volo.Abp.Uow;
using Elastic.Apm.Api;
using FRTTMO.CAM.PromotionAPI.DTOs.Promotion;
using Volo.Abp.ObjectMapping;
using Nest;
using Volo.Abp.EventBus.Distributed;
using ActionType = FRTTMO.CAM.PromotionAPI.Enum.ActionType;

namespace FRTTMO.CAM.PromotionAPI.Services;

public class HangFireAppService : DomainService, IHangFireAppService
{
    private readonly ITracer _tracer;
    private readonly IConfiguration _configuration;
    private readonly IManagersService _managersService;
    private readonly ICampaignRepository _campaignRepository;
    private readonly IPromotionRepository _promotionRepository;
    private readonly IProvinceConditionRepository _provinceConditionRepository;
    private readonly IElasticSearchAppService _elasticSearchAppService;
    private readonly IObjectMapper _objectMapper;
    public HangFireAppService(
        IConfiguration configuration,
        IManagersService managersService,
        ICampaignRepository campaignRepository,
        IPromotionRepository promotionRepository,
        IProvinceConditionRepository provinceConditionRepository,
        IElasticSearchAppService elasticSearchAppService,
        ITracer tracer,
        IObjectMapper objectMapper)
    {
        _configuration = configuration;
        _managersService = managersService;
        _campaignRepository = campaignRepository;
        _promotionRepository = promotionRepository;
        _provinceConditionRepository = provinceConditionRepository;
        _elasticSearchAppService = elasticSearchAppService;
        _tracer = tracer;
        _objectMapper = objectMapper;
    }

    #region Schedule Cachching Promotion

    public async Task ScheduleCreatePromotionAsync(EventPromotionEto promotionCache)
    {
        var transactionTracer = _tracer?.StartTransaction($"ScheduleCreatePromotionAsync", ApiConstants.TypeRequest);
        var span = _tracer?.CurrentTransaction?.StartSpan($"{promotionCache.HeaderCache.Code} : {promotionCache?.HeaderCache?.PromotionType}", ApiConstants.ActionExec);
        try
        {
            var state = new EnqueuedState($"{_configuration["Hangfire:PrefixPromotion"]}");
            var jobClient = new BackgroundJobClient();
            //if(promotionCache.HeaderCache.PromotionType == "BuyProductGetProduct")
            //{
            //    await _managersService.GetItemOutputCachingAsync(promotionCache.HeaderCache.Code, promotionCache);
            //}
            switch (promotionCache.HeaderCache.PromotionType)
            {
                case "BuyComboGetProduct":
                case "BuyProductGetProduct":
                    await Task.Run(() =>
                    {
                        jobClient.Create<IManagersService>(x => x.GetItemOutputCachingAsync(promotionCache.HeaderCache.Code, promotionCache), state);
                    });
                    break;

                case "BuyComboGetDiscount":
                case "BuyProductGetDiscount":
                    await Task.Run(() =>
                    {
                        jobClient.Create<IManagersService>(x => x.DiscountItemOutputCachingAsync(promotionCache.HeaderCache.Code, promotionCache), state);
                    });
                    break;

                case "BuyProductGetAscendingDiscount":
                    await Task.Run(() =>
                    {
                        jobClient.Create<IManagersService>(x => x.GetAscendingDiscountCachingAsync(promotionCache.HeaderCache.Code, promotionCache), state);
                    });
                    break;

                case "TotalBillAmount":
                    await Task.Run(() =>
                    {
                        jobClient.Create<IManagersService>(x => x.TotalBillAmountCachingAsync(promotionCache.HeaderCache.Code, promotionCache), state);
                    });
                    break;

                case "TotalProductAmount":
                    await Task.Run(() =>
                    {
                        jobClient.Create<IManagersService>(x => x.TotalProductAmountCachingAsync(promotionCache.HeaderCache.Code, promotionCache), state);
                    });
                    break;

                case "UsePaymentMethod":
                    await Task.Run(() =>
                    {
                        jobClient.Create<IManagersService>(x => x.PaymenMethodCachingAsync(promotionCache.HeaderCache.Code, promotionCache), state);
                    });
                    break;
            }
        }
        catch (Exception ex)
        {
            transactionTracer?.CaptureException(ex);
        }
        finally
        {
            span?.End();
            transactionTracer?.End();
        }
    }

    public async Task ScheduleCacheConditionAsync(EventConditionEto eventConditionEto)
    {
        var jobClient = new BackgroundJobClient();
        await Task.Run(() =>
        {
            jobClient.Create<IManagersService>(x => x.UpSetConditionCachingAsync(eventConditionEto),
                new EnqueuedState($"{_configuration["Hangfire:PrefixPromotion"]}"));
        });
    }

    public async Task UpdateHeaderPromotionAsync(EventPromotionEto eventPromotionEto)
    {
        var jobClient = new BackgroundJobClient();
        await Task.Run(() =>
        {
            jobClient.Create<IManagersService>(x => x.UpSetHeaderCachingAsync(eventPromotionEto),
                new EnqueuedState($"{_configuration["Hangfire:PrefixPromotion"]}"));
        });
    }

    public async Task ScheduleClearProvinceConditionAsync(List<string> shopCodes, string promotionCode)
    {
        var jobClient = new BackgroundJobClient();
        await Task.Run(() =>
        {
            jobClient.Create<IManagersService>(x => x.ClearProvinceConditionAsync(shopCodes, promotionCode),
                new EnqueuedState($"{_configuration["Hangfire:PrefixPromotion"]}"));
        });
    }
   
    #endregion
    
    /// <summary>
    /// tự động off khuyến mãi hết hạn theo ngày
    /// </summary>
    /// <returns></returns>
    public async Task JobActiveDailyAsync()
    {
        var campaigns = await _campaignRepository.GetListAsync(x => x.ToDate.Date <= DateTime.Now.Date &&
                                                                    x.Status != (int)CampaignStatusEnum.InActive);
        if (campaigns is { Count: > 0 })
        {
            campaigns.ForEach(x => x.Status = (int)CampaignStatusEnum.InActive);
            await _campaignRepository.UpdateManyAsync(campaigns);
        }

        var promotions = await _promotionRepository.GetListAsync(x => x.ToDate.Date <= DateTime.Now.Date && x.Status != (int)PromotionStatusEnum.InActive);
        var promotionsStatusActive = promotions.Where(p => p.Status == (int)PromotionStatusEnum.Activated).ToList();

        //Check các khuyến mãi 7 ngày trước hết hiệu lực
        var promotionPreSevenDay = await _promotionRepository.GetListAsync(x => x.ToDate.Date.AddDays(7) == DateTime.Now.Date
                                                                                && x.Status == 3);
        if (promotionsStatusActive is { Count: > 0 })
        {
            if(promotionPreSevenDay is { Count: > 0 })
            {
                //remove promotion cache
                var shopConditions = await _provinceConditionRepository
                    .GetListAsync(x => promotionPreSevenDay.Select(p => p.Code).Contains(x.PromotionCode));

                var timeExpireEto = promotionPreSevenDay.Select(promotion =>
                    new PromotionTimeExpireEto
                    {
                        PromotionId = promotion.Id,
                        PromotionCode = promotion.Code,
                        PromotionType = promotion.PromotionType,
                        ProvinceConditions = shopConditions.
                            Where(shop => shop.PromotionCode == promotion.Code).
                            Select(p => p.ProvinceCode).ToList()
                    }).ToList();
                await _managersService.TimeExpireCacheAsync(timeExpireEto);
            }

            //update databse
            promotionsStatusActive.ForEach(x => x.Status = (int)PromotionStatusEnum.InActive);
            await _promotionRepository.UpdateManyAsync(promotionsStatusActive);

            //Update redis nếu status = 3 mà ngày kết thúc sau 7 ngày
            var promotionResponseDto = _objectMapper.Map<List<Promotion>, List<PromotionDto>>(promotionsStatusActive);
            foreach(var promotion in promotionResponseDto)
            {
                var promotionCache = new EventPromotionEto { ActionType = ActionType.UpdateHeader };
                promotionCache = _objectMapper.Map(promotion, promotionCache);
                await UpdateHeaderPromotionAsync(promotionCache);
            }

            // update status to ES
            await _elasticSearchAppService.UpdatePromotionStatusAsync(promotions.Select(x => x.Id).ToList(), PromotionStatusEnum.InActive);
        }
    }

    public async Task UpdateStatusPromotionAsync(List<PromotionTimeExpireEto> model)
    {
        var jobClient = new BackgroundJobClient();
        await Task.Run(() =>
        {
            jobClient.Create<IManagersService>(x => x.TimeExpireCacheAsync(model, false),
                new EnqueuedState($"{_configuration["Hangfire:PrefixPromotion"]}"));
        });
    }

    public async Task UpdatePromotionExcludeRelatedAsync(EventPromotionExcludeEto model)
    {
        var jobClient = new BackgroundJobClient();
        await Task.Run(() =>
        {
            jobClient.Create<IPromotionAppService>(x => x.UpdatePromotionExcludeRelatedAsync(model),
                new EnqueuedState($"{_configuration["Hangfire:PrefixPromotion"]}"));
        });
    }
    public async Task UpdatePromotionExcludeAndSyncDataAsync(List<string> promotionCodes)
    {
        var jobClient = new BackgroundJobClient();
        await Task.Run(() =>
        {
            jobClient.Create<IPromotionAppService>(x => x.PromotionExcludeCacheAndSyncEcomAsync(promotionCodes),
                new EnqueuedState($"{_configuration["Hangfire:PrefixPromotion"]}"));
        });
    }

    public async Task JobActiveCampaignDailyAsync()
    {
        var campaigns = await _campaignRepository.GetListAsync(c => c.Status == (int)CampaignStatusEnum.Created && c.FromDate.Date == DateTime.Now.Date);
        if (campaigns is { Count: > 0 })
        {
            campaigns.ForEach(x => x.Status = (int)CampaignStatusEnum.Activated);
            await _campaignRepository.UpdateManyAsync(campaigns);
        }

        // update status to ES
        await _elasticSearchAppService.UpdateCampaignStatusAsync(campaigns.Select(x => x.Id).ToList(), CampaignStatusEnum.Activated);

    }

}