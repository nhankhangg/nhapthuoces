﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Domain.Services;

namespace FRTTMO.CAM.PromotionAPI.Services;

public interface IApiClientService : IDomainService
{
    Task<T> FetchAsync<T>(string baseUrl, string operation, string method, string payload = "",
        Dictionary<string, object> queryParams = null, string authorization = "");
}