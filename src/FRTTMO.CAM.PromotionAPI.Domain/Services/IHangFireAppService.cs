﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FRTTMO.CAM.PromotionAPI.ETOs;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;
using Volo.Abp.Domain.Services;

namespace FRTTMO.CAM.PromotionAPI.Services;

public interface IHangFireAppService : IDomainService
{
    Task JobActiveDailyAsync();
    Task ScheduleCreatePromotionAsync(EventPromotionEto model);
    Task ScheduleCacheConditionAsync(EventConditionEto model);
    Task UpdateHeaderPromotionAsync(EventPromotionEto model);

    Task UpdateStatusPromotionAsync(List<PromotionTimeExpireEto> model);

    Task UpdatePromotionExcludeRelatedAsync(EventPromotionExcludeEto model);
    Task UpdatePromotionExcludeAndSyncDataAsync(List<string> promotionCodes);
    Task ScheduleClearProvinceConditionAsync(List<string> shopCodes, string promotionCode);

    Task JobActiveCampaignDailyAsync();
}