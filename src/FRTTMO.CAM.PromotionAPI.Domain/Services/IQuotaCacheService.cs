﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FRTTMO.CAM.PromotionAPI.DTOs.Quota;
using Volo.Abp.Domain.Services;

namespace FRTTMO.CAM.PromotionAPI.Services;

public interface IQuotaCacheService: IDomainService
{
    Task<List<ResponseCheckQuotaDto>> CheckQuotaAsync(List<UseOrCheckQuotaDto> checkQuota);
    Task<List<ResponseQuotaDto>> UseQuotaAsync(List<UseOrCheckQuotaDto> checkQuota);
    Task UpdateQuotaAsync(string promotionCode, string resetQuotaType);
} 