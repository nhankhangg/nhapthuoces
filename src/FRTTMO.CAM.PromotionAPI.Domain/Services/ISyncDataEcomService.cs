﻿using FRTTMO.CAM.PromotionAPI.Etos;
using FRTTMO.CAM.PromotionAPI.ETOs;
using FRTTMO.CAM.PromotionAPI.ETOs.Sync;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Services;

namespace FRTTMO.CAM.PromotionAPI.Services
{
    public interface ISyncDataEcomService : IDomainService
    {
        //#region Sync Data To Ecom

        //Task SyncDataToEcomAsync(EventPromotionSyncEto eventData);

        //Task PimChangePriceAsync(PimChangePriceEto eventData);

        Task UpdatePromotionExcludeAsync(EventPromotionEto EventPromotionEto);

        //#endregion
    }
}
