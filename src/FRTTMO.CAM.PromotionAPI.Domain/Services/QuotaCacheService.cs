﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StackExchange.Redis;
using Volo.Abp.Domain.Services;
using FRTTMO.CAM.PromotionAPI.DTOs.Quota;
using FRTTMO.CAM.PromotionAPI.Entities.QuotaHistories;
using FRTTMO.CAM.PromotionAPI.ETOs.Cache;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using FRTTMO.CAM.PromotionAPI.Documents;

namespace FRTTMO.CAM.PromotionAPI.Services
{
    public class QuotaCacheService : DomainService, IQuotaCacheService
    {
        private readonly IConfiguration _configuration;
        private readonly IDatabase _databaseStackExchange;
        private const string ResetShop = "ResetShop";
        private const string ResetPromotion = "ResetPromotion";
        private readonly IQuotaHistoriesRepository _quotaHistoriesRepository;
        private readonly IElasticSearchAppService _elasticSearchAppService;
        public QuotaCacheService(
            IConfiguration configuration,
            IDatabase databaseStackExchange,
            IQuotaHistoriesRepository quotaHistoriesRepository,
            IElasticSearchAppService elasticSearchAppService)
        {
            _configuration = configuration;
            _quotaHistoriesRepository = quotaHistoriesRepository;
            _databaseStackExchange = databaseStackExchange;
            _elasticSearchAppService = elasticSearchAppService;
        }


        public async Task<List<ResponseCheckQuotaDto>> CheckQuotaAsync(List<UseOrCheckQuotaDto> checkQuotas)
        {
            var responseCheckQuota = new List<ResponseCheckQuotaDto>();

            var getDataCache = await _databaseStackExchange.StringGetAsync(
                checkQuotas.Select(quota => GetKeyPrefixPromotionCaching(quota.PromotionCode, "Header"))
                    .Select(redisKey => (RedisKey)redisKey).ToArray());

            var promotionHeader = getDataCache
                .Where(p => p.HasValue)
                .Select(p => JsonConvert.DeserializeObject<CacheValuePromotionHeader>(p))
                .ToList();

            foreach (var promotion in promotionHeader)
            {
                var data = new ResponseCheckQuotaDto
                {
                    PromotionCode = promotion.Code,
                    Quota = promotion.Quota
                };

                var quantity = checkQuotas.FirstOrDefault(p => p.PromotionCode == promotion.Code)!.Quantity;

                var keyQuota = GetKeyPrefixPromotionCaching(promotion.Code, "Quota");
                var dataPromotion = await _databaseStackExchange.StringGetAsync($"{keyQuota}:All");
                if (dataPromotion.HasValue)
                {
                    var promotionQuota = JsonConvert.DeserializeObject<CacheValueQuota>(dataPromotion);
                    data.CurrentQuantity = promotionQuota!.CurrentQuantity;
                    data.IsValid = promotion.Quota.Quantity >= (promotionQuota!.CurrentQuantity + +quantity);
                }

                var useShop = checkQuotas.FirstOrDefault(p => p.PromotionCode == promotion.Code)!.ProvinceCode;
                if (useShop != null)
                {
                    var keyShopFocus = $"{keyQuota}:{useShop}";
                    var getShopCache = await _databaseStackExchange.HashGetAsync(keyShopFocus, useShop);
                    if (getShopCache.HasValue)
                    {
                        var shopQuota = JsonConvert.DeserializeObject<CacheValueQuota>(getShopCache);
                        data.ResponseShop.Add(new ResponseCheckQuotaShop
                        {
                            ShopCode = useShop,
                            CurrentQuantity = shopQuota!.CurrentQuantity,
                            IsValid = promotion.Quota.LimitQuantityShop >= (shopQuota!.CurrentQuantity + quantity)
                        });
                    }
                    else
                    {
                        data.ResponseShop.Add(new ResponseCheckQuotaShop
                        {
                            ShopCode = useShop,
                            CurrentQuantity = 0,
                            IsValid = true
                        });
                    }
                }

                var usePhone = checkQuotas.FirstOrDefault(p => p.PromotionCode == promotion.Code)!.PhoneNumbers;
                if (usePhone != null)
                {
                    var keyPhoneFocus = $"{keyQuota}:{usePhone}";
                    var getPhoneCache = await _databaseStackExchange.HashGetAsync(keyPhoneFocus, usePhone);
                    if (promotion.Quota.FlagQuantityPhone)
                    {
                        if (getPhoneCache.HasValue)
                        {
                            var phoneQuota = JsonConvert.DeserializeObject<CacheValueQuota>(getPhoneCache);
                            data.ResponsePhone.Add(new ResponseCheckQuotaPhone
                            {
                                PhoneNumber = usePhone,
                                CurrentQuantity = phoneQuota!.CurrentQuantity,
                                IsValid = promotion.Quota.LimitQuantityShop >= (phoneQuota!.CurrentQuantity + quantity)
                            });
                        }
                        else
                        {
                            data.ResponsePhone.Add(new ResponseCheckQuotaPhone
                            {
                                PhoneNumber = usePhone,
                                CurrentQuantity = 0,
                                IsValid = true
                            });
                        }
                    }
                    else
                    {
                        if (getPhoneCache.HasValue)
                        {
                            var phoneQuota = JsonConvert.DeserializeObject<CacheValueQuota>(getPhoneCache);
                            data.ResponsePhone.Add(new ResponseCheckQuotaPhone
                            {
                                PhoneNumber = usePhone,
                                CurrentQuantity = phoneQuota!.CurrentQuantity,
                                IsValid = true
                            });
                        }
                        else
                        {
                            data.ResponsePhone.Add(new ResponseCheckQuotaPhone
                            {
                                PhoneNumber = usePhone,
                                CurrentQuantity = 0,
                                IsValid = true
                            });
                        }
                    }
                }

                responseCheckQuota.Add(data);
            }

            return responseCheckQuota;
        }

        public async Task<List<ResponseQuotaDto>> UseQuotaAsync(List<UseOrCheckQuotaDto> useQuotas)
        {
            var getDataCacheHeader = await _databaseStackExchange.StringGetAsync(
                useQuotas.Select(quota => GetKeyPrefixPromotionCaching(quota.PromotionCode, "Header"))
                .Select(redisKey => (RedisKey)redisKey).ToArray());

            var responseQuota = new List<ResponseQuotaDto>();

            if (getDataCacheHeader is { Length: > 0 })
            {
                var promotionHeader = getDataCacheHeader
                    .Where(p => p.HasValue)
                    .Select(p => JsonConvert.DeserializeObject<CacheValuePromotionHeader>(p))
                    .ToList();
                foreach (var promotion in promotionHeader)
                {
                    if (promotion.Quota is { Type: "Product" or "Order" })
                    {
                        var keyQuota = GetKeyPrefixPromotionCaching(promotion.Code, "Quota");
                        var quantity = useQuotas.FirstOrDefault(p => p.PromotionCode == promotion.Code)!.Quantity;

                        //cache quota promotion
                        var flagReset = promotion.Quota.ResetQuotaType == ResetPromotion;
                        var dataPromotion = await CacheQuotaPromotionAsync(keyQuota, quantity, flagReset);

                        // cache quota shop
                        var flagResetShop = promotion.Quota.ResetQuotaType == ResetShop;
                        var useShops = useQuotas.FirstOrDefault(p => p.PromotionCode == promotion.Code)!.ProvinceCode;
                        var shopCache = await CacheQuotaShopAsync(keyQuota, useShops, quantity, flagResetShop);

                        // cache quota phone
                        var usePhones = useQuotas.FirstOrDefault(p => p.PromotionCode == promotion.Code)!.PhoneNumbers;
                        var phoneCache = await CacheQuotaPhoneAsync(keyQuota, usePhones, quantity);

                        dataPromotion.PromotionCode = promotion.Code;
                        dataPromotion.ResponseShop.AddRange(shopCache);
                        dataPromotion.ResponsePhone.AddRange(phoneCache);
                        responseQuota.Add(dataPromotion);

                        // create quota history document
                        var quotaHistory = new QuotaHistoryDocument()
                        {
                            Id = Guid.NewGuid(),
                            PromotionCode = promotion.Code,
                            UsedDateTime = DateTime.Now,
                            PhoneNumber = useQuotas.FirstOrDefault(x => x.PromotionCode == promotion.Code).PhoneNumbers,
                            OrderCode = useQuotas.FirstOrDefault(x => x.PromotionCode == promotion.Code).PhoneNumbers.Substring(5),
                            Quantity = useQuotas.FirstOrDefault(x => x.PromotionCode == promotion.Code).Quantity,
                            ShopCode = useQuotas.FirstOrDefault(x => x.PromotionCode == promotion.Code).ProvinceCode,
                            ShopName = useQuotas.FirstOrDefault(x => x.PromotionCode == promotion.Code).ProvinceName
                        };

                        await _elasticSearchAppService.CreateQuotaHistoryAsync(quotaHistory);
                    }
                }
            }

            await QuotaHistoriesAsync(useQuotas);

            return responseQuota;
        }

        private async Task<ResponseQuotaDto> CacheQuotaPromotionAsync(string keyQuota, int quantity, bool flagReset)
        {
            var newkeyQuota = $"{keyQuota}:All";
            var getPromotionCache = await _databaseStackExchange.StringGetAsync(newkeyQuota);
            var cacheValueQuota = new CacheValueQuota();
            if (getPromotionCache.HasValue)
            {
                cacheValueQuota = JsonConvert.DeserializeObject<CacheValueQuota>(getPromotionCache);
                cacheValueQuota!.CurrentQuantity += quantity;
            }
            else
            {
                cacheValueQuota!.CurrentQuantity += quantity;
            }
            await _databaseStackExchange.StringSetAsync(newkeyQuota, JsonConvert.SerializeObject(cacheValueQuota));

            //reset quota every day or not
            DateTime? dateTime = flagReset ? DateTime.Now.Date : null;
            await SetTimeSpanCacheAsync(newkeyQuota, dateTime);

            return new ResponseQuotaDto
            {
                CurrentQuantity = cacheValueQuota.CurrentQuantity,
            };
        }

        private async Task<List<ResponseShopDto>> CacheQuotaShopAsync(string keyShop, string shopCode, int quantity, bool flagReset)
        {
            var responseShops = new List<ResponseShopDto>();

            var keyShopFocus = $"{keyShop}:Shop";
            var getQuotaCacheOld = await _databaseStackExchange.HashGetAsync(keyShopFocus, shopCode);
            var cacheValueQuota = new CacheValueQuota();
            if (getQuotaCacheOld.HasValue)
            {
                cacheValueQuota = JsonConvert.DeserializeObject<CacheValueQuota>(getQuotaCacheOld);
                cacheValueQuota!.CurrentQuantity += quantity;
            }
            else
            {
                cacheValueQuota!.CurrentQuantity += quantity;
            }
            var listHash = new List<HashEntry>
            { new HashEntry(shopCode,JsonConvert.SerializeObject(cacheValueQuota))};
            await _databaseStackExchange.HashSetAsync(keyShopFocus, listHash.ToArray());

            //reset quota every day or not
            DateTime? dateTime = flagReset ? DateTime.Now.Date : null;
            await SetTimeSpanCacheAsync(keyShopFocus, dateTime);

            responseShops.Add(new ResponseShopDto
            {
                ShopCode = shopCode,
                CurrentQuantity = cacheValueQuota.CurrentQuantity
            });

            return responseShops;
        }

        private async Task<List<ResponsePhoneDto>> CacheQuotaPhoneAsync(string keyPhone, string phone, int quantity)
        {
            phone = ParsePhoneNumberStartWith_0(phone);
            var responsePhone = new List<ResponsePhoneDto>();

            var keyPhoneFocus = $"{keyPhone}:Phone";
            var getQuotaCacheOld = await _databaseStackExchange.HashGetAsync(keyPhoneFocus, phone);
            var cacheValueQuota = new CacheValueQuota();
            if (getQuotaCacheOld.HasValue)
            {
                cacheValueQuota = JsonConvert.DeserializeObject<CacheValueQuota>(getQuotaCacheOld);
                cacheValueQuota!.CurrentQuantity += quantity;
            }
            else
            {
                cacheValueQuota!.CurrentQuantity += quantity;
            }

            var listHash = new List<HashEntry>
            {
                new HashEntry(phone,JsonConvert.SerializeObject(cacheValueQuota))
            };

            await _databaseStackExchange.HashSetAsync(keyPhoneFocus, listHash.ToArray());

            //phone not reset for day
            await SetTimeSpanCacheAsync(keyPhoneFocus, null);

            responsePhone.Add(new ResponsePhoneDto
            {
                PhoneNumber = phone,
                CurrentQuantity = cacheValueQuota.CurrentQuantity
            });

            return responsePhone;
        }

        private async Task QuotaHistoriesAsync(IEnumerable<UseOrCheckQuotaDto> useQuotas)
        {
            var quotaHistories = useQuotas.Select(p => new QuotaHistories
            {
                PromotionCode = p.PromotionCode,
                Quantity = p.Quantity,
                ProvinceCode = p.ProvinceCode,
                PhoneNumber = p.PhoneNumbers,
                UseDateTime = DateTime.Now
            }).ToList();

            await _quotaHistoriesRepository.InsertManyAsync(quotaHistories);
        }

        private async Task SetTimeSpanCacheAsync(string keyCache, DateTime? dateTime)
        {
            if (dateTime == null)
            {
                await _databaseStackExchange.KeyPersistAsync(keyCache);
            }
            else
            {
                // Tính thời gian hiện tại
                DateTime currentTime = DateTime.Now;
                // Tính thời gian đến cuối ngày (23:59:59) của cùng ngày
                DateTime endOfDay = currentTime.Date.AddDays(1).AddSeconds(-1);
                // Tính thời gian còn lại đến cuối ngày
                TimeSpan timeSpan = endOfDay - currentTime;
                await _databaseStackExchange.KeyExpireAsync(keyCache, timeSpan);
            }
        }

        private string GetKeyPrefixPromotionCaching(string keyName, string keyPromotion)
        {
            return keyPromotion switch
            {
                "TotalCost" => $"{_configuration["Redis:KeyPrefix"]}:{keyPromotion}",
                "ItemCode" => $"{_configuration["Redis:KeyPrefixItemCode"]}:{keyName}",

                "Header" => $"{_configuration["Redis:KeyPrefixPromotion"]}:{keyName}:{keyPromotion}",
                "Input" => $"{_configuration["Redis:KeyPrefixPromotion"]}:{keyName}:{keyPromotion}",
                "Output" => $"{_configuration["Redis:KeyPrefixPromotion"]}:{keyName}:{keyPromotion}",
                "OutputReplace" => $"{_configuration["Redis:KeyPrefixPromotion"]}:{keyName}:{keyPromotion}",
                "ProvinceCondition" => $"{_configuration["Redis:KeyPrefix"]}:{keyPromotion}:{keyName}",

                "Quota" => $"{_configuration["Redis:KeyPrefix"]}:{keyName}:{keyPromotion}",
                _ => ""
            };
        }
        
        private static string ParsePhoneNumberStartWith_0(string phoneNumber)
        {
            if (phoneNumber.IsNullOrEmpty()) return phoneNumber;

            if (phoneNumber.StartsWith("84"))
            {
                return $"0{phoneNumber.Substring(2)}";
            }
            return phoneNumber;
        }

        public async Task UpdateQuotaAsync(string promotionCode, string resetQuotaType)
        {
            var keyQuota = GetKeyPrefixPromotionCaching(promotionCode, "Quota");
            var keyPromotionFocus = $"{keyQuota}:All";
            var getKeyByResetPromotion = await _databaseStackExchange.KeyExistsAsync(keyPromotionFocus);
            if (getKeyByResetPromotion)
            {
                if (resetQuotaType == "ResetPromotion")
                {
                    await SetTimeSpanCacheAsync(keyPromotionFocus, DateTime.Now);
                }
                else
                {
                    await SetTimeSpanCacheAsync(keyPromotionFocus, null);
                }
            }
            var keyShopFocus = $"{keyQuota}:Shop";
            var getKeyByResetShop = await _databaseStackExchange.KeyExistsAsync(keyShopFocus);
            if (getKeyByResetShop)
            {
                if (resetQuotaType == "ResetShop")
                {
                   await SetTimeSpanCacheAsync(keyShopFocus, DateTime.Now);
                }
                else
                {
                    await SetTimeSpanCacheAsync(keyShopFocus, null);
                }    
            }
        }
    }
}