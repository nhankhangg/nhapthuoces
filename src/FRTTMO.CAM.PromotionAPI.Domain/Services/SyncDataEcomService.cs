﻿using FRTTMO.CAM.PromotionAPI.ETOs;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Domain.Services;

namespace FRTTMO.CAM.PromotionAPI.Services
{
    public class SyncDataEcomService : DomainService, ISyncDataEcomService
    {
        private readonly IConfiguration _configuration;
        private readonly IDatabase _stackExchangeRedis;
        public SyncDataEcomService(
            IConfiguration configuration,
            IDatabase stackExchangeRedis)
        {
            _configuration = configuration;
            _stackExchangeRedis = stackExchangeRedis;
        }

        /// <summary>
        /// get key prefix cach
        /// </summary>
        /// <param name="keyName"></param>
        /// <param name="keyPromotion"></param>
        /// <returns></returns>
        private string GetKeyPrefixCaching(string keyName, string keyPromotion)
        {
            return keyPromotion switch
            {
                "UsePaymentMethod" => $"{_configuration["Redis:KeyPrefix"]}:{keyPromotion}",
                "TotalCost" => $"{_configuration["Redis:KeyPrefix"]}:{keyPromotion}",
                "ItemCode" => $"{_configuration["Redis:KeyPrefixItemCode"]}:{keyName}",

                "Header" => $"{_configuration["Redis:KeyPrefixPromotion"]}:{keyName}:{keyPromotion}",
                "Input" => $"{_configuration["Redis:KeyPrefixPromotion"]}:{keyName}:{keyPromotion}",
                "Output" => $"{_configuration["Redis:KeyPrefixPromotion"]}:{keyName}:{keyPromotion}",
                "OutputReplace" => $"{_configuration["Redis:KeyPrefixPromotion"]}:{keyName}:{keyPromotion}",
                "ProvinceCondition" => $"{_configuration["Redis:KeyPrefix"]}:{keyPromotion}:{keyName}",
                _ => ""
            };
        }

        /// <summary>
        /// update promotion exclude redis
        /// </summary>
        /// <param name="EventEto"></param>
        /// <returns></returns>
        public async Task UpdatePromotionExcludeAsync(EventPromotionEto EventEto)
        {
            if (!EventEto.HeaderCache.Code.IsNullOrEmpty())
            {
                var keyCache = GetKeyPrefixCaching(EventEto.HeaderCache.Code, "Header");
                await _stackExchangeRedis.StringSetAsync(keyCache, JsonConvert.SerializeObject(EventEto.HeaderCache));
            }
        }
    }
}
