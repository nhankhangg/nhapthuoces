﻿using Volo.Abp.Settings;

namespace FRTTMO.CAM.PromotionAPI.Settings;

public class PromotionAPISettingDefinitionProvider : SettingDefinitionProvider
{
    public override void Define(ISettingDefinitionContext context)
    {
        /* Define module settings here.
         * Use names from PromotionAPISettings class.
         */
    }
}