﻿using FRTTMO.CAM.PromotionAPI.Entities.AmountCondition;
using FRTTMO.CAM.PromotionAPI.Entities.Campaign;
using FRTTMO.CAM.PromotionAPI.Entities.Common;
using FRTTMO.CAM.PromotionAPI.Entities.Condition;
using FRTTMO.CAM.PromotionAPI.Entities.CostDistribution;
using FRTTMO.CAM.PromotionAPI.Entities.CustomerTags;
using FRTTMO.CAM.PromotionAPI.Entities.FieldConfigure;
using FRTTMO.CAM.PromotionAPI.Entities.ImageConfigure;
using FRTTMO.CAM.PromotionAPI.Entities.InputCacheItem;
using FRTTMO.CAM.PromotionAPI.Entities.InputCacheItemReplace;
using FRTTMO.CAM.PromotionAPI.Entities.InstallmentCondition;
using FRTTMO.CAM.PromotionAPI.Entities.ItemInput;
using FRTTMO.CAM.PromotionAPI.Entities.ItemInputExclude;
using FRTTMO.CAM.PromotionAPI.Entities.ItemInputReplace;
using FRTTMO.CAM.PromotionAPI.Entities.Output;
using FRTTMO.CAM.PromotionAPI.Entities.OutputIntegration;
using FRTTMO.CAM.PromotionAPI.Entities.OutputReplace;
using FRTTMO.CAM.PromotionAPI.Entities.PaymentCondition;
using FRTTMO.CAM.PromotionAPI.Entities.Promotion;
using FRTTMO.CAM.PromotionAPI.Entities.PromotionExclude;
using FRTTMO.CAM.PromotionAPI.Entities.QualifierConfigure;
using FRTTMO.CAM.PromotionAPI.Entities.Quota;
using FRTTMO.CAM.PromotionAPI.Entities.QuotaHistories;
using FRTTMO.CAM.PromotionAPI.Entities.RegionCondition;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;

namespace FRTTMO.CAM.PromotionAPI.EntityFrameworkCore
{
    [ConnectionStringName(PromotionAPIDbProperties.ConnectionStringName)]
    public interface IPromotionAPIDbContext : IEfCoreDbContext
    {
        DbSet<Campaign> Campaigns { get; }
        DbSet<FieldConfigure> FieldConfigures { get; }
        DbSet<QualifierConfigure> QualifierConfigures { get; }
        DbSet<Quota> Quotas { get; }
        DbSet<ProvinceCondition> ProvinceConditions { get; }
        DbSet<Counter> Counters { get; }
        DbSet<ExtraCondition> Conditions { get; }
        DbSet<InputCacheItem> InputCacheItems { get; }
        DbSet<InputCacheItemReplace> InputCacheItemReplaces { get; }
        DbSet<ItemInputExclude> ItemInputExcludes { get; set; }
        DbSet<ItemInput> ItemInputs { get; }
        DbSet<ItemInputReplace> ItemInputReplaces { get; }
        DbSet<Output> Outputs { get; }
        DbSet<OutputReplace> OutputReplaces { get; }
        DbSet<Promotion> Promotions { get; }
        DbSet<CostDistribution> CostDistributions { get; }
        DbSet<AmountCondition> AmountConditions { get; }
        DbSet<InstallmentCondition> InstallmentConditions { get; }


        //out put intergation
        DbSet<GetItemOutput> GetItemOutputs { get; set; }
        DbSet<DiscountItemOutput> DiscountItemOutputs { get; set; }
        DbSet<GetAscendingDiscountOutPut> GetAscendingDiscountOutPuts { get; set; }
        DbSet<PaymentMethodOutput> PaymentMethodOutputs { get; set; }

        //QuotaHistories
        DbSet<QuotaHistories> QuotaHistories { get; set; }

        //Image Configure : Lưu ảnh promotion và voucher
        DbSet<ImageConfigure> ImageConfigures { get; set; }

        //PromotionExlcude
        DbSet<PromotionExclude> PromotionExcludes { get; set; }

        //PaymentCondition
        DbSet<PaymentCondition> PaymentConditions { get; set; }

        //CustomerTags
        DbSet<CustomerTags> CustomerTags { get; set; }
    }
}