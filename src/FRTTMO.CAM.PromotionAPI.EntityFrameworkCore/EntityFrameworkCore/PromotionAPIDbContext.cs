﻿using FRTTMO.CAM.PromotionAPI.Entities.AmountCondition;
using FRTTMO.CAM.PromotionAPI.Entities.Campaign;
using FRTTMO.CAM.PromotionAPI.Entities.Common;
using FRTTMO.CAM.PromotionAPI.Entities.Condition;
using FRTTMO.CAM.PromotionAPI.Entities.CostDistribution;
using FRTTMO.CAM.PromotionAPI.Entities.CustomerTags;
using FRTTMO.CAM.PromotionAPI.Entities.FieldConfigure;
using FRTTMO.CAM.PromotionAPI.Entities.ImageConfigure;
using FRTTMO.CAM.PromotionAPI.Entities.InputCacheItem;
using FRTTMO.CAM.PromotionAPI.Entities.InputCacheItemReplace;
using FRTTMO.CAM.PromotionAPI.Entities.InstallmentCondition;
using FRTTMO.CAM.PromotionAPI.Entities.ItemInput;
using FRTTMO.CAM.PromotionAPI.Entities.ItemInputExclude;
using FRTTMO.CAM.PromotionAPI.Entities.ItemInputReplace;
using FRTTMO.CAM.PromotionAPI.Entities.Output;
using FRTTMO.CAM.PromotionAPI.Entities.OutputIntegration;
using FRTTMO.CAM.PromotionAPI.Entities.OutputReplace;
using FRTTMO.CAM.PromotionAPI.Entities.PaymentCondition;
using FRTTMO.CAM.PromotionAPI.Entities.Promotion;
using FRTTMO.CAM.PromotionAPI.Entities.PromotionExclude;
using FRTTMO.CAM.PromotionAPI.Entities.QualifierConfigure;
using FRTTMO.CAM.PromotionAPI.Entities.Quota;
using FRTTMO.CAM.PromotionAPI.Entities.QuotaHistories;
using FRTTMO.CAM.PromotionAPI.Entities.RegionCondition;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;

namespace FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;

[ConnectionStringName(PromotionAPIDbProperties.ConnectionStringName)]
public class PromotionAPIDbContext : AbpDbContext<PromotionAPIDbContext>, IPromotionAPIDbContext
{
    /* Add DbSet for each Aggregate Root here. Example:
     * public DbSet<Question> Questions { get; set; }
     */

    public PromotionAPIDbContext(DbContextOptions<PromotionAPIDbContext> options)
        : base(options)
    {

    }

    public DbSet<Campaign> Campaigns { get; set; }
    public DbSet<FieldConfigure> FieldConfigures { get; set; }
    public DbSet<QualifierConfigure> QualifierConfigures { get; set; }
    public DbSet<Quota> Quotas { get; set; }
    public DbSet<ProvinceCondition> ProvinceConditions { get; set; }
    public DbSet<Counter> Counters { get; set; }
    public DbSet<ExtraCondition> Conditions { get; set; }
    public DbSet<InputCacheItem> InputCacheItems { get; set; }
    public DbSet<InputCacheItemReplace> InputCacheItemReplaces { get; set; }
    public DbSet<ItemInputExclude> ItemInputExcludes { get; set; }
    public DbSet<ItemInput> ItemInputs { get; set; }
    public DbSet<ItemInputReplace> ItemInputReplaces { get; set; }
    public DbSet<Output> Outputs { get; set; }
    public DbSet<OutputReplace> OutputReplaces { get; set; }
    public DbSet<Promotion> Promotions { get; set; }
    public DbSet<CostDistribution> CostDistributions { get; set; }
    public DbSet<AmountCondition> AmountConditions { get; set; }
    public DbSet<InstallmentCondition> InstallmentConditions { get; set; }

    /// <summary>
    /// out put intergation
    /// </summary>
    public DbSet<GetItemOutput> GetItemOutputs { get; set; }
    public DbSet<DiscountItemOutput> DiscountItemOutputs { get; set; }
    public DbSet<GetAscendingDiscountOutPut> GetAscendingDiscountOutPuts { get; set; }
    public DbSet<PaymentMethodOutput> PaymentMethodOutputs { get; set; }

    public DbSet<QuotaHistories> QuotaHistories { get; set; }

    //Image Configure : Lưu ảnh promotion và voucher
    public DbSet<ImageConfigure> ImageConfigures { get; set; }

    //PromotionExlcude
    public DbSet<PromotionExclude> PromotionExcludes { get; set; }

    //PaymentCondition
    public DbSet<PaymentCondition> PaymentConditions { get; set; }

    //CustomerFile
    public DbSet<CustomerTags> CustomerTags { get; set; }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        builder.ConfigurePromotionAPI();
    }
}
