﻿using FRTTMO.CAM.PromotionAPI.Entities.AmountCondition;
using FRTTMO.CAM.PromotionAPI.Entities.Campaign;
using FRTTMO.CAM.PromotionAPI.Entities.Common;
using FRTTMO.CAM.PromotionAPI.Entities.Condition;
using FRTTMO.CAM.PromotionAPI.Entities.CostDistribution;
using FRTTMO.CAM.PromotionAPI.Entities.CustomerTags;
using FRTTMO.CAM.PromotionAPI.Entities.FieldConfigure;
using FRTTMO.CAM.PromotionAPI.Entities.ImageConfigure;
using FRTTMO.CAM.PromotionAPI.Entities.InputCacheItem;
using FRTTMO.CAM.PromotionAPI.Entities.InputCacheItemReplace;
using FRTTMO.CAM.PromotionAPI.Entities.InstallmentCondition;
using FRTTMO.CAM.PromotionAPI.Entities.ItemInput;
using FRTTMO.CAM.PromotionAPI.Entities.ItemInputExclude;
using FRTTMO.CAM.PromotionAPI.Entities.ItemInputReplace;
using FRTTMO.CAM.PromotionAPI.Entities.Output;
using FRTTMO.CAM.PromotionAPI.Entities.OutputIntegration;
using FRTTMO.CAM.PromotionAPI.Entities.OutputReplace;
using FRTTMO.CAM.PromotionAPI.Entities.PaymentCondition;
using FRTTMO.CAM.PromotionAPI.Entities.Promotion;
using FRTTMO.CAM.PromotionAPI.Entities.PromotionExclude;
using FRTTMO.CAM.PromotionAPI.Entities.QualifierConfigure;
using FRTTMO.CAM.PromotionAPI.Entities.Quota;
using FRTTMO.CAM.PromotionAPI.Entities.QuotaHistories;
using FRTTMO.CAM.PromotionAPI.Entities.RegionCondition;
using Microsoft.EntityFrameworkCore;
using Volo.Abp;
using Volo.Abp.EntityFrameworkCore.Modeling;

namespace FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;

public static class PromotionAPIDbContextModelCreatingExtensions
{
    public static void ConfigurePromotionAPI(
        this ModelBuilder builder)
    {
        Check.NotNull(builder, nameof(builder));

        // Campaign
        builder.Entity<Campaign>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "Campaign", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();

            b.Property(q => q.Name).IsRequired().HasMaxLength(1000);
            b.Property(q => q.Code).IsRequired().HasMaxLength(50);
            b.Property(q => q.FromDate).IsRequired();
            b.Property(q => q.ToDate).IsRequired();
            b.Property(q => q.CreatedBy).HasMaxLength(50);
            b.Property(q => q.UpdateBy).HasMaxLength(50);

            b.HasIndex(q => q.Code);
            b.HasIndex(q => q.Name);
            b.HasIndex(q => q.FromDate);
            b.HasIndex(q => q.ToDate);
        });

        // Promotion
        builder.Entity<Promotion>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "Promotion", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.Name).IsRequired().HasMaxLength(1000);
            b.Property(q => q.Code).IsRequired().HasMaxLength(50);
            b.Property(q => q.Status).IsRequired();
            b.Property(q => q.FromDate).IsRequired();
            b.Property(q => q.ToDate).IsRequired();
            b.Property(q => q.CreatedBy).HasMaxLength(50);
            b.Property(q => q.CreatedByName).HasMaxLength(100);
            b.Property(q => q.UpdateBy).HasMaxLength(50);
            b.Property(q => q.UpdateByName).HasMaxLength(100);
            b.Property(q => q.PromotionClass).HasMaxLength(100).IsRequired();
            b.Property(q => q.PromotionType).HasMaxLength(100).IsRequired();
            b.Property(q => q.Channels).HasMaxLength(1000);
            b.Property(q => q.StoreTypes).HasMaxLength(1000);
            b.Property(q => q.TradeIndustryCode).HasMaxLength(200);
            b.Property(q => q.SourceOrders).HasMaxLength(1000);
            b.Property(q => q.VerifyScheme).HasMaxLength(1000);
            b.Property(q => q.ProgramType).HasMaxLength(200);
            b.HasIndex(q => q.Id);
            b.HasIndex(q => q.Code);
            b.HasIndex(q => q.Name);
        });

        //FieldConfigure
        builder.Entity<FieldConfigure>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "FieldConfigure", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.Code).IsRequired().HasMaxLength(500);
            b.Property(q => q.Name).IsRequired().HasMaxLength(1000);
            b.Property(q => q.Group).IsRequired().HasMaxLength(200);
            b.Property(q => q.ParentCode).HasMaxLength(200);
            b.Property(q => q.ParentGroup).HasMaxLength(200);

            b.HasIndex(q => q.Code);
            b.HasIndex(q => q.Group);
            b.HasIndex(q => q.ParentGroup);
            b.HasIndex(q => q.ParentCode);
        });

        //QualifierConfigure
        builder.Entity<QualifierConfigure>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "QualifierConfigure",
                PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.PromotionClass).HasMaxLength(100);
            b.Property(q => q.PromotionType).IsRequired().HasMaxLength(100);
            b.Property(q => q.QualifierCode).IsRequired().HasMaxLength(100);
            b.Property(q => q.OperatorCode).IsRequired();

            b.HasIndex(q => q.PromotionClass);
            b.HasIndex(q => q.PromotionType);
        });

        //Quota
        builder.Entity<Quota>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "Quota", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.Type).IsRequired().HasMaxLength(20);
            b.Property(q => q.PromotionId).IsRequired();
            b.Property(q => q.PromotionCode).IsRequired().HasMaxLength(50);

            b.HasIndex(q => q.PromotionCode);
            b.HasIndex(q => q.PromotionId);

        });

        //RegionCondition
        builder.Entity<ProvinceCondition>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "ProvinceCondition", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.PromotionId).IsRequired();
            b.Property(q => q.PromotionCode).IsRequired().HasMaxLength(50);
            b.Property(q => q.ProvinceCode).IsRequired().HasMaxLength(20);
            b.Property(q => q.ProvinceName).IsRequired().HasMaxLength(1000);
            b.HasIndex(q => q.PromotionCode);
            b.HasIndex(q => q.PromotionId);
            b.HasIndex(q => q.ProvinceCode);
        });

        // Counter
        builder.Entity<Counter>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "Counter", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.Value).IsRequired().ValueGeneratedOnAdd();
        });

        //Condition
        builder.Entity<ExtraCondition>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "ExtraCondition", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.PromotionId).IsRequired();
            b.Property(q => q.PromotionCode).HasMaxLength(50).IsRequired();
            b.Property(q => q.QualifierCode).HasMaxLength(100).IsRequired();
            b.Property(q => q.OperatorCode).HasMaxLength(100).IsRequired();
            b.Property(p => p.Number).HasColumnType("decimal(18,2)");

            b.HasIndex(q => q.PromotionId);
        });
        
        //InputCacheItem
        builder.Entity<InputCacheItem>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "InputCacheItem", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.PromotionCode).HasMaxLength(50);
            b.Property(q => q.ItemCode).HasMaxLength(200);
            b.Property(q => q.UnitCode).HasMaxLength(200);

            b.HasIndex(q => q.PromotionCode);
            b.HasIndex(q => q.PromotionId);
        });
        
        //InputCacheItemReplace
        builder.Entity<InputCacheItemReplace>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "InputCacheItemReplace", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.PromotionCode).HasMaxLength(50);
            b.Property(q => q.ItemCode).HasMaxLength(200);
            b.Property(q => q.UnitCode).HasMaxLength(200);

            b.HasIndex(q => q.PromotionCode);
            b.HasIndex(q => q.PromotionId);
        });
        
        //ItemInput
        builder.Entity<ItemInput>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "ItemInput", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.PromotionId).IsRequired();
            b.Property(q => q.CategoryCode).HasMaxLength(100);
            b.Property(q => q.GroupCode).HasMaxLength(100);
            b.Property(q => q.BrandCode).HasMaxLength(100);
            b.Property(q => q.ModelCode).HasMaxLength(100);
            b.Property(q => q.ItemCode).HasMaxLength(100);

            b.HasIndex(q => q.PromotionId);
        });

        //ItemInputExclude
        builder.Entity<ItemInputExclude>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "ItemInputExclude", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.PromotionId).IsRequired();
            b.Property(q => q.ItemCode).IsRequired().HasMaxLength(1000);

            b.HasIndex(q => q.PromotionId);
        });

        //ItemInputReplace
        builder.Entity<ItemInputReplace>(b =>
            {
                b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "ItemInputReplace", PromotionAPIDbProperties.DbSchema);
                b.ConfigureByConvention();
                b.Property(q => q.PromotionId).IsRequired();
                b.Property(q => q.InputItemId).IsRequired();
                b.Property(q => q.CategoryCode).HasMaxLength(100);
                b.Property(q => q.GroupCode).HasMaxLength(100);
                b.Property(q => q.BrandCode).HasMaxLength(100);
                b.Property(q => q.ModelCode).HasMaxLength(100);
                b.Property(q => q.ItemCode).HasMaxLength(100);

                b.HasIndex(q => q.PromotionId);
            });
       
        //Output
        builder.Entity<Output>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "Output", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.PromotionId).IsRequired();
            b.Property(q => q.QualifierCode).HasMaxLength(100);
            b.Property(q => q.OperatorCode).HasMaxLength(100);
            b.Property(q => q.UnitCode).HasMaxLength(100);
            b.Property(q => q.WarehouseCode).HasMaxLength(100);
            b.Property(q => q.WarehouseName).HasMaxLength(1000);
            b.Property(q => q.MaxValue).HasColumnType("decimal(18,2)");
            b.Property(q => q.MinValue).HasColumnType("decimal(18,2)");
            b.Property(q => q.Discount).HasColumnType("decimal(18,2)");
            b.Property(q => q.DiscountPlaceCode).HasMaxLength(100);
            b.Property(q => q.DiscountPlaceName).HasMaxLength(1000);

            b.HasIndex(q => q.PromotionId);
        });
        
        //OutputReplace
        builder.Entity<OutputReplace>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "OutputReplace", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.PromotionId).IsRequired();
            b.Property(q => q.OutputId).IsRequired();
            b.Property(q => q.QualifierCode).HasMaxLength(100);
            b.Property(q => q.OperatorCode).HasMaxLength(100);
            b.Property(q => q.UnitCode).HasMaxLength(100);
            b.Property(q => q.WarehouseCode).HasMaxLength(100);
            b.Property(q => q.WarehouseName).HasMaxLength(1000);
            b.Property(q => q.MaxValue).HasColumnType("decimal(18,2)");
            b.Property(q => q.MinValue).HasColumnType("decimal(18,2)");
            b.Property(q => q.Discount).HasColumnType("decimal(18,2)");
            b.Property(q => q.DiscountPlaceCode).HasMaxLength(100);
            b.Property(q => q.DiscountPlaceName).HasMaxLength(1000);

            b.HasIndex(q => q.PromotionId);
        });

        builder.Entity<CostDistribution>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "CostDistribution", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.Percentage).HasColumnType("decimal(18,2)");
            b.Property(q => q.PromotionId).IsRequired();
            b.HasIndex(q => q.PromotionId);
        });

        builder.Entity<AmountCondition>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "AmountCondition", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.MaxAmount).HasColumnType("decimal(18,2)");
            b.Property(q => q.MinAmount).HasColumnType("decimal(18,2)");
            b.Property(q => q.PromotionId).IsRequired();
            b.HasIndex(q => q.PromotionId);

        });

        builder.Entity<InstallmentCondition>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "InstallmentCondition", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.PromotionId).IsRequired();
            b.HasIndex(q => q.PromotionId);
        });
        
        /// <summary>
        /// out put intergation
        /// </summary>
        builder.Entity<GetItemOutput>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "GetItemOutput", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.PromotionId).IsRequired();
            b.Property(q => q.ItemCode).HasMaxLength(500);
            b.Property(q => q.VoucherCode).HasMaxLength(500);
            b.Property(q => q.UnitCode).HasMaxLength(100);
            b.Property(q => q.WarehouseCode).HasMaxLength(100);

            b.HasIndex(q => q.PromotionId);
        });

        builder.Entity<DiscountItemOutput>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "DiscountItemOutput", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.PromotionId).IsRequired();
            b.Property(q => q.ItemCode);
            b.Property(q => q.Discount).HasColumnType("decimal(18,2)").IsRequired();
            b.Property(q => q.MaxDiscount).HasColumnType("decimal(18,2)");
            b.Property(q => q.DiscountType).IsRequired();

            b.HasIndex(q => q.PromotionId);
        });

        builder.Entity<GetAscendingDiscountOutPut>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "GetAscendingDiscountOutPut", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.PromotionId).IsRequired();
            b.Property(q => q.Discount).HasColumnType("decimal(18,2)").IsRequired();
            b.Property(q => q.MaxDiscount).HasColumnType("decimal(18,2)");
            b.Property(q => q.DiscountType).IsRequired();

            b.HasIndex(q => q.PromotionId);
        });

        builder.Entity<PaymentMethodOutput>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "PaymentMethodOutput", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.PromotionId).IsRequired();
            b.Property(q => q.Discount).HasColumnType("decimal(18,2)").IsRequired();
            b.Property(q => q.MaxDiscount).HasColumnType("decimal(18,2)");
            b.Property(q => q.DiscountType).IsRequired();

            b.HasIndex(q => q.PromotionId);
        });

        builder.Entity<QuotaHistories>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "QuotaHistories", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.PromotionCode).IsRequired();
        });

        //ImageConfigure
        builder.Entity<ImageConfigure>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "ImageConfigure", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.Code).IsRequired().HasMaxLength(500);
            b.Property(q => q.Name).IsRequired().HasMaxLength(1000);
            b.Property(q => q.Group).IsRequired().HasMaxLength(200);

            b.HasIndex(q => q.Code);
            b.HasIndex(q => q.Group);
        });

        //PromotionExclude
        builder.Entity<PromotionExclude>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "PromotionExclude", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.Code).IsRequired().HasMaxLength(500);
            b.Property(q => q.Name).IsRequired().HasMaxLength(1000);
            b.Property(q => q.PromotionCode).IsRequired().HasMaxLength(100);

            b.HasIndex(q => q.Code);
            b.HasIndex(q => q.Name);
        });

        //BankCondition
        builder.Entity<PaymentCondition>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "PaymentCondition", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.BankCodes).HasMaxLength(1000);
            b.Property(q => q.PaymentType).HasMaxLength(1000);
            b.Property(q => q.CardTypes).HasMaxLength(1000);
            b.Property(q => q.PINCodes).HasMaxLength(1000);
            b.HasIndex(q => q.BankCodes);
        });

        //CustomerFile
        builder.Entity<CustomerTags>(b =>
        {
            b.ToTable(PromotionAPIDbProperties.DbTablePrefix + "CustomerTags", PromotionAPIDbProperties.DbSchema);
            b.ConfigureByConvention();
            b.Property(q => q.CustomerGroupCode).HasMaxLength(200);
            b.Property(q => q.CustomerGroupName).HasMaxLength(1000);
            b.Property(q => q.PromotionCode).IsRequired().HasMaxLength(200);
            b.Property(q => q.PromotionName).HasMaxLength(1000);

            b.HasIndex(q => q.CustomerGroupCode);
        });
    }
}