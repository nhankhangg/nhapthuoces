﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;

[DependsOn(
    typeof(PromotionAPIDomainModule),
    typeof(AbpEntityFrameworkCoreModule)
)]
public class PromotionAPIEntityFrameworkCoreModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddAbpDbContext<PromotionAPIDbContext>(options =>
        {
                /* Add custom repositories here. Example:
                 * options.AddRepository<Question, EfCoreQuestionRepository>();
                 */
        });
    }
}
