﻿using FRTTMO.CAM.PromotionAPI.Entities.AmountCondition;
using FRTTMO.CAM.PromotionAPI.Entities.Condition;
using FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;
using FRTTMO.CAM.PromotionAPI.Localization;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace FRTTMO.CAM.PromotionAPI.Repositories
{
    public class AmountConditionRepository : EfCoreRepository<IPromotionAPIDbContext, AmountCondition, Guid>,
            IAmountConditionRepository
    {
        public AmountConditionRepository(IDbContextProvider<IPromotionAPIDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}
