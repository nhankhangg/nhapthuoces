﻿using FRTTMO.CAM.PromotionAPI.Entities.Campaign;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FRTTMO.CAM.PromotionAPI.Dtos.Campaigns;
using FRTTMO.CAM.PromotionAPI.DTOs.Campaigns;
using Microsoft.Extensions.Localization;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;
using FRTTMO.CAM.PromotionAPI.Localization;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Repositories;
using FRTTMO.CAM.PromotionAPI.Enum;

namespace FRTTMO.CAM.PromotionAPI.Repositories
{
    public class CampaignRepository : EfCoreRepository<IPromotionAPIDbContext, Campaign, Guid>, ICampaignRepository
    {
        public CampaignRepository(IDbContextProvider<IPromotionAPIDbContext> dbContextProvider) 
            : base(dbContextProvider)
        {
        }

        public Task<List<Campaign>> SearchCampaignAsync(SearchCampaignDto searchCampaignDto)
        {
            throw new NotImplementedException();
        }
        public async Task<List<Campaign>> GetMyEntitiesAsync(
            string keyword, 
            string sortBy, 
            int skipCount, 
            int maxResultCount)
        {
            var context = await GetDbContextAsync();

            return await context.Campaigns
                .Where(x => x.Code == keyword)
                .OrderBy(x => "Name")
                .Skip(skipCount)
                .Take(maxResultCount)
                .ToListAsync();
           
        }

        public async Task UpdateCampaignStatusExpiredAsync(Guid id)
        {
            var campaign = await FindAsync(id);
            if (campaign != null)
            {
                campaign.Status = (int)CampaignStatusEnum.InActive;
                await UpdateAsync(campaign);
            }
        }
    }
}