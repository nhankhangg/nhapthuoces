﻿using EFCore.BulkExtensions;
using FRTTMO.CAM.PromotionAPI.Entities.CostDistribution;
using FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;
using FRTTMO.CAM.PromotionAPI.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Uow;

namespace FRTTMO.CAM.PromotionAPI.Repositories
{
    public class CostDistributionRepository : EfCoreRepository<IPromotionAPIDbContext, CostDistribution, Guid>,
            ICostDistributionRepository
    {

        private readonly IPromotionAPIDbContext _dbContext;
        public CostDistributionRepository(
            IDbContextProvider<IPromotionAPIDbContext> dbContextProvider,
            IPromotionAPIDbContext dbContext)
            : base(dbContextProvider)
        {
            _dbContext = dbContext;
        }
        public async Task BulkDeleteAsync(Guid promotionId)
        {
            try
            {
                    var context = await GetDbContextAsync() as PromotionAPIDbContext;

                    var records = await _dbContext.CostDistributions
                              .AsNoTracking()
                              .Where(p => p.PromotionId == promotionId)
                              .ToListWithNoLockAsync();

                    await context!.BulkDeleteAsync(records);
                    await context.SaveChangesAsync();
                
            }
            catch (Exception ex)
            {
                throw new Exception(message: ex.Message);
            }
        }
    }
}
