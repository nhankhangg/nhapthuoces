﻿using FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;
using FRTTMO.CAM.PromotionAPI.Entities.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace FRTTMO.CAM.PromotionAPI.Repositories
{
    public class CounterRepository
        : EfCoreRepository<IPromotionAPIDbContext, Counter, Guid>,
            ICounterRepository
    {
        public CounterRepository(IDbContextProvider<IPromotionAPIDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public async Task ResetCounter()
        {
            var dbContext = await GetDbContextAsync();
            await dbContext.Database.ExecuteSqlRawAsync("TRUNCATE TABLE [counter]");
        }

        private async Task<string> GetCounter()
        {
            try
            {
                bool isCounterEmpty = false;
                var query = await GetQueryableAsync();
                if (!query.Any())
                {
                    isCounterEmpty = true;
                    await InsertAsync(new Counter(), true);
                }

                var lastCounter = query
                    .OrderByDescending(p => p.CreationTime)
                    .FirstOrDefault();
                if (lastCounter != null && (lastCounter.CreationTime.Month < DateTime.Now.Month || lastCounter.CreationTime.Year < DateTime.Now.Year))
                {
                    // reset counter
                    await ResetCounter();
                }

                if (!isCounterEmpty)
                {
                    // Get new counter
                    var newCounter = await InsertAsync(new Counter(), true);
                    lastCounter = query.FirstOrDefault(x => x.Value == newCounter.Value);
                }

                if (lastCounter != null)
                {
                    return lastCounter.Value.ToString("D4");
                }

                throw new Exception("Counter is empty");
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
        }

        public async Task<string> GenerateCode(string prefix)
        {
            return $"{prefix}-{DateTime.Now:MMyy}-{await GetCounter()}";
        }
    }
}