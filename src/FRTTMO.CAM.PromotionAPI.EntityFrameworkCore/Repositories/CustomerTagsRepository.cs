﻿using FRTTMO.CAM.PromotionAPI.Entities.Campaign;
using FRTTMO.CAM.PromotionAPI.Entities.CustomerTags;
using FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace FRTTMO.CAM.PromotionAPI.Repositories
{
    public class CustomerTagsRepository : EfCoreRepository<IPromotionAPIDbContext, CustomerTags, Guid>, ICustomerTagsRepository
    {
        public CustomerTagsRepository(IDbContextProvider<IPromotionAPIDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}
