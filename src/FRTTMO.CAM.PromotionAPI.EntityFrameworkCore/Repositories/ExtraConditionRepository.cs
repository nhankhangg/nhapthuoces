﻿using System;
using FRTTMO.CAM.PromotionAPI.Entities.Condition;
using FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;
using FRTTMO.CAM.PromotionAPI.Localization;
using Microsoft.Extensions.Localization;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace FRTTMO.CAM.PromotionAPI.Repositories
{
    public class ExtraConditionRepository
        : EfCoreRepository<IPromotionAPIDbContext, ExtraCondition, Guid>,
            IExtraConditionRepository
    {

        public ExtraConditionRepository(IDbContextProvider<IPromotionAPIDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}