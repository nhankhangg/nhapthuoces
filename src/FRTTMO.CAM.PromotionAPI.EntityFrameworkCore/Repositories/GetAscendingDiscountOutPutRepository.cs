﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using EFCore.BulkExtensions;
using FastMember;
using FRTTMO.CAM.PromotionAPI.Entities.OutputIntegration;
using FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;
using FRTTMO.CAM.PromotionAPI.Localization;
using FRTTMO.CAM.PromotionAPI.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Uow;

namespace FRTTMO.CAM.PromotionAPI.Repositories
{

    public class GetAscendingDiscountOutPutRepository
        : EfCoreRepository<IPromotionAPIDbContext, GetAscendingDiscountOutPut, Guid>,
            IGetAscendingDiscountOutPutRepository
    {
        private readonly IPromotionAPIDbContext _dbContext;
        public GetAscendingDiscountOutPutRepository(
            IDbContextProvider<IPromotionAPIDbContext> dbContextProvider,
            IPromotionAPIDbContext dbContext)
            : base(dbContextProvider)
        {
            _dbContext = dbContext;
        }
        public async Task BulkCopyAsync(List<GetAscendingDiscountOutPut> getAscendingDiscountOutPut)
        {
            var context = await GetDbContextAsync();
            try
            {
                var dataTable = new DataTable();
                await using (var reader = ObjectReader.Create(getAscendingDiscountOutPut))
                {
                    dataTable.Load(reader);
                }
                using var bulkCopy = new SqlBulkCopy(context.Database.GetConnectionString());
                bulkCopy.DestinationTableName = "dbo.GetAscendingDiscountOutPut";
                bulkCopy.ColumnMappings.Clear();
                foreach (DataColumn col in dataTable.Columns)
                {
                    bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                }

                await bulkCopy.WriteToServerAsync(dataTable);
            }
            catch (Exception ex)
            {
                throw new Exception(message: ex.Message);
            }
        }
        public async Task BulkDeleteAsync(Guid promotionId)
        {
            try
            {

                var context = await GetDbContextAsync() as PromotionAPIDbContext;
                while (true)
                {
                    var records = await _dbContext.GetAscendingDiscountOutPuts
                         .AsNoTracking()
                         .Where(p => p.PromotionId == promotionId)
                         .Take(10000)
                         .ToListWithNoLockAsync();

                    if (records.Count == 0)
                    {
                        break;
                    }

                    await context!.BulkDeleteAsync(records);
                }

                await context.SaveChangesAsync();
                //context.Dispose();

            }
            catch (Exception ex)
            {
                throw new Exception(message: ex.Message);
            }
        }
    }
}
