﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using FRTTMO.CAM.PromotionAPI.Entities.OutputIntegration;
using FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using FastMember;
using EFCore.BulkExtensions;
using Volo.Abp.Uow;
using System.Linq;
using FRTTMO.CAM.PromotionAPI.Repository;

namespace FRTTMO.CAM.PromotionAPI.Repositories
{
    public class GetItemOutputRepository : EfCoreRepository<IPromotionAPIDbContext, GetItemOutput, Guid>, IGetItemOutputRepository
    {
        private readonly IPromotionAPIDbContext _dbContext;
        public GetItemOutputRepository(
            IDbContextProvider<IPromotionAPIDbContext> dbContextProvider,
            IPromotionAPIDbContext dbContext)
            : base(dbContextProvider)
        {
            _dbContext = dbContext;
        }
        public async Task BulkCopyAsync(List<GetItemOutput> getItemOutput)
        {
            var context = await GetDbContextAsync();
            try
            {
                var dataTable = new DataTable();
                await using (var reader = ObjectReader.Create(getItemOutput))
                {
                    dataTable.Load(reader);
                }
                using var bulkCopy = new SqlBulkCopy(context.Database.GetConnectionString());
                bulkCopy.DestinationTableName = "dbo.GetItemOutput";
                bulkCopy.ColumnMappings.Clear();
                foreach (DataColumn col in dataTable.Columns)
                {
                    bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                }

                await bulkCopy.WriteToServerAsync(dataTable);
            }
            catch (Exception ex)
            {
                throw new Exception(message: ex.Message);
            }
        }
        public async Task BulkDeleteAsync(Guid promotionId)
        {
            try
            {
                var context = await GetDbContextAsync() as PromotionAPIDbContext;
                while (true)
                {
                    var records = await _dbContext.GetItemOutputs
                          .AsNoTracking()
                          .Where(p => p.PromotionId == promotionId)
                          .Take(10000)
                          .ToListWithNoLockAsync();

                    if (records.Count == 0)
                    {
                        break;
                    }

                    await context!.BulkDeleteAsync(records);
                }

                await context.SaveChangesAsync();
                //context.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception(message: ex.Message);
            }
        }
    }
}
