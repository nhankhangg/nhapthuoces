﻿using FRTTMO.CAM.PromotionAPI.Entities.ImageConfigure;
using FRTTMO.CAM.PromotionAPI.Entities.OutputIntegration;
using FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace FRTTMO.CAM.PromotionAPI.Repositories
{
    public class ImageConfigureRepository : EfCoreRepository<IPromotionAPIDbContext, ImageConfigure, Guid>,
            IImageConfigureRepository
    {
        public ImageConfigureRepository(IDbContextProvider<IPromotionAPIDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}
