﻿using System;
using FastMember;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using FRTTMO.CAM.PromotionAPI.Entities.InputCacheItemReplace;
using FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using System.Data.SqlClient;
using EFCore.BulkExtensions;
using System.Linq;
using Volo.Abp.Uow;
using FRTTMO.CAM.PromotionAPI.Repository;

namespace FRTTMO.CAM.PromotionAPI.Repositories
{
    public class InputCacheItemReplaceRepository
        : EfCoreRepository<IPromotionAPIDbContext, InputCacheItemReplace, Guid>,
            IInputCacheItemReplaceRepository
    {
        private readonly IPromotionAPIDbContext _dbContext;
        public InputCacheItemReplaceRepository(
            IDbContextProvider<IPromotionAPIDbContext> dbContextProvider,
            IPromotionAPIDbContext dbContext)
            : base(dbContextProvider)
        {
            _dbContext = dbContext;
        }
        public async Task BulkCopyAsync(List<InputCacheItemReplace> inputCacheItemReplaces)
        {
            var context = await GetDbContextAsync();
            try
            {
                var dataTable = new DataTable();
                await using (var reader = ObjectReader.Create(inputCacheItemReplaces))
                {
                    dataTable.Load(reader);
                }
                using var bulkCopy = new SqlBulkCopy(context.Database.GetConnectionString());
                bulkCopy.DestinationTableName = "dbo.InputCacheItemReplace";
                bulkCopy.ColumnMappings.Clear();
                foreach (DataColumn col in dataTable.Columns)
                {
                    bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                }

                await bulkCopy.WriteToServerAsync(dataTable);
            }
            catch (Exception ex)
            {
                throw new Exception(message: ex.Message);
            }
        }

        public async Task BulkDeleteAsync(Guid promotionId)
        {
            try
            {
                var context = await GetDbContextAsync() as PromotionAPIDbContext;
                while (true)
                {
                    var records = await _dbContext.InputCacheItemReplaces
                      .AsNoTracking()
                      .Where(p => p.PromotionId == promotionId)
                      .Take(10000)
                      .ToListWithNoLockAsync();

                    if (records.Count == 0)
                    {
                        break;
                    }

                    await context!.BulkDeleteAsync(records);
                }

                await context.SaveChangesAsync();
                //context.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception(message: ex.Message);
            }
        }

        public async Task<double> CountInputCacheItemReplaceNolockAsync(Guid promotionId)
        {
            return await _dbContext.InputCacheItemReplaces
                   .AsNoTracking()
                   .Where(p => p.PromotionId == promotionId)
                   .CountWithNoLockAsync();
        }

        public async Task<List<InputCacheItemReplace>> GetListInputCacheItemReplaceNolockAsync(Guid promotionId)
        {
            return await _dbContext.InputCacheItemReplaces.AsNoTracking()
                          .Where(p => p.PromotionId == promotionId)
                          .ToListWithNoLockAsync();
        }

        public async Task<List<InputCacheItemReplace>> GetListSkipTakeInputCacheItemReplaceNolockAsync(Guid promotionId, int fromSize)
        {
            return await _dbContext.InputCacheItemReplaces.AsNoTracking()
                          .Where(p => p.PromotionId == promotionId)
                          .Skip(fromSize)
                          .Take(10000)
                          .ToListWithNoLockAsync();
        }
    }
}