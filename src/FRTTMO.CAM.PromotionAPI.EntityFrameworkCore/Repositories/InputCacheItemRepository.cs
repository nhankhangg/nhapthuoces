﻿using System;
using FastMember;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using FRTTMO.CAM.PromotionAPI.Entities.InputCacheItem;
using FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using System.Data.SqlClient;
using Volo.Abp.Uow;
using System.Linq;
using EFCore.BulkExtensions;
using FRTTMO.CAM.PromotionAPI.Repository;

namespace FRTTMO.CAM.PromotionAPI.Repositories
{
    public class InputCacheItemRepository
        : EfCoreRepository<IPromotionAPIDbContext, InputCacheItem, Guid>,
            IInputCacheItemRepository
    {
        private readonly IPromotionAPIDbContext _dbContext;
        public InputCacheItemRepository(
            IDbContextProvider<IPromotionAPIDbContext> dbContextProvider, 
            IPromotionAPIDbContext dbContext)
            : base(dbContextProvider)
        {
            _dbContext = dbContext;
        }

        public async Task BulkDeleteAsync(Guid promotionId)
        {
            try
            {
                    var context = await GetDbContextAsync() as PromotionAPIDbContext;
                    while (true)
                    {
                        var records = await _dbContext.InputCacheItems
                           .AsNoTracking()
                           .Where(p => p.PromotionId == promotionId)
                           .Take(10000)
                           .ToListWithNoLockAsync();

                        if (records.Count == 0)
                        {
                            break;
                        }

                        await context!.BulkDeleteAsync(records);
                    }

                    await context.SaveChangesAsync();
                    //context.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception(message: ex.Message);
            }
        }
        public async Task BulkCopyAsync(List<InputCacheItem> inputCacheItems)
        {
            var context = await GetDbContextAsync();
            try
            {
                var dataTable = new DataTable();
                await using (var reader = ObjectReader.Create(inputCacheItems))
                {
                    dataTable.Load(reader);
                }
                using var bulkCopy = new SqlBulkCopy(context.Database.GetConnectionString());
                bulkCopy.DestinationTableName = "dbo.InputCacheItem";
                bulkCopy.ColumnMappings.Clear();
                foreach (DataColumn col in dataTable.Columns)
                {
                    bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                }

                await bulkCopy.WriteToServerAsync(dataTable);
            }
            catch (Exception ex)
            {
                throw new Exception(message: ex.Message);
            }
        }
        public async Task<List<InputCacheItem>> GetListInputCacheItemNolockAsync(string promotionCode)
        {
            return await _dbContext.InputCacheItems.AsNoTracking()
                          .Where(p => p.PromotionCode == promotionCode)
                          .ToListWithNoLockAsync();
        }
    }
}