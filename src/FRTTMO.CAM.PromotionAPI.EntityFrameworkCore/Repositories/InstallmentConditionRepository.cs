﻿using EFCore.BulkExtensions;
using FRTTMO.CAM.PromotionAPI.Entities.Condition;
using FRTTMO.CAM.PromotionAPI.Entities.InstallmentCondition;
using FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;
using FRTTMO.CAM.PromotionAPI.Localization;
using FRTTMO.CAM.PromotionAPI.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Uow;

namespace FRTTMO.CAM.PromotionAPI.Repositories
{
    public class InstallmentConditionRepository : EfCoreRepository<IPromotionAPIDbContext, InstallmentCondition, Guid>,
            IInstallmentConditionRepository
    {
        private readonly IPromotionAPIDbContext _dbContext;
        public InstallmentConditionRepository(
            IDbContextProvider<IPromotionAPIDbContext> dbContextProvider,
            IPromotionAPIDbContext dbContext)
            : base(dbContextProvider)
        {
            _dbContext = dbContext;
        }

        public async Task BulkDeleteAsync(Guid promotionId)
        {
            try
            {
                var context = await GetDbContextAsync() as PromotionAPIDbContext;
                var records = await _dbContext.InstallmentConditions
                .AsNoTracking()
                .Where(p => p.PromotionId == promotionId)
                .ToListWithNoLockAsync();
                await context!.BulkDeleteAsync(records);

                await context.SaveChangesAsync();
                //context.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception(message: ex.Message);
            }
        }
    }
}
