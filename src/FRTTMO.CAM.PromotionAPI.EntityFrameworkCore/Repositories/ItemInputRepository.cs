﻿using System;
using EFCore.BulkExtensions;
using System.Threading.Tasks;
using FRTTMO.CAM.PromotionAPI.Entities.ItemInput;
using FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;
using FRTTMO.CAM.PromotionAPI.Localization;
using Microsoft.Extensions.Localization;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Volo.Abp.Uow;
using FRTTMO.CAM.PromotionAPI.Repository;

namespace FRTTMO.CAM.PromotionAPI.Repositories
{
    public class ItemInputRepository
        : EfCoreRepository<IPromotionAPIDbContext, ItemInput, Guid>,
            IItemInputRepository
    {
        private readonly IPromotionAPIDbContext _dbContext;
        public ItemInputRepository(
            IDbContextProvider<IPromotionAPIDbContext> dbContextProvider,
            IPromotionAPIDbContext dbContext)
            : base(dbContextProvider)
        {
            _dbContext = dbContext;
        }
        public async Task BulkDeleteAsync(Guid promotionId)
        {
            try
            {
                var context = await GetDbContextAsync() as PromotionAPIDbContext;
                while (true)
                {
                    var records = await _dbContext.ItemInputs
                      .AsNoTracking()
                      .Where(p => p.PromotionId == promotionId)
                      .Take(10000)
                      .ToListWithNoLockAsync();

                    if (records.Count == 0)
                    {
                        break;
                    }

                    await context!.BulkDeleteAsync(records);
                }
                await context.SaveChangesAsync();
                //context.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception(message: ex.Message);
            }
        }
    }
}