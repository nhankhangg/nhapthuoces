﻿using EFCore.BulkExtensions;
using FRTTMO.CAM.PromotionAPI.Entities.PaymentCondition;
using FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;
using FRTTMO.CAM.PromotionAPI.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace FRTTMO.CAM.PromotionAPI.Repositories
{
    public class PaymentConditionRepository : EfCoreRepository<IPromotionAPIDbContext, PaymentCondition, Guid>,
            IPaymentConditionRepository
    {

        private readonly IPromotionAPIDbContext _dbContext;
        public PaymentConditionRepository(
            IDbContextProvider<IPromotionAPIDbContext> dbContextProvider,
            IPromotionAPIDbContext dbContext)
            : base(dbContextProvider)
        {
            _dbContext = dbContext;
        }
        public async Task BulkDeleteAsync(Guid promotionId)
        {
            try
            {
                var context = await GetDbContextAsync() as PromotionAPIDbContext;

                var records = await _dbContext.PaymentConditions
                          .AsNoTracking()
                          .Where(p => p.PromotionId == promotionId)
                          .ToListWithNoLockAsync();

                await context!.BulkDeleteAsync(records);
                await context.SaveChangesAsync();

            }
            catch (Exception ex)
            {
                throw new Exception(message: ex.Message);
            }
        }
    }
}
