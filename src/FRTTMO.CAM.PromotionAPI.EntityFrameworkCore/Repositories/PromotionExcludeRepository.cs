﻿using EFCore.BulkExtensions;
using FastMember;
using FRTTMO.CAM.PromotionAPI.Entities.Promotion;
using FRTTMO.CAM.PromotionAPI.Entities.PromotionExclude;
using FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;
using FRTTMO.CAM.PromotionAPI.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace FRTTMO.CAM.PromotionAPI.Repositories
{
    public class PromotionExcludeRepository : EfCoreRepository<IPromotionAPIDbContext, PromotionExclude, Guid>,
            IPromotionExcludeRepository
    {
        private readonly IPromotionAPIDbContext _dbContext;
        public PromotionExcludeRepository(
            IDbContextProvider<IPromotionAPIDbContext> dbContextProvider,
            IPromotionAPIDbContext dbContext)
            : base(dbContextProvider)
        {
            _dbContext = dbContext;
        }
        public async Task BulkCopyAsync(List<PromotionExclude> promotionExcludes)
        {
            var context = await GetDbContextAsync();
            try
            {
                var dataTable = new DataTable();
                await using (var reader = ObjectReader.Create(promotionExcludes))
                {
                    dataTable.Load(reader);
                }
                using var bulkCopy = new SqlBulkCopy(context.Database.GetConnectionString());
                bulkCopy.DestinationTableName = "dbo.PromotionExclude";
                bulkCopy.ColumnMappings.Clear();
                foreach (DataColumn col in dataTable.Columns)
                {
                    bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                }

                await bulkCopy.WriteToServerAsync(dataTable);
            }
            catch (Exception ex)
            {
                throw new Exception(message: ex.Message);
            }
        }
       
        public async Task BulkDeleteAsync(string promotionCode)
        {
            try
            {
                var context = await GetDbContextAsync() as PromotionAPIDbContext;
                while (true)
                {
                    var records = await _dbContext.PromotionExcludes
                      .AsNoTracking()
                      .Where(p => p.PromotionCode == promotionCode)
                      .Take(10000)
                      .ToListWithNoLockAsync();

                    if (records.Count == 0)
                    {
                        break;
                    }

                    await context!.BulkDeleteAsync(records);
                }

                await context.SaveChangesAsync();
                //context.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception(message: ex.Message);
            }
        }

        public async Task BulkDeleteAsync(List<PromotionExclude> promotionCodes)
        {
            try
            {
                var context = await GetDbContextAsync() as PromotionAPIDbContext;
                await context!.BulkDeleteAsync(promotionCodes);
                await context.SaveChangesAsync();
                //context.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception(message: ex.Message);
            }
        }

        public async Task<List<PromotionExclude>> GetListPromotionExcludeNolockAsync(string promotionCode)
        {
            return await _dbContext.PromotionExcludes.AsNoTracking()
                          .Where(p => p.PromotionCode == promotionCode)
                          .ToListWithNoLockAsync();
        }

        public async Task<List<PromotionExclude>> GetListPromotionExcludeNolockAsync(List<string> promotionCodes)
        {
            return await _dbContext.PromotionExcludes.AsNoTracking()
                          .Where(p => promotionCodes.Contains(p.PromotionCode))
                          .ToListWithNoLockAsync();
        }
    }
}
