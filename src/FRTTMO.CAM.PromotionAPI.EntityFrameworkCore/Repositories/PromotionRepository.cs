﻿using FRTTMO.CAM.PromotionAPI.Entities.Promotion;
using System;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;
using System.Threading.Tasks;
using FRTTMO.CAM.PromotionAPI.Enum;
using System.Collections.Generic;
using FRTTMO.CAM.PromotionAPI.Repository;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace FRTTMO.CAM.PromotionAPI.Repositories
{
    public class PromotionRepository
        : EfCoreRepository<IPromotionAPIDbContext, Promotion, Guid>,
            IPromotionRepository
    {
        private readonly IPromotionAPIDbContext _dbContext;

        public PromotionRepository(IDbContextProvider<IPromotionAPIDbContext> dbContextProvider, IPromotionAPIDbContext dbContext)
            : base(dbContextProvider)
        {
            _dbContext = dbContext;
        }

        public async Task UpdatePromotionStatusExpiredAsync(Guid id)
        {
            var promotion = await FindAsync(id);
            if (promotion != null)
            {
                promotion.Status = (int)PromotionStatusEnum.InActive;
                await UpdateAsync(promotion);
            }    
        }
        
        public async Task<List<string>> GetPromotionReCacheAsync()
        {
            var listStatus = new List<int> { 1, 2 };
            var promotions = await _dbContext.Promotions.Where(w => listStatus.Contains(w.Status)).Select(s => new { s.Id,s.Code }).ToListWithNoLockAsync();
            var inputs = await _dbContext.ItemInputs.Where(w=>(string.Equals(w.ItemCode,null) || string.Equals(w.ItemCode,string.Empty))&&
                                                            (string.Equals(w.GiftCode, null) || string.Equals(w.GiftCode, string.Empty)) &&
                                                            promotions.Select(s=>s.Id).Contains(w.PromotionId))
                                                    .Select(s=>s.PromotionId).ToListWithNoLockAsync();
            var inputReplaces = await _dbContext.ItemInputReplaces.Where(w => (string.Equals(w.ItemCode, null) || string.Equals(w.ItemCode, string.Empty)) &&
                                                            (string.Equals(w.GiftCode, null) || string.Equals(w.GiftCode, string.Empty)) &&
                                                            promotions.Select(s => s.Id).Contains(w.PromotionId))
                                                    .Select(s => s.PromotionId).ToListWithNoLockAsync();
            var promotionIds = inputs.Union(inputReplaces);
            return promotions.Where(w=>promotionIds.Contains(w.Id)).Select(s=>s.Code).ToList();
        }

        public async Task<List<Promotion>> GetPromotionActiveDateAsync()
        {
            var promotions = await _dbContext.Promotions.AsNoTracking().Where(x => 
                x.ActiveDate == DateTime.Now.Date 
             && x.Status != (int)PromotionStatusEnum.InActive
             && x.ToDate.Date >= DateTime.Now.Date).ToListWithNoLockAsync();
            return promotions;
        }

        public async Task<List<Promotion>> GetPromotionFromStastusInActiveAsync(List<string> promotionCodes)
        {
            var promotions = await _dbContext.Promotions.AsNoTracking()
                .Where(x => x.Status == (int)PromotionStatusEnum.InActive && promotionCodes.Contains(x.Code))
                .ToListWithNoLockAsync();
            return promotions;
        }
    }
}