﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using EFCore.BulkExtensions;
using FastMember;
using FRTTMO.CAM.PromotionAPI.Entities.RegionCondition;
using FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;
using FRTTMO.CAM.PromotionAPI.Repository;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace FRTTMO.CAM.PromotionAPI.Repositories
{
    public class ProvinceConditionRepository : EfCoreRepository<IPromotionAPIDbContext, ProvinceCondition, Guid>, IProvinceConditionRepository
    {
        private readonly IPromotionAPIDbContext _dbContext;
        public ProvinceConditionRepository(
            IDbContextProvider<IPromotionAPIDbContext> dbContextProvider,
            IPromotionAPIDbContext dbContext)
            : base(dbContextProvider)
        {
            _dbContext = dbContext;
        }
        public async Task BulkCopyAsync(List<ProvinceCondition> shopConditions)
        {
            var context = await GetDbContextAsync();
            try
            {
                var dataTable = new DataTable();
                await using (var reader = ObjectReader.Create(shopConditions))
                {
                    dataTable.Load(reader);
                }
                using var bulkCopy = new SqlBulkCopy(context.Database.GetConnectionString());
                bulkCopy.DestinationTableName = "dbo.ProvinceCondition";
                bulkCopy.ColumnMappings.Clear();
                foreach (DataColumn col in dataTable.Columns)
                {
                    bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                }

                await bulkCopy.WriteToServerAsync(dataTable);
            }
            catch (Exception ex)
            {
                throw new Exception(message: ex.Message);
            }
        }

        public async Task<List<string>> BulkDeleteAsync(Guid promotionId)
        {
            try
            {
                    var listShop = new List<string>();
                    var context = await GetDbContextAsync() as PromotionAPIDbContext;

                    var records = await _dbContext.ProvinceConditions
                              .AsNoTracking()
                              .Where(p => p.PromotionId == promotionId)
                              .ToListWithNoLockAsync();
                    listShop.AddRange(records.Select(p=>p.ProvinceCode));
                    await context!.BulkDeleteAsync(records);
                    await context.SaveChangesAsync();
                return listShop;
            }
            catch (Exception ex)
            {
                throw new Exception(message: ex.Message);
            }
        }
    }   
}