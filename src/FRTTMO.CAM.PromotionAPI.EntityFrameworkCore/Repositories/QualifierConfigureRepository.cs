﻿using System;
using FRTTMO.CAM.PromotionAPI.Entities.QualifierConfigure;
using FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;
using FRTTMO.CAM.PromotionAPI.Localization;
using Microsoft.Extensions.Localization;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace FRTTMO.CAM.PromotionAPI.Repositories
{
    public class QualifierConfigureRepository : EfCoreRepository<IPromotionAPIDbContext, QualifierConfigure, Guid>, IQualifierConfigureRepository
    {
        private readonly IStringLocalizer<PromotionAPIResource> L;
        
        public QualifierConfigureRepository(IDbContextProvider<IPromotionAPIDbContext> dbContextProvider,
            IStringLocalizer<PromotionAPIResource> l)
            : base(dbContextProvider)
        {
            L = l;
        }
    }   
}