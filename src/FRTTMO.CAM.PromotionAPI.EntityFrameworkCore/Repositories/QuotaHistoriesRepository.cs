﻿using System;
using Volo.Abp.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using FRTTMO.CAM.PromotionAPI.Localization;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using FRTTMO.CAM.PromotionAPI.Entities.QuotaHistories;
using FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;

namespace FRTTMO.CAM.PromotionAPI.Repositories
{
    public class QuotaHistoriesRepository : EfCoreRepository<IPromotionAPIDbContext, QuotaHistories, Guid>, IQuotaHistoriesRepository
    {
        private readonly IStringLocalizer<PromotionAPIResource> _l;

        public QuotaHistoriesRepository(IDbContextProvider<IPromotionAPIDbContext> dbContextProvider,
            IStringLocalizer<PromotionAPIResource> l)
            : base(dbContextProvider)
        {
            _l = l;
        }
    }
}
