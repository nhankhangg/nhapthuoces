﻿using System;
using FRTTMO.CAM.PromotionAPI.Entities.Quota;
using FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;
using FRTTMO.CAM.PromotionAPI.Localization;
using Microsoft.Extensions.Localization;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace FRTTMO.CAM.PromotionAPI.Repositories
{
    public class QuotaRepository : EfCoreRepository<IPromotionAPIDbContext, Quota, Guid>, IQuotaRepository
    {
        private readonly IStringLocalizer<PromotionAPIResource> L;

        public QuotaRepository(IDbContextProvider<IPromotionAPIDbContext> dbContextProvider,
            IStringLocalizer<PromotionAPIResource> l)
            : base(dbContextProvider)
        {
            L = l;
        }
    }
}