﻿using System;
using System.Threading.Tasks;
using FRT.Common.Authorize.Attribute;
using FRTTMO.CAM.PromotionAPI.Constants;
using FRTTMO.CAM.PromotionAPI.Dtos.Campaigns;
using FRTTMO.CAM.PromotionAPI.DTOs.Campaigns;
using FRTTMO.CAM.PromotionAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Auditing;

namespace FRTTMO.CAM.PromotionAPI.Controllers;

[Route("api/campaigns")]
public class CampaignController : PromotionAPIController
{
    private readonly ICampaignAppService _campaignAppService;

    public CampaignController(ICampaignAppService campaignAppService)
    {
        _campaignAppService = campaignAppService;
    }

    [DisableAuditing]
    [HttpGet]
    [Route("{id:guid}")]
    [ProducesResponseType(200)]
    [AuthorizePermissionCommon(PermissionPromotionConstants.Promotion_Permission_Campaign_View)]
    public Task<CampaignDto> GetAsync(Guid id)
    {
        return _campaignAppService.GetAsync(id);
    }

    [HttpPost]
    [ProducesResponseType(200)]
    [AuthorizePermissionCommon(PermissionPromotionConstants.Promotion_Permission_Campaign_Create)]
    public Task<CampaignDto> CreateAsync([FromBody] CreateCampaignDto createDto)
    {
        return _campaignAppService.CreateAsync(createDto);
    }

    [HttpPut]
    [Route("{id:guid}")]
    [ProducesResponseType(200)]
    [AuthorizePermissionCommon(PermissionPromotionConstants.Promotion_Permission_Campaign_Update)]
    public Task<CampaignDto> UpdateAsync(Guid id, [FromBody] UpdateCampaignDto updateDto)
    {
        return _campaignAppService.UpdateAsync(id, updateDto);
    }

    [DisableAuditing]
    [HttpPost]
    [Route("filter")]
    [ProducesResponseType(200)]
    [AuthorizePermissionCommon(PermissionPromotionConstants.Promotion_Permission_Campaign_View)]
    public Task<PagedResultDto<CampaignDto>> SearchCampaignAsync([FromBody] SearchCampaignDto searchCampaign)
    {
        return _campaignAppService.SearchCampaignAsync(searchCampaign);
    }
}


