﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using FRTTMO.CAM.PromotionAPI.DTOs.Common;
using FRTTMO.CAM.PromotionAPI.DTOs.FieldConfigures;
using FRTTMO.CAM.PromotionAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Auditing;

namespace FRTTMO.CAM.PromotionAPI.Controllers;

public class CommonController : PromotionAPIController, ICommonAppService
{
    private readonly ICommonAppService _commonAppService;

    public CommonController(ICommonAppService commonAppService)
    {
        _commonAppService = commonAppService;
    }

    [HttpPost("api/field-configure")]
    public Task<List<CommonDto>> CreatePromotionImageAsync(List<CreateCommonDto> input)
    => _commonAppService.CreatePromotionImageAsync(input);

    [HttpPut("api/field-configure")]
    public async Task<bool> DeletePromotionImageAsync([Required]string group,[FromBody] IList<string> code)
    {
        return await _commonAppService.DeletePromotionImageAsync(group, code);
    }

    [DisableAuditing]
    [HttpPost("api/search-field-configure/promotion-image")]
    public async Task<PagedResultDto<CommonDto>> FilterPromotionImageAsync(FilterPromotionImageDto imageDto)
    {
        return await _commonAppService.FilterPromotionImageAsync(imageDto);
    }

    [DisableAuditing]
    [HttpGet("api/app-version")]
    public Task<CommonDto> GetAppVersionAsync()
    {
        return _commonAppService.GetAppVersionAsync();
    }

    [DisableAuditing]
    [HttpGet("api/field-configure")]
    public async Task<List<CommonDto>> GetFieldConfiguresAsync([FromQuery] FieldConfigureFilterDto input)
    {
        return await _commonAppService.GetFieldConfiguresAsync(input);
    }

    [DisableAuditing]
    [HttpGet("api/qualifier-configure")]
    public async Task<List<QualifierConfigureDto>> GetQualifierConfigureAsync(
        [FromQuery] QualifierConfigureFilterDto input)
    {
        return await _commonAppService.GetQualifierConfigureAsync(input);
    }
    [HttpGet("api/get-voucher-define")]
    public async Task<List<VoucherDefineDto>> GetVoucherDefineAsync(List<string> voucherCodes)
    {
        return await _commonAppService.GetVoucherDefineAsync(voucherCodes);
    }

    [DisableAuditing]
    [HttpPut("api/update-app-version")]
    public Task<CommonDto> UpdateAppVersionAsync(string version)
    {
        return _commonAppService.UpdateAppVersionAsync(version);
    }
}