﻿using FRT.Common.Authorize.Attribute;
using FRTTMO.CAM.PromotionAPI.Constants;
using FRTTMO.CAM.PromotionAPI.Dtos.Campaigns;
using FRTTMO.CAM.PromotionAPI.DTOs.Campaigns;
using FRTTMO.CAM.PromotionAPI.DTOs.CustomerFiles;
using FRTTMO.CAM.PromotionAPI.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;

namespace FRTTMO.CAM.PromotionAPI.Controllers
{
    [Route("api/customerTag")]
    public class CustomerTagController : PromotionAPIController
    {
        private readonly ICustomerTagAppService _customerFileAppService;
        public CustomerTagController(ICustomerTagAppService customerFileAppService)
        {
            _customerFileAppService = customerFileAppService;
        }

        [HttpPost]
        [Route("filter")]
        [ProducesResponseType(200)]
        public Task<PagedResultDto<CustomerTagDto>> SearchCustomerFileAsync([FromBody] SearchCustomerTagDto searchCustomerFile)
        {
            return _customerFileAppService.SearchCustomerFileAsync(searchCustomerFile);
        }

        [HttpPost]
        [ProducesResponseType(200)]
        public Task<CustomerTagDto> CreateAsync([FromBody] CreateCustomerTagDto createDto)
        {
            return _customerFileAppService.CreateAsync(createDto);
        }

        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(200)]
        public Task<CustomerTagDto> GetAsync(Guid id)
        {
            return _customerFileAppService.GetAsync(id);
        }

        [HttpPut]
        [Route("{id:guid}")]
        [ProducesResponseType(200)]
        public Task<CustomerTagDto> UpdateAsync(Guid id, [FromBody] UpdateCustomerTagDto updateDto)
        {
            return _customerFileAppService.UpdateAsync(id, updateDto);
        }
    }
}
