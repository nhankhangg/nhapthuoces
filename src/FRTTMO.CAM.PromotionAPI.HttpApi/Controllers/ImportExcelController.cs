﻿using FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels;
using FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels.ItemExcludes;
using FRTTMO.CAM.PromotionAPI.DTOs.ImportExcels.Shops;
using FRTTMO.CAM.PromotionAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FRTTMO.CAM.PromotionAPI.Controllers
{
    [Route("api/import/")]
    public class ImportExcelController : PromotionAPIController, IImportExcelAppService
    {
        private readonly IImportExcelAppService _importExcelAppService;
        public ImportExcelController(IImportExcelAppService importExcelAppService)
        {
            _importExcelAppService = importExcelAppService;
        }


        //[HttpPost("input-condition")]
        //public Task<ImportExcelDto> ImportExcelInputConditionAsync(IFormFile formFile,string promotionType, string templateType)
        //{
        //    return _importExcelAppService.ImportExcelInputConditionAsync(formFile, promotionType, templateType);
        //}

        //[HttpPost("output-condition")]
        //public Task<ImportExcelDto> ImportExcelOutputConditionAsync(IFormFile formFile, string promotionType, string templateType)
        //{
        //    return _importExcelAppService.ImportExcelOutputConditionAsync(formFile, promotionType, templateType);
        //}

        [HttpPost("input-output-condition")]
        public Task<ImportExcelDto> ImportExcelSchemeAsync(IFormFile formFile, string promotionType, string templateType, bool isInput)
        {
            return _importExcelAppService.ImportExcelSchemeAsync(formFile, promotionType, templateType, isInput);
        }

        [HttpPost("product-exclude-condition")]
        public Task<ImportItemExcludesDto> ImportItemExcludesAsync(IFormFile formFile)
        {
            return _importExcelAppService.ImportItemExcludesAsync(formFile);
        }

        [HttpPost("shop-condition")]
        public Task<ImportItemShopDto> ImportShopAsync([FromQuery] List<string> storeType, DateTime fromDate, DateTime toDate ,IFormFile formFile)
        {
            return _importExcelAppService.ImportShopAsync(storeType, fromDate, toDate, formFile);
        }
    }
}
