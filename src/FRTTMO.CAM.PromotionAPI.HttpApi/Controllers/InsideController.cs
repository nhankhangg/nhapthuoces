﻿using FRTTMO.CAM.PromotionAPI.Dtos.InsideApi;
using FRTTMO.CAM.PromotionAPI.DTOs.Insides;
using FRTTMO.CAM.PromotionAPI.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Auditing;

namespace FRTTMO.CAM.PromotionAPI.Controllers
{
    [Route("api/inside")]
    [DisableAuditing]
    public class InsideController : PromotionAPIController
    {
        private readonly IInsideAppService _insideAppService;
        public InsideController(IInsideAppService insideAppService)
        {
            _insideAppService = insideAppService;
        }

        [HttpGet("shop")]
        public Task<List<ListShopOutputDto>> ShopAll(string shopType)
        {
            return _insideAppService.ShopAll(shopType);
        }

        [HttpGet("department")]
        public Task<List<DepartmentDto>> GetListDepartmentAsync()
            => _insideAppService.GetListDepartmentAsync();

    }
}
