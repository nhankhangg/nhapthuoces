﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using FRTTMO.CAM.PromotionAPI.Dtos;
using FRTTMO.CAM.PromotionAPI.DTOs;
using FRTTMO.CAM.PromotionAPI.DTOs.PIM;
using FRTTMO.CAM.PromotionAPI.Paginations;
using FRTTMO.CAM.PromotionAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.Auditing;

namespace FRTTMO.CAM.PromotionAPI.Controllers;

[Area(PromotionAPIRemoteServiceConsts.ModuleName)]
[RemoteService(Name = PromotionAPIRemoteServiceConsts.RemoteServiceName)]
[Route("api/pim")]
[DisableAuditing]
public class PIMController : PromotionAPIController
{
    private readonly IPIMAppService _pimService;

    public PIMController(IPIMAppService pimService)
    {
        _pimService = pimService;
    }
    [HttpGet("search-category-level")]
    public async Task<PaginationResultDto<CategoryLevelDto>> SearchCategoryLevelAsync(SearchCategoryLevelInputDto input, Pagination pagination)
    {
        return await _pimService.SearchCategoryLevelAsync(input, pagination);
    }

    [HttpGet]
    [Route("search-product")]
    public async Task<PaginationResultDto<ProductDetailsDto>> SearchProductAsync(SearchProductInputDto input,
        Pagination pagination)
    {
        return await _pimService.SearchProductAsync(input, pagination);
    }

    //[HttpGet]
    //[Route("list-detail")]
    //public async Task<List<ItemDetails>> ListProductDetailAsync([FromQuery] [Required] List<string> skus)
    //{
    //    return await _pimService.ListProductDetailAsync(skus);
    //}

    //[HttpGet]
    //[Route("list-product-by-pim-cache-product")]
    //public async Task<PimProductCacheDto> GetListProductByPimCacheProductAsync(string skus)
    //{
    //    return await _pimService.GetProductByPimCacheAsync(skus);
    //}

    //[HttpGet]
    //[Route("list-product-by-pim-cache")]
    //public async Task<PimProductCacheDto> GetListProductByPimCacheAsync([FromQuery] PimCacheSearch pimCacheSearch)
    //{
    //    return await _pimService.GetListProductByPimCacheAsync(pimCacheSearch);
    //}

    [HttpGet]
    [Route("list-measures")]
    public async Task<List<MeasureUnitDto>> ListMeasureAsync()
    {
        return await _pimService.MeasureUnitsAsync();
    }

    [HttpPost]
    [Route("validation-categories")]
    public async Task<List<CategoryValidationDto>> CategoriesValidationAsync(List<CategoryValidationInputDto> pIMValidateInputDtoss)
    {
        return await _pimService.CategoriesValidationAsync(pIMValidateInputDtoss);
    }

    //[HttpGet]
    //[Route("group")]
    //public async Task<GetProductCategoryOutputDto> GetGroupByCategoryId(GroupInputDto input)
    //{
    //    return await _pimService.GetGroupByCategoryId(input);
    //}
    [HttpGet]
    [Route("list-pim")]
    public async Task<PimProductCacheDto> GetListProductByPimCacheAsync(PimCacheSearch pimCacheSearch)
    {
        return await _pimService.GetListProductByPimCacheAsync(pimCacheSearch);
    }
}