﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using FRTTMO.CAM.PromotionAPI.DTOs.Promotion;
using FRTTMO.CAM.PromotionAPI.DTOs.Quota;
using FRTTMO.CAM.PromotionAPI.ETOs.Event;
using FRTTMO.CAM.PromotionAPI.Services;
using Microsoft.AspNetCore.Mvc;
using FRTTMO.CAM.PromotionAPI.Enum;
using FRTTMO.CAM.PromotionAPI.DTOs.PromotionExcludes;
using FRT.Common.Authorize.Attribute;
using FRTTMO.CAM.PromotionAPI.Constants;
using FRTTMO.CAM.PromotionAPI.ETOs.Sync;
using Volo.Abp.Auditing;

namespace FRTTMO.CAM.PromotionAPI.Controllers;

[Route("api/promotion")]
public class PromotionController : PromotionAPIController
{
    private readonly IPromotionAppService _promotionAppService;

    public PromotionController(IPromotionAppService promotionAppService)
    {
        _promotionAppService = promotionAppService;
    }

    [HttpPost]
    [Route("pim-change-price")]
    [ProducesResponseType(200)]
    public Task<bool> PimChangePrice([FromBody] PimChangePriceEto pimChangePrice)
        => _promotionAppService.ChangePricePromotionAsync(pimChangePrice);


    [HttpPost]
    [Route("recache")]
    [ProducesResponseType(200)]
    public Task<List<string>> ReCachePromotion([FromBody] List<string> promotionCodes, ActionType actionType = ActionType.Create)
        => _promotionAppService.ReCachePromotionAsync(promotionCodes, actionType);

    [HttpGet]
    [Route("{id:Guid}")]
    [ProducesResponseType(200)]
    [AuthorizePermissionCommon(PermissionPromotionConstants.Promotion_Permission_Promotion_View)]
    public Task<PromotionDto> GetAsync(Guid id)
        => _promotionAppService.GetAsync(id);
    

    [HttpGet]
    [Route("all")]
    [ProducesResponseType(200)]
    [AuthorizePermissionCommon(PermissionPromotionConstants.Promotion_Permission_Promotion_View)]
    public Task<List<PromotionDto>> GetListAsync(SearchPromotionDto searchPromotionDto)
         => _promotionAppService.GetListAsync(searchPromotionDto);
    

    [HttpPost]
    [ProducesResponseType(200)]
    [AuthorizePermissionCommon(PermissionPromotionConstants.Promotion_Permission_Promotion_Create)]
    public Task<PromotionDto> CreateAsync([FromBody] CreatePromotionDto createDto)
        => _promotionAppService.CreateAsync(createDto);

    [HttpPut]
    [Route("{promotionId:guid}")]
    [ProducesResponseType(200)]
    [AuthorizePermissionCommon(PermissionPromotionConstants.Promotion_Permission_Promotion_Update)]
    public Task<PromotionDto> UpdateAsync(Guid promotionId,[FromBody] UpdatePromotionDto updateDto)
        => _promotionAppService.UpdateAsync(promotionId,updateDto);


    [DisableAuditing]
    [HttpPost]
    [Route("filter")]
    [ProducesResponseType(200)]
    [AuthorizePermissionCommon(PermissionPromotionConstants.Promotion_Permission_Promotion_View)]
    public Task<PagedResultDto<PromotionDto>> SearchPromotionAsync([FromBody] SearchPromotionDto searchPromotionDto)
            => _promotionAppService.SearchPromotionAsync(searchPromotionDto);

    [HttpPost]
    [Route("time-expire-cache")]
    [ProducesResponseType(200)]
    public Task TimeExpireCacheAsync([FromBody] List<PromotionTimeExpireEto> promotionTimeExpire)
        => _promotionAppService.TimeExpireCacheAsync(promotionTimeExpire);

    [HttpPost]
    [Route("use-quota")]
    [ProducesResponseType(200)]
    public Task<List<ResponseQuotaDto>> UseQuotaAsync([FromBody] List<UseOrCheckQuotaDto> useOrCheckQuotaDto)
        => _promotionAppService.UseQuotaAsync(useOrCheckQuotaDto);

    [HttpPost]
    [Route("check-quota")]
    [ProducesResponseType(200)]
    public Task<List<ResponseCheckQuotaDto>> CheckQuotaAsync([FromBody] List<UseOrCheckQuotaDto> useOrCheckQuotaDto)
        => _promotionAppService.CheckQuotaAsync(useOrCheckQuotaDto);

    [DisableAuditing]
    [HttpPost]
    [Route("exclude/suggest")]
    [ProducesResponseType(200)]
    public Task<List<SuggestPromotionExcludeDto>> SuggestPromotionExcludeAsync([FromBody] SuggestPromotionExcludeRequestDto input)
    {
        return _promotionAppService.SuggestPromotionExcludeAsync(input);
    }

    [HttpGet]
    [Route("{promotionId}/check-update-permission")]
    [ProducesResponseType(200)]
    public Task<CheckUpdatePermissionDto> CheckUpdatePermissionAsync(Guid promotionId)
    {
        return _promotionAppService.CheckUpdatePermissionAsync(promotionId);
    }

    [DisableAuditing]
    [HttpPost]
    [Route("search-by-item")]
    [ProducesResponseType(200)]
    public Task<PagedResultDto<PromotionByItemDto>> SearchPromotionWithItemCodeAsync([FromBody] SearchPromotionByItemDto input)
    {
        return _promotionAppService.SearchPromotionWithItemCodeAsync(input);
    }

    [DisableAuditing]
    [HttpGet("{promotionCode}/quota")]
    [ProducesResponseType(200)]
    public Task<UsedQuotaDto> GetUsedQuotaAsync(string promotionCode)
    {
        return _promotionAppService.GetUsedQuotaAsync(promotionCode);
    }

    [HttpGet]
    [Route("{promotionCode}")]
    [ProducesResponseType(200)]
    public Task<PromotionDto> GetByCodeAsync(string promotionCode)
        => _promotionAppService.GetByCodeAsync(promotionCode);

    [HttpPut]
    [Route("clear-promotion-inactive-redis")]
    [ProducesResponseType(200)]
    public Task ClearPromotionInactiveFromRedisAsync([FromBody] List<string> promotionCodes)
        => _promotionAppService.ClearPromotionInactiveFromRedisAsync(promotionCodes);
}