﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FRTTMO.CAM.PromotionAPI.DTOs.Promotion;
using FRTTMO.CAM.PromotionAPI.Services;
using Microsoft.AspNetCore.Mvc;
using FRTTMO.CAM.PromotionAPI.DTOs.Quota;
using FRTTMO.CAM.PromotionAPI.DTOs.CostDistribution;
using FRTTMO.CAM.PromotionAPI.DTOs.RegionCondition;
using FRTTMO.CAM.PromotionAPI.DTOs.InstallmentConditions;
using FRTTMO.CAM.PromotionAPI.DTOs.Promotion.PromotionDetails;
using UpdatePromotionHeaderDto = FRTTMO.CAM.PromotionAPI.DTOs.Promotion.PromotionDetails.UpdatePromotionHeaderDto;
using FRTTMO.CAM.PromotionAPI.DTOs.ItemInput;
using FRTTMO.CAM.PromotionAPI.DTOs.Output;
using FRTTMO.CAM.PromotionAPI.DTOs.AmountConditions;
using FRTTMO.CAM.PromotionAPI.DTOs.Condition;

namespace FRTTMO.CAM.PromotionAPI.Controllers
{

    public class PromotionDetailsController : PromotionAPIController
    {

        private readonly IPromotionDetailsAppService _promotionDetailsAppService;

        public PromotionDetailsController(IPromotionDetailsAppService promotionDetailsAppService)
        {
            _promotionDetailsAppService = promotionDetailsAppService;
        }


        //[HttpPost]
        //[Route("api/promotion/quota")]
        //[ProducesResponseType(200)]
        //public Task<QuotaDto> QuotaAsync([FromBody] CreateQuotaDto createQuotaDto) 
        //    => _promotionDetailsAppService.QuotaAsync(createQuotaDto);
        

        //[HttpPut]
        //[Route("api/promotion/{promotionId}/quota")]
        //[ProducesResponseType(200)]
        //public Task<QuotaDto> UpdateQuotaAsync(Guid promotionId, [FromBody] UpdateQuotaDto updateQuotaDto)
        //    => _promotionDetailsAppService.UpdateQuotaAsync(promotionId, updateQuotaDto);

        //[HttpPost]
        //[Route("api/promotion/cost-distribution")]
        //[ProducesResponseType(200)]
        //public Task<List<CostDistributionDto>> CostDistributionAsync([FromBody] List<CreateCostDistributionDto> createCostDistributionDto)
        //    => _promotionDetailsAppService.CostDistributionAsync(createCostDistributionDto);
        

        //[HttpPut]
        //[Route("api/promotion/{promotionId}/cost-distribution")]
        //[ProducesResponseType(200)]
        //public Task<List<CostDistributionDto>> UpdateCostDistributionAsync(Guid promotionId, [FromBody] List<UpdateCostDistributionDto> updateCostDistributionDto)
        //    => _promotionDetailsAppService.UpdateCostDistributionAsync(promotionId,updateCostDistributionDto);

        //[HttpPost]
        //[Route("api/promotion/installment-condition")]
        //[ProducesResponseType(200)]
        //public Task<List<InstallmentConditionDto>> InstallmentConditionAsync([FromBody]List<CreateInstallmentConditionDto> createInstallmentConditionDto)
        //    => _promotionDetailsAppService.InstallmentConditionAsync(createInstallmentConditionDto);

        //[HttpPut]
        //[Route("api/promotion/{promotionId}/installment-condition")]
        //[ProducesResponseType(200)]
        //public Task<List<InstallmentConditionDto>> UpdateInstallmentConditionAsync(Guid promotionId, 
        //    [FromBody] List<UpdateInstallmentConditionDto> updateInstallmentConditionDtos)
        //{
        //    return _promotionDetailsAppService.UpdateInstallmentConditionAsync(promotionId, updateInstallmentConditionDtos);
        //}


        //[HttpPost]
        //[Route("api/promotion/shop-condition")]
        //[ProducesResponseType(200)]
        //public Task<List<ShopConditionDto>> ShopConditionAsync([FromBody] List<CreateShopConditionDto> createShopConditionDto)
        //    => _promotionDetailsAppService.ShopConditionAsync(createShopConditionDto);

        //[HttpPut]
        //[Route("api/promotion/{promotionId}/shop-condition")]
        //[ProducesResponseType(200)]
        //public Task<List<ShopConditionDto>> UpdateShopConditionAsync(Guid promotionId, [FromBody] List<UpdateRegionConditionDto> updateShopConditionDtos)
        //    => _promotionDetailsAppService.UpdateShopConditionAsync(promotionId, updateShopConditionDtos);
        


        //[HttpPost]
        //[Route("api/promotion/extracondition")]
        //[ProducesResponseType(200)]
        //public Task<PromotionDto> ExtraconditionAsync(Guid id)
        //{
        //    return _promotionDetailsAppService.GetAsync(id);
        //}

        //[HttpPut]
        //[Route("api/promotion/{id}/extracondition")]
        //[ProducesResponseType(200)]
        //public Task<PromotionDto> UpdateExtraconditionAsync(Guid id)
        //{
        //    return _promotionDetailsAppService.GetAsync(id);
        //}

        [HttpPut]
        [Route("api/promotion/{promotionId}/header-promotion")]
        [ProducesResponseType(200)]
        public Task<PromotionHeaderDto> UpdatePromotionHeaderAsync(Guid promotionId,[FromBody] UpdatePromotionHeaderDto updatePromotionHeaderDto)
        {
            return _promotionDetailsAppService.UpdatePromotionHeaderAsync(promotionId, updatePromotionHeaderDto);
        }

        [HttpPut]
        [Route("api/promotion/{promotionId}/web-promotion")]
        [ProducesResponseType(200)]
        public Task<PromotionWebDto> UpdatePromotionWebAsync(Guid promotionId, [FromBody] UpdatePromotionWebDto updatePromotionWebDto)
        {
            return _promotionDetailsAppService.UpdatePromotionWebAsync(promotionId, updatePromotionWebDto);
        }

        [HttpPut]
        [Route("api/promotion/{promotionId}/status-promotion")]
        [ProducesResponseType(200)]
        public Task<PromotionUpdateDetailResponseDto> UpdatePromotionStatusAsync(Guid promotionId, int? status)
        {
            return _promotionDetailsAppService.UpdateStatusPromotionAsync(promotionId, status);
        }

        
        //[HttpPut]
        //[Route("api/promotion/{promotionId}/input-output-promotion")]
        //[ProducesResponseType(200)]
        //public Task<PromotionUpdateDetailResponseDto> UpdateInputOutputPromotionAsync(Guid promotionId, [FromBody] UpdateInputOutputDto updateInputOutputDto)
        //{
        //    return _promotionDetailsAppService.UpdateInputOutputAsync(promotionId, updateInputOutputDto);
        //}

        [HttpPut]
        [Route("api/promotion/{promotionId}/cost-distribution-promotion")]
        [ProducesResponseType(200)]
        public Task<PromotionUpdateDetailResponseDto> UpdateCostDistributionPromotionAsync(Guid promotionId, [FromBody] List<UpdateCostDistributionDto> updateCostDistributionDtos)
        {
            return _promotionDetailsAppService.UpdateCostDistributionAsync(promotionId, updateCostDistributionDtos);
        }

        [HttpPut]
        [Route("api/promotion/{promotionId}/quota-promotion")]
        [ProducesResponseType(200)]
        public Task<QuotaDto> UpdateQuotaPromotionAsync(Guid promotionId, [FromBody] UpdateQuotaDto updateQuotaDto)
        {
            return _promotionDetailsAppService.UpdateQuotaAsync(promotionId, updateQuotaDto);
        }

        [HttpPut]
        [Route("api/promotion/{promotionId}/amount-condition-promotion")]
        [ProducesResponseType(200)]
        public Task<AmountConditionDto> UpdateAmountConditionPromotionAsync(Guid promotionId, [FromBody] UpdateAmountConditionDto updateAmountConditionDto)
        {
            return _promotionDetailsAppService.UpdateAmountConditionAsync(promotionId, updateAmountConditionDto);
        }

        [HttpPut]
        [Route("api/promotion/{promotionId}/installment-condition-promotion")]
        [ProducesResponseType(200)]
        public Task<PromotionUpdateDetailResponseDto> UpdateInstallmentConditionPromotionAsync(Guid promotionId, [FromBody] List<UpdateInstallmentConditionDto> updateInstallmentConditionDtos)
        {
            return _promotionDetailsAppService.UpdateInstallmentConditionAsync(promotionId, updateInstallmentConditionDtos);
        }

        [HttpPut]
        [Route("api/promotion/{promotionId}/extra-condition-promotion")]
        [ProducesResponseType(200)]
        public Task<PromotionUpdateDetailResponseDto> UpdateExtraConditionPromotionAsync(Guid promotionId, [FromBody] UpdateExtraConditionDto updateExtraConditionDto)
        {
            return _promotionDetailsAppService.UpdateExtraConditionAsync(promotionId, updateExtraConditionDto);
        }

        [HttpPut]
        [Route("api/promotion/{promotionId}/shop-condition-promotion")]
        [ProducesResponseType(200)]
        public Task<PromotionUpdateDetailResponseDto> UpdateShopConditionPromotionAsync(Guid promotionId, [FromBody] List<UpdateProvinceConditionDto> updateShopConditionDtos)
        {
            return _promotionDetailsAppService.UpdateShopConditionAsync(promotionId, updateShopConditionDtos);
        }

        [HttpPut]
        [Route("api/promotion/{promotionId:guid}/input-promotion")]
        [ProducesResponseType(200)]
        public Task<PromotionUpdateDetailResponseDto> UpdateInputAsync(Guid promotionId, [FromBody] UpdateInputDetailsDto updateItemInputDto)
        {
            return _promotionDetailsAppService.UpdateInputAsync(promotionId, updateItemInputDto);
        }

        [HttpPut]
        [Route("api/promotion/{promotionId:guid}/output-promotion")]
        [ProducesResponseType(200)]
        public Task<PromotionUpdateDetailResponseDto> UpdateOutputAsync(Guid promotionId, [FromBody] UpdateOutputDetailsDto updateOutputDto)
        {
            return _promotionDetailsAppService.UpdateOutputAsync(promotionId, updateOutputDto);
        }
    }
}
