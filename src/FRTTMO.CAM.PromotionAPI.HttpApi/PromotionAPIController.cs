﻿using FRTTMO.CAM.PromotionAPI.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace FRTTMO.CAM.PromotionAPI;

public abstract class PromotionAPIController : AbpControllerBase
{
    protected PromotionAPIController()
    {
        LocalizationResource = typeof(PromotionAPIResource);
    }
}