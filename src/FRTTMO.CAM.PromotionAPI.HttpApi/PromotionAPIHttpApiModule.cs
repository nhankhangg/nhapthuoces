﻿using FRTTMO.CAM.PromotionAPI.Localization;
using Localization.Resources.AbpUi;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;

namespace FRTTMO.CAM.PromotionAPI;

[DependsOn(
    typeof(PromotionAPIApplicationContractsModule),
    typeof(AbpAspNetCoreMvcModule))]
public class PromotionAPIHttpApiModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        PreConfigure<IMvcBuilder>(mvcBuilder =>
        {
            mvcBuilder.AddApplicationPartIfNotExists(typeof(PromotionAPIHttpApiModule).Assembly);
        });
    }

    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpLocalizationOptions>(options =>
        {
            options.Resources
                .Get<PromotionAPIResource>()
                .AddBaseTypes(typeof(AbpUiResource));
        });
    }
}