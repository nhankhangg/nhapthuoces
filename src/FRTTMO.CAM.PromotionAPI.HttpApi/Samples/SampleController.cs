﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;

namespace FRTTMO.CAM.PromotionAPI.Samples;

[Area(PromotionAPIRemoteServiceConsts.ModuleName)]
[RemoteService(Name = PromotionAPIRemoteServiceConsts.RemoteServiceName)]
[Route("api/PromotionAPI/sample")]
public class SampleController : PromotionAPIController, ISampleAppService
{
    private readonly ISampleAppService _sampleAppService;

    public SampleController(ISampleAppService sampleAppService)
    {
        _sampleAppService = sampleAppService;
    }

    [HttpGet]
    public async Task<SampleDto> GetAsync()
    {
        return await _sampleAppService.GetAsync();
    }

    [HttpGet]
    [Route("authorized")]
    [Authorize]
    public async Task<SampleDto> GetAuthorizedAsync()
    {
        return await _sampleAppService.GetAsync();
    }
}