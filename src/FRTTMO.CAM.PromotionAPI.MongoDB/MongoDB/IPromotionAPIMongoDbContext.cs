﻿using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace FRTTMO.CAM.PromotionAPI.MongoDB;

[ConnectionStringName(PromotionAPIDbProperties.ConnectionStringName)]
public interface IPromotionAPIMongoDbContext : IAbpMongoDbContext
{
    /* Define mongo collections here. Example:
     * IMongoCollection<Question> Questions { get; }
     */
}