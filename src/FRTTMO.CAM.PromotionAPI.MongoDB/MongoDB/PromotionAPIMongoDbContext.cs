﻿using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace FRTTMO.CAM.PromotionAPI.MongoDB;

[ConnectionStringName(PromotionAPIDbProperties.ConnectionStringName)]
public class PromotionAPIMongoDbContext : AbpMongoDbContext, IPromotionAPIMongoDbContext
{
    /* Add mongo collections here. Example:
     * public IMongoCollection<Question> Questions => Collection<Question>();
     */

    protected override void CreateModel(IMongoModelBuilder modelBuilder)
    {
        base.CreateModel(modelBuilder);

        modelBuilder.ConfigurePromotionAPI();
    }
}