﻿using Volo.Abp;
using Volo.Abp.MongoDB;

namespace FRTTMO.CAM.PromotionAPI.MongoDB;

public static class PromotionAPIMongoDbContextExtensions
{
    public static void ConfigurePromotionAPI(
        this IMongoModelBuilder builder)
    {
        Check.NotNull(builder, nameof(builder));
    }
}