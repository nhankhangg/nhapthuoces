﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;
using Volo.Abp.MongoDB;

namespace FRTTMO.CAM.PromotionAPI.MongoDB;

[DependsOn(
    typeof(PromotionAPIDomainModule),
    typeof(AbpMongoDbModule)
)]
public class PromotionAPIMongoDbModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddMongoDbContext<PromotionAPIMongoDbContext>(options =>
        {
            /* Add custom repositories here. Example:
             * options.AddRepository<Question, MongoQuestionRepository>();
             */
        });
    }
}