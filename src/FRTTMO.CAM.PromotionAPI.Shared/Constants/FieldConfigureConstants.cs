﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.Constants
{
    public static class FieldConfigureConstants
    {
        public static string AmountCondition = "AmountCondition";
        public static string ApplicableMethod = "ApplicableMethod";
        public static string CampaignStatus = "CampaignStatus";
        public static string CardType = "CardType";
        public static string Channel = "Channel";
        public static string CustomerGroup = "CustomerGroup";
        public static string Department = "Department";
        public static string DisplayArea = "DisplayArea";
        public static string InstallmentCondition = "InstallmentCondition";
        public static string InterestRateType = "InterestRateType";
        public static string Operator = "Operator";
        public static string OrderType = "OrderType";
        public static string Organization = "Organization";
        public static string Priority = "Priority";
        public static string PromotionClass = "PromotionClass";
        public static string PromotionStatus = "PromotionStatus";
        public static string PromotionType = "PromotionType";
        public static string Qualifier = "Qualifier";
        public static string Quota = "Quota";
        public static string ResetQuotaType = "ResetQuotaType";
        public static string StoreType = "StoreType";
        public static string Tenure = "Tenure";
        public static string Warehouse = "Warehouse";
        public static string SourceOrder = "SourceOrder";
        public static string SendMethod = "SendMethod";
        public static string PromotionImage = "PromotionImage";
        public static string VoucherImage = "VoucherImage";
        public static string VerifyScheme = "VerifyScheme";
        public static string GetItemCode = "GetItemCode";
        public const string AmountOff = "AmountOff";
        public static string FixedPrice = "FixedPrice";
        public static string GetGiftCard = "GetGiftCard";
        public static string PercentOff = "PercentOff";
        public static string GetCoupon = "GetCoupon";
    }
}
