﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.Constants
{
    public static class ImportExcelConstants
    {
        //PromotionType
        public const string BuyComboGetDiscount = "BuyComboGetDiscount";
        public const string BuyComboGetProduct = "BuyComboGetProduct";
        public const string BuyProductGetProduct = "BuyProductGetProduct";
        public const string TotalBillAmount = "TotalBillAmount";
        public const string TotalProductAmount = "TotalProductAmount";
        public const string BuyProductGetAscendingDiscount = "BuyProductGetAscendingDiscount";
        public const string BuyProductGetDiscount = "BuyProductGetDiscount";
        public const string UsePaymentMethod = "UsePaymentMethod";

        //Template Type
        public const string Product = "Product";
        public const string Category = "Category";

        //Type Condition
        public const string Input = "Input";
        public const string Output = "Output";

        //Shop Condition Channel
        public const string FShop = "FShop";
        public const string FStudio = "FStudio";


        //Unit
        public const string MaxUnit = "Đơn vị lớn nhất";
        public const string AllUnit = "Tất cả";
    }
}
