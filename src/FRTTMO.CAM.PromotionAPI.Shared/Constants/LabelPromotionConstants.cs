﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.Constants
{
    public static class LabelPromotionConstants
    {
        public const string BuyProductGetAscendingDiscount = "BuyProductGetAscendingDiscount";
        public const string BuyProductGetProduct = "BuyProductGetProduct";
        public const string GetProduct = "Tặng quà";
        public const string BigDeal = "Ưu đãi lớn";
    }
}
