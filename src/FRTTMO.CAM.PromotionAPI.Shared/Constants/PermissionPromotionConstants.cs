﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.Constants
{
    public static class PermissionPromotionConstants
    {
        public const string Promotion_Permission_Campaign_Create = "Promotion_Permission_Campaign_Create";
        public const string Promotion_Permission_Campaign_View = "Promotion_Permission_Campaign_View";
        public const string Promotion_Permission_Campaign_Update = "Promotion_Permission_Campaign_Update";
        public const string Promotion_Permission_Promotion_Create = "Promotion_Permission_Promotion_Create";
        public const string Promotion_Permission_Promotion_View = "Promotion_Permission_Promotion_View";
        public const string Promotion_Permission_Promotion_Update = "Promotion_Permission_Promotion_Update";
    }
}
