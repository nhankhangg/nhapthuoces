﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FRTTMO.CAM.PromotionAPI.DynamicQueries;

public static class ExtensionQuery
{
    public static Task<List<T>> ToListAsyncAsCustom<T>(this IQueryable<T> query)
    {
        return Task.Factory.StartNew(query.ToList);
    }
}