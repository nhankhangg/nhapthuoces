﻿using System.ComponentModel.DataAnnotations;

namespace FRTTMO.CAM.PromotionAPI.Enum;

public enum ApplicableMethodEnum
{
    [Display(Name = "Đơn hàng")] Order = 1,
    [Display(Name = "Khách hàng")] Customer = 2,

    [Display(Name = "Sau hoàn tất đơn hàng")]
    AfterCompletedOrder = 3
}