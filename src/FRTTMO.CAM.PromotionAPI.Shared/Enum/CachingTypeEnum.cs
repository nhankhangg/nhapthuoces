﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.Enum
{
    public enum CachingTypeEnum
    {
        [Display(Name = "tặng sản phẩm")]
        GetProduct,
        [Display(Name = "giảm giá sản phẩm")]
        GetDiscount,
        [Display(Name = "càng mua càng rẻ giảm giá")]
        GetAscendingDiscount,
        [Display(Name = "Tổng giá trị theo hóa đơn")]
        TotalBillAmount,
        [Display(Name = "Tổng giá trị theo sản phẩm")]
        TotalProductAmount,
        [Display(Name = "Phương thức thanh toán")]
        PaymentMethodCode
    }

    public enum ActionType
    {
        Create,
        Update,
        Active,
        InActive,
        UpdateHeader,
        UpdateInput,
        UpdateOutPut,
        UpdateShopCondition,
        ChangePrice
    }
}
