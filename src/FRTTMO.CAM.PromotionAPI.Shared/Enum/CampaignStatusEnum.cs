﻿using System.ComponentModel.DataAnnotations;

namespace FRTTMO.CAM.PromotionAPI.Enum;

public enum CampaignStatusEnum
{
    [Display(Name = "Khởi tạo")] Created = 1,
    [Display(Name = "Còn hiệu lực")] Activated = 2,
    [Display(Name = "Hết hiệu lực")] InActive = 3
}