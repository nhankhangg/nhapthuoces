﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Xml.Linq;

namespace FRTTMO.CAM.PromotionAPI.Enum
{
    public enum CustomerFileStatusEnum
    {
        [Display(Name = "Còn hiệu lực")] Activated = 1,
        [Display(Name = "Hết hiệu lực")] InActive = 2
    }
}
