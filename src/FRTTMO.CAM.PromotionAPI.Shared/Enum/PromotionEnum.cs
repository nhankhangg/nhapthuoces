﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FRTTMO.CAM.PromotionAPI.Enum;

public enum PromotionStatusEnum
{
    [Display(Name = "Khởi tạo")] 
    Created = 1,
    [Display(Name = "Còn hiệu lực")] 
    Activated = 2,
    [Display(Name = "Hết hiệu lực")] 
    InActive = 3
}

public enum DownPaymentTypeEnum
{
    None,
    PercentOff,
    AmountOff
}

public enum InterestRateTypeEnum
{
    Optional,
    Zero,
    HalfPercent,
    NinetyNineHundredthsPercent,
    NonZero
}

public enum PIMCategoryLevelEnum
{
    Category = 1,
    Type,
    Group
}