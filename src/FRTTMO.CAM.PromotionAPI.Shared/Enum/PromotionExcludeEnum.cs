﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.Enum
{
    public enum PromotionTypeEnum
    {
        BuyProductGetProduct = 1,
        BuyProductGetDiscount,
        BuyProductGetAscendingDiscount,
        BuyComboGetProduct,
        BuyComboGetDiscount,
        TotalBillAmount,
        TotalProductAmount,
        UsePaymentMethod
    }
    public enum PromotionExcludeTypeEnum
    {
        ByProduct = 1,
        ByOrder,
        ByPayment,
        Option
    }
    public static class PromotionExcludeTypeMapping
    {
        public static Dictionary<PromotionExcludeTypeEnum, Tuple<List<PromotionTypeEnum>, List<PromotionTypeEnum>, bool>> PromotionExcludeType = 
            new Dictionary<PromotionExcludeTypeEnum, Tuple<List<PromotionTypeEnum>, List<PromotionTypeEnum>, bool>>
        {
            {
                PromotionExcludeTypeEnum.ByProduct,
                Tuple.Create(new List<PromotionTypeEnum>()
                {
                    PromotionTypeEnum.BuyProductGetProduct,
                    PromotionTypeEnum.BuyProductGetDiscount,
                    PromotionTypeEnum.BuyProductGetAscendingDiscount,
                    PromotionTypeEnum.BuyComboGetProduct,
                    PromotionTypeEnum.BuyComboGetDiscount,
                    PromotionTypeEnum.TotalProductAmount
                },
                new List<PromotionTypeEnum>()
                {
                    PromotionTypeEnum.TotalBillAmount,
                }, true)
            },
            {
                PromotionExcludeTypeEnum.ByOrder,
                Tuple.Create(new List<PromotionTypeEnum>()
                {
                    PromotionTypeEnum.TotalBillAmount,
                    PromotionTypeEnum.TotalProductAmount
                },
                new List<PromotionTypeEnum>(), false)
            },
            {
                PromotionExcludeTypeEnum.ByPayment,
                Tuple.Create(new List<PromotionTypeEnum>()
                {
                    PromotionTypeEnum.UsePaymentMethod
                },
                new List<PromotionTypeEnum>(), false)
            },
            {
                PromotionExcludeTypeEnum.Option,
                Tuple.Create(new List<PromotionTypeEnum>(), new List<PromotionTypeEnum>(), false)
            }
        };
    }
}
