﻿using System.ComponentModel.DataAnnotations;

namespace FRTTMO.CAM.PromotionAPI.Enum
{
    public enum QualifierTypeEnum
    {
        [Display(Name = "Tặng sản phẩm")]
        GetItemCode = 1,
        [Display(Name = "Giảm giá tiền")]
        AmountOff = 2,
        [Display(Name = "Giảm giá cố định")]
        FixedPrice = 3,
        [Display(Name = "Giảm giá phần trăm")]
        PercentOff = 4,
        [Display(Name = "Voucher không giá")]
        GetGiftCard = 5,
        [Display(Name = "Voucher có giá")]
        GetCoupon = 6,
        [Display(Name = "Giảm giá sản phẩm theo phần trăm %")]
        ProductPercentOff = 7,
        [Display(Name = "Giảm giá sản phẩm theo số tiền cụ thể")]
        ProductAmountOff = 8,
        [Display(Name = "Giảm giá sản phẩm theo số tiền cố định")]
        ProductFixedPrice = 9
    }
}
