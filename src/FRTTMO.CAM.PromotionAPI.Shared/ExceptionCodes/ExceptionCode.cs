﻿namespace FRTTMO.CAM.PromotionAPI.ExceptionCodes;

public static class ExceptionCode
{
    public static readonly string BadRequest = "PROMOTION:00403";

    public static readonly string InternalServerError = "PROMOTION:00500";

    public static readonly string NotFound = "PROMOTION:00404";
}