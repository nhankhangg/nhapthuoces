﻿using System;
using System.Runtime.Serialization;
using Microsoft.Extensions.Logging;
using Volo.Abp;
using Volo.Abp.ExceptionHandling;
using Volo.Abp.Logging;

namespace FRTTMO.CAM.PromotionAPI.ExceptionCodes;

[Serializable]
public class FrtPromotionValidationException : Exception, IHasErrorCode, IHasErrorDetails, IHasLogLevel,
    IUserFriendlyException
{
    public FrtPromotionValidationException(
        string code = null,
        string message = null,
        string details = null,
        Exception innerException = null,
        LogLevel logLevel = LogLevel.Error) : base(message, innerException)
    {
        Code = code;
        Details = details;
        LogLevel = logLevel;
    }

    public FrtPromotionValidationException(SerializationInfo serializationInfo, StreamingContext context) : base(
        serializationInfo, context)
    {
    }

    public string Code { get; set; }
    public string Details { get; set; }

    public LogLevel LogLevel { get; set; }

    public FrtPromotionValidationException WithData(string name, object value)
    {
        Data[name] = value;
        return this;
    }
}