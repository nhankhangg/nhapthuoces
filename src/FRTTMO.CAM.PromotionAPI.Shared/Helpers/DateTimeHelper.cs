﻿using System;
using System.Globalization;

namespace FRTTMO.CAM.PromotionAPI.Helpers;

public static class DateTimeHelper
{
    public static bool IsValidHourFormat(this string input)
    {
        var cultureInfo = CultureInfo.InvariantCulture;
        return DateTime.TryParseExact(input, DateTimeFormat.HourFormat, cultureInfo, DateTimeStyles.None,
            out var result);
    }

    public static TimeSpan FromHourToTimeSpan(this string input)
    {
        return DateTime.ParseExact(input, "HH:mm:ss", CultureInfo.InvariantCulture).TimeOfDay;
    }

    public static string FromTimeSpanToTimeFormat(this TimeSpan input)
    {
        return input.ToString();
    }

    public static bool IsBetweenDate(this DateTime input, DateTime? minDate, DateTime? maxDate)
    {
        return input >= minDate && input <= maxDate;
    }

    public static bool IsBetweenHour(this DateTime now, TimeSpan start, TimeSpan end)
    {
        if (start == end)
        {
            return true;
        }

        var time = now.TimeOfDay;
        if (start <= end)
        {
            return time >= start && time <= end;
        }

        return time >= start || time <= end;
    }
}