﻿using System;
using System.Text.RegularExpressions;

namespace FRTTMO.CAM.PromotionAPI.Helpers;

public static class MobilePhoneHelper
{
    public const string PatternPhoneNumber = "^[0-9]{8,16}$";

    public static string ParseMobilePhone(string input)
    {
        if (input.IsNullOrEmpty())
        {
            return DateTime.Now.ToString("yyMMddHHmmssfff");
        }

        if (input.Length < 1)
        {
            return DateTime.Now.ToString("yyMMddHHmmssfff");
        }

        if (input.Length == 10 && input.StartsWith("0"))
        {
            return "84" + input.Substring(1).Trim();
        }

        return "Err_" + input;
    }

    public static string ParsePhoneNumber(this string phoneNumber)
    {
        if (phoneNumber.IsNullOrEmpty())
        {
            return phoneNumber;
        }

        if (phoneNumber.StartsWith("0"))
        {
            return $"84{phoneNumber.Substring(1)}";
        }

        return phoneNumber;
    }

    public static bool ValidatePhoneNumberRegex(string phoneNumber)
    {
        Regex validatePhoneNumberRegex = new(PatternPhoneNumber, RegexOptions.Compiled | RegexOptions.IgnoreCase);
        return validatePhoneNumberRegex.IsMatch(phoneNumber);
    }
}