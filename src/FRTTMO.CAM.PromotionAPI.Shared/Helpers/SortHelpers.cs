﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace FRTTMO.CAM.PromotionAPI.Helpers;

public static class SortHelpers
{
    public static List<T> Sort<T>(List<T> input, string property, string action)
    {
        return action.ToLower() switch
        {
            "desc" => input.OrderByDescending(p => p.GetType()
                    .GetProperty(property, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance)!
                    .GetValue(p, null))
                .ToList(),
            _ => input.OrderBy(p => p.GetType()
                    .GetProperty(property, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance)!
                    .GetValue(p, null))
                .ToList()
        };
    }
}