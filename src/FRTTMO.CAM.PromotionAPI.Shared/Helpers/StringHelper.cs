﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FRTTMO.CAM.PromotionAPI.Helpers;

public static class StringHelper
{
    public static string SafeStringJoin(this string[] arr, string separator = ",")
    {
        if (arr == null || !arr.Any())
        {
            return null;
        }

        return arr.Where(x => !x.IsNullOrEmpty() && !x.Trim().IsNullOrEmpty()).JoinAsString(separator);
    }

    public static string SafeStringJoin(this IEnumerable<string> arr, string separator = ",")
    {
        var enumerable = arr.ToList();
        if (!enumerable.Any())
        {
            return null;
        }

        return enumerable.Where(x => !x.IsNullOrEmpty() && !x.Trim().IsNullOrEmpty()).JoinAsString(separator);
    }

    public static string[] SafeStringSplit(this string str, string separator = ",")
    {
        if (str.IsNullOrEmpty())
        {
            return null;
        }

        return str?.Split(separator).Select(x => x.Trim()).Where(x => !x.IsNullOrEmpty()).ToArray();
    }

    public static IList<Guid> SafeStringToListGuidSplit(this string str, string separator = ",")
    {
        if (str.IsNullOrEmpty())
        {
            return null;
        }

        return str?.Split(separator).Select(x => x.Trim())
            .Where(x => !x.IsNullOrEmpty() && Guid.TryParse(x, out var result)).Select(x => Guid.Parse(x)).ToList();
    }

    public static bool CompareAsLowerCase(this string a, string b)
    {
        return string.Equals(a, b, StringComparison.OrdinalIgnoreCase);
    }

    public static string BuildUrl(this string baseUrl, Dictionary<string, object> queryParams)
    {
        if (queryParams == null || !queryParams.Any())
        {
            return $"{baseUrl}";
        }


        var parameters = queryParams.Select(item =>
            {
                var value = Convert.ToString(item.Value);

                if (value.Contains(","))
                {
                    value = value.Replace(",", $"&{item.Key}=");
                }

                return $"{item.Key}={Convert.ToString(value)}";
            })
            .ToList();

        return $"{baseUrl}?{string.Join("&", parameters)}";
    }
}