﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FRTTMO.CAM.PromotionAPI.Kafka
{
    public class KafkaTopics
    {
        //topic from pim change price
        public const string PromotionChangePrice = "ict.pim.pricing.productprice.active";

        //topic for ecom

        public const string PromotionDetailsSync = "ict.promotion.details.sync";

        public const string PromotoinFinishSync = "ict.promotion.finish.sync";

        public const string PromotoinChangePriceSync = "ict.promotion.change.price.sync";

        public const string PromotoinInActiveSync = "ict.promotion.inactive.sync";

        public const string PromotoinActiveSync = "ict.promotion.active.sync";
    }
}
