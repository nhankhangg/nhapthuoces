﻿using Volo.Abp.Localization;

namespace FRTTMO.CAM.PromotionAPI.Localization;

[LocalizationResourceName("PromotionAPI")]
public class PromotionAPIResource
{
}