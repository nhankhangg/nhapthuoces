﻿namespace FRTTMO.CAM.PromotionAPI.Paginations;

public class Pagination
{
    public int? SkipCount { get; set; } = 0;
    public int? MaxResultCount { get; set; } = 10;
}