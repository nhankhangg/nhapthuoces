﻿using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs.Hangfire;
using Volo.Abp.Http.Client.IdentityModel;
using Volo.Abp.Modularity;

namespace FRTTMO.CAM.PromotionAPI;

[DependsOn(
    typeof(AbpAutofacModule),
    typeof(PromotionAPIHttpApiClientModule),
    typeof(AbpHttpClientIdentityModelModule),
    typeof(AbpBackgroundJobsHangfireModule)

    )]
public class PromotionAPIConsoleApiClientModule : AbpModule
{

}
