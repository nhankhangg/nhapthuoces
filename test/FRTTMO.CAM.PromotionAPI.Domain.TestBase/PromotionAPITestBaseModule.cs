﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using FRTTMO.CAM.PromotionAPI.Services;
using Hangfire;
using Hangfire.RecurringJobExtensions;
using Volo.Abp;
using Volo.Abp.Authorization;
using Volo.Abp.Autofac;
//using Volo.Abp.Caching.StackExchangeRedis;
//using Volo.Abp.BackgroundJobs;
using Volo.Abp.Data;
//using Volo.Abp.IdentityServer;
using Volo.Abp.Modularity;
using Volo.Abp.Threading;
using Microsoft.Extensions.Options;
using Volo.Abp.BackgroundJobs.Hangfire;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Http.Client;

namespace FRTTMO.CAM.PromotionAPI;

[DependsOn(
    typeof(AbpAutofacModule),
    typeof(AbpTestBaseModule),
    typeof(AbpAuthorizationModule),
    typeof(PromotionAPIDomainModule),
    typeof(AbpBackgroundJobsHangfireModule)

    )]
public class PromotionAPITestBaseModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        var configBuilderOpts = new AbpConfigurationBuilderOptions
        {
            EnvironmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")
        };
        var config = ConfigurationHelper.BuildConfiguration(configBuilderOpts);
        var configuration = context.Services.GetConfiguration();

        Configure<AbpDbContextOptions>(options =>
        {
            options.UseSqlServer();
        });

        context.Services.AddDistributedRedisCache(options =>
        {
            options.Configuration = config["Redis:Configuration"];
        });

        context.Services.AddHangfire(config =>
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            {
                config.UseSqlServerStorage(configuration.GetConnectionString("AbpAuditLogging"))
                    .UseRecurringJob(typeof(HangFireAppService));
            }
        });
        context.Services.AddHttpClient("PIM").ConfigureHttpClient((service, client) =>
        {
            var options = service.GetRequiredService<IOptions<AbpRemoteServiceOptions>>();
            var posOption = options.Value.RemoteServices.GetConfigurationOrDefault("PIM");
            client.BaseAddress = new Uri(posOption.BaseUrl);
        });
    }

    public override void OnApplicationInitialization(ApplicationInitializationContext context)
    {
        SeedTestData(context);
    }

    private static void SeedTestData(ApplicationInitializationContext context)
    {
        AsyncHelper.RunSync(async () =>
        {
            using (var scope = context.ServiceProvider.CreateScope())
            {
                await scope.ServiceProvider
                    .GetRequiredService<IDataSeeder>()
                    .SeedAsync();
            }
        });
    }
}
