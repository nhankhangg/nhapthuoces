﻿namespace FRTTMO.CAM.PromotionAPI;

/* Inherit from this class for your domain layer tests.
 * See SampleManager_Tests for example.
 */
public abstract class PromotionAPIDomainTestBase : PromotionAPITestBase<PromotionAPIDomainTestModule>
{

}
