﻿using FRTTMO.CAM.PromotionAPI.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace FRTTMO.CAM.PromotionAPI;

/* Domain tests are configured to use the EF Core provider.
 * You can switch to MongoDB, however your domain tests should be
 * database independent anyway.
 */
[DependsOn(
    typeof(PromotionAPIEntityFrameworkCoreTestModule)
    )]
public class PromotionAPIDomainTestModule : AbpModule
{

}
